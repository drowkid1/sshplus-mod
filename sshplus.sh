#!/bin/bash

declare -A scode=( [soporte]="https://t.me/drowkid01" [slogan]="✧ | ᴅʀᴏᴡᴋɪᴅ | ✧" )
declare -A color=( [1]="\e[1;31m" [2]="\e[1;32m" [3]="\e[1;33m" [4]="\e[1;34m" [5]="\e[1;35m" [6]="\e[1;36m" [7]="\e[1;37m" )

			        function bai()
		{ echo -e "${color[1]} [✗] $1 [✗]" && exit 1 ; }

if [ `whoami` != 'root' ]; then
	bai "necesitas ser usuario root para ejecutar el script"
fi
	if [[ "$(uname -m)" = @("amd64"|"x86_64"|"aarch64"|"armv8") ]]; then
		arch=$(jq '{exec: [{arch: {amd64: "64", x86_64: "64", aarch64: "arm64", armv8: "arm64" }}]}' -n|jq -r .exec[].arch."$(uname -m)")
	else
		bai "arquitectura incompatible"
	fi
		if grep -qs "ubuntu" /etc/os-release; then
			if [[ $(grep 'VERSION_ID' /etc/os-release | cut -d '"' -f 2 | tr -d '.') -lt '1804' ]]; then bai "requieres la versión 18.04 ó superior para usar el script" ; fi
		elif [[ -e /etc/debian_version ]]; then
			if [[ $(grep -oE '[0-9]+' /etc/debian_version | head -1) -lt '9' ]]; then bai "requieres la versión 9 ó superior de debian" ; fi
		else
			bai "sistema operativo incompatible, usa DEBIAN/UBUNTU"
		fi

clear
echo -e "${color[4]}ESPERE... \e[1;33mVERIFICANDO ARQUITECTURA.."
	      (
  dpkg --configure -a && apt update -y && apt upgrade -y
  for upx in `echo "figlet lolcat wget curl git iptables boxes"` ; do
	apt-get install $upx
  done
	      ) & >/dev/null

sysctl -w net.ipv6.conf.all.disable_ipv6=1 && sysctl -p
echo 'net.ipv6.conf.all.disable_ipv6 = 1' > /etc/sysctl.d/70-disable-ipv6.conf
sysctl -p -f /etc/sysctl.d/70-disable-ipv6.conf
echo -e "${color[2]}ARQUITECTURA: ${color[3]}$arch"

wget -O plus.sh https://gitlab.com/drowkid1/sshplus-mod/-/raw/main/init.sh
chmod +rwx plus.sh && ./plus.sh --drowkid
